@echo off
cls

ghc -O2 -Wincomplete-patterns -outputdir "./build" -o "./pure.exe" -rtsopts=ignoreAll -i"./src" -i"../hs-util/src" src/Main.hs