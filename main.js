"use strict"

const fs = require("fs")
const path = require("path")
const assert = require("./gui/assert")
const O = require("../js-util")

const cwd = __dirname
const th_dir = path.join(cwd, "theory")
const th_init_file = path.join(th_dir, "000.pure")
const db_dir = path.join(cwd, "db")
const proofs_file = path.join(db_dir, "proofs.txt")

const main = () => {
  // buf = fs.readFileSync("pure.prof")
  // fs.writeFileSync("prof.txt", buf.slice(0, 1 << 20))
  
  let src = O.rfs(th_init_file, 1)
  let proofs = O.rfs(proofs_file, 1)
  
  let mp = new Map()
  
  for(let s of O.sanll(proofs)){
    let lines = O.sanl(s)
    let id = lines.shift()
    s = lines.join("\n")
    mp.set(id, s)
  }
  
  src = O.sanll(src).map(s => {
    if(s.startsWith("--")) return s
    
    let m = s.match(/^thm \x01(\d+)/m)
    if(m === null) return s
    
    let id = m[1]
    assert(mp.has(id))
    
    if(s.includes(":=")) return s
    
    return `${s} :=\n${mp.get(id)}`
  }).join("\n\n")
  
  O.wfs(th_init_file, src)
}


main()