module Config where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Prelude hiding (putStr, putStrLn, print)

import Util
import FileSystem
import Path

config_dir = "config"
config_file = "config.txt"

type Config = Map String String

get_config :: IO Config
get_config = do
  cwd <- fs_cwd
  let pth = pth_joins cwd [config_dir, config_file]
  str <- readFile pth
  return # Map.fromList # read str