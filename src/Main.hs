import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Prelude hiding (print, putStrLn, log)
import Data.Char
import Data.List
import Data.Maybe
import Data.Either
import Data.Bits
import Data.Word
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Data.Time.Clock
import Control.Applicative hiding (Const)
import Control.Monad
import Control.Monad.State
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Except
import System.IO hiding (print, putStrLn)
import System.Directory
import System.Environment as Env
import Control.DeepSeq

import Util
import Sctr
import Bit
import Trie
import Tree
import Bimap
import Trimap
import Serializer
import FileSystem
import Path
import Config
import Parser
import JSON
import Elaborator hiding (Elab)
import Elaborator.Parser
import Elaborator.ProofCompressor
import Elaborator.Reconstruct
import Elaborator.Command

import qualified Kernel as K

cache_file_ext = "bin"
th_dir = "./theory"
cache_dir = "./cache"
names_file = "./db/names.txt"
-- proofs_file = "./db/proofs.txt"
data_dir = "./data"
mathlib_dir = "./data/mathlib"
mathlib_index_file = "./data/mathlib/index.json"

type Elab = ExceptT Sctr IO

main :: IO ()
main = do
  mapM_ (hSetBuffering <~ NoBuffering) [stdin, stdout, stderr]
  init_server

is_inter :: IO Prop
is_inter = do
  args <- getArgs
  return # null args

init_server :: IO ()
init_server = do
  fs_md data_dir
  
  res <- runExceptT # do
    args <- liftIO getArgs
    case args of
      [] -> elab_session
      [main_th_file] -> elab_file
      _ -> err [dfsc "Invalid arguments"]
  case res of
    Right _ -> nop
    Left msg -> do
      logb
      putStrLn # msg >>= snd

elab_file :: Elab ()
elab_file = do
  names_src <- liftIO # read_file names_file
  let names_xs = zip [1..] # sanl names_src
  let mp_id_name = bimap_from_list names_xs
  
  op_defs <- pure
    [ OpDef {_op_def_name = 38,   _op_def_sym = "τ",   _op_def_type = Binder,         _op_def_rel = Just (2, EQ)}
    , OpDef {_op_def_name = 3,    _op_def_sym = "¬",   _op_def_type = Prefix,         _op_def_rel = Just (1, GT)}
    , OpDef {_op_def_name = 44,   _op_def_sym = "∨",   _op_def_type = Infix (Just 1), _op_def_rel = Just (3, LT)}
    , OpDef {_op_def_name = 15,   _op_def_sym = "∧",   _op_def_type = Infix (Just 1), _op_def_rel = Just (44, GT)}
    , OpDef {_op_def_name = 33,   _op_def_sym = "↔",   _op_def_type = Infix (Just 0), _op_def_rel = Just (1, LT)}
    , OpDef {_op_def_name = 4,    _op_def_sym = "∃",   _op_def_type = Binder,         _op_def_rel = Just (2, EQ)}
    , OpDef {_op_def_name = 13,   _op_def_sym = "∈",   _op_def_type = Infix (Just 0), _op_def_rel = Just (15, GT)}
    , OpDef {_op_def_name = 27,   _op_def_sym = "=",   _op_def_type = Infix (Just 0), _op_def_rel = Just (3, LT)}
    , OpDef {_op_def_name = 140,  _op_def_sym = "≠",   _op_def_type = Infix (Just 0), _op_def_rel = Just (27, EQ)}
    , OpDef {_op_def_name = 49,   _op_def_sym = "∃!",  _op_def_type = Binder,         _op_def_rel = Just (2, EQ)}
    , OpDef {_op_def_name = 149,  _op_def_sym = "⊆",   _op_def_type = Infix (Just 0), _op_def_rel = Just (13, GT)}
    , OpDef {_op_def_name = 152,  _op_def_sym = "⊂",   _op_def_type = Infix (Just 0), _op_def_rel = Just (149, EQ)}
    , OpDef {_op_def_name = 155,  _op_def_sym = "∪",   _op_def_type = Infix (Just 0), _op_def_rel = Just (27, GT)}
    , OpDef {_op_def_name = 158,  _op_def_sym = "∩",   _op_def_type = Infix (Just 0), _op_def_rel = Just (155, GT)}
    , OpDef {_op_def_name = 11,   _op_def_sym = "∅",   _op_def_type = Const,          _op_def_rel = Nothing}
    , OpDef {_op_def_name = 40,   _op_def_sym = "THE", _op_def_type = Binder,         _op_def_rel = Just (38, EQ)}
    , OpDef {_op_def_name = 195,  _op_def_sym = "∉",   _op_def_type = Infix (Just 0), _op_def_rel = Just (13, EQ)}
    , OpDef {_op_def_name = 212,  _op_def_sym = "\\",  _op_def_type = Infix (Just 0), _op_def_rel = Just (155, EQ)}
    , OpDef {_op_def_name = 232,  _op_def_sym = "∘",   _op_def_type = Infix (Just 0), _op_def_rel = Just (158, GT)}
    , OpDef {_op_def_name = 311,  _op_def_sym = "≤",   _op_def_type = Infix (Just 0), _op_def_rel = Just (27, EQ)}
    , OpDef {_op_def_name = 313,  _op_def_sym = "<",   _op_def_type = Infix (Just 0), _op_def_rel = Just (311, EQ)}
    , OpDef {_op_def_name = 1733, _op_def_sym = "ℕ",   _op_def_type = Const,          _op_def_rel = Nothing}
    , OpDef {_op_def_name = 410,  _op_def_sym = "+",   _op_def_type = Infix (Just 0), _op_def_rel = Just (155, LT)}
    ]
  
  Just kth <- liftIO # runMaybeT # execStateT K.init_th undefined
  th0 <- pure # init_th kth mp_id_name op_defs
  
  (fn1 :: Elab TheoryT) <- pure # do
    main_file <- pure "./theory/000.pure"
    src <- liftIO # read_file main_file
    let cmds = sanll src
    
    -- s <- liftIO # read_file proofs_file
    -- let xs = sanll # unlines' # sanl s
    -- ps <- pure # Map.fromList #
    --   map <~ xs # \s -> case sanl s of
    --     [] -> error ""
    --     (i : xs) -> (read i, unlines' xs)
    -- () <- pure # seq ps ()
    -- th0 <- pure # th0 {_th_compr_proofs = ps}
    
    th <- foldm_l cmds th0 # \th cmd -> do
      -- liftIO # do
      --   putStrLn ""
      --   putStrLn cmd
      --   putStrLn ""
      
      -- putStrLn ""
      
      res <- elab_cmd th cmd
      case res of
        Left msg -> do
          -- i <- do
          --   s <- pure # tail' # dropWhile (/= '\x01') cmd
          --   s <- pure # takeWhile is_digit s
          --   return # read s
          -- 
          -- case bimap_get i mp_id_name of
          --   Nothing -> print i
          --   Just name -> putStrLn name
          
          putStrLn # head' undefined # lines cmd
          
          raise msg
        Right (_, th) -> pure th
    
    let buf = ser_to_buf th
    let th' = dser_from_buf buf
    
    asrt # _th_kth th == _th_kth th'
    asrt # _th_simp_map th == _th_simp_map th'
    asrt # _th_tcs th == _th_tcs th'
    asrt # _th_schemes th == _th_schemes th'
    asrt # _th_op_defs th == _th_op_defs th'
    asrt # _th_next_index th == _th_next_index th'
    asrt # _th_mp_id_name th == _th_mp_id_name th'
    asrt # _th_mp_id_index th == _th_mp_id_index th'
    asrt # _th_rels th == _th_rels th'
    asrt # _th_mp_e_e th == _th_mp_e_e th'
    asrt # _th_mp_e_stat th == _th_mp_e_stat th'
    asrt # _th_mp_e_p th == _th_mp_e_p th'
    asrt # _th_mp_p_p th == _th_mp_p_p th'
    -- asrt # _th_mp_p_sctr th == _th_mp_p_sctr th'
    
    print # BS.length buf
    
    -- liftIO logb
    -- mp_id_name <- pure # _th_mp_id_name th
    -- mp <- pure # _th_rels th
    -- mapM_ <~ trimap_to_list mp # \(i, j, k) -> do
    --   putStrLn # unwords # map <~ [i, j, k] # \i ->
    --     fromJust # bimap_get i mp_id_name
    
    liftIO # do
      BS.writeFile "./data/000.bin" buf
      -- write_file "./data/000-sctr.json" #
      --   show_json # _th_mp_p_sctr th
    
    !th <- pure th
    return th
  
  (fn2 :: Elab TheoryT) <- pure # do
    th0 <- get_th0
    
    main_file <- pure "./theory/main.pure"
    src <- liftIO # read_file main_file
    let cmds = sanll src
    
    th <- foldm_l cmds th0 # \th cmd -> do
      res <- elab_cmd th cmd
      case res of
        Left msg -> do
          putStrLn # head' undefined # lines cmd
          raise msg
        Right (_, th) -> pure th
    
    !th <- pure th
    return th
  
  -- th <- fn2
  -- mp_id_name <- pure # _th_mp_id_name th
  -- mp <- pure # _th_rels th
  -- mapM_ <~ trimap_to_list mp # \(i, j, k) -> do
  --   putStrLn # unwords # map <~ [i, j, k] # \i ->
  --     fromJust # bimap_get i mp_id_name
  
  -- void fn1
  export_mathlib

data ElabInfoT = ElabInfoT
  { _elab_info_th :: TheoryT
  , _elab_info_info :: Map String JSON
  }

type ElabInfo = StateT ElabInfoT Elab

write_as_json :: (ToJSON a) => FilePath -> a -> Elab ()
write_as_json pth a = liftIO # write_file pth # show_json a

put_info :: (ToJSON v) => String -> v -> ElabInfo ()
put_info k v = modify # \s -> s
  {_elab_info_info = map_insert k (to_json v) # _elab_info_info s}

put_info_ctx :: (CtxShow v) => String -> v -> ElabInfo ()
put_info_ctx k v = do
  th <- gets _elab_info_th
  res <- liftIO # th_show' th v
  case res of
    Left msg -> raise msg
    Right s -> put_info k s

export_mathlib :: Elab ()
export_mathlib = do
  th <- get_th0
  
  let kth = _th_kth th
  let kdefs = K._defs kth
  let kthms = K._thms kth
  let defs = _th_defs th
  let thms = _th_thms th
  let simp_map = _th_simp_map th
  let tcs = _th_tcs th
  let schemes = _th_schemes th
  let next_index = _th_next_index th
  let mp_id_name = _th_mp_id_name th
  let mp_id_index = _th_mp_id_index th
  let rels = _th_rels th
  let mp_e_e = _th_mp_e_e th
  let mp_e_stat = _th_mp_e_stat th
  let mp_e_p = _th_mp_e_p th
  let mp_p_p = _th_mp_p_p th
  let op_defs = _th_op_defs th
  let term_par = _th_term_par th
  
  liftIO # fs_md mathlib_dir
  
  ids <- pure # bimap_keys mp_id_index
  i0 <- pure # head' (-1) ids
  
  write_as_json mathlib_index_file # Map.fromList
    [ ("mp_id_name", to_json # _bimapl mp_id_name)
    , mk_pair "ops" # to_json # concat
      [ map op_to_op_def [term_par_op_imp, term_par_op_univ]
      , op_defs
      ]
    ]
  
  mapM_ <~ ids # \i -> ifm (i == 1048) # do
    Just name <- pure # bimap_get i mp_id_name
    Just index <- pure # bimap_get i mp_id_index
    
    -- ifm (i /= i0) # putStrLn ""
    putStrLn # concat [show i, " ", name]
    
    info <- pure # ElabInfoT
      { _elab_info_th = th
      , _elab_info_info = Map.empty
      }
    
    info <- execStateT <~ info # do
      put_info "name" name
      put_info "index" index
      put_info_ctx "src" # ThIdent name
      put_info "rels1" # map_get_dflt i # _trimap1 rels
      put_info "rels2" # Set.map swap # map_get_dflt i # _trimap3 rels
      
      case map_get i defs of
        Just def -> do
          put_info "kind" "term"
          put_info "ps_num" # _def_ps_num def
          put_info "unf_level" # _def_level def
          put_info "mp_e" # bimap_nn_get_dflt i mp_e_e
          -- put_info "mp_p" # Set.union
          --   (bimap_nn_get_dflt i mp_e_stat)
          --   (bimap_nn_get_dflt i mp_e_p)
          put_info "mp_stat" # bimap_nn_get i mp_e_stat
          put_info "mp_p" # bimap_nn_get i mp_e_p
        Nothing -> do
          Just thm <- pure # map_get i thms
          Just kthm <- pure # map_get i kthms
          put_info "kind" "proof"
          put_info "ps_num" # _thm_ps_num thm
          put_info "axioms" # K._thm_axioms kthm
          put_info "mp_p" # bimap_nn_get_dflt i mp_p_p
    
    pth <- pure # pth_join mathlib_dir # concat [show i, ".json"]
    write_as_json pth # _elab_info_info info

escape_char :: Char -> String
escape_char c = show (ord c) ++ "."

escape_str :: String -> String
escape_str s = s >>= escape_char

put_str :: String -> IO ()
put_str s = do
  inter <- is_inter
  s <- pure # ite inter (escape_str s) s
  putStr s

put_str_ln :: String -> IO ()
put_str_ln s = put_str s >> put_str "\n"

-----

scoped_str_to_json :: (String, String) -> JSON
scoped_str_to_json (scope, str) = JSON.List [JSON.Str scope, JSON.Str str]

send_msg :: Name -> JSON -> IO ()
send_msg tp msg = put_str_ln # show # JSON.List [JSON.Str tp, msg]

send_scoped_msg :: String -> Sctr -> IO ()
send_scoped_msg tp xs = send_msg tp # JSON.List # map scoped_str_to_json xs

send_scopes :: Sctr -> IO ()
send_scopes = send_scoped_msg "scopes"

-----

read_char_code' :: IO String
read_char_code' = do
  c <- getChar
  if c == '.' then pure "" else do
    s <- read_char_code'
    return # c : s

read_char_code :: IO N
read_char_code = do
  n <- read_char_code'
  return # read n

read_char :: IO Char
read_char = do
  n <- read_char_code
  return # chr # fi n

read_ln :: IO String
read_ln = do
  c <- read_char
  if c == '\n' then pure "" else do
    s <- read_ln
    return # c : s

read_msg :: IO [String]
read_msg = do
  s <- read_ln
  return # read s

elab_session_cmd :: N -> [TheoryT] -> N -> String -> Elab ()
elab_session_cmd ths_num ths index cmd = do
  ths <- pure # drop (fi # ths_num - index) ths
  
  th_orig <- head' get_th0 # map pure ths
  res <- elab_cmd th_orig cmd
  case res of
    Left msg -> do
      liftIO # send_scoped_msg "info" msg
      elab_session_file index ths
    Right res@(scopes, th) -> do
      -- insert_newly_added_names_into_db th
      liftIO # send_scopes scopes
      elab_session_file (index + 1) # th : ths

elab_session_file :: N -> [TheoryT] -> Elab ()
elab_session_file ths_num ths = do
  (tp : args) <- liftIO read_msg
  case tp of
    "elab_cmd" -> do
      [index, cmd] <- pure args
      elab_session_cmd ths_num ths (read index) cmd
    "close_file" -> elab_session
    _ -> error tp

elab_session :: Elab ()
elab_session = do
  (tp : args) <- liftIO read_msg
  case tp of
    "open_file" -> do
      -- [fid] <- pure # map read args
      elab_session_file 0 []
    _ -> error tp

elab_cmd :: TheoryT -> String -> Elab (Either Sctr (Sctr, TheoryT))
elab_cmd th cmd = liftIO # runExceptT #
  runStateT (parse_and_run_cmd cmd) th

get_th0 :: Elab TheoryT
get_th0 = do
  buf <- liftIO # BS.readFile "./data/000.bin"
  !th <- pure # dser_from_buf buf
  return th