module Kernel.Kernel where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Maybe
import Data.List
import Data.Functor.Identity
import Control.Monad.State
import Control.Monad.Trans.Maybe

import Util

imp_id :: Id
imp_id = 1

univ_id :: Id
univ_id = 2

type Id = N
type KernelM = IO

-- error' :: HCS => a -> String -> a
-- error' x msg = trace' (Lit [msg]) x
-- error' x msg = error msg

-- class error' a where
--   error' :: HCS => String -> a

-- instance error' Prop where
--   error' = error' False

-- instance error' (Maybe a) where
--   error' = error' Nothing

data Type
  = TypeVar N
  | Prop
  | SetT
  | Func Type Type
  deriving (Eq, Ord, Show, Read)

data Term
  = TermRef Id [Type]
  | TermVar N
  | Lam Type Term
  | App Term Term
  deriving (Eq, Ord, Show, Read)

type Stat = Term

data Proof
  = ProofRef Id [Type]
  | ProofVar N
  | ImpI Term Proof
  | UnivI Type Proof
  | ImpE Proof Proof
  | UnivE Proof Term
  deriving (Eq, Ord, Show, Read)

data Def = Def
  { _def_params_num :: N
  , _def_type :: Type
  } deriving (Eq, Ord, Show, Read)

data Thm = Thm
  { _thm_params_num :: N
  , _thm_stat :: Term
  , _thm_proof :: Maybe Proof
  , _thm_axioms :: Set Id
  } deriving (Eq, Ord, Show, Read)

data Theory = TheoryT
  { _defs :: Map Id Def
  , _thms :: Map Id Thm
  } deriving (Eq, Ord, Show, Read)

type Kernel = StateT Theory (MaybeT KernelM)

th_get_defs :: HCS => Kernel (Map Id Def)
th_get_defs = gets _defs

th_get_thms :: HCS => Kernel (Map Id Thm)
th_get_thms = gets _thms

th_has_def :: HCS => Id -> Kernel Prop
th_has_def i = gets # Map.member i . _defs

th_has_thm :: HCS => Id -> Kernel Prop
th_has_thm i = gets # Map.member i . _thms

th_has_id :: HCS => Id -> Kernel Prop
th_has_id i = do
  h <- th_has_def i
  if h then pure True else th_has_thm i

th_mk_raw :: HCS => Map Id Def -> Map Id Thm -> Theory
th_mk_raw defs thms = TheoryT
  { _defs = defs
  , _thms = thms
  }

init_th_raw :: HCS => Theory
init_th_raw = th_mk_raw Map.empty Map.empty

th_add_def_raw :: HCS => Id -> Def -> Kernel ()
th_add_def_raw i def = modify # \th -> th
  {_defs = map_insert i def # _defs th}

th_add_thm_raw :: HCS => Id -> Thm -> Kernel ()
th_add_thm_raw i thm = modify # \th -> th
  {_thms = map_insert i thm # _thms th}

imp_def :: HCS => Def
imp_def = Def
  { _def_params_num = 0
  , _def_type = Func Prop (Func Prop Prop)
  }

univ_def :: HCS => Def
univ_def = Def
  { _def_params_num = 1
  , _def_type = Func (Func (TypeVar 0) Prop) Prop
  }

init_th :: HCS => Kernel ()
init_th = do
  put # TheoryT
    { _defs = Map.empty
    , _thms = Map.empty
    }
  th_add_def_raw imp_id imp_def
  th_add_def_raw univ_id univ_def

is_finite :: HCS => [a] -> Prop
is_finite [] = True
is_finite (x:xs) = is_finite xs

max_param_in_type :: HCS => Type -> N
max_param_in_type t = case t of
  TypeVar i -> i
  Func t1 t2 -> max (max_param_in_type t1) (max_param_in_type t2)
  _ -> -1

max_param_in_term :: HCS => Term -> N
max_param_in_term e = case e of
  TermRef name ps -> foldr max (-1) # map max_param_in_type ps
  Lam t e -> max (max_param_in_type t) (max_param_in_term e)
  App e1 e2 -> max (max_param_in_term e1) (max_param_in_term e2)
  _ -> -1

max_param_in_proof :: HCS => Proof -> N
max_param_in_proof p = case p of
  ProofRef name ps -> foldr max (-1) # map max_param_in_type ps
  ImpI s p -> max (max_param_in_term s) (max_param_in_proof p)
  UnivI t p -> max (max_param_in_type t) (max_param_in_proof p)
  ImpE p1 p2 -> max (max_param_in_proof p1) (max_param_in_proof p2)
  UnivE p e -> max (max_param_in_proof p) (max_param_in_term e)
  _ -> -1

check_def_params :: HCS => Def -> Term -> Prop
check_def_params def val = let pn = _def_params_num def in
  max_param_in_type (_def_type def) < pn &&
  max_param_in_term val < pn

th_get_def :: HCS => Id -> Kernel Def
th_get_def i = do
  res <- from_just # gets # map_get i . _defs
  return res

th_get_thm :: HCS => Id -> Kernel Thm
th_get_thm i = do
  res <- gets # map_get i . _thms
  case res of
    Nothing -> error # show i
    Just res -> pure res

subst_type_params_in_type :: HCS => [Type] -> Type -> Type
subst_type_params_in_type ps t = case t of
  TypeVar i -> fromJust # list_get i ps
  Func t1 t2 -> Func (subst_type_params_in_type ps t1) (subst_type_params_in_type ps t2)
  _ -> t

subst_type_params_in_term :: HCS => [Type] -> Term -> Term
subst_type_params_in_term ps e = case e of
  TermRef name ps1 -> TermRef name # map (subst_type_params_in_type ps) ps1
  Lam t e -> Lam (subst_type_params_in_type ps t) (subst_type_params_in_term ps e)
  App e1 e2 -> App (subst_type_params_in_term ps e1) (subst_type_params_in_term ps e2)
  _ -> e

get_def_type :: HCS => Id -> [Type] -> Kernel Type
get_def_type i ps = do
  def <- th_get_def i
  asrt' # _def_params_num def == len ps
  return # subst_type_params_in_type ps # _def_type def

get_thm_term :: HCS => Id -> [Type] -> Kernel Term
get_thm_term i ps = do
  thm <- th_get_thm i
  asrt' # _thm_params_num thm == len ps
  return # subst_type_params_in_term ps # _thm_stat thm

calc_term_type' :: HCS => [Type] -> Term -> Kernel Type
calc_term_type' ts e = case e of
  TermRef i ps -> get_def_type i ps
  TermVar i -> case list_get i ts of
    Just a -> pure a
    Nothing -> error' # show (ts, e)
  Lam t e -> calc_term_type' (t : ts) e >>= \t1 -> pure # Func t t1
  App e1 e2 -> calc_term_type' ts e1 >>= \t -> case t of
    Func t1 t2 -> calc_term_type' ts e2 >>= \t ->
      if t /= t1 then error' # show (t1, t) else pure t2
    _ -> error' ""

calc_term_type :: HCS => Term -> Kernel Type
calc_term_type = calc_term_type' []

mk_ps :: HCS => N -> [Type]
mk_ps pn = map TypeVar [0 .. pn - 1]

asrt_no_def :: HCS => Id -> Kernel ()
asrt_no_def i = do
  asrt_not_m # th_has_def i
  nop

check_def :: HCS => Id -> Def -> Term -> Kernel ()
check_def i def val = do
  asrt_no_def i
  asrt' # check_def_params def val
  val' <- reduce_term val
  asrt' # val == val'
  t <- calc_term_type val
  asrt' # t == _def_type def
  nop

th_add_const :: HCS => Id -> N -> Type -> Kernel ()
th_add_const i pn t = do
  asrt_no_def i
  def <- pure # Def
    { _def_params_num = pn
    , _def_type = t
    }
  th_add_def_raw i def

th_add_def :: HCS => Id -> Id -> N -> Type -> Term -> Kernel ()
th_add_def i pi pn t val = do
  def <- pure # Def
    { _def_params_num = pn
    , _def_type = t
    }
  check_def i def val
  th_add_def_raw i def
  s <- reduce_term #
    mk_univ (Func t Prop) # mk_imp (App (TermVar 0) val) #
      App (TermVar 0) # TermRef i # mk_ps pn
  thm <- pure # Thm
    { _thm_params_num = pn
    , _thm_stat = s
    , _thm_proof = Nothing
    , _thm_axioms = Set.empty
    }
  th_add_thm_raw pi thm

check_thm_params :: HCS => Thm -> Proof -> Kernel ()
check_thm_params thm p = do
  let pn = _thm_params_num thm
  asrt' # max_param_in_term (_thm_stat thm) < pn
  asrt' # max_param_in_proof p < pn

asrt_prop :: HCS => [Type] -> Term -> Kernel ()
asrt_prop ts e = do
  res <- calc_term_type' ts e
  case res of
    Prop -> nop
    t -> error' # show t

lift_term_vars' :: HCS => N -> N -> Term -> Term
lift_term_vars' n m e = case e of
  TermVar i -> if i >= m then TermVar # i + n else e
  Lam t e -> Lam t # lift_term_vars' n (m + 1) e
  App a b -> App (lift_term_vars' n m a) (lift_term_vars' n m b)
  _ -> e

lift_term_vars :: HCS => N -> Term -> Term
lift_term_vars n = lift_term_vars' n 0

subst_term_var' :: HCS => Term -> N -> Term -> Term
subst_term_var' arg n e = case e of
  TermVar i -> if i < n then TermVar i
    else if i > n then TermVar # i - 1
    else lift_term_vars n arg
  Lam t e -> Lam t # subst_term_var' arg (n + 1) e
  App a b -> App (subst_term_var' arg n a) (subst_term_var' arg n b)
  _ -> e

subst_term_var :: HCS => Term -> Term -> Term
subst_term_var arg = subst_term_var' arg 0

term_has_term_var :: HCS => N -> Term -> Prop
term_has_term_var i e = case e of
  TermVar j -> i == j
  Lam t e -> term_has_term_var (i + 1) e
  App a b -> term_has_term_var i a || term_has_term_var i b
  _ -> False

beta_reduce :: HCS => [Type] -> Term -> Kernel Term
beta_reduce ts e = case e of
  Lam t func -> do
    f <- beta_reduce (t : ts) func
    return # Lam t f
  App (Lam t func) arg -> do
    t1 <- calc_term_type' ts arg
    asrt' # t1 == t
    beta_reduce ts # subst_term_var arg func
  App a b -> do
    c <- beta_reduce ts a
    if c == a
      then do
        d <- beta_reduce ts b
        return # App a d
      else beta_reduce ts # App c b
  _ -> pure e

eta_reduce :: HCS => Term -> Term
eta_reduce e = case e of
  Lam t (App e a) -> let
    e' = eta_reduce e
    a' = eta_reduce a
    in if a' /= TermVar 0 || term_has_term_var 0 e'
      then Lam t (App e' a')
      else subst_term_var undefined e'
  Lam t e -> Lam t # eta_reduce e
  App a b -> App (eta_reduce a) (eta_reduce b)
  _ -> e

reduce_term' :: HCS => [Type] -> Term -> Kernel Term
reduce_term' ts e = do
  e <- beta_reduce ts e
  return # eta_reduce e

reduce_term :: HCS => Term -> Kernel Term
reduce_term = reduce_term' []

mk_imp :: HCS => Term -> Term -> Term
mk_imp a b = App (App (TermRef imp_id []) a) b

mk_univ :: HCS => Type -> Term -> Term
mk_univ t e = App (TermRef univ_id [t]) # Lam t e

mk_imp' :: HCS => [Type] -> Term -> Term -> Kernel Term
mk_imp' ts a b = do
  asrt_prop ts a
  asrt_prop ts b
  return # mk_imp a b

mk_univ' :: HCS => [Type] -> Type -> Term -> Kernel Term
mk_univ' ts t e = do
  asrt_prop (t : ts) e
  return # mk_univ t e

imp_to_pair :: HCS => Term -> Kernel (Term, Term)
imp_to_pair e = case e of
  App (App (TermRef name []) a) b -> do
    asrt' # name == imp_id
    return (a, b)
  e -> error' # show e

univ_to_pair :: HCS => Term -> Kernel (Type, Term)
univ_to_pair e = do
  case e of
    App (TermRef name [t]) e -> do
      asrt' # name == univ_id
      return # case e of
        Lam _ e -> (t, e)
        e -> (t, App (lift_term_vars 1 e) (TermVar 0))
    e -> error' # show e

calc_proof_stat' :: HCS => [Type] -> [(Term, N)] -> Proof -> Kernel Term
calc_proof_stat' ts es p = (>>= reduce_term' ts) # case p of
  ProofRef name ps -> get_thm_term name ps
  ProofVar i -> do
    (e, n) <- from_just # pure # list_get i es
    return # lift_term_vars n e
  ImpI s p -> do
    s <- reduce_term s
    asrt_prop ts s
    e <- calc_proof_stat' ts ((s, 0) : es) p
    mk_imp' ts s e
  ImpE p1 p2 -> do
    e <- calc_proof_stat' ts es p1
    (a, b) <- imp_to_pair e
    e1 <- calc_proof_stat' ts es p2
    -- tracem # Lit # pure "<<<"
    -- tracem a
    -- tracem b
    -- tracem e1
    -- tracem # Lit # pure ">>>\n"
    if a /= e1 then error' # show (a, e1) else pure b
  UnivI t p -> do
    let es' = map (\(e, n) -> (e, n + 1)) es
    e <- calc_proof_stat' (t : ts) es' p
    e <- mk_univ' ts t e
    return # eta_reduce e
  UnivE p e -> do
    e <- reduce_term e
    e1 <- calc_proof_stat' ts es p
    (t, a) <- univ_to_pair e1
    t1 <- calc_term_type' ts e
    if t1 /= t then error' # show (t, t1) else
      return # subst_term_var e a

calc_proof_stat :: HCS => Proof -> Kernel Term
calc_proof_stat p = do
  s <- calc_proof_stat' [] [] p
  Prop <- calc_term_type s
  return s

calc_proof_axioms :: HCS => Proof -> Kernel (Set Id)
calc_proof_axioms p = case p of
  ProofRef name ps -> th_get_thm name >>= \thm ->
    pure # _thm_axioms thm
  ImpI e p -> calc_proof_axioms p
  ImpE p1 p2 -> calc_proof_axioms p1 >>= \s1 ->
    calc_proof_axioms p2 >>= \s2 ->
    pure # Set.union s1 s2
  UnivI t p -> calc_proof_axioms p
  UnivE p e -> calc_proof_axioms p
  _ -> pure Set.empty

check_thm :: HCS => Id -> Thm -> Maybe Proof -> Kernel ()
check_thm name thm p = do
  asrt_not_m # th_has_thm name
  let stat = _thm_stat thm
  
  t <- calc_term_type stat
  case t of
    Prop -> nop
    t -> error# show t
  
  stat' <- reduce_term stat
  asrt' # stat == stat'
  case p of
    Nothing -> nop
    Just p -> do
      check_thm_params thm p
      s <- calc_proof_stat p
      asrt' # stat == s

th_add_axiom :: HCS => Id -> N -> Term -> Kernel ()
th_add_axiom name pn stat = do
  thm <- pure # Thm
    { _thm_params_num = pn
    , _thm_stat = stat
    , _thm_proof = Nothing
    , _thm_axioms = Set.singleton name
    }
  check_thm name thm Nothing
  th_add_thm_raw name thm

th_add_thm :: HCS => Id -> N -> Term -> Proof -> Kernel ()
th_add_thm name pn stat proof = do
  axs <- calc_proof_axioms proof
  thm <- pure # Thm
    { _thm_params_num = pn
    , _thm_stat = stat
    , _thm_proof = Just proof
    , _thm_axioms = axs
    }
  check_thm name thm # Just proof
  th_add_thm_raw name thm

calc_term_refs :: HCS => Term -> Set Id
calc_term_refs e = case e of
  TermRef name _ -> Set.singleton name
  Lam t e -> calc_term_refs e
  App a b -> Set.union (calc_term_refs a) (calc_term_refs b)
  _ -> Set.empty

th_ok :: HCS => Theory -> ()
th_ok (TheoryT _ _) = ()

asrt_not_m :: HCS => Kernel Prop -> Kernel ()
asrt_not_m m = do
  res <- m
  asrt' # not res

from_just :: HCS => Kernel (Maybe a) -> Kernel a
from_just m = do
  res <- m
  case res of
    Nothing -> asrt' False >> undefined
    Just a -> pure a

asrt' :: HCS => Prop -> Kernel ()
asrt' p = ifmn p # error' ""

error' :: HCS => String -> Kernel a
error' s = error s