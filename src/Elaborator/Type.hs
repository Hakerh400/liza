{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Elaborator.Type where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Maybe
import Data.Either
import Data.List
import Data.Char
import Control.Monad
import Control.Monad.State

import Util
import Sctr
import Parser
import Elaborator.Parser

import qualified Kernel as K

import Elaborator.Data
import Elaborator.Util

subst_type_mvar_in_type :: N -> Type -> Type -> Type
subst_type_mvar_in_type i t1 t = case _type_val t of
  TypeMvar j -> if i == j then t1 else t
  Func a b -> mk_func (subst_type_mvar_in_type i t1 a)
    (subst_type_mvar_in_type i t1 b)
  _ -> t

subst_type_mvars_in_type :: Map N Type -> Type -> Type
subst_type_mvars_in_type mp t = case _type_val t of
  TypeMvar i -> Map.findWithDefault t i mp
  Func a b -> mk_func (subst_type_mvars_in_type mp a)
    (subst_type_mvars_in_type mp b)
  _ -> t

subst_type_vars_in_type :: [Type] -> Type -> Type
subst_type_vars_in_type ts t = case _type_val t of
  TypeVar i -> fromJust # list_get i ts
  Func a b -> mk_func (subst_type_vars_in_type ts a)
    (subst_type_vars_in_type ts b)
  _ -> t

mk_func_type1 :: [Type] -> Type
mk_func_type1 = foldr1 mk_func

mk_func_type :: [Type] -> Type -> Type
mk_func_type ts t = mk_func_type1 # ts ++ [t]

-----

parse_type_name :: Elab Name
parse_type_name = p_scope "type" parse_name

parse_type_mvar :: Elab Type
parse_type_mvar = do
  p_scope "type" p_usc
  ctx_mk_type_mvar

parse_type_ident :: Elab Type
parse_type_ident = do
  name <- parse_type_name
  ps <- gets_ctx _ctx_types
  case Map.lookup name ps of
    Just i -> pure # mk_type_var # fi i
    Nothing -> if name == "Prop" then pure mk_prop_t else
      if name == "Set" then pure mk_set_t else
        err [dfsc"Unknown type ", ("type", name)]

parse_type_group :: Elab Type
parse_type_group = p_parens parse_type

parse_type_1 :: Elab Type
parse_type_1 = trimmed # choice
  [ parse_type_mvar
  , parse_type_ident
  , parse_type_group
  ]

parse_type :: Elab Type
parse_type = do
  ts <- p_sepf parse_type_1 # p_scope "fn_type" # p_tok sym_fn_type
  return # mk_func_type1 ts

get_fn_type_op :: Elab Op
get_fn_type_op = do
  par <- gets_th _th_term_par
  Just op <- pure # map_get namei_imp # _term_par_names par
  return # op {_op_sym = sym_fn_type}

type_to_expr :: Type -> Elab Expr
type_to_expr t = case _type_val t of
  TypeMvar i -> expr_mk_atom [("type", concat ["?", show i])]
  TypeVar i -> expr_mk_atom [("type", fromJust # list_get i all_type_names)]
  PropT -> expr_mk_atom [("type", "Prop")]
  SetT -> expr_mk_atom [("type", "Set")]
  Func a b -> do
    op <- get_fn_type_op
    opnd1 <- type_to_expr a
    opnd2 <- type_to_expr b
    expr_mk_bin_op scope_dflt op opnd1 opnd2

show_type :: Type -> Elab Sctr
show_type t = do
  s <- type_to_expr t
  return # _expr_val s

res_show_type :: Type -> Elab Sctr
res_show_type t = do
  t <- res_type t
  show_type t

instance CtxShow Type where
  ctx_show = show_type

type_has_mvar :: N -> Type -> Prop
type_has_mvar i t = case _type_val t of
  TypeMvar j -> i == j
  Func a b -> type_has_mvar i a || type_has_mvar i b
  _ -> False

get_type_mvars_in_type :: Type -> Set N
get_type_mvars_in_type t = case _type_val t of
  TypeMvar i -> Set.singleton i
  Func a b -> Set.union
    (get_type_mvars_in_type a)
    (get_type_mvars_in_type b)
  _ -> Set.empty

-----

get_type_mvar :: N -> Elab (Maybe Type)
get_type_mvar i = do
  ctx <- get_ctx
  let mp = _ctx_type_asgns ctx
  case Map.lookup i mp of
    Nothing -> pure Nothing
    Just t -> do
      t' <- res_type t
      if t == t' then nop else do
        put_ctx # ctx {_ctx_type_asgns = map_insert i t' mp}
      return # Just t'

res_type :: Type -> Elab Type
res_type t = if e_res t then pure t else case _type_val t of
  TypeMvar i -> do
    res <- get_type_mvar i
    return # fromMaybe t res
  Func a b -> do
    a <- res_type a
    b <- res_type b
    return # mk_func a b
  _ -> error # show t

asgn_type :: N -> Type -> Elab ()
asgn_type i t = do
  -- n <- gets_ctx _ctx_mvars_num
  -- if i < n then nop else trace' # error ""
  if type_has_mvar i t
    then err [dfsc "Occurs check"]
    else modify_ctx # \ctx -> ctx
      {_ctx_type_asgns = map_insert i t # _ctx_type_asgns ctx}

add_type_eq :: Type -> Type -> Elab ()
add_type_eq lhs rhs = do
  lhs <- res_type lhs
  rhs <- res_type rhs

  -- dd <- th_to_elab # gets _th_dbg
  -- if not dd then nop else do
  --   s1 <- show_type lhs
  --   s2 <- show_type rhs
  --   tracem # Msg ["\n", s1, "\n", s2]

  if lhs == rhs then nop else
    case (_type_val lhs, _type_val rhs) of
      (TypeMvar i, _) -> asgn_type i rhs
      (_, TypeMvar i) -> asgn_type i lhs
      (Func a b, Func c d) -> do
        add_type_eq a c
        add_type_eq b d
      _ -> do
        s1 <- res_show_type lhs
        s2 <- res_show_type rhs
        err # [dfsc "Type mismatch:\n\n", dfsc tab] ++
          s1 ++ [dfsc "\n", dfsc tab] ++ s2

-----

type_to_ktype :: Type -> K.Type
type_to_ktype t = case _type_val t of
  TypeVar i -> K.TypeVar i
  PropT -> K.Prop
  SetT -> K.SetT
  Func a b -> K.Func (type_to_ktype a) (type_to_ktype b)
  _ -> error # show t

ktype_to_type :: K.Type -> Theory Type
ktype_to_type t = case t of
  K.TypeVar i -> pure # mk_type_var i
  K.Prop -> pure mk_prop_t
  K.SetT -> pure mk_set_t
  K.Func a b -> do
    a <- ktype_to_type a
    b <- ktype_to_type b
    return # mk_func a b

-----

norm_type_mvars' :: Type -> State (Map N N) Type
norm_type_mvars' t = case _type_val t of
  TypeMvar i -> do
    j <- norm_mvar i
    return # mk_type_mvar j
  Func a b -> do
    a <- norm_type_mvars' a
    b <- norm_type_mvars' b
    return # mk_func a b
  _ -> pure t

-- norm_type_mvars :: Type -> Type
-- norm_type_mvars t = evalState (norm_type_mvars' t) Map.empty

-----

type_to_pat' :: Type -> PatBuilder ()
type_to_pat' t = case _type_val t of
  TypeMvar i -> pat_push' [PatKind PtTypeMvar, PatN i]
  TypeVar i -> pat_push' [PatKind PtTypeVar, PatN i]
  PropT -> pat_push # PatKind PtProp
  SetT -> pat_push # PatKind PtSet
  Func a b -> do
    pat_push # PatKind PtFunc
    type_to_pat' a
    type_to_pat' b

-- type_to_pat :: Type -> Pat
-- type_to_pat = build_pat_fn type_to_pat'

-----

-- match_type' :: Type -> PatM a ()
-- match_type' t = do
--   k <- pat_get_kind
--   case k of
--     PtTypeMvar -> do
--       i <- pat_get_n
--       pat_asgn_type i t
--     PtTypeVar -> do
--       TypeVar i <- pure # _type_val t
--       pat_get_n_lit i
--     PtProp -> if _type_val t == PropT then nop else pat_fail
--     PtSet -> if _type_val t == SetT then nop else pat_fail
--     PtFunc -> do
--       Func a b <- pure # _type_val t
--       match_type' a
--       match_type' b
--     _ -> error # show k

type_vars_to_mvars_in_type :: Type -> Type
type_vars_to_mvars_in_type t = case _type_val t of
  TypeVar i -> mk_type_mvar i
  Func a b -> mk_func
    (type_vars_to_mvars_in_type a)
    (type_vars_to_mvars_in_type b)
  _ -> t

ctx_trace_type :: Type -> Elab ()
ctx_trace_type t = do
  s1 <- show_type t
  tracem # Lit # map snd s1