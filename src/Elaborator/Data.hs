{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Elaborator.Data where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Maybe
import Data.Either
import Data.List
import Data.Char
import Data.Functor.Identity
import Control.Monad
import Control.Monad.State
import Control.Monad.Except
import Control.Monad.Trans.Except

import Util
import Sctr
import Bit
import Tree
import Trie
import Bimap
import Trimap
import Parser

import qualified Kernel as K

type Id = K.Id

class Resolvable a where
  e_res :: a -> Prop

instance (Resolvable a, Resolvable b) => Resolvable (Either a b) where
  e_res x = case x of
    Left a -> e_res a
    Right b -> e_res b

data TypeVal
  = TypeMvar N
  | TypeVar N
  | PropT
  | SetT
  | Func Type Type
  deriving (Eq, Ord, Show)

data Type = Type
  { _type_val :: TypeVal
  , _type_res :: Prop
  } deriving (Eq, Ord)
  -- }

-- instance Eq Type where
--   a == b = _type_val a == _type_val b
-- 
-- instance Ord Type where
--   compare a b = compare (_type_val a) (_type_val b)

instance Resolvable Type where
  e_res = _type_res

instance Show Type where
  show t = put_in_parens # show # _type_val t

data TermVal
  = TermMvar N
  | TermRef Id [Type]
  | TermVar N
  | Lam (Maybe Name) Type Term
  | App Term Term
  | WithType Type Term
  deriving (Eq, Ord, Show)

data Term = Term
  { _term_depth :: N
  , _term_type :: Type
  , _term_val :: TermVal
  , _term_res :: Prop
  } deriving (Eq, Ord)
  -- }

-- instance Eq Term where
--   a == b = _term_val a == _term_val b
-- 
-- instance Ord Term where
--   compare a b = compare (_term_val a) (_term_val b)

instance Resolvable Term where
  e_res = _term_res

instance Show Term where
  show e =
    put_in_parens # show # _term_val e
    -- put_in_parens # concat [show # _term_val e, ", ", show # _term_type e]
    -- put_in_parens # concat ["[", show # _term_depth e, "]", show # _term_val e]
    -- put_in_parens # concat ["[", show # _term_depth e, ", ", show # _term_type e, "]", show # _term_val e]
    -- show # _term_depth e

type Stat = Term

data ProofVal
  = ProofMvar Prop N
  | ProofRef Id [Type]
  | ProofVar N
  | ImpI (Maybe Name) (Maybe N) N Stat Proof
  | UnivI (Maybe Name) Type Proof
  | ImpE Proof Proof
  | UnivE Proof Term
  | WithStat Proof
  | ProofClear [N] [N] Proof
  deriving (Eq, Ord, Show)

data Proof = Proof
  { _proof_stat :: Stat
  , _proof_val :: ProofVal
  , _proof_expl :: Prop
  , _proof_res :: Prop
  , _proof_target_asm :: Maybe N
  } deriving (Eq, Ord)

instance Resolvable Proof where
  e_res = _proof_res

instance Show Proof where
  show p =
    -- put_in_parens # show # _proof_val p
    put_in_parens # concat ["[", show # _term_depth # _proof_stat p, "]", show # _proof_val p]

type LocIdent = Either N N
type BinderArg = Either (Name, Type) Term

data ExprBinderGroup
  = ExprBinderGroupType [Sctr] Sctr
  | ExprBinderGroupTc Sctr
  | ExprBinderGroupTcMem Sctr Sctr
  | ExprBinderGroupMem [Sctr] Sctr
  | ExprBinderGroupUnivI [Sctr] Sctr
  | ExprBinderGroupImpI [Sctr] Sctr
  deriving (Eq, Ord, Show)

data ExprInfo
  = ExprBinder Sctr [ExprBinderGroup] Sctr
  | ExprFinset [Sctr]
  deriving (Eq, Ord, Show)

data Expr = Expr
  { _expr_val :: Sctr
  , _expr_pri_left :: N
  , _expr_pri_right :: N
  , _expr_info :: Maybe ExprInfo
  } deriving (Ord, Eq, Show)

type ToExprFn a = Set Name -> Map N Name -> a -> Elab Expr

data Location
  = LocAsm Name
  | LocGoal
  deriving (Eq, Ord, Show)

data Def = Def
  { _def_ps_num :: N
  , _def_type :: Type
  , _def_level :: N
  } deriving (Ord, Eq, Show)

data Thm = Thm
  { _thm_ps_num :: N
  , _thm_stat :: Stat
  -- , _thm_level :: N
  } deriving (Ord, Eq, Show)

data OpType
  = Const
  | Infix (Maybe Bit)
  | Prefix
  | Postfix
  | Binder
  deriving (Ord, Eq, Show)

data Op = Op
  { _op_name :: Id
  , _op_sym :: String
  , _op_type :: OpType
  , _op_pri :: N
  } deriving (Eq, Ord, Show)

-- instance Show Op where
--   show op = _op_sym op

data Rel = Rel
  { _rel_op :: Op
  , _rel_ord :: Ordering
  } deriving (Ord, Eq, Show)

data OpDef = OpDef
  { _op_def_name :: Id
  , _op_def_sym :: String
  , _op_def_type :: OpType
  , _op_def_rel :: Maybe (Id, Ordering)
  } deriving (Eq, Ord, Show)

data TermParser = TermParser
  { _term_par_tree :: Tree [Op]
  , _term_par_syms :: Trie Char [Op]
  , _term_par_names :: Map Id Op
  , _term_par_max_op_pri :: N
  } deriving (Ord, Eq, Show)

data PrettyPrinter = PrettyPrinter
  { _pp_term_sym :: Prop
  , _pp_term_un_op :: Prop
  , _pp_term_bin_op :: Prop
  , _pp_term_binder :: Prop
  , _pp_term_binder_join :: Prop
  , _pp_term_ref_types :: Prop
  , _pp_proof_ref_types :: Prop
  , _pp_set_fin :: Prop
  } deriving (Eq, Ord, Show)

type ThRels = Trimap Id Id Id

data TheorySchemes = TheorySchemes
  { _schemes_ind :: Map Id Id
  , _schemes_ind_tc :: Map Id Id
  , _schemes_cs :: Map Id Id
  , _schemes_cs_tc :: Map Id Id
  } deriving (Eq, Ord, Show)

data TheoryT = TheoryT
  { _th_kth :: K.Theory
  , _th_defs :: Map Id Def
  , _th_thms :: Map Id Thm
  , _th_simp_map :: RwMap
  , _th_tcs :: RwMap
  , _th_schemes :: TheorySchemes
  
  -----
  , _th_next_index :: N
  , _th_sim_index :: Maybe N
  , _th_mp_id_name :: Bimap Id Name
  , _th_mp_id_index :: Bimap Id N
  , _th_rels :: ThRels
  , _th_mp_e_e :: Bimap_nn Id Id
  , _th_mp_e_stat :: Bimap_nn Id Id
  , _th_mp_e_p :: Bimap_nn Id Id
  , _th_mp_p_p :: Bimap_nn Id Id
  
  -- , _th_mp_p_simp :: Bimap Id Id
  -- , _th_mp_p_tc :: Bimap Id Id
  
  -- , _th_mp_e_p_stat :: Bimap_nn Id Id
  -- , _th_mp_e_p :: Bimap_nn Id Id
  -- , _th_mp_p_p :: Bimap_nn Id Id
  -----
  
  -- , _th_simp_enabled :: Prop
  -- , _th_tc_enabled :: Prop
  
  -- , _th_pp :: PrettyPrinter
  -- , _th_compr_proofs :: Map Id String
  , _th_op_defs :: [OpDef]
  , _th_term_par :: TermParser
  , _th_recon :: Prop
  } deriving (Eq, Ord, Show)

data Ctx = Ctx
  { _ctx_id :: N
  , _ctx_is_tc :: Prop
  , _ctx_term_depth :: N
  , _ctx_proof_depth :: N
  , _ctx_types :: Map Name N
  , _ctx_terms :: Map N Type
  , _ctx_term_names :: Map Name N
  , _ctx_proofs :: Map N Stat
  , _ctx_terms_hidden :: Set N
  , _ctx_proofs_hidden :: Set N
  , _ctx_asms :: [(Name, N)]
  , _ctx_asm_deps :: Map N (Name, Maybe N)
  , _ctx_asms_normed :: Prop
  , _ctx_tcs :: Maybe RwMap
  , _ctx_goal_stat :: Maybe Stat
  , _ctx_term_stack :: [Term]
  , _ctx_op_stack :: [Op]
  , _ctx_term_parser_state :: Bit
  , _ctx_mvars_num :: N
  , _ctx_term_mvar_depths :: Map N N
  , _ctx_type_asgns :: Map N Type
  , _ctx_term_asgns :: Map N Term
  , _ctx_eqs_on_hold :: Map N [(Term, Term)]
  , _ctx_mvars_map :: Map N N
  -- , _ctx_tac_terms :: Map N Name
  -- , _ctx_tac_proofs :: Map N Name
  , _ctx_tacs :: [Tac]
  } deriving (Eq, Ord, Show)

data ProofState = ProofState
  { _pst_ctxs :: [Ctx]
  , _pst_fn :: [Proof] -> Proof
  }

data ElabT = ElabT
  { _elab_th :: TheoryT
  , _elab_pst :: ProofState
  , _elab_goals_num :: N
  }

type ElabM = IO
type Theory = StateT TheoryT (ExceptT Sctr ElabM)
type Elab = ParserM ElabT ElabM
type Cmd = Elab
type Tactic = Elab

data Goal = Goal
  { _goal_id :: N
  , _goal_types :: Map Name N
  , _goal_terms :: Map Name (N, Type)
  , _goal_terms' :: Map N Name
  , _goal_proofs :: [(Name, Stat)]
  , _goal_stat :: Stat
  } deriving (Eq, Ord, Show)

data SchemeType
  = SchemeInd
  | SchemeCs
  deriving (Eq, Ord, Show)

data Attrib
  = AttribExpand
  | AttribUnfold
  | AttribSimp
  | AttribComp
  | AttribTc
  | AttribScheme SchemeType
  | AttribLocal
  deriving (Eq, Ord, Show)

-- data RwArg = RwArg
--   { _rw_arg_rev :: Prop
--   , _rw_arg_name :: Name
--   , _rw_arg_args :: [Name]
--   } deriving (Eq, Ord, Show)

data PatKindT
  = PtProp
  | PtSet
  | PtFunc
  | PtTypeVar
  | PtTermRef
  | PtTermVar
  | PtLam
  | PtApp
  | PtTypeMvar
  | PtTermMvar
  deriving (Enum, Eq, Ord, Show)

data PatElem
  = PatDepth N
  | PatKind PatKindT
  | PatN N
  | PatId Id
  deriving (Eq, Ord, Show)

type Pat = [PatElem]
type PatBuilder = State Pat

type ProofInfo = Either Id N

data RwInfo = RwInfo
  { _rw_proof_info :: ProofInfo
  , _rw_proof :: Maybe Proof
  , _rw_types :: [N]
  , _rw_terms :: [N]
  } deriving (Eq, Ord, Show)

type RwMap = TrieList PatElem RwInfo
type RwMatch = (Map N Type, Map N Term)
type RwMatchRes = [(RwInfo, RwMatch)]
type RwMatchM = TrieM PatElem [RwInfo] RwMatchRes Elab
type NthRw = StateT N Tactic (Maybe (RwInfo, Proof, Term, RwMatch))

-- data RwState = RwState
--   { _rw_state_simp_map :: RwMap
--   , _rw_state_nth :: N
--   , _rw_state_offset :: N
--   , _rw_state_info :: Maybe RwInfo
--   } deriving (Eq, Ord, Show)

data SimpOpts = SimpOpts
  { _simp_single_pass :: Prop
  } deriving (Eq, Ord, Show)

data TacVar = TVar Name | PVar Name
  deriving (Eq, Ord)

-- instance Show TacVar where
--   show v = case v of
--     TVar i -> [fromJust # list_get i ['a' ..]]
--     PVar i -> 'h' : nat_to_sub i

instance Show TacVar where
  show v = case v of
    TVar name -> name
    PVar name -> name

data TacProof
  = TacProofRef Id
  | TacProofVar N
  | TacProofTerm Term
  | TacProofApp TacProof TacProof
  deriving (Eq, Ord, Show)

data TacRw
  = TacRwRef Id
  deriving (Eq, Ord, Show)

data Tac1
  = TacIntro [TacVar]
  | TacImpE Stat
  | TacUnivE Term Term
  | TacApply Prop TacProof
  | TacTc
  | TacTriv
  | TacRefl
  | TacRw [TacRw]
  | TacSimp [TacRw]
  | TacOther String
  deriving (Eq, Ord, Show)

data Tac = Tac
  { _tac_tac1 :: Tac1
  , _tac_tacs :: [Tac]
  } deriving (Eq, Ord, Show)