module Elaborator.Ser where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Maybe
import Data.Either
import Data.List
import Data.Char
import Control.Monad
import Control.Monad.State

import Util
import Serializer
import qualified Kernel as K
import Elaborator.Util
import Elaborator.Data
import Elaborator.ProofCompressor
import Elaborator.Parser

ser_ktype :: K.Type -> Ser ()
ser_ktype t = case t of
  K.TypeVar i -> ser_bnat 3 0 >> ser i
  K.Prop -> ser_bnat 3 1
  K.SetT -> ser_bnat 3 2
  K.Func a b -> ser_bnat 3 3 >> ser_ktype a >> ser_ktype b

dser_ktype :: Dser K.Type
dser_ktype = dser_bnat 3 >>= \k -> case k of
  0 -> dser >>= liftm K.TypeVar
  1 -> pure # K.Prop
  2 -> pure # K.SetT
  3 -> do
    a <- dser_ktype
    b <- dser_ktype
    return # K.Func a b
  _ -> error ""

instance Serializable K.Type where
  ser = ser_ktype
  dser = dser_ktype

ser_kterm :: K.Term -> Ser ()
ser_kterm e = case e of
  K.TermRef i ps -> ser_bnat 3 0 >> ser i >> ser ps
  K.TermVar i -> ser_bnat 3 1 >> ser i
  K.Lam t e -> ser_bnat 3 2 >> ser t >> ser_kterm e
  K.App a b -> ser_bnat 3 3 >> ser_kterm a >> ser_kterm b

dser_kterm :: Dser K.Term
dser_kterm = dser_bnat 3 >>= \k -> case k of
  0 -> do
    i <- dser
    ps <- dser
    return # K.TermRef i ps
  1 -> dser >>= liftm K.TermVar
  2 -> do
    t <- dser
    e <- dser_kterm
    return # K.Lam t e
  3 -> do
    a <- dser_kterm
    b <- dser_kterm
    return # K.App a b
  _ -> error ""

instance Serializable K.Term where
  ser = ser_kterm
  dser = dser_kterm

-- ser_kproof :: K.Proof -> Ser ()
-- ser_kproof p = case p of
--   K.ProofRef i ps -> ser_bnat 5 0 >> ser i >> ser ps
--   K.ProofVar i -> ser_bnat 5 1 >> ser i
--   K.ImpI s p -> ser_bnat 5 2 >> ser s >> ser p
--   K.UnivI t p -> ser_bnat 5 3 >> ser t >> ser p
--   K.ImpE p1 p2 -> ser_bnat 5 4 >> ser p1 >> ser p2
--   K.UnivE p e -> ser_bnat 5 5 >> ser p >> ser e
-- 
-- dser_kproof :: Dser K.Proof
-- dser_kproof = dser_bnat 5 >>= \k -> case k of
--   0 -> do
--     i <- dser
--     ps <- dser
--     return # K.ProofRef i ps
--   1 -> dser >>= liftm K.ProofVar
--   2 -> do
--     s <- dser
--     p <- dser
--     return # K.ImpI s p
--   3 -> do
--     t <- dser
--     p <- dser
--     return # K.UnivI t p
--   4 -> do
--     p1 <- dser
--     p2 <- dser
--     return # K.ImpE p1 p2
--   5 -> do
--     p <- dser
--     e <- dser
--     return # K.UnivE p e
--   _ -> error ""
-- 
-- instance Serializable K.Proof where
--   ser = ser_kproof
--   dser = dser_kproof

ser_kdef :: K.Def -> Ser ()
ser_kdef def = do
  ser # K._def_params_num def
  ser # K._def_type def

dser_kdef :: Dser K.Def
dser_kdef = do
  pn <- dser
  t <- dser_ktype
  return # K.Def
    { K._def_params_num = pn
    , K._def_type = t
    }

instance Serializable K.Def where
  ser = ser_kdef
  dser = dser_kdef

ser_kthm :: K.Thm -> Ser ()
ser_kthm thm = do
  ser # K._thm_params_num thm
  ser # K._thm_stat thm
  ser # K._thm_proof thm
  ser # K._thm_axioms thm

dser_kthm :: Dser K.Thm
dser_kthm = do
  pn <- dser
  stat <- dser
  proof <- dser
  axioms <- dser
  return # K.Thm
    { K._thm_params_num = pn
    , K._thm_stat = stat
    , K._thm_proof = proof
    , K._thm_axioms = axioms
    }

instance Serializable K.Thm where
  ser = ser_kthm
  dser = dser_kthm

ser_kth :: K.Theory -> Ser ()
ser_kth th = do
  ser # K._defs th
  ser # K._thms th

dser_kth :: Dser K.Theory
dser_kth = do
  defs <- dser
  thms <- dser
  return # K.th_mk_raw defs thms

instance Serializable K.Theory where
  ser = ser_kth
  dser = dser_kth

ser_type_val :: TypeVal -> Ser ()
ser_type_val t = case t of
  TypeMvar i -> ser_bnat 4 0 >> ser i
  TypeVar i -> ser_bnat 4 1 >> ser i
  PropT -> ser_bnat 4 2
  SetT -> ser_bnat 4 3
  Func a b -> ser_bnat 4 4 >> ser a >> ser b

dser_type_val :: Dser TypeVal
dser_type_val = dser_bnat 4 >>= \k -> case k of
  0 -> dser >>= liftm TypeMvar
  1 -> dser >>= liftm TypeVar
  2 -> pure # PropT
  3 -> pure # SetT
  4 -> do
    a <- dser
    b <- dser
    return # Func a b
  _ -> error ""

instance Serializable TypeVal where
  ser = ser_type_val
  dser = dser_type_val

ser_type :: Type -> Ser ()
ser_type t = do
  ser # _type_val t
  ser # e_res t

dser_type :: Dser Type
dser_type = do
  t <- dser
  res <- dser
  return # Type
    { _type_val = t
    , _type_res = res
    }

instance Serializable Type where
  ser = ser_type
  dser = dser_type

ser_term_val :: TermVal -> Ser ()
ser_term_val e = case e of
  TermMvar i -> ser_bnat 4 0 >> ser i
  TermRef name ps -> ser_bnat 4 1 >> ser name >> ser ps
  TermVar i -> ser_bnat 4 2 >> ser i
  Lam mn t e -> ser_bnat 4 3 >> ser mn >> ser t >> ser e
  App a b -> ser_bnat 4 4 >> ser a >> ser b
  WithType _ _ -> error ""

dser_term_val :: Dser TermVal
dser_term_val = dser_bnat 4 >>= \k -> case k of
  0 -> dser >>= liftm TermMvar
  1 -> do
    name <- dser
    ps <- dser
    return # TermRef name ps
  2 -> dser >>= liftm TermVar
  3 -> do
    mn <- dser
    t <- dser
    e <- dser
    return # Lam mn t e
  4 -> do
    a <- dser
    b <- dser
    return # App a b
  _ -> error ""

instance Serializable TermVal where
  ser = ser_term_val
  dser = dser_term_val

ser_term :: Term -> Ser ()
ser_term e = do
  ser # _term_depth e
  ser # _term_type e
  ser # _term_val e
  ser # e_res e

dser_term :: Dser Term
dser_term = do
  d <- dser
  t <- dser
  e <- dser
  res <- dser
  return # Term
    { _term_depth = d
    , _term_type = t
    , _term_val = e
    , _term_res = res
    }

instance Serializable Term where
  ser = ser_term
  dser = dser_term

ser_proof_val :: Proof -> Ser ()
ser_proof_val p = error ""

dser_proof_val :: Dser Proof
dser_proof_val = error ""

instance Serializable Proof where
  ser = ser_proof_val
  dser = dser_proof_val

ser_def :: Def -> Ser ()
ser_def def = do
  ser # _def_ps_num def
  ser # _def_type def
  ser # _def_level def

dser_def :: Dser Def
dser_def = do
  ps_num <- dser
  t <- dser
  lvl <- dser
  return # Def
    { _def_ps_num = ps_num
    , _def_type = t
    , _def_level = lvl
    }

instance Serializable Def where
  ser = ser_def
  dser = dser_def

ser_thm :: Thm -> Ser ()
ser_thm thm = do
  ser # _thm_ps_num thm
  ser # _thm_stat thm
  -- ser # _thm_level ref

dser_thm :: Dser Thm
dser_thm = do
  ps_num <- dser
  s <- dser
  -- level <- dser
  return # Thm
    { _thm_ps_num = ps_num
    , _thm_stat = s
    -- , _thm_level = level
    }

instance Serializable Thm where
  ser = ser_thm
  dser = dser_thm

ser_op_type :: OpType -> Ser ()
ser_op_type ot = case ot of
  Const -> ser_bnat 4 0
  Infix dir -> ser_bnat 4 1 >> ser dir
  Prefix -> ser_bnat 4 2
  Postfix -> ser_bnat 4 3
  Binder -> ser_bnat 4 4

dser_op_type :: Dser OpType
dser_op_type = dser_bnat 4 >>= \k -> case k of
  0 -> pure Const
  1 -> dser >>= liftm Infix
  2 -> pure Prefix
  3 -> pure Postfix
  4 -> pure Binder
  _ -> error ""

instance Serializable OpType where
  ser = ser_op_type
  dser = dser_op_type

ser_op_def :: OpDef -> Ser ()
ser_op_def def = do
  ser # _op_def_name def
  ser # _op_def_sym def
  ser # _op_def_type def
  ser # _op_def_rel def

dser_op_def :: Dser OpDef
dser_op_def = do
  name <- dser
  sym <- dser
  t <- dser
  rel <- dser
  return # OpDef
    { _op_def_name = name
    , _op_def_sym = sym
    , _op_def_type = t
    , _op_def_rel = rel
    }

instance Serializable OpDef where
  ser = ser_op_def
  dser = dser_op_def

-- ser_sexpr :: SExpr -> Ser ()
-- ser_sexpr se = case se of
--   SVar i -> ser_bnat 4 0 >> ser i
--   SRef name -> ser_bnat 4 1 >> ser name
--   SArg i -> ser_bnat 4 2 >> ser i
--   SLam -> ser_bnat 4 3
--   SApp -> ser_bnat 4 4
-- 
-- dser_sexpr :: Dser SExpr
-- dser_sexpr = dser_bnat 4 >>= \k -> case k of
--   0 -> dser >>= liftm SVar
--   1 -> dser >>= liftm SRef
--   2 -> dser >>= liftm SArg
--   3 -> pure SLam
--   4 -> pure SApp
-- 
-- instance Serializable SExpr where
--   ser = ser_sexpr
--   dser = dser_sexpr

ser_rw_info :: RwInfo -> Ser ()
ser_rw_info info = do
  ser # from_left # _rw_proof_info info
  Nothing <- pure # _rw_proof info
  -- ser # _rw_proof info
  ser # _rw_types info
  ser # _rw_terms info

dser_rw_info :: Dser RwInfo
dser_rw_info = do
  proof_info <- dser
  -- proof <- dser
  types <- dser
  terms <- dser
  return # RwInfo
    { _rw_proof_info = Left proof_info
    , _rw_proof = Nothing --proof
    , _rw_types = types
    , _rw_terms = terms
    }

instance Serializable RwInfo where
  ser = ser_rw_info
  dser = dser_rw_info

ser_pat_kind :: PatKindT -> Ser ()
ser_pat_kind pk = ser_enum pk

dser_pat_kind :: Dser PatKindT
dser_pat_kind = dser_enum

instance Serializable PatKindT where
  ser = ser_pat_kind
  dser = dser_pat_kind

ser_pat_elem :: PatElem -> Ser ()
ser_pat_elem pe = case pe of
  PatDepth d -> ser_bnat 3 0 >> ser d
  PatKind pk -> ser_bnat 3 1 >> ser pk
  PatN i -> ser_bnat 3 2 >> ser i
  PatId i -> ser_bnat 3 3 >> ser i

dser_pat_elem :: Dser PatElem
dser_pat_elem = dser_bnat 3 >>= \k -> case k of
  0 -> dser >>= liftm PatDepth
  1 -> dser >>= liftm PatKind
  2 -> dser >>= liftm PatN
  3 -> dser >>= liftm PatId
  _ -> error ""

instance Serializable PatElem where
  ser = ser_pat_elem
  dser = dser_pat_elem

ser_schemes :: TheorySchemes -> Ser ()
ser_schemes schemes = do
  ser # _schemes_ind schemes
  ser # _schemes_ind_tc schemes
  ser # _schemes_cs schemes
  ser # _schemes_cs_tc schemes

dser_schemes :: Dser TheorySchemes
dser_schemes = do
  ind <- dser
  ind_tc <- dser
  cs <- dser
  cs_tc <- dser
  return # TheorySchemes
    { _schemes_ind = ind
    , _schemes_ind_tc = ind_tc
    , _schemes_cs = cs
    , _schemes_cs_tc = cs_tc
    }

instance Serializable TheorySchemes where
  ser = ser_schemes
  dser = dser_schemes

ser_th :: TheoryT -> Ser ()
ser_th th = do
  ser # K._defs # _th_kth th
  ser # K._thms # _th_kth th
  ser # _th_defs th
  ser # _th_thms th
  ser # _th_simp_map th
  ser # _th_tcs th
  ser # _th_schemes th
  ser # _th_op_defs th
  ser # _th_next_index th
  ser # _th_sim_index th
  ser # _th_mp_id_name th
  ser # _th_mp_id_index th
  ser # _th_rels th
  ser # _th_mp_e_e th
  ser # _th_mp_e_stat th
  ser # _th_mp_e_p th
  ser # _th_mp_p_p th
  -- ser # _th_mp_p_sctr th

dser_th :: Dser TheoryT
dser_th = do
  kdefs <- dser
  kthms <- dser
  defs <- dser
  thms <- dser
  simp_map <- dser
  tcs <- dser
  schemes <- dser
  op_defs <- dser
  next_index <- dser
  sim_index <- dser
  mp_id_name <- dser
  mp_id_index <- dser
  rels <- dser
  mp_e_e <- dser
  mp_e_stat <- dser
  mp_e_p <- dser
  mp_p_p <- dser
  -- mp_p_sctr <- pure Map.empty
  return # TheoryT
    { _th_kth = K.th_mk_raw kdefs kthms
    , _th_defs = defs
    , _th_thms = thms
    , _th_simp_map = simp_map
    , _th_tcs = tcs
    , _th_schemes = schemes
    , _th_op_defs = op_defs
    , _th_term_par = op_defs_to_parser op_defs
    , _th_next_index = next_index
    , _th_sim_index = sim_index
    , _th_mp_id_name = mp_id_name
    , _th_mp_id_index = mp_id_index
    , _th_rels = rels
    , _th_mp_e_e = mp_e_e
    , _th_mp_e_stat = mp_e_stat
    , _th_mp_e_p = mp_e_p
    , _th_mp_p_p = mp_p_p
    -- , _th_mp_p_sctr = mp_p_sctr
    , _th_recon = False
    }

instance Serializable TheoryT where
  ser = ser_th
  dser = dser_th