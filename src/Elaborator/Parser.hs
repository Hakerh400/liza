module Elaborator.Parser where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Maybe
import Data.Either
import Data.List
import Data.Char
import Control.Monad
import Control.Monad.State
import Control.Monad.Trans.Except
import Prelude hiding (print, putStrLn)

import Util
import Sctr
import Bit
import Tree
import Trie
import Serializer
import Parser
import Elaborator.Data
import Elaborator.Util

keywords :: HCS => Set Name
keywords = Set.fromList
  [ sym_lam
  , "at"
  , "as"
  ]

term_kws :: HCS => Set Name
term_kws = Set.union keywords # Set.fromList
  [ "let"
  , "in"
  , "if"
  , "then"
  , "else"
  ]

parse_keyword :: HCS => Name -> Elab ()
parse_keyword name = p_scope "keyword" # parse_word name

parse_name :: HCS => Elab Name
parse_name = do
  name <- p_ident
  False <- pure # Set.member name keywords
  return name

parse_name_no_trim :: HCS => Elab Name
parse_name_no_trim = do
  name <- p_ident_no_trim
  False <- pure # Set.member name keywords
  return name

parse_name_id_aux :: HCS => Prop -> Elab Id
parse_name_id_aux is_new = trimmed # do
  sctr <- plift # gets _par_sctr
  name <- parse_name_no_trim
  i <- name_to_id_aux is_new name
  -- let s = name_id_char : show i
  -- pt <- plift get
  -- plift # modify # \par -> par
  --   { _par_sctr = (_par_scope par, reverse s) : sctr
  --   , _par_modified = True
  --   }
  return i

parse_name_id :: HCS => Elab Id
parse_name_id = parse_name_id_aux False

parse_new_name_id :: HCS => Elab Id
parse_new_name_id = parse_name_id_aux True

parse_id_aux :: HCS => Prop -> Elab Id
parse_id_aux is_new = parse_name_id_aux is_new
  -- ++> do
  --   trimmed # do
  --     p_char name_id_char
  --     ds <- p_take_while1 is_digit
  --     True <- pure # head' undefined ds /= '0'
  --     return # read ds

parse_id :: HCS => Elab Id
parse_id = parse_id_aux False

parse_new_id :: HCS => Elab Id
parse_new_id = parse_id_aux True

-----

ctx_enter_lam_aux :: HCS => Maybe Name -> Type -> Ctx -> Ctx
ctx_enter_lam_aux mn t ctx = let
  d = _ctx_term_depth ctx
  name = fromMaybe [usc', 'A'] mn
  in ctx
    { _ctx_terms = map_insert d t # _ctx_terms ctx
    , _ctx_term_names = map_insert name d # _ctx_term_names ctx
    , _ctx_term_depth = d + 1
    }

ctx_enter_lam' :: HCS => Maybe Name -> Type -> Elab ()
ctx_enter_lam' mn t = modify_ctx # ctx_enter_lam_aux mn t

ctx_enter_lam :: HCS => Name -> Type -> Elab ()
ctx_enter_lam name = ctx_enter_lam' # Just name

ctx_enter_proof_lam' :: HCS => Maybe Name -> N -> Stat -> Ctx -> Ctx
ctx_enter_proof_lam' mn = ctx_insert_proof' mn Nothing

ctx_enter_proof_lam :: HCS => Maybe Name -> N -> Stat -> Elab ()
ctx_enter_proof_lam mn i s = modify_ctx # ctx_enter_proof_lam' mn i s

-----

ctx_get_term_ref :: HCS => Id -> Elab Def
ctx_get_term_ref i = do
  res <- sim_dflt_id i # gets_th # map_get i . _th_defs
  case res of
    Nothing -> do
      name <- id_to_name i
      err [dfsc "Undefined global term ", ("term", name)]
    Just res -> pure res

ctx_get_term_var_by_name' :: HCS => Name -> Elab (Maybe (N, Type))
ctx_get_term_var_by_name' name = do
  ctx <- get_ctx
  return # do
    i <- map_get name # _ctx_term_names ctx
    t <- map_get i # _ctx_terms ctx
    return (i, t)

ctx_get_term_var_by_name :: HCS => Name -> Elab (N, Type)
ctx_get_term_var_by_name name = do
  res <- ctx_get_term_var_by_name' name
  case res of
    Nothing -> err [dfsc "Undefined local term ", ("term", name)]
    Just res -> pure res

ctx_get_term_var' :: HCS => N -> Elab (Maybe Type)
ctx_get_term_var' i = gets_ctx # map_get i . _ctx_terms

ctx_get_term_var :: HCS => N -> Elab Type
ctx_get_term_var i = do
  Just t <- ctx_get_term_var' i
  return t

ctx_get_proof_ref' :: HCS => Id -> Elab (Maybe Thm)
ctx_get_proof_ref' i = sim_dflt_id i # do
  mp <- gets_th _th_thms
  return # map_get i mp

ctx_get_proof_ref :: HCS => Id -> Elab Thm
ctx_get_proof_ref i = do
  ref <- ctx_get_proof_ref' i
  case ref of
    Nothing -> do
      name <- id_to_name i
      
      -- ifm (i == 22) # error # show (i, name)
      
      err [dfsc "Undefined global proof ", ("proof", name)]
    Just ref -> pure ref

-- ctx_get_proof_name :: HCS => Name -> Elab (Maybe Name)
-- ctx_get_proof_name name = gets_ctx # map_get name . _ctx_proof_names_mp

-- ctx_err_no_local_proof :: HCS => Name -> Elab a
-- ctx_err_no_local_proof name = err
--   [dfsc "Undefined local proof ", ("proof", name)]

ctx_get_proof_var' :: HCS => N -> Elab (Maybe Stat)
ctx_get_proof_var' i = do
  ctx <- get_ctx
  return # map_get i # _ctx_proofs ctx

ctx_get_proof_var :: HCS => N -> Elab Stat
ctx_get_proof_var i = do
  ps <- gets_ctx _ctx_proofs
  res <- ctx_get_proof_var' i
  case res of
    Nothing -> err [dfsc "ctx_get_proof_var"]
    Just s -> pure s

-- ctx_get_proof_var_res' :: HCS => Name -> Elab (Maybe (Name, Stat))
-- ctx_get_proof_var_res' name = do
--   mp <- gets_ctx _ctx_proof_names_mp1
--   case map_get name mp of
--     Nothing -> pure Nothing
--     Just name -> do
--       res <- ctx_get_proof_var' name
--       case res of
--         Nothing -> pure Nothing
--         Just s -> pure # Just (name, s)
-- 
-- ctx_get_proof_var_res :: HCS => Name -> Elab (Name, Stat)
-- ctx_get_proof_var_res name = do
--   res <- ctx_get_proof_var_res' name
--   case res of
--     Nothing -> ctx_err_no_local_proof name
--     Just res -> pure res

-----

ctx_clear_ids' :: HCS => [N] -> [N] -> Ctx -> Ctx
ctx_clear_ids' e_ids p_ids ctx = ctx
  { _ctx_terms_hidden = foldr Set.insert (_ctx_terms_hidden ctx) e_ids
  , _ctx_proofs_hidden = foldr Set.insert (_ctx_proofs_hidden ctx) p_ids
  , _ctx_asms_normed = False
  }

ctx_clear_ids :: HCS => [N] -> [N] -> Elab ()
ctx_clear_ids e_ids p_ids = modify_ctx #
  ctx_clear_ids' e_ids p_ids

-----

elab_to_parser :: HCS => Elab a -> Parser ElabM a
elab_to_parser (ParserM pa) = do
  res <- runExceptT # evalStateT pa undefined
  case res of
    Left _ -> par_fail
    Right a -> pure a

-----

-- term_par_trace :: HCS => Elab a -> Elab a
-- term_par_trace m = m ++> do
--   str <- p_rest
--   tracem # Msg [quote str]
--   pfail

-- term_par_dbg :: HCS => (Show a) => a -> Elab ()
-- term_par_dbg = th_to_elab . th_dbg

-----

init_ctx' :: HCS => N -> Ctx
init_ctx' i = Ctx
  { _ctx_id = i
  , _ctx_is_tc = False
  , _ctx_term_depth = 0
  , _ctx_proof_depth = 0
  , _ctx_types = Map.empty
  , _ctx_terms = Map.empty
  , _ctx_term_names = Map.empty
  , _ctx_proofs = Map.empty
  , _ctx_terms_hidden = Set.empty
  , _ctx_proofs_hidden = Set.empty
  , _ctx_asms = []
  , _ctx_asm_deps = Map.empty
  , _ctx_asms_normed = True
  , _ctx_tcs = Nothing
  , _ctx_goal_stat = Nothing
  , _ctx_term_stack = []
  , _ctx_op_stack = []
  , _ctx_term_parser_state = 0
  , _ctx_mvars_num = 0
  , _ctx_term_mvar_depths = Map.empty
  , _ctx_type_asgns = Map.empty
  , _ctx_term_asgns = Map.empty
  , _ctx_eqs_on_hold = Map.empty
  , _ctx_mvars_map = Map.empty
  -- , _ctx_tac_terms = Map.empty
  -- , _ctx_tac_proofs = Map.empty
  , _ctx_tacs = []
  }

init_ctx :: HCS => Ctx
init_ctx = init_ctx' 0

ctx_reset_mvars :: HCS => Elab ()
ctx_reset_mvars = modify_ctx # \ctx -> ctx
  { _ctx_type_asgns = Map.empty
  , _ctx_term_asgns = Map.empty
  , _ctx_mvars_num = 0
  }

ctx_with_new_mvars :: HCS => Elab a -> Elab a
ctx_with_new_mvars m = do
  ctx <- get_ctx
  ctx_reset_mvars
  res <- m
  put_ctx ctx
  return res

-----

term_par_op_appr :: HCS => Op
term_par_op_appr = Op
  { _op_name = namei_appr
  , _op_sym = sym_appr
  , _op_type = Infix (Just 1)
  , _op_pri = 0
  }

term_par_op_lam :: HCS => Op
term_par_op_lam = Op
  { _op_name = namei_lam
  , _op_sym = sym_lam
  , _op_type = Binder
  , _op_pri = 0
  }

term_par_op_univ :: HCS => Op
term_par_op_univ = Op
  { _op_name = namei_univ
  , _op_sym = sym_univ
  , _op_type = Binder
  , _op_pri = 0
  }

term_par_op_imp :: HCS => Op
term_par_op_imp = Op
  { _op_name = namei_imp
  , _op_sym = sym_imp
  , _op_type = Infix (Just 1)
  , _op_pri = 1
  }

term_par_init_tree :: HCS => Tree [Op]
term_par_init_tree =
  Node [term_par_op_appr, term_par_op_lam, term_par_op_univ] Leaf #
  Node [term_par_op_imp] Leaf Leaf

term_par_tree_to_syms :: HCS => Tree [Op] -> Trie Char [Op]
term_par_tree_to_syms tree = map_to_trie # list_to_map_list #
  map (\op -> (_op_sym op, op)) # concat (inorder tree)

term_par_tree_to_names :: HCS => Tree [Op] -> Map Id Op
term_par_tree_to_names tree = case tree of
  Leaf -> Map.empty
  Node ops left right -> Map.unions
    [ Map.fromList # map <~ ops # \op -> (_op_name op, op)
    , term_par_tree_to_names left
    , term_par_tree_to_names right
    ]

term_par_calc_new_pri :: HCS => Ordering -> N -> N
term_par_calc_new_pri ord pri = case ord of
  GT -> pri + 1
  _ -> pri

term_par_op_inc_pri :: HCS => Op -> Op
term_par_op_inc_pri op = op {_op_pri = _op_pri op + 1}

term_par_ops_inc_pri :: HCS => [Op] -> [Op]
term_par_ops_inc_pri = map term_par_op_inc_pri

term_par_tree_inc_pri :: HCS => Tree [Op] -> Tree [Op]
term_par_tree_inc_pri = fmap term_par_ops_inc_pri

show_op :: HCS => Op -> Theory String
show_op op = do
  name <- th_id_to_name # _op_name op
  return # concat [name, ":", show # _op_pri op]

show_par_tree :: HCS => Tree [Op] -> Theory String
show_par_tree = show_tree_m "/" # show_list_m show_op

term_par_tree_insert_op :: HCS => Ordering -> Op -> Tree [Op] -> Tree [Op]
term_par_tree_insert_op ord op tree = case tree of
  Leaf -> tree_singleton [op]
  Node ops left right -> let
    pri = _op_pri op
    ops_pri = _op_pri # head' undefined ops
    ord1 = compare pri ops_pri
    in if ord1 == LT || (ord1 == EQ && ord == LT)
      then Node (term_par_ops_inc_pri ops)
        (term_par_tree_insert_op ord op left)
        (term_par_tree_inc_pri right)
      else if ord1 == EQ
        then if ord == EQ
          then Node (op : ops) left right
          else term_par_tree_insert_op LT op tree
        else Node ops left #
          term_par_tree_insert_op ord op right

term_par_tree_max_op_pri :: HCS => Tree [Op] -> N
term_par_tree_max_op_pri tree = case tree of
  Node (op : _) _ right -> case right of
    Leaf -> _op_pri op
    _ -> term_par_tree_max_op_pri right
  _ -> error ""

term_par_tree_to_par :: HCS => Tree [Op] -> TermParser
term_par_tree_to_par tree = TermParser
  { _term_par_tree = tree
  , _term_par_syms = term_par_tree_to_syms tree
  , _term_par_names = term_par_tree_to_names tree
  , _term_par_max_op_pri = term_par_tree_max_op_pri tree
  }

term_par_insert_op :: HCS => Ordering -> Op -> TermParser -> TermParser
term_par_insert_op ord op par = term_par_tree_to_par #
  term_par_tree_insert_op ord op # _term_par_tree par

term_par_init :: HCS => TermParser
term_par_init = term_par_tree_to_par term_par_init_tree

get_par :: HCS => Elab TermParser
get_par = get_th >>= liftm _th_term_par

with_new_ctx_stack :: HCS => Elab a -> Elab a
with_new_ctx_stack m = do
  ctx <- get_ctx
  put_ctx # ctx
    { _ctx_term_stack = []
    , _ctx_op_stack = []
    , _ctx_term_parser_state = 0
    }
  a <- m
  ctx_new <- get_ctx
  let term_stack = _ctx_term_stack ctx_new
  let op_stack = _ctx_op_stack ctx_new
  if null term_stack && null op_stack then nop else do
    err # map dfsc
      [ "term_stack: ", show term_stack
      , "\nop_stack: ", show op_stack
      ]
  put_ctx # ctx_new
    { _ctx_term_stack = _ctx_term_stack ctx
    , _ctx_op_stack = _ctx_op_stack ctx
    , _ctx_term_parser_state = _ctx_term_parser_state ctx
    }
  return a

with_new_ctx :: HCS => Elab a -> Elab a
with_new_ctx m = do
  ctx0 <- get_ctx
  a <- with_new_ctx_stack m
  modify_ctx # \ctx -> ctx
    { _ctx_term_depth = _ctx_term_depth ctx0
    , _ctx_proof_depth = _ctx_proof_depth ctx0
    , _ctx_types = _ctx_types ctx0
    , _ctx_terms = _ctx_terms ctx0
    , _ctx_term_names = _ctx_term_names ctx0
    , _ctx_proofs = _ctx_proofs ctx0
    }
  return a

ctx_push_term :: HCS => Term -> Elab ()
ctx_push_term e = do
  es <- gets_ctx _ctx_term_stack
  modify_ctx # \ctx -> ctx {_ctx_term_stack = e : es}

ctx_pop_term :: HCS => Elab Term
ctx_pop_term = do
  (e : es) <- gets_ctx _ctx_term_stack
  modify_ctx # \ctx -> ctx {_ctx_term_stack = es}
  return e

ctx_push_op :: HCS => Op -> Elab ()
ctx_push_op op = do
  ops <- gets_ctx _ctx_op_stack
  modify_ctx # \ctx -> ctx {_ctx_op_stack = op : ops}

ctx_pop_op :: HCS => Elab Op
ctx_pop_op = do
  (op:ops) <- gets_ctx _ctx_op_stack
  modify_ctx # \ctx -> ctx {_ctx_op_stack = ops}
  return op

-----

ctx_set_term_mvar_depth :: HCS => N -> N -> Elab ()
ctx_set_term_mvar_depth i d = do
  mp <- gets_ctx _ctx_term_mvar_depths
  mp <- pure # map_insert i d mp
  modify_ctx # \ctx -> ctx {_ctx_term_mvar_depths = mp}

ctx_get_term_mvar_depth :: HCS => N -> Elab N
ctx_get_term_mvar_depth i = do
  mp <- gets_ctx _ctx_term_mvar_depths
  return # case map_get i mp of
    Just d -> d
    Nothing -> -1

ctx_mk_mvar :: HCS => Elab N
ctx_mk_mvar = do
  i <- gets_ctx _ctx_mvars_num
  -- tracem i
  modify_ctx # \ctx -> ctx {_ctx_mvars_num = i + 1}
  return i

ctx_mk_type_mvar_aux :: HCS => N -> Elab Type
ctx_mk_type_mvar_aux i = do
  return # mk_type_mvar i

ctx_mk_type_mvar :: HCS => Elab Type
ctx_mk_type_mvar = do
  i <- ctx_mk_mvar
  ctx_mk_type_mvar_aux i

ctx_mk_term_mvar_aux :: HCS => Type -> N -> Elab Term
ctx_mk_term_mvar_aux t i = do
  d <- ctx_term_depth
  ctx_set_term_mvar_depth i d
  return # mk_term_mvar d t i

ctx_mk_term_mvar' :: HCS => Type -> Elab Term
ctx_mk_term_mvar' t = do
  i <- ctx_mk_mvar
  ctx_mk_term_mvar_aux t i

ctx_mk_term_mvar :: HCS => Elab Term
ctx_mk_term_mvar = do
  t <- ctx_mk_type_mvar
  ctx_mk_term_mvar' t

ctx_mk_stat_mvar :: HCS => Elab Stat
ctx_mk_stat_mvar = ctx_mk_term_mvar' mk_prop_t

ctx_mk_stat_mvars :: HCS => N -> Elab [Stat]
ctx_mk_stat_mvars n = replicateM (fi n) ctx_mk_stat_mvar

ctx_get_term_mvar :: HCS => Type -> N -> Elab Term
ctx_get_term_mvar t i = do
  d <- ctx_get_term_mvar_depth i
  return # mk_term_mvar d t i

ctx_mk_proof_mvar :: HCS => Stat -> Elab Proof
ctx_mk_proof_mvar s = do
  i <- ctx_mk_mvar
  return # mk_proof_mvar s i

ctx_mk_proof_mvar_tc' :: HCS => Prop -> Stat -> Elab Proof
ctx_mk_proof_mvar_tc' is_tc s = do
  i <- ctx_mk_mvar
  return # mk_proof_mvar' is_tc s i

ctx_mk_proof_mvar_tc :: HCS => Stat -> Elab Proof
ctx_mk_proof_mvar_tc = ctx_mk_proof_mvar_tc' True

ctx_mk_proof_mvar' :: HCS => Elab Proof
ctx_mk_proof_mvar' = do
  s <- ctx_mk_stat_mvar
  ctx_mk_proof_mvar s

ctx_mk_mvars :: HCS => N -> Elab [N]
ctx_mk_mvars n = replicateM (fi n) ctx_mk_mvar

ctx_mk_type_mvars :: HCS => N -> Elab [Type]
ctx_mk_type_mvars n = replicateM (fi n) ctx_mk_type_mvar

term_par_get_op :: HCS => Id -> Elab Op
term_par_get_op i = do
  par <- get_par
  case map_get i # _term_par_names par of
    Nothing -> do
      name <- id_to_name i
      err [dfsc "Undefined symbol ", ("lit_str", name)]
    Just op -> pure op

ctx_set_types :: HCS => [Name] -> Elab ()
ctx_set_types names = do
  let ps = list_to_map_inv names
  modify_ctx # \ctx -> ctx {_ctx_types = ps}

term_par_add_const :: HCS => Id -> String -> TermParser -> TermParser
term_par_add_const name sym par = let
  op = Op
    { _op_name = name
    , _op_sym = sym
    , _op_type = Const
    , _op_pri = 0
    }
  in term_par_insert_op EQ op par

term_par_add_op :: HCS => OpType -> Id -> String -> Rel -> TermParser -> TermParser
term_par_add_op op_type name sym rel par = let
  ref = _rel_op rel
  ord = _rel_ord rel
  pri_ref = _op_pri ref
  pri = term_par_calc_new_pri ord pri_ref
  op = Op
    { _op_name = name
    , _op_sym = sym
    , _op_type = op_type
    , _op_pri = pri
    }
  in term_par_insert_op ord op par

op_defs_to_parser' :: HCS => TermParser -> OpDef -> TermParser
op_defs_to_parser' par def = let
  name = _op_def_name def
  sym = _op_def_sym def
  op_type = _op_def_type def
  in case _op_def_rel def of
    Nothing -> term_par_add_const name sym par
    Just (rel_name, ord) -> let
      rel = Rel
        { _rel_op = case map_get rel_name # _term_par_names par of
          Just op -> op
          Nothing -> trace' <~ error "" # mk_pair rel_name #
            map _op_name # Map.elems # _term_par_names par
        , _rel_ord = ord
        }
      in term_par_add_op op_type name sym rel par

op_defs_to_parser :: HCS => [OpDef] -> TermParser
op_defs_to_parser defs = foldl' op_defs_to_parser' term_par_init #
  sort_by <~ defs # \def1 def2 -> let
    name1 = _op_def_name def1
    name2 = _op_def_name def2
    in if (_op_def_rel def1 >>= liftm fst) == Just name2 then GT
      else if (_op_def_rel def2 >>= liftm fst) == Just name1 then LT
      else EQ

pst_get_proof :: HCS => ProofState -> Proof
pst_get_proof pst = _pst_fn pst []

init_elab' :: HCS => TheoryT -> Ctx -> ElabT
init_elab' th ctx = ElabT
  { _elab_th = th
  , _elab_pst = init_pst ctx
  , _elab_goals_num = 1
  }

init_elab :: HCS => TheoryT -> ElabT
init_elab th = init_elab' th init_ctx

ctx_init_pst :: HCS => Elab ProofState
ctx_init_pst = do
  ctx <- get_ctx
  return # init_pst ctx

ctx_init :: HCS => Elab ()
ctx_init = put_pst # init_pst init_ctx

ctx_init' :: HCS => Stat -> Elab ()
ctx_init' s = do
  ctx_init
  ctx_put_goal_stat s

-----

-- th_parse :: HCS => Elab a -> String -> Theory (a, Sctr)
-- th_parse (ParserM m) str = StateT # \th -> do
--   let elab = init_elab th
--   let rp = runExceptT # runStateT m elab
--   let res = parse rp str
--   case res of
--     Nothing -> err [dfsc "No parse"]
--     Just (res, str, sctr) -> do
--       (a, pt) <- res
--       ifmn (null str) # err [dfsc "Syntax error\n\n", ("lit_str", str)]
--       return ((a, sctr), _elab_th pt)

th_parse :: HCS => Elab a -> String -> Theory (a, Sctr)
th_parse m str = do
  th <- get
  res <- th_lift_m # parse m (init_elab th) str
  case res of
    Left msg -> raise msg
    Right (a, elab, pt) -> do
      put # _elab_th elab
      return (a, _par_sctr pt)

elab_to_th' :: HCS => Ctx -> Elab a -> Theory (a, Sctr)
elab_to_th' ctx m = th_parse (put_ctx ctx >> m) ""

elab_to_th :: HCS => Elab a -> Theory (a, Sctr)
elab_to_th = elab_to_th' init_ctx

-----

-- proof_index_to_name :: HCS => N -> Name
-- proof_index_to_name i = 'h' :
--   map (\d -> chr # 8320 - 48 + ord d) (show i)

-- ctx_get_avail_proof_name :: HCS => Elab Name
-- ctx_get_avail_proof_name = do
--   names <- error "" -- ctx_get_proof_names
--   let indices = Set.map proof_name_to_index names
--   let i = last' 0 (Set.toList indices) + 1
--   -- let index = nat_find' (\i -> not # Set.member i indices) 1
--   return # proof_index_to_name i

ctx_get_avail_proof_id :: HCS => Elab N
ctx_get_avail_proof_id = gets_ctx _ctx_proof_depth

-- ctx_get_deps' :: HCS => N -> Elab [Name]
-- ctx_get_deps' i = do
--   mp <- gets_ctx _ctx_proofs
--   return # map fst #
--     filter (\(_, s) -> term_has_term_var i s) # Map.toList mp
-- 
-- ctx_get_deps :: HCS => Name -> Elab [Name]
-- ctx_get_deps name = do
--   res <- ctx_get_term_var' name
--   case res of
--     Nothing -> error ""
--     Just (i, _) -> ctx_get_deps' i

ctx_get_goal_stat :: HCS => Elab Stat
ctx_get_goal_stat = do
  res <- gets_ctx _ctx_goal_stat
  case res of
    Just s -> pure s
    Nothing -> error "ctx_get_goal_stat"

ctx_put_goal_stat' :: HCS => Maybe Stat -> Elab ()
ctx_put_goal_stat' s = modify_ctx # \ctx -> ctx
  {_ctx_goal_stat = s}

ctx_put_goal_stat :: HCS => Stat -> Elab ()
ctx_put_goal_stat s = ctx_put_goal_stat' # Just s {-if e_res s
  then ctx_put_goal_stat' # Just s
  else err ["ctx_put_goal_stat"]-}

ctx_clear_goal_stat :: HCS => Elab ()
ctx_clear_goal_stat = ctx_put_goal_stat' Nothing

-----

-- (xs, b)
-- xs ---> The proofs that depend on `name`
-- b  ---> Whether the goal statement depends on `name`
ctx_calc_deps_for_ident :: HCS => N -> Elab (Maybe ([N], Prop))
ctx_calc_deps_for_ident i = do
  hidden <- gets_ctx _ctx_proofs_hidden
  mp <- gets_ctx _ctx_proofs
  mp <- pure # Map.filterWithKey <~ mp # \i _ ->
    not # Set.member i hidden
  xs <- pure # map fst # filter <~ Map.toList mp #
    \(_, s) -> term_has_free_var i s
  s <- ctx_get_goal_stat
  return # Just (xs, term_has_free_var i s)

ctx_calc_deps_for_idents_fn :: HCS => ([N], [N]) -> N -> Elab ([N], [N])
ctx_calc_deps_for_idents_fn (xs, ys) i = do
  res <- ctx_calc_deps_for_ident i
  case res of
    Nothing -> pure (xs, ys)
    Just (zs, b) -> do
      let xs' = xs ++ zs
      let ys' = ite b (i : ys) ys
      return (xs', ys')

-- (xs, ys)
-- xs ---> The new proofs that depend on the terms from `names`
-- ys ---> The terms from the `names` that appear in the goal statement
ctx_calc_deps_for_idents :: HCS => [LocIdent] -> Elab ([N], [N])
ctx_calc_deps_for_idents idents = do
  let names = fst # partitionEithers idents
  (xs, ys) <- foldM ctx_calc_deps_for_idents_fn ([], []) names
  return (nub xs, reverse # nub ys)

-----

-- instance OrElse (Elab a) where
--   m1 <|> m2 = do
--     res <- try m1
--     case res of
--       Left msg -> m2
--       Right x -> pure x

-----

-- ctx_try :: HCS => Elab a -> Elab (Either Sctr a)
-- ctx_try m = do
--   st <- plift get
--   elab <- get
--   let str = _par_str st
--   let m1 = runExceptT # runStateT m elab
--   res <- plift # par_try m1
--   case res of
--     Nothing -> return # Left ["ctx_try: Syntax error"]
--     Just res -> case res of
--       Left msg -> do
--         plift # par_put_str str
--         return # Left msg
--       Right (x, elab) -> do
--         put elab
--         return # Right x

ctx_try :: HCS => Elab a -> Elab (Either Sctr a)
ctx_try (ParserM m) = do
  st <- plift get
  elab <- get
  let m1 = runExceptT # runStateT m elab
  res <- plift # par_try m1
  case join res of
    Left msg -> do
      plift # put st
      return # Left msg
    Right (x, elab) -> do
      put elab
      return # Right x

ctx_try' :: HCS => Elab a -> Elab ()
ctx_try' = void . ctx_try

ctx_try_p :: HCS => Elab a -> Elab Prop
ctx_try_p m = do
  res <- ctx_try m
  return # isRight res

ctx_trace :: HCS => Sctr -> Elab ()
ctx_trace msg = print # Lit # map snd msg

-----

ctx_repeat_aux :: HCS => ParserT -> ParserT -> Elab a -> Elab [a]
ctx_repeat_aux st1 st2 m = do
  res <- ctx_try m
  case res of
    Left _ -> do
      plift # put st2
      return []
    Right x -> do
      plift # put st1
      xs <- ctx_repeat_aux st1 st2 m
      return # x : xs

ctx_repeat' :: HCS => Elab a -> Elab [a]
ctx_repeat' m = do
  st1 <- plift get
  x <- m
  st2 <- plift get
  plift # put st1
  xs <- ctx_repeat_aux st1 st2 m
  return # x : xs

ctx_repeat :: HCS => Elab a -> Elab ()
ctx_repeat m = ctx_repeat' m >> nop

ctx_repeat0 :: HCS => Elab a -> Elab ()
ctx_repeat0 m = ctx_try' # ctx_repeat m

ctx_repeat_fn_aux :: HCS => ParserT -> ParserT -> a -> (a -> Elab a) -> Elab a
ctx_repeat_fn_aux st1 st2 z f = do
  res <- ctx_try # f z
  case res of
    Left _ -> do
      plift # put st2
      return z
    Right z -> do
      plift # put st1
      ctx_repeat_fn_aux st1 st2 z f

ctx_repeat_fn :: HCS => a -> (a -> Elab a) -> Elab a
ctx_repeat_fn z f = do
  st1 <- plift get
  z <- f z
  st2 <- plift get
  plift # put st1
  ctx_repeat_fn_aux st1 st2 z f

ctx_repeat0_fn :: HCS => a -> (a -> Elab a) -> Elab a
ctx_repeat0_fn z f = ctx_repeat_fn z f <|> pure z

-----

ctx_iter_aux :: HCS => N -> Elab a -> Elab [a]
ctx_iter_aux n m = do
  st <- plift get
  x <- m
  if n == 1 then pure [x] else do
    plift # put st
    xs <- ctx_iter' (n - 1) m
    return # x : xs

ctx_iter' :: HCS => N -> Elab a -> Elab [a]
ctx_iter' n m = do
  ifm (n == 0) # err [("tactic", "ctx_iter")]
  ctx_iter_aux n m

ctx_iter :: HCS => N -> Elab a -> Elab ()
ctx_iter n m = ctx_iter' n m >> nop

-----

ctx_mvars_map_get_aux :: HCS => Maybe N -> N -> Tactic N
ctx_mvars_map_get_aux d k = do
  mp <- gets_ctx _ctx_mvars_map
  case map_get k mp of
    Just i -> pure i
    Nothing -> do
      i <- ctx_mk_mvar
      case d of
        Nothing -> nop
        Just d -> ctx_set_term_mvar_depth i d
      modify_ctx # \ctx -> ctx {_ctx_mvars_map = map_insert k i mp}
      return i

ctx_mvars_map_get' :: HCS => N -> N -> Tactic N
ctx_mvars_map_get' d = ctx_mvars_map_get_aux # Just d

ctx_mvars_map_get :: HCS => N -> Tactic N
ctx_mvars_map_get = ctx_mvars_map_get_aux Nothing

ctx_mvars_map_get_type :: HCS => N -> Tactic N
ctx_mvars_map_get_type = ctx_mvars_map_get

ctx_mvars_map_get_term :: HCS => N -> N -> Tactic N
ctx_mvars_map_get_term = ctx_mvars_map_get'

-----

parse_term_ref_id :: HCS => Elab Id
parse_term_ref_id = p_scope "term" parse_id

parse_proof_ref_id :: HCS => Elab Id
parse_proof_ref_id = p_scope "proof" parse_id

parse_term_var_name :: HCS => Elab Name
parse_term_var_name = do
  name <- p_scope "term" parse_name
  False <- pure # elem name term_kws
  return name

parse_proof_var_name :: HCS => Elab Name
parse_proof_var_name = do
  name <- p_scope "proof" parse_name
  False <- pure # elem name term_kws
  return name

-----

-- ctx_proof_name_to_id :: HCS => Name -> Elab N
-- ctx_proof_name_to_id name = do
--   Just i <- pure # proof_name_to_id name
--   return i

-- parse_proof_id :: HCS => Elab N
-- parse_proof_id = do
--   name <- parse_proof_name
--   ctx_proof_name_to_id name

-- ctx_parse_name' :: HCS => (TheoryT -> Map Name a) -> (Ctx -> Map Name b) -> Name -> Tactic Name
-- ctx_parse_name' f g name = do
--   res <- gets_ctx # map_get name . g
--   case res of
--     Just _ -> pure name
--     Nothing -> do
--       Just _ <- gets_th # map_get name . f
--       return name
-- 
-- ctx_parse_name :: HCS => Tactic Name
-- ctx_parse_name = choice
--   [ parse_term_name >>= ctx_parse_name' _th_defs _ctx_terms
--   , parse_proof_name >>= ctx_parse_name' _th_thms _ctx_proof_names_mp1
--   ]

-----

parse_term_id_or_name :: HCS => Tactic (Either Id Name)
parse_term_id_or_name = p_either' parse_term_ref_id # do
  name <- parse_term_var_name
  True <- gets_ctx # Map.member name . _ctx_term_names
  return name

parse_term_index :: HCS => Tactic N
parse_term_index = do
  name <- parse_term_var_name
  Just i <- gets_ctx # map_get name . _ctx_term_names
  return i

get_asm_index' :: HCS => Name -> Elab (Maybe N)
get_asm_index' name = do
  res <- gets_ctx # find ((== name) . fst) . _ctx_asms
  return # fmap snd res

get_asm_index :: HCS => HCS => Name -> Elab N
get_asm_index name = do
  res <- get_asm_index' name
  case res of
    Nothing -> error "get_asm_index"
    Just i -> pure i

get_asm_name' :: HCS => N -> Elab (Maybe Name)
get_asm_name' i = do
  res <- gets_ctx # find ((== i) . snd) . _ctx_asms
  case res of
    Just (name, _) -> pure # Just name
    Nothing -> do
      res <- gets_ctx # map_get i . _ctx_asm_deps
      return # fmap fst res

get_asm_name :: HCS => HCS => N -> Elab Name
get_asm_name i = do
  res <- get_asm_name' i
  case res of
    Nothing -> error "get_asm_name"
    Just name -> pure name

parse_proof_index :: HCS => Elab N
parse_proof_index = do
  name <- parse_proof_var_name
  Just name <- get_asm_index' name
  return name

-----

-- ctx_parse_proof_name :: HCS => Tactic Name
-- ctx_parse_proof_name = do
--   name <- parse_proof_name
--   res <- gets_ctx # map_get name . _ctx_proof_names_mp1
--   case res of
--     Just name -> pure name
--     Nothing -> do
--       Just _ <- gets_th # map_get name . _th_thms
--       return name

-- ctx_parse_name :: HCS => Tactic Name
-- ctx_parse_name = ctx_parse_term_name <++ ctx_parse_proof_name

-----

insert_asm_dep :: HCS => Maybe Name -> Maybe N -> N -> Map N (Name, Maybe N) ->
  Map N (Name, Maybe N)
insert_asm_dep mn i0 i deps = case mn of
  Nothing -> deps
  Just name -> map_insert i (name, i0) deps

ctx_insert_proof' :: HCS => Maybe Name -> Maybe N -> N -> Stat -> Ctx -> Ctx
ctx_insert_proof' mn i0 i s ctx = ctx
  { _ctx_proof_depth = 1 + _ctx_proof_depth ctx
  , _ctx_proofs = map_insert i s # _ctx_proofs ctx
  , _ctx_asm_deps = insert_asm_dep mn i0 i # _ctx_asm_deps ctx
  , _ctx_asms_normed = False
  }

ctx_insert_proof :: HCS => Maybe Name -> Maybe N -> N -> Stat -> Elab ()
ctx_insert_proof mn i0 i s = modify_ctx # ctx_insert_proof' mn i0 i s

-- ctx_get_normed_list :: HCS => Ctx -> Map [(Name, Stat)]
-- ctx_get_normed_list ctx = Map.fromList # do
--   (name, i) <- Map.toList # _ctx_asms ctx
--   Just s <- pure # map_get i # _ctx_proofs ctx
--   -- let pr = if Set.member i # _ctx_tcs_ids ctx then "*" else ""
--   -- name <- pure # pr ++ name
--   return (name, s)

-- ctx_get_not_normed_list :: HCS => Ctx -> Map [(Name, Stat)]
-- ctx_get_not_normed_list ctx = Map.fromList # do
--   (i, s) <- Map.toList # _ctx_proofs ctx
--   return (show i, s)

ctx_get_asm_list' :: HCS => Ctx -> [(Name, Stat)]
ctx_get_asm_list' ctx = let ps = _ctx_proofs ctx in
  map <~ _ctx_asms ctx # \(name, i) -> let
    s = case map_get i ps of
      Just s -> s
      Nothing -> mk_term_ref 0 mk_set_t 1784 []
    in (name, s)

ctx_get_asm_list :: HCS => Elab [(Name, Stat)]
ctx_get_asm_list = do
  ctx <- get_ctx
  ifmn (_ctx_asms_normed ctx) # err [dfsc "Not normed"]
  return # ctx_get_asm_list' ctx

-----

ctx_or' :: HCS => Elab a -> Elab a -> Elab a
ctx_or' m1 m2 = do
  res <- ctx_try m1
  case res of
    Right a -> pure a
    Left _ -> m2

ctx_or_ :: HCS => Elab a -> Elab a -> Elab a
ctx_or_ m1 m2 = do
  res <- ctx_try m1
  case res of
    Right a -> pure a
    Left msg -> do
      res <- ctx_try m2
      case res of
        Right a -> pure a
        Left _ -> raise msg

ctx_ors_aux :: HCS => (Elab a -> Elab a -> Elab a) -> [Elab a] -> Elab a
ctx_ors_aux f ms = case ms of
  [] -> pfail
  (m : ms) -> if null ms then m else
    f m # ctx_ors_aux f ms

ctx_ors :: HCS => [Elab a] -> Elab a
ctx_ors = ctx_ors_aux (<|>)

ctx_ors' :: HCS => [Elab a] -> Elab a
ctx_ors' = ctx_ors_aux ctx_or'

instance Try Elab where
  try m = fmap either_to_maybe # ctx_try m

-----

ctx_get_thm :: HCS => Id -> Elab Thm
ctx_get_thm i = from_just_m # sim_dflt_id i # do
  mp <- gets_th _th_thms
  return # map_get i mp

get_ctxs :: HCS => Elab [Ctx]
get_ctxs = gets # _pst_ctxs . _elab_pst

get_ctx_ids :: HCS => Elab [N]
get_ctx_ids = do
  ctxs <- get_ctxs
  return # map _ctx_id ctxs

get_max_ctx_id :: HCS => Elab N
get_max_ctx_id = do
  is <- get_ctx_ids
  return # maximum is

ctx_get_new_ids' :: HCS => Tactic a -> Tactic (a, [N])
ctx_get_new_ids' tac = do
  n <- get_max_ctx_id
  res <- tac
  is <- get_ctx_ids
  return (res, filter (> n) is)

ctx_get_new_ids :: HCS => Tactic a -> Tactic [N]
ctx_get_new_ids tac = do
  (_, is) <- ctx_get_new_ids' tac
  return is

-----

ctx_parse_term_ref_id :: HCS => Elab Id
ctx_parse_term_ref_id = do
  i <- parse_term_ref_id
  has_def i >>= asrt
  return i

ctx_parse_proof_ref_id :: HCS => Elab Id
ctx_parse_proof_ref_id = do
  i <- parse_proof_ref_id
  has_thm i >>= asrt
  return i

ctx_parse_ref_id :: HCS => Elab (Either Id Id)
ctx_parse_ref_id = p_either
  ctx_parse_term_ref_id ctx_parse_proof_ref_id

ctx_parse_ref_id' :: HCS => Elab Id
ctx_parse_ref_id' = do
  res <- ctx_parse_ref_id
  return # from_either_same res

-----

ctx_query :: HCS => Elab a -> Elab a
ctx_query (ParserM m) = ParserM # query m

ctx_catch :: HCS => Elab a -> Elab a
ctx_catch m = do
  res <- ctx_try m
  case res of
    Right a -> pure a
    Left msg -> do
      ctx_trace msg
      error "ctx_catch"

op_to_op_def :: HCS => Op -> OpDef
op_to_op_def op = OpDef
  { _op_def_name = _op_name op
  , _op_def_sym = _op_sym op
  , _op_def_type = _op_type op
  , _op_def_rel = Nothing
  }