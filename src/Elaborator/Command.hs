module Elaborator.Command where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Maybe
import Data.Either
import Data.List
import Data.Char
import Control.Monad
import Control.Monad.State
import Prelude hiding (print, putStrLn)

import Util
import Sctr
import Bit
import Tree
import Trie
import Bimap
import Serializer
import Parser

import Elaborator.Util
import Elaborator.Data
import Elaborator.Parser
import Elaborator.Type
import Elaborator.Term
import Elaborator.Proof
import Elaborator.Tactic
import Elaborator.Theory
import Elaborator.Goal
import Elaborator.ProofCompressor
import Elaborator.Reconstruct

import qualified Kernel as K

auto_intro_terms = False

parse_cmd_term_id :: Elab Id
parse_cmd_term_id = p_scope "cmd_term" parse_new_id

parse_cmd_proof_id :: Elab Id
parse_cmd_proof_id = p_scope "cmd_proof" parse_new_id

parse_comment :: Elab ()
parse_comment = p_scope "comment" # do
  p_tok comment_prefix
  str <- p_rest
  cmd_comment str

parse_cmd_dbg :: [Attrib] -> Cmd ()
parse_cmd_dbg attrs = cmd_dbg

parse_type_param_names :: Elab [Name]
parse_type_param_names = pure [] ++> do
  p_toks [".", "{"]
  names <- p_sepf parse_type_name (p_tok ",")
  p_tok "}"
  ctx_set_types names
  return names

-- parse_def_arg :: Elab [(Name, Type)]
-- parse_def_arg = p_parens # do
--   names <- p_all parse_name
--   p_tok ":"
--   t <- parse_type
--   return # map (\name -> (name, t)) names
-- 
-- parse_def_args :: Prop -> Elab [(Name, Type)]
-- parse_def_args strict = do
--   let f = ite strict p_all p_all'
--   args <- f parse_def_arg
--   return # concat args

parse_term_name_and_params :: Elab (Id, [Name])
parse_term_name_and_params = do
  name <- parse_cmd_term_id
  ps <- parse_type_param_names
  return (name, ps)

parse_cmd_term :: [Attrib] -> Prop -> Elab (Id, [Name], Either Type Term)
parse_cmd_term attrs h = do
  (name, ps) <- parse_term_name_and_params
  ts <- do
    args <- parse_binder_args
    mapM <~ args # \arg -> do
      Left (_, t) <- pure arg
      return t
  t <- ctx_mk_type_mvar ++> do
    p_tok ":"
    parse_type
  te <- if not h
    then do
      t <- res_type # foldr mk_func t ts
      return # Left t
    else do
      p_tok ":="
      e <- with_new_ctx parse_term

      -- s1 <- th_to_elab # show_type t
      -- s2 <- th_to_elab # show_term e
      -- err [s1, "\n", s2]
      -- dbg e

      add_type_eq t # _term_type e
      e <- pure # mk_lams ts e
      e <- res_term e
      e <- reduce_term e

      return # Right e
  return (name, ps, te)

parse_cmd_const :: [Attrib] -> Cmd ()
parse_cmd_const attrs = do
  let is_local = elem AttribLocal attrs
  (name, ps, Left t) <- parse_cmd_term attrs False
  cmd_const name ps t is_local

parse_cmd_def' :: [Attrib] -> Cmd ()
parse_cmd_def' attrs = do
  let is_local = elem AttribLocal attrs
  (name, ps, Right e) <- parse_cmd_term attrs True
  cmd_def name ps e is_local

-- parse_cmd_desc :: [Attrib] -> Cmd ()
-- parse_cmd_desc attrs = do
--   let is_final = True
--   let is_local = elem AttribLocal attrs
--   (i, ps) <- parse_term_name_and_params
--   t <- ctx_mk_type_mvar ++> do
--     p_tok ":"
--     parse_type
--   p_tok "|"
--   name <- id_to_name i
--   ctx_enter_lam name t
--   cnd <- with_new_ctx parse_term
--   add_type_eq mk_prop_t # _term_type cnd
--   cnd <- pure # mk_lam t cnd
--   cnd <- res_term cnd
--   cnd <- reduce_term cnd
--   cmd_desc i ps cnd is_final is_local

parse_cmd_def :: [Attrib] -> Cmd ()
parse_cmd_def attrs = parse_cmd_def' attrs -- <++ parse_cmd_desc attrs

-- parse_proof_arg_e :: Elab [(Name, Type)]
-- parse_proof_arg_e = do
--   p_tok "{"
--   args <- parse_binder_args_group'
--   p_tok "}"
--   return args

-- parse_proof_args_e :: Elab [(Name, Type)]
-- parse_proof_args_e = do
--   args <- p_all' # parse_proof_arg_e
--   return # concat args

-- parse_proof_arg_p :: Elab [(Name, Term)]
-- parse_proof_arg_p = p_parens # do
--   names <- p_all # trimmed # parse_name
--   p_tok ":"
--   s <- parse_term
--   return # map (\name -> (name, s)) names

-- parse_proof_args_p :: Elab [(Name, Term)]
-- parse_proof_args_p = do
--   args <- p_all' # parse_proof_arg_p
--   return # concat args

get_expand_fn_name :: Name -> Elab Name
get_expand_fn_name name = case split_at_elem usc' name of
  (name : _) -> pure name
  _ -> err
   [ dfsc "Theorem with name ", ("proof", name)
   , dfsc " cannot be used with attribute ", ("lit_str", "expand") ]

attrs_map :: Map Name (Elab Attrib)
attrs_map = Map.fromList #
  [ ("expand", pure AttribExpand)
  , ("unf", pure AttribUnfold)
  , ("simp", pure AttribSimp)
  , ("comp", pure AttribComp)
  , ("tc", pure AttribTc)
  , ("ind", pure # AttribScheme SchemeInd)
  , ("cs", pure # AttribScheme SchemeCs)
  , ("local", pure AttribLocal)
  ]

parse_attrib :: Elab Attrib
parse_attrib = trimmed # do
  name <- parse_name
  case map_get name attrs_map of
    Nothing -> err [dfsc "Unknown attribute ", ("attrib", name)]
    Just m -> m

parse_attrs :: Elab [Attrib]
parse_attrs = (pure [] ++>) # p_scope "attrib" # do
  p_toks ["@", "["]
  attrs <- p_sepf parse_attrib # p_tok ","
  p_tok "]"
  return attrs

cmd_into_names :: [Name] -> Elab ()
cmd_into_names names = case names of
  [] -> nop
  (name : names) -> do
    res <- ctx_try # tac_univ_i name
    ifm (isRight res) # cmd_into_names names

parse_cmd_proof :: [Attrib] -> Prop -> Prop ->
  Elab (Maybe Id, [Name], Stat, Maybe (Either Proof K.Proof))
parse_cmd_proof attrs pname h = do
  name <- if not pname then pure Nothing else do
    i <- parse_cmd_proof_id
    th_to_elab # th_assert_no_id i
    
    -- id_to_name i >>= putStrLn . ("---> " ++)
    
    return # Just i
  ps <- parse_type_param_names
  let ps' = list_to_map_inv ps
  p_tok ":"
  
  s <- parse_term
  let names = term_get_intro_names s
  add_type_eq mk_prop_t # _term_type s
  s <- res_term s
  
  ifmn (e_res s) # do
    s1 <- ctx_show s
    err # dfsc "Meta variables in theorem statement:\n\n" : s1
  
  ks <- th_to_elab # term_to_kterm s
  -- let is_local = elem AttribLocal attrs
  -- is_local <- pure # if is_local then True else
  --   kterm_is_local ks
  
  -- name_loc <- if not is_local then pure name else case name of
  --   Nothing -> pure Nothing
  --   Just name -> do
  --     name <- th_to_elab # th_localize_name_inc name
  --     return # pure name
  
  let name_loc = name
  
  p <- if not h then pure Nothing else do
    pt <- plift get
    let pt_orig = pt

    -- f_eof <- pure # do
    --   p_eof
    --   Just name_loc <- pure name_loc
    --   res <- gets_th # map_get name_loc . _th_compr_proofs
    --   case res of
    --     Nothing -> err [dfsc "No compressed proof found"]
    --     Just s -> do
    --       let p = decompress_proof s
    --       () <- pure # seq p ()
    --       
    --       -- let s1 = compress_proof p
    --       -- ifm (s1 /= s) # modify_th # \th -> th
    --       --   {_th_cur_proof = Just (name_loc, s1)}
    -- 
    --       return # Just # Right p
    
    f_proof <- pure # do
      p_tok ":="
      p <- parse_proof s
      p <- res_proof p
      return # Just # Left p

    f_compressed_proof <- pure # do
      p_tok ":="
      s <- p_scope "lit_str" # do
        s@('`' : _) <- p_rest
        -- case name_loc of
        --   Nothing -> nop
        --   Just name_loc -> do
        --     modify_th # \th -> th
        --       {_th_cur_proof = Just (name_loc, s)}
        --     pt <- pure # pt {_par_str = ""}
        --     plift # put pt
        return s
      return # Just # Right # decompress_proof s
    
    f_tac_block <- pure # do
      p_tok ":="
      pt <- plift get
      p_tok "{"

      -- err [dfsc # show name]
      
      pt0 <- get
      ctx_put_goal_stat s
      
      ifm auto_intro_terms # do
        ctx0 <- get_ctx
        cmd_into_names names
        elab_norm ctx0
      
      itac_block_wout_open_brace
      p <- ctx_pst_get_proof
      modify # \pt -> pt
        { _elab_pst = _elab_pst pt0
        , _elab_goals_num = _elab_goals_num pt0
        }
      p <- res_proof p

      dflt <- pure # pure # Just # Left p

      case name_loc of
        Nothing -> dflt
        Just name_loc -> do
          kp <- th_to_elab # proof_to_kproof p
          res <- try # k_to_elab # K.calc_proof_axioms kp
          case res of
            Nothing -> dflt
            Just axioms -> do
              if Set.member namei_sorry axioms then dflt else do
                fn <- pure # do
                  -- let s = compress_proof kp
                  -- modify_th # \th -> th
                  --   {_th_cur_proof = Just (name_loc, s)}
                  
                  -- pt <- pure # pt_orig {_par_str = ""}
                  -- plift # put pt
                  
                  return # Just # Right kp
                sctr <- plift # gets _par_sctr
                case sctr of
                  (_, "}") : _ -> fn
                  (_, '}' : '\n' : _) : _ -> fn
                  _ -> dflt
    
    choice
      [ --f_eof
        f_tac_block
      , f_compressed_proof
      , f_proof
      ]
  
  return # (name, ps, s, p)

parse_cmd_axiom :: [Attrib] -> Cmd ()
parse_cmd_axiom attrs = do
  let is_local = elem AttribLocal attrs
  (Just name, ps, s, _) <- parse_cmd_proof attrs True False
  ifm is_local # err [dfsc "Axiom cannot be local"]
  cmd_axiom name attrs ps s False

parse_cmd_thm :: [Attrib] -> Cmd ()
parse_cmd_thm attrs = do
  let is_local = elem AttribLocal attrs
  (Just name, ps, s, Just p) <- parse_cmd_proof attrs True True
  cmd_thm name attrs ps s p is_local

parse_cmd_example :: [Attrib] -> Cmd ()
parse_cmd_example attrs = do
  (_, ps, s, Just p) <- parse_cmd_proof attrs False True
  cmd_thm' Nothing attrs ps s p False

-- parse_rel_ord :: Elab Ordering
-- parse_rel_ord = choice # flip map
--   [("<", LT), (">", GT), ("~", EQ)] # \(s, p_ord) ->
--     p_tok s >> pure p_ord
-- 
-- parse_rel :: Elab Rel
-- parse_rel = p_parens # do
--   p_ord <- parse_rel_ord
--   name <- parse_term_ref_id
--   op <- term_par_get_op name
--   return # Rel op p_ord
-- 
-- parse_symbol :: Elab String
-- parse_symbol = trimmed # p_take_while1 #
--   \c -> not # elem c " \t\r\n()"
-- 
-- parse_op_symbol :: Elab String
-- parse_op_symbol = p_scope "op" parse_symbol
-- 
-- parse_cmd_symbol :: [Attrib] -> Cmd ()
-- parse_cmd_symbol attrs = do
--   name <- parse_cmd_term_id
--   sym <- p_scope "term" parse_symbol
--   cmd_symbol name sym
-- 
-- parse_cmd_prefix :: [Attrib] -> Cmd ()
-- parse_cmd_prefix attrs = do
--   name <- parse_cmd_term_id
--   sym <- parse_op_symbol
--   rel <- parse_rel
--   cmd_prefix name sym rel
-- 
-- parse_cmd_infixl :: [Attrib] -> Cmd ()
-- parse_cmd_infixl attrs = do
--   name <- parse_cmd_term_id
--   sym <- parse_op_symbol
--   rel <- parse_rel
--   cmd_infixl name sym rel
-- 
-- parse_cmd_infixr :: [Attrib] -> Cmd ()
-- parse_cmd_infixr attrs = do
--   name <- parse_cmd_term_id
--   sym <- parse_op_symbol
--   rel <- parse_rel
--   cmd_infixr name sym rel
-- 
-- parse_cmd_binder :: [Attrib] -> Cmd ()
-- parse_cmd_binder attrs = do
--   name <- parse_cmd_term_id
--   sym <- parse_op_symbol
--   rel <- parse_rel
--   cmd_binder name sym rel

parse_cmd_show :: [Attrib] -> Cmd ()
parse_cmd_show attrs = do
  name <- ctx_parse_ref_id
  cmd_show name

parse_cmd_show_axioms :: [Attrib] -> Cmd ()
parse_cmd_show_axioms attrs = do
  name <- parse_proof_ref_id
  cmd_show_axioms name

-- parse_cmd_show_prefix :: [Attrib] -> Cmd ()
-- parse_cmd_show_prefix attrs = do
--   name <- ctx_parse_ref_name'
--   cmd_show_prefix name

-- parse_cmd_add_simp :: CmdParser
-- parse_cmd_add_simp par th = do
--   p_tok' "add_simp"
--   names <- p_all # trimmed # P.parse_name'
--   return # CmdAddSimp names

-- parse_cmd_tau :: CmdParser
-- parse_cmd_tau par th = do
--   p_tok' "tau"
--   name <- p_maybe # trimmed # P.parse_name'
--   args <- parse_def_args par True
--   p_tok ":="
--   pred_f <- parse_term par
--   return # CmdTau name args pred_f

-- th_add_def :: Name -> TermRef -> Theory ()
-- th_add_def name ref = do
--   kth <- gets _th_kth
--   name_loc <- th_localize_name name
--   let Just kdef = K.th_get_def kth name_loc
--   modify # \th -> th
--     {_th_terms = map_insert name ref # _th_terms th}
--
--   local <- th_name_is_local name
--   ifmn local # do
--     res <- gets _th_elab_res
--     res <- pure # res
--       { _res_kdefs = map_insert name_loc kdef # _res_kdefs res
--       , _res_terms = map_insert name ref # _res_terms res
--       }
--     modify # \th -> th {_th_elab_res = res}

cmd_add_const :: Id -> N -> Type -> Elab ()
cmd_add_const name ps_num t = do
  t <- res_type t
  
  ifmn (e_res t) # do
    s1 <- show_type t
    err # [dfsc "cmd_add_const: not resolved\n\n"] ++ s1
  
  let kt = type_to_ktype t
  
  k_to_elab # K.th_add_const name ps_num kt
  
  let ref = th_mk_def 1 ps_num t
  th_to_elab # th_add_def name ref

cmd_add_def :: Id -> Id -> N ->
  Term -> Prop -> Elab ()
cmd_add_def name thm_name ps_num val is_local = do
  val <- res_term val
  
  ifmn (e_res val) # do
    s1 <- show_term val
    err # [dfsc "cmd_add_desc: not resolved\n\n"] ++ s1
  
  let t = _term_type val
  let kt = type_to_ktype t
  ke <- th_to_elab # term_to_kterm val
  
  -- loc <- gets_th _th_local_names
  -- tracem loc
  
  -- is_local <- pure # if is_local then True else
  --   kterm_is_local ke
  
  -- (name_loc, thm_name_loc) <- if not is_local
  --   then pure (name, thm_name) else th_to_elab # do
  --     name_loc <- th_add_loc_name name
  --     thm_name_loc <- th_add_loc_name thm_name
  --     return (name_loc, thm_name_loc)
  let name_loc = name
  let thm_name_loc = thm_name
  
  -- kth <- gets_th _th_kth
  -- tracem (name_loc, thm_name_loc)
  -- let xs = Map.keys # K.th_get_defs kth
  -- tracem # Lit # pure # unwords xs
  
  th_thm <- k_to_elab # do
    K.th_add_def name_loc thm_name_loc ps_num kt ke
    K.th_get_thm thm_name_loc
  
  let ks = K._thm_stat th_thm
  
  th_to_elab # do
    th_mp_add_e_e name ke
    th_mp_add_e_stat thm_name ks
  
  lvl <- term_calc_level val
  let ref = th_mk_def (lvl + 1) ps_num t
  th_to_elab # th_add_def name ref
  
  thm_stat <- th_to_elab # kterm_to_term ks
  cmd_add_proof thm_name ps_num thm_stat

parse_cmd_init_simp :: [Attrib] -> Cmd ()
parse_cmd_init_simp attrs = do
  names <- p_all' parse_proof_ref_id
  cmd_init_simp names

parse_cmd_init_tc :: [Attrib] -> Cmd ()
parse_cmd_init_tc attrs = do
  names <- p_all' parse_proof_ref_id
  cmd_init_tc names

parse_cmd_init_comm :: [Attrib] -> Cmd ()
parse_cmd_init_comm attrs = do
  names <- p_all' parse_term_ref_id
  cmd_init_comm names

-- parse_cmd_snippet :: [Attrib] -> Cmd ()
-- parse_cmd_snippet attrs = cmd_snippet

-- parse_cmd_pp_raw :: [Attrib] -> Cmd ()
-- parse_cmd_pp_raw attrs = cmd_pp_raw

show_reconstruct_proof :: Id -> Elab Sctr
show_reconstruct_proof i = ctx_query # do
  thm <- k_to_elab # K.th_get_thm i
  Just p <- pure # K._thm_proof thm
  
  sh <- ctx_query # do
    ctx_init
    thm <- k_to_elab # K.th_get_thm i
    let s_ts = mk_type_params_sctr # K._thm_params_num thm
    s <- th_to_elab # kterm_to_term # K._thm_stat thm
    s1 <- ctx_show s
    let kw = "example"
    return # show_sctr # concat
      [ [("keyword", kw)], s_ts, [dfsc " : "]
      , s1, [dfsc " :=\n"]
      ]
  
  tac <- kproof_to_tac' p
  s <- show_tac tac
  
  s <- pure # sh ++ s
  
  -- putStrLn # s ++ "\n\n"
  
  modify_th # \th -> th {_th_recon = True}
  s <- th_to_elab # parse_and_run_cmd s
  
  return # sctr_drop (len sh) s

parse_cmd_reconstruct :: [Attrib] -> Cmd ()
parse_cmd_reconstruct attrs = do
  i <- parse_proof_ref_id
  
  thm <- k_to_elab # K.th_get_thm i
  Just p <- pure # K._thm_proof thm
  
  tac <- kproof_to_tac p
  s <- show_tac tac
  
  pt <- plift get
  
  let s0 = reverse # snd # last' (dfsc "") # _par_sctr pt
  s0 <- pure # case s0 of
    ('\n' : _) -> "\n"
    _ -> ""
  
  sh <- do
    thm <- k_to_elab # K.th_get_thm i
    let s_ts = mk_type_params_sctr # K._thm_params_num thm
    s <- th_to_elab # kterm_to_term # K._thm_stat thm
    s1 <- ctx_show s
    let kw = "example"
    s1 <- pure # [("keyword", kw)] ++ s_ts ++ [dfsc " : "]
      ++ s1 ++ [dfsc " :=\n"]
    return # (>>= snd) s1
  
  s <- pure # sh ++ s
  
  raise [dfsc s]
  plift # put # pt {_par_sctr = [dfsc # reverse # s0 ++ s]}

cmd_comment :: String -> Elab ()
cmd_comment str = nop

cmd_dbg :: Elab ()
cmd_dbg = enter_debug_mode

-- cmd_term :: Name -> [Name] -> Either Type Term -> Elab ()
-- cmd_term name ps te = do
--   th_to_elab # th_assert_no_id name
--   th <- get_kth
--   let thm_name = mk_def_thm_name name
--   let ps' = Set.fromList ps
--   let ps_num = len ps
--   if e_res te then nop else error "cmd_term: not resolved"
--   t <- case te of
--     Left t -> do
--       t <- res_type t
--       let kt = type_to_ktype t
--       put_kth # K.th_add_const th name ps_num kt
--       return t
--     Right e -> do
--       e <- res_term e
--       let t = _term_type e
--       let kt = type_to_ktype t
--       let ke = term_to_kterm e
-- 
--       put_kth # K.th_add_def th name thm_name ps_num kt ke
--       th <- get_kth
-- 
--       let th_thm = fromJust # K.th_get_thm th thm_name
-- 
--       th_to_elab # do
--         thm_stat <- kterm_to_term # K._thm_stat th_thm
--         thm_ref <- pure # Thm
--           { _thm_ps_num = ps_num
--           , _thm_stat = thm_stat
--           }
--         th_add_thm thm_name thm_ref
-- 
--       return t
--   ref <- pure # Def
--     { _term_ref_ps_num = ps_num
--     , _term_ref_type = t
--     }
--   th_to_elab # th_add_def name ref

-- cmd_desc :: Id -> [Name] -> Term -> Prop -> Prop -> Elab ()
-- cmd_desc name ps cnd is_final is_local = do
--   let file = "FILE"
-- 
--   th_to_elab # th_assert_no_id name
--   thm_name <- mk_desc_thm_name name
--   let ps' = Set.fromList ps
--   let ps_num = len ps
-- 
--   cmd_add_desc file name thm_name ps_num cnd is_final is_local

cmd_def :: Id -> [Name] -> Term -> Prop -> Elab ()
cmd_def name ps val is_local = do
  th_to_elab # th_assert_no_id name
  modify_ctx # \ctx -> ctx {_ctx_term_depth = 0}
  
  thm_name <- mk_def_thm_name name
  -- let ps' = Set.fromList ps
  let pn = len ps
  
  val <- res_term val
  ifmn (e_res val) # do
    s1 <- show_term val
    err # [dfsc "cmd_def: not resolved\n\n"] ++ s1
  
  cmd_add_def name thm_name pn val is_local
  
  -- Try to add desc thm
  res <- ctx_try # do
    let ps0 = ps
    
    App target cnd <- pure # _term_val val
    TermRef tau [t] <- pure # _term_val target
    True <- pure # tau == namei_tau
    
    let ts = [t]
    let ps = mk_list pn mk_type_var
    let t_names = map pure # take (fi pn) greek_letters_low
    ctx_set_types t_names
    
    let x = mk_term_var 1 t 0
    r <- ctx_mk_term_ref' name ps
    e1 <- pure # mk_app mk_prop_t cnd x
    e2 <- pure # mk_app mk_prop_t cnd r
    s0 <- res_term # mk_univ t # mk_imp e1 e2
    
    ctx_init' s0
    tac_univ_i "x"
    tac_imp_i "h"
    
    -- let tp = mk_func t mk_prop_t
    -- let ep = mk_term_var 1 tp 0
    -- e1 <- pure # mk_app mk_prop_t ep r
    -- e2 <- pure # mk_app mk_prop_t ep val
    -- s <- res_term # mk_univ tp # mk_imp e1 e2
    
    -- p <- ctx_mk_proof_ref' namei_subst_symm ts
    -- p <- ctx_univs_e p [val, r]
    -- p1 <- ctx_mk_proof_ref' thm_name ps
    -- p <- ctx_imp_e p p1
    -- p <- ctx_univ_e p cnd
    p <- ctx_mk_proof_ref' thm_name ps
    -- tracem "_true_def"
    -- ctx_trace_proof_stat p
    p <- ctx_univ_e p cnd
    
    p1 <- ctx_mk_proof_ref' namei_tau_ind ts
    -- tracem "tau_ind"
    -- ctx_trace_proof_stat p1
    p1 <- ctx_univs_e p1 [cnd, x]
    
    h <- ctx_mk_proof_var 0
    p1 <- ctx_imp_e p1 h
    p <- ctx_imp_e p p1
    tac_exact p
    
    p <- ctx_pst_get_proof
    -- tracem p
    -- kp <- th_to_elab # proof_to_kproof p
    -- tracem # show kp
    
    thm_name_p <- mk_desc_thm_name name
    cmd_thm thm_name_p [] ps0 (_proof_stat p) (Left p) is_local
    
    -- th_to_elab # th_add_id_rel name namei_def thm_name
  
  case res of
    Left msg -> do
      -- ifm (name == 71) # raise msg
      nop
    _ -> nop

cmd_const :: Id -> [Name] -> Type -> Prop -> Elab ()
cmd_const name ps t is_local = do
  th_to_elab # th_assert_no_id name
  modify_ctx # \ctx -> ctx {_ctx_term_depth = 0}
  
  -- thm_name <- mk_desc_thm_name name
  let ps' = Set.fromList ps
  let ps_num = len ps
  
  -- cnd <- ctx_mk_term_ref namei_true
  -- cnd <- pure # mk_lam t cnd

  -- cmd_add_desc file name thm_name ps_num cnd True is_local
  cmd_add_const name ps_num t

-- cmd_add_simp_1 :: Name -> Theory ()
-- cmd_add_simp_1 name = do
--   th <- get_kth
--   n <- case K.th_get_thm th name of
--     Nothing -> err ["add_simp: Theorem ", quote name, " not found"]
--     Just thm -> pure # K._thm_params_num thm
--   let ps = mk_list n type_var_to_str
--   let strict = False
--   let ts = map TypeVar ps
--   (p, s) <- elab_proof_raw # ProofVar name ts
--   (pat, info) <- simp_mk_trie_elem_for_proof p s
--   let simp_name = name ++ ".simp"
--   (p, s) <- elab_proof_raw # _rw_info_pf info
--   cmd_add_thm simp_name ps s p
--   info <- pure # info {_rw_info_pf = ProofVar simp_name ts}
--   add_simp_info pat info

-- cmd_add_simp :: [Name] -> Theory ()
-- cmd_add_simp = mapM_ cmd_add_simp_1

cmd_add_proof :: Id -> N -> Stat -> Elab ()
cmd_add_proof i ps_num s = th_to_elab # do
  -- level <- gets # \th -> map_size # _th_thms th
  th_add_thm i # Thm
    { _thm_ps_num = ps_num
    , _thm_stat = s
    -- , _thm_level = level
    }

cmd_add_axiom :: Id -> [Name] -> Stat -> Elab ()
cmd_add_axiom name ps s = do
  th_to_elab # do
    ks <- term_to_kterm s
    k_to_th # K.th_add_axiom name (len ps) ks
    th_mp_add_e_stat name ks
  cmd_add_proof name (len ps) s

cmd_add_thm :: HCS => Id -> [Name] -> Stat ->
  Either Proof K.Proof -> Prop -> Elab ()
cmd_add_thm name ps s p is_local = do
  th <- get_kth

  ks <- th_to_elab # term_to_kterm s
  
  -- is_local <- pure # if is_local then True else
  --   kterm_is_local ks
  -- 
  -- name_loc <- if not is_local then pure name
  --   else th_to_elab # th_add_loc_name name
  
  let name_loc = name
  
  kp <- case p of
    Left p -> th_to_elab # proof_to_kproof p
    Right kp -> pure kp

  -- st <- plift get
  -- tracem st
  -- undefined

  -- kp <- pure # decompress_proof # compress_proof kp
  -- tracem # kp == kp1

  -- tracem # Lit # pure # compress_proof kp
  -- tracem # Lit []
  
  -- ifm (null name_loc) # tracem p
  k_to_elab # K.th_add_thm name_loc (len ps) ks kp
  
  th_to_elab # do
    th_mp_add_e_stat name_loc ks
    th_mp_add_p name_loc kp
  
  cmd_add_proof name (len ps) s
  
  -- ifm (name /= -1) # do
  --   s <- show_reconstruct_proof name
  --   modify_th # \th -> th
  --     {_th_mp_p_sctr = map_insert name s # _th_mp_p_sctr th}

cmd_proof :: Maybe Id -> [Attrib] -> [Name] -> Stat ->
  Maybe (Either Proof K.Proof) -> Prop -> Elab ()
cmd_proof mname attrs ps s p is_local = do
  -- tracem mname
  let (has_name, name) = maybe_to_pair mname
  let if_name = \m -> ite has_name m nop
  if_name # th_to_elab # th_assert_no_id name
  th <- get_kth
  let ps' = list_to_map_inv ps
  
  case p of
    Nothing -> if_name # cmd_add_axiom name ps s
    Just p -> do
      p <- case p of
        Left p -> do
          p <- res_proof p
          -- ifmn (e_res p) # tracem p
          return # Left p
        Right kp -> pure # Right kp
      
      p_ws
      rest <- p_rest
      ifmn (null rest) #
        err [dfsc "\n\n", ("lit_str", rest)]
      
      case p of
        Left p -> do
          let goals = proof_mvars_to_goals p
          ifmn (null goals) # display_goals goals
          -- eqs <- gets_ctx _ctx_eqs_on_hold
          -- flip mapM_ (concat # Map.elems eqs) # \(lhs, rhs) -> do
          --   s1 <- res_show_term lhs
          --   s2 <- res_show_term rhs
          --   tracem # Lit # map snd s1 ++ [" === "] ++ map snd s2
          
          ifmn (e_res p) # do
            s1 <- res_show_proof p
            err # dfsc "Meta variables in proof:\n\n" : s1
        _ -> nop
      
      -- s2 <- show_proof p
      -- raise s2
      
      st <- get
      let name' = ite has_name name (-1)
      cmd_add_thm name' ps s p False --is_local
      ifmn has_name # put st
      
      -- put_ctx # init_ctx 0
      -- s <- with_pp_raw # show_proof p
      -- raise s
  
  [name', fn_name, name0] <- if not # elem AttribExpand attrs
    then pure [name, undefined, name] else do
      name <- id_to_name name
      fn_name <- get_expand_fn_name name
      let name' = drop (length fn_name + 1) name
      let name0 = mk_priv_name_aux1 False fn_name name'
      mapM name_to_id [name', fn_name, name0]

  if_name # flip mapM_ attrs # \attr -> case attr of
    AttribExpand -> add_expand_thm' fn_name name name'
    AttribUnfold -> add_unfold_thm name
    AttribSimp -> do
      name1 <- mk_priv_name namei_simp name0
      add_simp_thm' name0 name1 True
    AttribComp -> do
      name1 <- mk_priv_name namei_simp name0
      add_simp_thm' name0 name1 True
    AttribTc -> do
      name1 <- mk_priv_name namei_tc name0
      add_tc_thm' name0 name1 True
    AttribScheme sch -> add_scheme sch name
    _ -> nop

cmd_axiom :: Id -> [Attrib] -> [Name] -> Stat -> Prop -> Elab ()
cmd_axiom name attrs ps s = cmd_proof (Just name) attrs ps s Nothing

cmd_thm' :: Maybe Id -> [Attrib] -> [Name] -> Stat ->
  Either Proof K.Proof -> Prop -> Elab ()
cmd_thm' name attrs ps s p = cmd_proof name attrs ps s (Just p)

cmd_thm :: Id -> [Attrib] -> [Name] -> Stat ->
  Either Proof K.Proof -> Prop -> Elab ()
cmd_thm name = cmd_thm' # Just name

-- cmd_symbol :: Id -> String -> Elab ()
-- cmd_symbol name sym = th_to_elab # th_add_const name sym
-- 
-- cmd_prefix :: Id -> String -> Rel -> Elab ()
-- cmd_prefix name sym rel = th_to_elab # th_add_op Prefix name sym rel
-- 
-- cmd_infixl :: Id -> String -> Rel -> Elab ()
-- cmd_infixl name sym rel = th_to_elab # th_add_op (Infix # Just 0) name sym rel
-- 
-- cmd_infixr :: Id -> String -> Rel -> Elab ()
-- cmd_infixr name sym rel  = th_to_elab # th_add_op (Infix # Just 1) name sym rel
-- 
-- -- cmd_infix :: Id -> String -> Rel -> Elab ()
-- -- cmd_infix = th_to_elab # th_add_op # Infix Nothing
-- 
-- cmd_binder :: Id -> String -> Rel -> Elab ()
-- cmd_binder name sym rel = th_to_elab # th_add_op Binder name sym rel

cmd_show' :: Either Id Id -> Elab Sctr
cmd_show' name = do
  name_loc <- th_to_elab # th_localize_name # from_either_same name
  th <- get_kth
  case name of
    Left name -> do
      def <- k_to_elab # K.th_get_def name_loc
      let s_ts = mk_type_params_sctr # K._def_params_num def
      t <- th_to_elab # ktype_to_type # K._def_type def
      s1 <- ctx_show t
      msg <- do
        -- name' <- mk_def_thm_name name
        -- thm_name_loc <- th_to_elab # th_localize_name name'
        
        res <- try # do
          Just i <- get_priv_name' namei_def name
          k_to_elab # K.th_get_thm i
        
        case res of
          Just thm -> do
            let s_ts = mk_type_params_sctr # K._thm_params_num thm
            let stat = K._thm_stat thm
            let ke = stat
            e <- th_to_elab # kterm_to_term ke
            e <- ctx_term_part_simple e [1, 0, 0, 1, 1]
            e <- pure # lift_term (-1) e
            se <- ctx_show e
            name <- id_to_name name
            return # [("keyword", "def "), ("cmd_term", name)] ++ s_ts ++ [dfsc " : "] ++
              s1 ++ [dfsc " :=\n"] ++ se
          Nothing -> do
            -- name' <- mk_desc_thm_name name
            -- thm_name_loc <- th_to_elab # th_localize_name name'
            -- res <- try # k_to_elab # K.th_get_thm thm_name_loc
            
            res <- try # do
              Just i <- get_priv_name' namei_desc name
              k_to_elab # K.th_get_thm i
            
            case res of
              Just thm -> do
                let s_ts = mk_type_params_sctr # K._thm_params_num thm
                let stat = K._thm_stat thm
                let ke = stat
                e <- th_to_elab # kterm_to_term ke
                e <- ctx_term_part_simple e [1, 0, 1]
                e <- pure # lift_term (-1) e
                se <- ctx_show e
                name <- id_to_name name
                return # [("keyword", "def "), ("cmd_term", name)] ++ s_ts ++ [dfsc " : "] ++
                  s1 ++ [dfsc " |\n"] ++ se
              Nothing -> do
                name <- id_to_name name
                return # [("keyword", "const "), ("cmd_term", name)] ++ s_ts ++ [dfsc " : "] ++ s1
      return msg
    Right name -> do
      thm <- k_to_elab # K.th_get_thm name_loc
      let s_ts = mk_type_params_sctr # K._thm_params_num thm
      s <- th_to_elab # kterm_to_term # K._thm_stat thm
      s1 <- ctx_show s
      s1 <- case K._thm_proof thm of
        Nothing -> do
          let kw = ite (null # K._thm_axioms thm) "thm" "axiom"
          -- let kw = "thm"
          name <- id_to_name name
          return # [("keyword", kw), dfsc " ", ("cmd_proof", name)] ++ s_ts ++ [dfsc " : "] ++ s1
        Just p -> do
          s2 <- do
            -- p <- th_to_elab # kproof_to_proof p
            -- show_proof p
            show_reconstruct_proof name
          
          name <- id_to_name name
          return # concat
            [ [("keyword", "thm "), ("cmd_proof", name)] ++ s_ts ++ [dfsc " : "], s1
            , [dfsc " :=\n"], s2
            ]
      -- s2 <- show_thm_axioms' thm
      return # concat
        [ s1
        -- , [dfsc "\n\n"], s2
        ]

cmd_show :: Either Id Id -> Elab ()
cmd_show i = cmd_show' i >>= raise

show_thm_axioms' :: K.Thm -> Elab Sctr
show_thm_axioms' thm = case Set.toList # K._thm_axioms thm of
  [] -> pure [dfsc "No axioms used"]
  names -> do
    names <- mapM id_to_name names
    return # (dfsc "Axioms used:" :) # do
      name <- names
      let scope = ite (name == "sorry") "sorry" "proof"
      [dfsc "\n", dfsc # indent' 1, (scope, name)]

show_thm_axioms :: Id -> Elab Sctr
show_thm_axioms name = do
  res <- try # k_to_elab # K.th_get_thm name
  case res of
    Just thm -> show_thm_axioms' thm
    Nothing -> do
      name <- id_to_name name
      err [dfsc "Undefined theorem or axiom ", ("proof", name)]

cmd_show_axioms :: Id -> Elab ()
cmd_show_axioms name = do
  s1 <- show_thm_axioms name
  raise s1

-- show_prefix_fn :: Id -> (TheoryT -> Map Id a) -> Name -> Elab Sctr
-- show_prefix_fn name f scope = do
--   name <- id_to_name name
--   name <- pure # snoc name usc'
--   let name' = usc' : name
--   names <- gets_th # Map.keysSet . f
--   return # map (mk_pair scope) # Set.toList # Set.filter <~ names # \name1 ->
--     starts_with name name1 || starts_with name' name1
-- 
-- show_prefix :: Id -> Elab Sctr
-- show_prefix name = do
--   names1 <- show_prefix_fn name _th_terms "term"
--   names2 <- show_prefix_fn name _th_thms "proof"
--   return # intersperse (dfsc "\n") # names1 ++ names2
-- 
-- cmd_show_prefix :: Id -> Elab ()
-- cmd_show_prefix name = do
--   s1 <- show_prefix name
--   raise s1

-- enable_simp :: Elab ()
-- enable_simp = do
--   th <- get_th
--   let res = _th_elab_res th
--   put_th # th
--     { _th_elab_res = res {_res_simp_enabled = True}
--     , _th_simp_enabled = True
--     }

-- enable_tc :: Elab ()
-- enable_tc = do
--   th <- get_th
--   let res = _th_elab_res th
--   put_th # th
--     { _th_elab_res = res {_res_tc_enabled = True}
--     , _th_tc_enabled = True
--     }

add_expand_thm' :: Id -> Id -> Id -> Elab ()
add_expand_thm' fn_name thm_name name = do
  res <- get_thm' thm_name
  case res of
    Nothing -> do
      thm_name <- id_to_name thm_name
      kw_err "add_expand_thm" [("proof", thm_name)]
    Just ref -> do
      thm_name' <- mk_priv_name_no_usc fn_name name
      let pn = _thm_ps_num ref
      let ps = mk_list pn mk_type_var
      let t_names = map pure # take (fi pn) greek_letters_low
      ctx_set_types t_names
      
      ctx_init' # _thm_stat ref
      tac_norm_goal
      s <- ctx_get_goal_stat
      s <- res_term s
      
      ctx_init' s
      p <- ctx_mk_proof_ref' thm_name ps
      i <- tac_have (Just "h") p
      i <- tac_norm_asm i
      p <- ctx_mk_proof_var i
      tac_exact p
      
      -- is_local <- th_to_elab # do
      --   xs <- mapM th_name_is_local [fn_name, thm_name, name]
      --   return # or xs
      
      p <- ctx_pst_get_proof
      cmd_add_thm thm_name' t_names s (Left p) False --is_local
      
      th_to_elab # th_add_id_rel thm_name namei_expand thm_name'
      -- th_to_elab # modify # \th -> th
      --   { _th_mp_p_expand = bimap_insert thm_name thm_name' #
      --     _th_mp_p_expand th
      --   }
      
      ifm (fn_name == namei_symm) # do
        ctx_init
        ctx_try' # add_comm_thm name
      
      -- ifm (fn_name == namei_comm) # th_to_elab #
      --   th_add_id_rel name namei_comm thm_name'

add_expand_thm :: Id -> Id -> Elab ()
add_expand_thm fn_name thm_name = do
  name <- do
    fn_name <- id_to_name fn_name
    thm_name <- id_to_name thm_name
    register_name # drop (length fn_name + 1) thm_name
  add_expand_thm' fn_name thm_name name

add_simp_thm' :: Id -> Id -> Prop -> Elab ()
add_simp_thm' name name' add_simp = do
  res <- get_thm' name
  case res of
    Nothing -> do
      name <- id_to_name name
      kw_err "add_simp_thm" [("proof", name)]
    Just ref -> do
      -- local <- th_to_elab # th_name_is_local name
      ctx_init
      
      let p_info = Left name'
      
      let pn = _thm_ps_num ref
      let t_names = map pure # take (fi pn) greek_letters_low
      ctx_set_types t_names
      
      let ps = mk_list pn mk_type_var
      ts <- ctx_mk_type_mvars pn
      p <- ctx_mk_proof_ref' name ts
      
      -----
      
      (pat, info) <- rw_norm_pat True p_info p
      p <- pure # fromJust # _rw_proof info
      
      ifm add_simp # do
        info <- pure # info {_rw_proof = Nothing}
        th_to_elab # th_add_simp {-local-} False pat info
      
      -----
      
      zipWithM_ add_type_eq ts ps
      p <- res_proof p
      
      -- is_local <- th_to_elab # th_name_is_local name
      
      let s = _proof_stat p
      -- tracem "{ ELAB"
      -- tracem name'
      -- tracem s
      -- tracem p
      -- tracem "ELAB }"
      cmd_add_thm name' t_names s (Left p) False --is_local

add_simp_thm :: Id -> Elab ()
add_simp_thm thm_name = do
  thm_name' <- mk_priv_name namei_simp thm_name
  add_simp_thm' thm_name thm_name' True

add_tc_thm' :: Id -> Id -> Prop -> Elab ()
add_tc_thm' name name' add_tc = do
  res <- get_thm' name
  case res of
    Nothing -> do
      name <- id_to_name name
      kw_err "add_tc_thm" [("proof", name)]
    Just ref -> do
      -- local <- th_to_elab # th_name_is_local name
      ctx_init
      
      let p_info = Left name'
      
      let pn = _thm_ps_num ref
      let t_names = map pure # take (fi pn) greek_letters_low
      ctx_set_types t_names

      let ps = mk_list pn mk_type_var
      ts <- ctx_mk_type_mvars pn
      p <- ctx_mk_proof_ref' name ts
      
      -----
      
      (pat, info) <- rw_norm_pat False p_info p
      Just p <- pure # _rw_proof info
      
      ifm add_tc # do
        info <- pure # info {_rw_proof = Nothing}
        th_to_elab # th_add_tc {-local-} False pat info
      
      -----
      
      zipWithM_ add_type_eq ts ps
      p <- res_proof p
      s <- reduce_term # _proof_stat p

      -- if_dbg # do
      --   ctx_trace_proof_stat p
      --   tracem # e_res s
      --   tracem s
      --   s <- reduce_term s
      --   tracem # e_res s
      --   tracem s
      --   undefined
      
      -- is_local <- th_to_elab # th_name_is_local name
      cmd_add_thm name' t_names s (Left p) False --is_local

add_tc_thm :: Id -> Elab ()
add_tc_thm thm_name = do
  thm_name' <- mk_priv_name namei_tc thm_name
  add_tc_thm' thm_name thm_name' True

add_unfold_thm :: Id -> Elab ()
add_unfold_thm thm_name = do
  ctx_init
  p <- ctx_mk_proof_ref thm_name
  p <- ctx_all_univs_imps_e p
  let (_, [lhs, rhs]) = split_app # _proof_stat p
  name <- ctx_term_head_name lhs
  unf_name <- mk_priv_name namei_unf name
  add_simp_thm' thm_name unf_name False
  
  lvl <- term_calc_level rhs
  ref <- ctx_get_term_ref name
  ref <- pure # ref {_def_level = lvl + 1}
  
  th_to_elab # do
    th_add_unf name ref
    th_replace_id_rel' name namei_unf thm_name

add_comm_thm :: HCS => Id -> Elab ()
add_comm_thm name = do
  thm_name <- get_priv_name namei_symm name
  res <- get_thm' thm_name
  case res of
    Nothing -> do
      name <- id_to_name name
      kw_err "add_comm_thm" [("proof", name)]
    Just ref -> do
      name' <- mk_priv_name_no_usc namei_comm name
      let p_info = Left name'
      
      let pn = _thm_ps_num ref
      let t_names = map pure # take (fi pn) greek_letters_low
      ctx_set_types t_names
      
      let ps = mk_list pn mk_type_var
      p <- ctx_mk_proof_ref' thm_name ps
      
      -----
      
      p1 <- ctx_mk_proof_ref namei_comm_of_symm'
      f <- ctx_mk_term_ref' name ps
      p1 <- ctx_univ_e p1 f
      p <- ctx_imp_e p1 p
      
      -----
      
      -- is_local <- th_to_elab # th_name_is_local name
      p <- res_proof p
      
      let s = _proof_stat p
      cmd_add_thm name' t_names s (Left p) False --is_local
      
      -- th_to_elab # do
      --   mp <- gets _th_mp_id_name
      --   Just name <- pure # bimap_get name mp
      --   Just name_symm <- pure # bimap_get thm_name mp
      --   Just name_comm <- pure # bimap_get name' mp
      --   print (name, name_symm, name_comm)
      
      -- th_to_elab # do
      --   th_add_id_rel name namei_symm thm_name
      --   th_add_id_rel name namei_comm name'

scheme_ind_get_info' :: Stat -> StateT (Maybe (Either Id Id)) Elab ()
scheme_ind_get_info' s = case term_to_univ' s of
  Just (_, s) -> scheme_ind_get_info' s
  Nothing -> case term_head s of
    Nothing -> nop
    Just (name, args) -> ifm (name == namei_imp) # do
      [s1, s2] <- pure args
      case term_head s1 of
        Just (name_tc, args_tc) ->
          if name_tc == namei_mem then do
            [_, e] <- pure args_tc
            case term_head_name e of
              Nothing -> nop
              Just name -> put # Just # Left name
          else do put # Just # Right name_tc
        _ -> nop
      scheme_ind_get_info' s2
      -- "mem" -> do
      --   [_, e] <- pure args
      --   name <- lift # ctx_term_head_name e
      --   put # Just # Left name
      -- _ -> nop --put # Just # Left name

scheme_ind_get_info :: Stat -> Elab (Either Id Id)
scheme_ind_get_info s = do
  res <- ctx_try # execStateT (scheme_ind_get_info' s) Nothing
  case res of
    Right (Just res) -> pure res
    -- Right Nothing -> do
    _ -> do
      s1 <- show_term s
      err #
        [ dfsc "The following statement is not a valid induction scheme:\n\n"
        , dfsc # indent' 1 ] ++ s1
    -- Left msg -> do
    --   raise msg

add_scheme :: SchemeType -> Id -> Elab ()
add_scheme sch thm_name = do
  ref <- ctx_get_proof_ref thm_name
  let s = _thm_stat ref
  case sch of
    SchemeInd -> do
      info <- scheme_ind_get_info s
      case info of
        Left name_tc -> th_modify_schemes # \s ->
          s {_schemes_ind = map_insert name_tc thm_name # _schemes_ind s}
        Right name -> th_modify_schemes # \s ->
          s {_schemes_ind_tc = map_insert name thm_name # _schemes_ind_tc s}
    SchemeCs -> do
      info <- scheme_ind_get_info s
      case info of
        Left name_tc -> th_modify_schemes # \s ->
          s {_schemes_cs = map_insert name_tc thm_name # _schemes_cs s}
        Right name -> th_modify_schemes # \s ->
          s {_schemes_cs_tc = map_insert name thm_name # _schemes_cs_tc s}

cmd_init_simp :: [Id] -> Elab ()
cmd_init_simp names = do
  -- enable_simp
  mapM_ add_simp_thm names

cmd_init_tc :: [Id] -> Elab ()
cmd_init_tc names = do
  -- enable_tc
  mapM_ add_tc_thm names

cmd_init_comm :: [Id] -> Elab ()
cmd_init_comm = mapM_ add_comm_thm

-- cmd_snippet :: Elab ()
-- cmd_snippet = nop

-- cmd_pp_raw :: Elab ()
-- cmd_pp_raw = put_pp pp_raw

-- parse_cmd_tau_arg_group :: Elab [(Name, [Name], Type)]
-- parse_cmd_tau_arg_group = p_parens # do
--   names <- p_all parse_name
--   p_tok ":"
--   t <- parse_type
--   return # flip map names # \name -> (name, [], t)

-- parse_cmd_tau_arg :: Elab [(Name, [Name], Type)]
-- parse_cmd_tau_arg = parse_cmd_tau_arg_group <++ do
--   (name, ps) <- parse_term_name_and_params
--   t <- ctx_mk_type_mvar
--   return [(name, ps, t)]

-- parse_cmd_tau_args :: Elab [(Name, [Name], Type)]
-- parse_cmd_tau_args = do
--   args_list <- p_all parse_cmd_tau_arg
--   return # concat args_list

-- cmd_tau_aux :: [Name] -> Term -> [(Name, Type, Bit)] -> [(Name, Type)] -> Elab ()
-- cmd_tau_aux ps f args loc_args0 = case args of
--   [] -> nop
--   ((name, t, b) : args) -> do
--     loc_args <- pure # case b of
--       0 -> loc_args0
--       1 -> tail loc_args0
--     let loc_args_num = len loc_args0
--     s <- with_new_ctx # do
--       -- mapM_ (uncurry ctx_enter_lam) loc_args
--       -- d <- ctx_term_depth
--       let d = loc_args_num
--       s <- pure # lift_term_to d f
--       s <- pure # mk_apps_safe # (s:) #
--         flip mapi loc_args0 # \i (_, t) ->
--           mk_term_var d t i
--       s <- pure # foldr mk_exi s # map snd loc_args
--       -- show_term s >>= err . pure
--       return s
--     nop
--     -- s1 <- show_term f
--     -- s2 <- show_type # _term_type f
--     -- err [s1, "\n", s2]

-- parse_cmd_tau :: Elab ()
-- parse_cmd_tau = do
--   parse_keyword "tau"
--   args <- parse_cmd_tau_args
--   args <- pure args ++> do
--     p_tok ":"
--     t <- parse_type
--     case args of
--       [(name, ps, _)] -> pure [(name, ps, t)]
--       _ -> pfail
--   p_tok ":="
--   let names = map (\(name, _, _) -> name) args
--   case list_find_dup names of
--     Nothing -> nop
--     Just name -> err ["Duplicate name ", quote name]
--   ps <- case args of
--     [(_, ps, _)] -> pure ps
--     _ -> case find (\(_, ps, _) -> not # null ps) args of
--       Nothing -> pure []
--       Just (name, _, _) -> err
--         [ "Definition ", quote name, " cannot have type parameters"
--         , " if there are multiple arguments to command `tau`" ]
--   let ps_num = len ps
--   args <- pure # map (\(name, _, t) -> (name, t)) args
--   args <- flip mapM args # \(name, t) -> do
--     mp <- gets_th _th_terms
--     case map_get name mp of
--       Nothing -> pure (name, t, 0)
--       Just ref -> do
--         if _term_ref_ps_num ref == ps_num then nop else
--           err ["ps_num"]
--         let t0 = _term_ref_type ref
--         add_type_eq t0 t
--         return (name, t0, 1)
--   cnd <- with_new_ctx # do
--     flip mapM_ args # \(name, t, b) -> ctx_enter_lam name t
--     s <- parse_stat
--     res_term s
--   args <- flip mapM args # \(name, t, b) -> do
--     t <- res_type t
--     if e_res t then nop else err ["not e_res ", quote name]
--     return (name, t, b)
--   cnd <- pure # mk_lams (map (\(_, t, _) -> t) args) cnd
--   let ((name0, _, _) : _) = args
--   tau_name0 <- elab_get_avail_name # mk_priv_name "tau" name0
--   cmd_def tau_name0 ps cnd
--
--   nop
--   -- let ts = mk_list ps_num mk_type_var
--   -- f <- ctx_mk_term_ref' tau_name0 ts
--   -- cmd_tau_aux ps f args

parse_cmd :: Cmd ()
parse_cmd = parse_comment <++ do
  attrs <- parse_attrs
  name <- p_scope "keyword" parse_name

  -- db <- gets_th _th_dbg
  -- s <- p_query p_rest
  -- tracem ("cmd", name, db, s)

  case map_get name cmd_parsers of
    Nothing -> case name of
      "import" -> err
        [ dfsc "Command ", ("keyword", name)
        , dfsc " can only be used at the beginning of the file" ]
      _ -> err [dfsc "Unknown command ", ("keyword", name)]
    Just cmd -> cmd attrs

cmd_parsers :: Map Name ([Attrib] -> Cmd ())
cmd_parsers = Map.fromList
  [ ("dbg", parse_cmd_dbg)
  , ("const", parse_cmd_const)
  , ("def", parse_cmd_def)
  , ("axiom", parse_cmd_axiom)
  , ("thm", parse_cmd_thm)
  , ("example", parse_cmd_example)
  -- , ("symbol", parse_cmd_symbol)
  -- , ("prefix", parse_cmd_prefix)
  -- , ("infixl", parse_cmd_infixl)
  -- , ("infixr", parse_cmd_infixr)
  -- -- , ("infix", parse_cmd_infix)
  -- , ("binder", parse_cmd_binder)
  -- , ("add_simp", parse_cmd_add_simp)
  -- , ("tau", parse_cmd_tau)
  , ("init_simp", parse_cmd_init_simp)
  , ("init_tc", parse_cmd_init_tc)
  , ("init_comm", parse_cmd_init_comm)
  -- , ("snippet", parse_cmd_snippet)
  , ("show", parse_cmd_show)
  , ("show_axioms", parse_cmd_show_axioms)
  -- , ("show_prefix", parse_cmd_show_prefix)
  -- , ("pp_raw", parse_cmd_pp_raw)
  , ("rct", parse_cmd_reconstruct)
  ]

parse_and_run_cmd :: String -> Theory Sctr
parse_and_run_cmd str = do
  -- tracem # Lit [str]
  (_, scope) <- th_parse parse_cmd str
  () <- gets _th_kth >>= pure . K.th_ok
  return # reverse # map (fmap reverse) scope

-- parse_import :: Elab FilePath
-- parse_import = do
--   parse_keyword "import"
--   p_scope "lit_str" #
--     p_take_while1 # \c -> c /= '\r' && c /= '\n'
-- 
-- parse_imports :: Elab [FilePath]
-- parse_imports = p_sepf' parse_import p_new_line

ctx_show_ident :: Name -> Elab Sctr
ctx_show_ident name = ctx_query # do
  i <- name_to_id name
  
  h <- has_thm i
  ifm h # do
    index <- id_to_index i
    modify_th # \th -> th {_th_sim_index = Just # index + 1}
  
  res <- get_def' i
  if isJust res then cmd_show' # Left i else do
    has_thm i >>= asrt
    cmd_show' # Right i

instance CtxShow ThIdent where
  ctx_show (ThIdent name) = ctx_show_ident name