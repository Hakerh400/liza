module Elaborator.Tactic where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Maybe
import Data.Either
import Data.List
import Data.Char
import Control.Monad
import Control.Monad.State
import Prelude hiding (print, putStrLn)

-- import Util hiding ((<|>))
import Util
import Sctr
import Bit
import Tree
import Trie
import Bimap
import Parser

import Elaborator.Data
import Elaborator.Util
import Elaborator.Parser
import Elaborator.Type
import Elaborator.Term
import Elaborator.Goal
import Elaborator.Proof
import Elaborator.Reconstruct

import qualified Kernel as K

trie_m_lift = lift
trie_m_fail = fail ""

-- (<|>) = ctx_or'

parse_tac_name :: HCS => Tactic Name
parse_tac_name = p_scope "tactic" parse_name

parse_tac_word :: HCS => Name -> Tactic ()
parse_tac_word name = p_scope "tactic" # parse_word name

parse_proof_tac_block :: HCS => Stat -> Elab Proof
parse_proof_tac_block s = do
  ctx <- get_ctx
  i <- elab_mk_goal_id
  ctx <- pure # ctx
    { _ctx_id = i
    , _ctx_goal_stat = Just s
    }
  pst <- get_pst
  put_pst # init_pst ctx
  itac_block
  p <- ctx_pst_get_proof
  put_pst pst
  return p

tac_parse_term :: HCS => Elab Term
tac_parse_term = do
  ctx_set_term_parser_state 0
  e <- parse_term
  res_term e

tac_parse_term_1 :: HCS => Elab Term
tac_parse_term_1 = do
  ctx_set_term_parser_state 0
  e <- parse_term_1
  res_term e

tac_parse_stat :: HCS => Elab Stat
tac_parse_stat = do
  ctx_set_term_parser_state 0
  s <- parse_term
  add_type_eq mk_prop_t # _term_type s
  res_term s

tac_parse_proof_aux :: HCS => Stat -> Elab Proof
tac_parse_proof_aux s = do
  p <- parse_proof_aux s
  let i = _proof_target_asm p
  p <- tac_res_proof_tc p <|> res_proof p
  return # p {_proof_target_asm = i}

tac_parse_proof_aux' :: HCS => Elab Proof
tac_parse_proof_aux' = do
  s <- ctx_mk_stat_mvar
  tac_parse_proof_aux s

tac_parse_proof :: HCS => Stat -> Elab Proof
tac_parse_proof = tac_parse_proof_aux

tac_parse_proof' :: HCS => Elab Proof
tac_parse_proof' = tac_parse_proof_aux'

tac_parse_proof_1 :: HCS => Stat -> Elab Proof
tac_parse_proof_1 s = do
  p <- parse_proof_1 s
  res_proof p

tac_parse_proof_1' :: HCS => Elab Proof
tac_parse_proof_1' = do
  s <- ctx_mk_stat_mvar
  tac_parse_proof_1 s

tac_parse_at_1 :: HCS => Elab (Maybe N)
tac_parse_at_1 = choice
  [ p_tok "⊢" >> pure Nothing
  , parse_proof_index >>= liftm Just
  ]

tac_parse_at_aux :: HCS => Prop -> Elab [Maybe N]
tac_parse_at_aux p_at = pure [Nothing] ++> do
  ifm p_at # parse_tac_word "at"
  p_all tac_parse_at_1 ++> do
    p_tok "*"
    mp <- gets_ctx _ctx_proofs
    return # Nothing : map Just (Map.keys mp)

tac_parse_at :: HCS => Elab [Maybe N]
tac_parse_at = tac_parse_at_aux True

tac_parse_at' :: HCS => Elab [Maybe N]
tac_parse_at' = tac_parse_at_aux False

tac_parse_with :: HCS => Elab [Name]
tac_parse_with = pure [] ++> do
  -- p_tok sym_arrow_right
  parse_tac_word "as"
  p_all parse_term_var_name

tac_parse_with_1' :: HCS => Elab (Maybe Name)
tac_parse_with_1' = do
  xs <- tac_parse_with
  case xs of
    [] -> pure Nothing
    [x] -> pure # Just x
    _ -> tac_err' "tac_parse_with_1'"

tac_parse_with_1 :: HCS => Elab Name
tac_parse_with_1 = do
  Just name <- tac_parse_with_1'
  return name

-----

tac_show_type :: HCS => Type -> Elab a
tac_show_type t = do
  s1 <- show_type t
  err s1

tac_show_term :: HCS => Term -> Elab a
tac_show_term e = do
  s1 <- show_term e
  err s1

tac_show_proof :: HCS => Proof -> Elab a
tac_show_proof p = tac_show_term # _proof_stat p

-----

-- tac_refine_get_h_id :: HCS => Proof -> Tactic N
-- tac_refine_get_h_id p = case _proof_val p of
--   ProofVar i -> pure i
--   _ -> tac_have p

-- tac_refine_norm_aux :: HCS => Prop -> N -> Tactic N
-- tac_refine_norm_aux last i = do
--   s1 <- ctx_get_proof_var i
--   s2 <- ctx_get_goal_stat
--   (name1, n) <- ctx_term_head_name' last s1 <++ pure ("", -1)
--   name2 <- ctx_term_head_name s2 <++ pure ""

--   get_lvl <- pure # \name ->
--     if null name then pure (-1) else
--       ctx_get_term_level name
  
--   lvl1 <- get_lvl name1
--   lvl2 <- get_lvl name2
  
--   True <- pure # lvl1 >= 0 || lvl2 >= 0

--   -- tracem (name1, name2, lvl1, lvl2)
  
--   if lvl1 >= lvl2 then do
--     Just i <- tac_unfold (Just i) n name1
--     return i
--   else do
--     tac_unfold_goal name2
--     return i

-- tac_refine_norm' :: HCS => Prop -> N -> (Proof -> Tactic a) -> Tactic (a, N)
-- tac_refine_norm' last i tac = do
--   (res, Just i) <- tac_mk_norm (Just i)
--     (\(Just i) -> tac_refine_norm_aux last i >>= liftm Just) #
--     \(Just i) -> do
--       -- s <- ctx_get_proof_var i
--       -- s <- show_term s
--       -- ctx_trace s
--       -- ctx_try ctx_display_goals >>= \(Left s) -> ctx_trace s
--       p <- ctx_mk_proof_var i
--       tac p
--   return (res, i)

-- tac_refine_norm :: HCS => N -> (Proof -> Tactic a) -> Tactic (a, N)
-- tac_refine_norm = tac_refine_norm' False

-- tac_refine_mk_adv :: HCS => Prop -> (Proof -> Tactic a) -> Proof -> Tactic a
-- tac_refine_mk_adv last tac p = ctx_or (tac p) # do
--   i <- tac_refine_get_h_id p
--   (a, _) <- tac_refine_norm' last i tac
--   return a

tac_refine_calc_new_ctxs' :: HCS => Ctx -> Proof -> Tactic [Ctx]
tac_refine_calc_new_ctxs' ctx p = case _proof_val p of
  ProofMvar is_tc _ -> do
    let s = _proof_stat p
    s <- if not is_tc then pure s else
      tac_res_stat_using_asms s
    -- ifmn (e_res s) # do
    --   s1 <- show_term s
    --   tac_err "refine_calc_new_ctxs" # 
    --     [dfsc "Goal statement is not resolved:\n\n"] ++ s1
    i <- elab_mk_goal_id
    return # pure # ctx
      { _ctx_id = i
      , _ctx_is_tc = is_tc
      , _ctx_goal_stat = Just s
      , _ctx_asms_normed = False
      }
  ImpI mn i0 i s p -> do
    ifm (i /= _ctx_proof_depth ctx) #
      tac_err "refine_calc_new_ctxs" #
      pure # dfsc # concat
      [show # _ctx_proof_depth ctx, " ", show i]
    let ctx' = ctx_insert_proof' mn i0 i s ctx
    tac_refine_calc_new_ctxs' ctx' p
  UnivI mn t p -> do
    let ctx' = ctx_enter_lam_aux mn t ctx
    tac_refine_calc_new_ctxs' ctx' p
  ImpE p1 p2 -> do
    xs <- tac_refine_calc_new_ctxs' ctx p1
    ys <- tac_refine_calc_new_ctxs' ctx p2
    return # xs ++ ys
  UnivE p e -> tac_refine_calc_new_ctxs' ctx p
  WithStat p -> error ""
  ProofClear e_ids p_ids p -> do
    let ctx' = ctx_clear_ids' e_ids p_ids ctx
    tac_refine_calc_new_ctxs' ctx' p
  _ -> pure []

tac_refine_calc_new_ctxs :: HCS => Proof -> Tactic (Proof, [Ctx])
tac_refine_calc_new_ctxs p = do
  ctx <- get_ctx
  ctxs <- tac_refine_calc_new_ctxs' ctx p

  asgns <- gets_ctx _ctx_term_asgns
  ctxs <- pure # map <~ ctxs # \ctx ->
    ctx {_ctx_term_asgns = asgns}
  
  -- ifm_dbg # do
  --   tracem # map <~ ctxs # \ctx ->
  --     (_ctx_id ctx, Map.member 205 # _ctx_term_asgns ctx)
  
  p <- res_proof p

  ctx <- get_ctx
  ctxs <- tac_refine_calc_new_ctxs' ctx p

  -- ifm_dbg # do
  --   tracem # map <~ ctxs # \ctx ->
  --     (_ctx_id ctx, Map.member 205 # _ctx_term_asgns ctx)

  return (p, ctxs)

tac_refine :: HCS => Proof -> Tactic ()
tac_refine p = do
  -- ctx_trace_proof_stat p

  s <- ctx_get_goal_stat
  add_term_eq (_proof_stat p) s
  p <- res_proof p
  
  (p, ctxs_new) <- tac_refine_calc_new_ctxs p

  let n = len ctxs_new
  
  modify_pst # \pst -> ProofState
    { _pst_ctxs = ctxs_new ++ tail' (_pst_ctxs pst)
    , _pst_fn = \ps -> let
      (ps', rest) = splitAt (fi n) ps
      p' = proof_subst_proof_mvars_list ps' p
      in _pst_fn pst # p' : rest
    }
  
  is <- pure # map _ctx_id ctxs_new
  tac_work_on_goals is # do
    d <- ctx_term_depth
    s <- ctx_get_goal_stat
    let ds = _term_depth s
    ifm (ds /= d) #
      ctx_put_goal_stat # lift_term_to d s

  is <- pure # map _ctx_id # filter _ctx_is_tc ctxs_new
  tac_work_on_goals is # ctx_try' # do
    -- tracem "<<<"
    tac_tc
    -- tracem ">>>"

tac_exact :: HCS => Proof -> Tactic ()
tac_exact p = do
  s0 <- ctx_get_goal_stat
  let s = _proof_stat p
  
  -- res <- ctx_query # tac_refine p
  -- ifm_dbg # do
  --   s_goal <- ctx_get_goal_stat
  --   ctx_trace_term s
  --   ctx_trace_term s_goal
  --   tracem res
  
  tac_refine p <|> do
    p1 <- proof_cong_norm s s0
    p <- ctx_imp_e p1 p
    tac_refine p

tac_apply :: HCS => Proof -> Tactic ()
tac_apply p = tac_exact p <|> do
  (p, info) <- proof_to_univ_or_imp p
  case info of
    Left (t, s) -> do
      e <- ctx_mk_term_mvar' t
      
      let d = max (_term_depth s) (_term_depth e)
      e <- pure # lift_term_to d e
      
      p <- ctx_univ_e p e
      tac_apply p
    Right (s1, s2) -> do
      p1 <- ctx_mk_proof_mvar s1
      p <- ctx_imp_e p p1
      tac_apply p

tac_imp_i_index_stat :: HCS => Maybe Name -> Maybe N -> N -> Stat -> Stat -> Tactic ()
tac_imp_i_index_stat mn i0 i s1 s2 = do
  p <- ctx_mk_proof_mvar s2
  tac_refine # mk_imp_i_aux mn i0 i s1 p

tac_imp_i_id :: HCS => Maybe Name -> Maybe N -> N -> Tactic ()
tac_imp_i_id mn i0 i = do
  s <- ctx_get_goal_stat
  case term_to_imp s of
    Nothing -> pfail --tac_err' "imp_i_name"
    Just (s1, s2) -> tac_imp_i_index_stat mn i0 i s1 s2

tac_imp_i_aux :: HCS => Maybe Name -> Maybe N -> Tactic N
tac_imp_i_aux mn i0 = do
  i <- ctx_get_avail_proof_id
  tac_imp_i_id mn i0 i
  return i

tac_imp_i_dep :: HCS => Maybe Name -> N -> Tactic N
tac_imp_i_dep mn i0 = tac_imp_i_aux mn # Just i0

tac_imp_i :: HCS => Name -> Tactic N
tac_imp_i name = do
  tac_norm_full_goal
  tac_imp_i_aux (Just name) Nothing

-- tac_imp_i' :: HCS => Maybe Name -> Tactic ()
-- tac_imp_i' mn = tac_imp_i mn >> nop
-- 
-- tac_imps_i_n_aux :: HCS => [Maybe Name] -> Maybe N -> Maybe N -> Tactic N
-- tac_imps_i_n_aux mns dep n = if n == Just 0 || null mns then pure 0 else do
--   s <- ctx_get_goal_stat
--   res <- pure # term_to_imp s
--   case res of
--     Nothing -> pure 0
--     Just (s1, s2) -> do
--       -- let thm_name = "tc_univ_of"
--       -- ref <- ctx_get_proof_ref' thm_name
-- 
--       -- if is_tc && isNothing ref then pure 0 else do
--       --   ifm is_tc # do
--       --     p <- ctx_mk_proof_ref thm_name
--       --     p <- ctx_univs_e p [s1, s2]
--       --     p1 <- ctx_mk_proof_mvar'
--       --     p <- ctx_imp_e p p1
--       --     tac_refine p
--       
--       (mn : mns) <- pure mns
--       i <- ctx_get_avail_proof_id
--       tac_imp_i_index_stat mn dep i s1 s2
-- 
--       n <- tac_imps_i_n_aux mns dep # n >>= \n -> pure # n - 1
--       return # n + 1
-- 
-- tac_imps_i_n' :: HCS => [Maybe Name] -> Maybe N -> Tactic N
-- tac_imps_i_n' mns = tac_imps_i_n_aux mns Nothing
-- 
-- tac_imps_i_n :: HCS => [Maybe Name] -> N -> Tactic N
-- tac_imps_i_n mns n = tac_imps_i_n' mns # Just n
-- 
-- tac_imps_i :: HCS => [Maybe Name] -> Tactic N
-- tac_imps_i mns = tac_imps_i_n' mns Nothing
-- 
-- tac_imps_i' :: HCS => [Maybe Name] -> Tactic ()
-- tac_imps_i' mns = tac_imps_i mns >> nop

tac_univ_i_name_fn :: HCS => Name -> Type -> Term -> Tactic ()
tac_univ_i_name_fn name t f = do
  let Lam _ _ s = _term_val # eta_expand f
  p <- ctx_mk_proof_mvar s
  tac_refine # mk_univ_i' name t p

tac_univ_i :: HCS => Name -> Tactic ()
tac_univ_i name = do
  tac_norm_full_goal
  s <- ctx_get_goal_stat
  case term_to_univ s of
    Nothing -> pfail --tac_err' "univ_i"
    Just (t, f) -> tac_univ_i_name_fn name t f

-- tac_imp_i :: HCS => Name -> Tactic N
-- tac_imp_i name = do
--   s <- ctx_get_goal_stat
--   case term_to_univ s of
--     Nothing -> tac_err' "univ_i"
--     Just (t, f) -> tac_univ_i_name_fn name t f

-- tac_intro_1 :: HCS => Name -> Tactic ()
-- tac_intro_1 name = do
--   tac_univ_i name
--   -- tac_imps_i'
-- 
-- tac_intro :: HCS => [Name] -> Tactic N
-- tac_intro names = do
--   n <- tac_imps_i
--   mapM_ tac_intro_1 names
--   return n
-- 
-- tac_intro' :: HCS => [Name] -> Tactic ()
-- tac_intro' names = tac_intro names >> nop

-- tac_intro_adv_1 :: HCS => Name -> Tactic ()
-- tac_intro_adv_1 name = do
--   tac_norm_full_goal
--   tac_univ_i name <|> tac_imp_i name
--   -- n <- tac_imps_i
--   -- if n /= 0 then tac_intro_adv_1 name else do
--   --   tac_norm_full_goal
--   --   tac_intro_1 name

-- tac_intro_adv' :: HCS => [Name] -> Tactic N
-- tac_intro_adv' names = do
--   tac_norm_full_goal
--   n <- tac_imps_i
--   mapM_ tac_intro_adv_1 names
--   return n
-- 
-- tac_intro_adv :: HCS => [Name] -> Tactic ()
-- tac_intro_adv names = do
--   n <- tac_intro_adv' names
--   ifm (null names && n == 0) # tac_err' "in"
-- 
-- -- tac_intro_take :: HCS => [Name] -> Tactic ()
-- -- tac_intro_take (name : names) = do
-- --   tac_imps_i'
-- --   ctx_try' # do
-- --     tac_intro_1 name
-- --     tac_intro_take names

tac_clear' :: HCS => [LocIdent] -> Tactic ()
tac_clear' idents = do
  s <- ctx_get_goal_stat
  p <- ctx_mk_proof_mvar s
  let (es, ps) = partitionEithers idents
  tac_refine # mk_proof_clear es ps p

tac_clear :: HCS => [LocIdent] -> Tactic ()
tac_clear idents = do
  (deps, gdeps) <- ctx_calc_deps_for_idents idents
  
  -- mp <- gets_ctx _ctx_proofs
  -- mapM_ <~ deps # \i -> do
  --   Just s <- pure # map_get i mp
  --   s1 <- show_term s
  --   ctx_trace s1

  ifmn (null gdeps) # tac_err' "clear"
  tac_clear' # idents ++ map Right deps

tac_clear_1 :: HCS => LocIdent -> Tactic ()
tac_clear_1 ident = tac_clear' [ident]

tac_clear_term_1 :: HCS => N -> Tactic ()
tac_clear_term_1 = tac_clear_1 . Left

tac_clear_asm_1 :: HCS => N -> Tactic ()
tac_clear_asm_1 = tac_clear_1 . Right

tac_revert_1 :: HCS => LocIdent -> Tactic ()
tac_revert_1 ident = do
  s <- ctx_get_goal_stat
  let d = _term_depth s
  case ident of
    Left i -> do
      t <- ctx_get_term_var i
      (_, f) <- ctx_abstract_var s i
      let s' = lam_to_univ f
      p <- ctx_mk_proof_mvar s'
      let x = mk_term_var d t i
      tac_refine # mk_univ_e s p x
    Right i -> do
      Just h <- ctx_get_proof_var' i
      h <- pure # lift_term_to d h
      p1 <- ctx_mk_proof_mvar # mk_imp h s
      let p2 = mk_proof_var h i True
      tac_refine # mk_imp_e s p1 p2
  tac_clear_1 ident

tac_revert_term_1 :: HCS => N -> Tactic ()
tac_revert_term_1 = tac_revert_1 . Left

tac_revert_asm_1 :: HCS => N -> Tactic ()
tac_revert_asm_1 = tac_revert_1 . Right

tac_revert' :: HCS => [LocIdent] -> Tactic ()
tac_revert' = mapM_ tac_revert_1

tac_revert :: HCS => [LocIdent] -> Tactic ()
tac_revert idents = do
  (deps, _) <- ctx_calc_deps_for_idents idents
  idents <- pure # reverse # idents ++ map Right deps
  tac_revert' idents

-- tac_mk_norm :: HCS => Maybe N -> (Maybe N -> Tactic (Maybe N)) ->
--   (Maybe N -> Tactic a) -> Tactic (a, Maybe N)
-- tac_mk_norm i norm_tac tac = ctx_or
--   (tac i >>= \a -> pure (a, i)) # do
--     i <- norm_tac i
--     tac_mk_norm i norm_tac tac

-- tac_mk_norm_asm :: HCS => N -> (N -> Tactic a) -> Tactic (a, N)
-- tac_mk_norm_asm i tac = do
--   (a, Just i) <- tac_mk_norm (Just i)
--     (\(Just i) -> tac_norm_asm i >>= liftm Just) # \i -> do
--       Just i <- pure i
--       tac i
--   return (a, i)

-- tac_mk_norm_goal :: HCS => Tactic a -> Tactic a
-- tac_mk_norm_goal tac = do
--   (a, _) <- tac_mk_norm Nothing
--     (\_ -> tac_norm_goal >> pure Nothing) # const tac
--   return a

tac_at_to_h_num_g :: HCS => Maybe N -> (N, Prop)
tac_at_to_h_num_g at = ite (isJust at) (1, False) (0, True)

tac_at_get_stat :: HCS => Maybe N -> Tactic Stat
tac_at_get_stat at = do
  s <- ctx_get_goal_stat
  case at of
    Nothing -> pure s
    Just _ -> ctx_term_part_simple s [0, 1]

-- tac_at_mk_unf_tac :: HCS => Maybe N -> (Maybe N -> Tactic a) -> Tactic a
-- tac_at_mk_unf_tac at tac = do
--   (a, _) <- tac_mk_norm at <~ tac # \at -> do
--     s <- tac_at_get_stat at
--     name <- ctx_term_head_name s
--     tac_unfold_aux at name 0
--     return at
--   return a

tac_at_i :: HCS => N -> Tactic a -> Tactic (a, N)
tac_at_i i tac = do
  tac_revert_asm_1 i
  res <- tac
  mn <- get_asm_name' i
  i <- tac_imp_i_dep mn i
  return (res, i)

tac_at :: HCS => Maybe N -> (Maybe N -> Tactic a) -> Tactic (a, Maybe N)
tac_at at tac = do
  let tac' = tac at --tac_at_mk_unf_tac at tac
  case at of
    Nothing -> do
      res <- tac'
      return (res, Nothing)
    Just i -> do
      (res, i) <- tac_at_i i tac'
      return (res, Just i)

tac_at' :: HCS => Maybe N -> (Maybe N -> Tactic ()) -> Tactic (Maybe N)
tac_at' at tac = do
  (_, at) <- tac_at at tac
  return at

tac_at_goal :: HCS => Tactic a -> Tactic a
tac_at_goal tac = do
  (res, _) <- tac_at Nothing # const tac
  return res

tac_ats :: HCS => [Maybe N] -> (Maybe N -> Tactic a) -> Tactic ()
tac_ats ats tac = mapM_ tac ats

tac_norm_aux :: HCS => Maybe N -> Tactic ()
tac_norm_aux at = do
  s <- tac_at_get_stat at
  name <- ctx_term_head_name s
  tac_unfold_aux at name 0

-- tac_norm_asm' :: HCS => N -> Tactic ()
-- tac_norm_asm' i = tac_norm_aux # Just i

-- tac_norm_goal' :: HCS => Tactic ()
-- tac_norm_goal' = tac_norm_aux Nothing

tac_norm :: HCS => Maybe N -> Tactic (Maybe N)
tac_norm at = do
  (_, i) <- tac_at at tac_norm_aux
  return i

tac_norm_asm :: HCS => N -> Tactic N
tac_norm_asm i = do
  Just i <- tac_norm # Just i
  return i

tac_norm_goal :: HCS => Tactic ()
tac_norm_goal = tac_norm Nothing >> nop

tac_norm_full_aux :: HCS => Maybe N -> Tactic ()
tac_norm_full_aux at = ctx_repeat0 # tac_norm_aux at

tac_norm_full :: HCS => Maybe N -> Tactic (Maybe N)
tac_norm_full at = do
  (_, i) <- tac_at at tac_norm_full_aux
  return i

tac_norm_full_asm :: HCS => N -> Tactic N
tac_norm_full_asm i = do
  Just i <- tac_norm_full # Just i
  return i

tac_norm_full_goal :: HCS => Tactic ()
tac_norm_full_goal = tac_norm_full Nothing >> nop

rw_norm_pat_check_arg :: HCS => Term -> Prop
rw_norm_pat_check_arg e = let
  k = _term_depth e - 1
  in case _term_val e of
    App a b -> case _term_val a of
      TermVar i -> i == k && not (term_has_term_var k b)
      _ -> False
    _ -> False

rw_norm_pat' :: HCS => Prop -> ProofInfo -> Proof -> [Type] -> Tactic (Pat, RwInfo)
rw_norm_pat' is_simp p_info p ts = do
  let s = _proof_stat p
  d <- ctx_term_depth
  
  mk_result <- pure # \e p -> do
    -- p <- res_proof p
    ts <- pure # reverse ts

    -- tracem # Set.toList # get_type_mvars_in_proof p
    -- tracem p
    -- ctx_trace_proof_stat p
    
    terms_i <- mapM (const ctx_mk_mvar) ts
    terms_e <- zipWithM ctx_mk_term_mvar_aux ts terms_i
    
    e <- pure # fold_r terms_e e # \arg e -> lift_term (-1) #
      subst_term_var_in_term (_term_depth e - 1) arg e
    
    -- types_raw <- pure # Set.toList # get_type_mvars_in_proof p
    -- types_i <- mapM (const ctx_mk_mvar) types_raw
    -- types_t <- mapM ctx_mk_type_mvar_aux types_i

    -- e <- pure # (\f -> foldr f e # zip types_raw types_t) #
    --   \(i, t) e -> subst_type_mvar_in_term i t e
    
    -- ifm_dbg # do
    --   tracem "dbg"
    --   tracem types_i
    --   ctx_trace_proof_stat p
    --   tracem # Set.toList # get_type_mvars_in_proof p

    let types = Set.toList # get_type_mvars_in_term e
    -- tracem "{"
    -- ctx_trace_term e
    -- tracem e
    -- tracem "}"
    let pat = term_to_pat e

    return # mk_pair pat # RwInfo
      { _rw_proof_info = p_info
      , _rw_proof = Just p
      , _rw_types = types
      , _rw_terms = terms_i
      }
  
  subst_of' <- pure # \s -> do
    p1 <- ctx_mk_proof_ref namei_subst_of'
    p1 <- ctx_all_univs_e p1
    p <- ctx_imp_e p1 p
    mk_result s p
  
  subst_of <- pure # subst_of' s

  check_subst <- pure # \lhs rhs m ->
    ite (terms_eq lhs rhs) subst_of m
  
  dflt <- pure # if not is_simp then mk_result s p else do
    let (target, args) = split_app s
    case _term_val target of
      TermRef name ps ->
        if name == namei_eq then do
          let [lhs, rhs] = args
          case _term_val lhs of
            Lam _ t e -> do
              p1 <- ctx_mk_proof_ref namei_fn_arg
              p1 <- ctx_univs_e p1 [lhs, rhs]
              p <- ctx_imp_e p1 p
              rw_norm_pat' is_simp p_info p ts
            _ -> check_subst lhs rhs # do
              p1 <- ctx_mk_proof_ref namei_eq_subst'
              p1 <- ctx_univs_e p1 [lhs, rhs]
              p <- ctx_imp_e p1 p
              mk_result lhs p
        else if name == namei_iff then do
          let [lhs, rhs] = args
          check_subst lhs rhs # do
            p1 <- ctx_mk_proof_ref namei_iff_subst'
            p1 <- ctx_univs_e p1 [lhs, rhs]
            p <- ctx_imp_e p1 p
            mk_result lhs p
        else if name == namei_not then do
          let [lhs] = args
          rhs <- pure # mk_term_ref (_term_depth lhs)
            mk_prop_t namei_false []
          check_subst lhs rhs # do
            p1 <- ctx_mk_proof_ref namei_not_subst'
            p1 <- ctx_all_univs_e p1
            p <- ctx_imp_e p1 p
            mk_result lhs p
        else subst_of
      _ -> do
        let lhs = s
        rhs <- pure # mk_term_ref (_term_depth lhs)
          mk_prop_t namei_true []
        check_subst lhs rhs # subst_of' lhs

  case term_to_univ' s of
    Nothing -> do
      fn <- pure # case term_head s of
        Just (name, [c, s]) -> if name /= namei_imp then dflt else do
          dp <- gets_ctx _ctx_proof_depth
          (pat, info) <- with_new_ctx # do
            -- p1 <- ctx_mk_proof_ref "of_tc_univ"
            -- p1 <- ctx_univs_e p1 [c, s]
            -- p <- ctx_imp_e p1 p
            ctx_enter_proof_lam Nothing dp c
            p1 <- ctx_mk_proof_var dp
            p <- ctx_imp_e p p1
            rw_norm_pat' is_simp p_info p ts
          Just p <- pure # _rw_proof info
          p <- pure # mk_imp_i_aux Nothing Nothing dp c p
          
          -- p1 <- ctx_mk_proof_ref "tc_univ_of"
          -- p1 <- ctx_univs_e p1 [c, s]
          -- p <- ctx_imp_e p1 p
          
          return # mk_pair pat # info
            { _rw_proof = Just p
            -- , _rw_tcs = c : _rw_tcs info
            }
        _ -> dflt
      
      -- Temporary hack
      h <- gets_th # trie_nnull . _th_tcs
      if h then fn else dflt
    Just (t, e) -> do
      dflt_univ <- pure # do
        (pat, info) <- with_new_ctx # do
          ctx_enter_lam' Nothing t
          e <- ctx_mk_term_var' t d
          p <- ctx_univ_e p e
          rw_norm_pat' is_simp p_info p # t : ts
        Just p <- pure # _rw_proof info
        p <- pure # mk_univ_i_aux Nothing t p
        return (pat, info {_rw_proof = Just p})
      
      let (target, args) = split_app e
      case _term_val target of
        TermRef name _ -> if name /= namei_imp then dflt_univ else
          if not # all rw_norm_pat_check_arg args
            then dflt_univ else do
              let [arg1, arg2] = args
              App _ lhs <- pure # _term_val arg2
              App _ rhs <- pure # _term_val arg1
              lhs <- res_term # lift_term (-1) lhs
              case _term_val lhs of
                Lam _ _ _ -> do
                  p1 <- ctx_mk_proof_ref namei_fn_arg_subst
                  p1 <- ctx_univs_e p1 [rhs, lhs]
                  p <- ctx_imp_e p1 p
                  rw_norm_pat' is_simp p_info p ts
                _ -> mk_result lhs p
        _ -> dflt_univ

rw_norm_pat :: HCS => Prop -> ProofInfo -> Proof -> Tactic (Pat, RwInfo)
rw_norm_pat is_simp p_info p = do
  p <- proof_mvars_to_univs p
  rw_norm_pat' is_simp p_info p []

rw_info_get_proof :: HCS => RwInfo -> Tactic Proof
rw_info_get_proof info = case _rw_proof info of
  Just p -> pure p
  Nothing -> do
    let Left name = _rw_proof_info info
    ctx_mk_proof_ref name

rw_match_m_type :: HCS => Type -> RwMatchM ()
rw_match_m_type t = do
  x <- trie_m_next
  case x of
    PatKind PtTypeMvar -> do
      PatN i <- trie_m_next
      trie_m_lift # do
        j <- ctx_mvars_map_get_type i
        let t1 = mk_type_mvar j
        add_type_eq t1 t
    _ -> case _type_val t of
      TypeVar i -> do
        PatKind PtTypeVar <- pure x
        trie_m_exact # PatN i
      PropT -> do
        PatKind PtProp <- pure x
        nop
      SetT -> do
        PatKind PtSet <- pure x
        nop
      Func a b -> do
        PatKind PtFunc <- pure x
        rw_match_m_type a
        rw_match_m_type b
      _ -> trie_m_fail

rw_match_m_term :: HCS => N -> N -> Term -> RwMatchM ()
rw_match_m_term d_pat d0 e = do
  let d = _term_depth e
  let d_dif = d_pat - d0
  
  x <- trie_m_next
  
  case x of
    PatKind PtTermMvar -> do
      PatN i <- trie_m_next
      let t = _term_type e
      trie_m_lift # do
        j <- ctx_mvars_map_get_term d0 i
        
        let e1 = mk_term_mvar d t j
        -- ctx_set_term_mvar_depth j d
        
        let dm = max_term_var_in_term e
        ifm (dm >= d0) # err [dfsc "rw_match_m_term"]
        add_term_eq e1 e
      rw_match_m_type t
    _ -> case _term_val e of
      TermRef name ps -> do
        PatKind PtTermRef <- pure x
        trie_m_exact # PatId name
        mapM_ rw_match_m_type ps
      TermVar k -> do
        PatKind PtTermVar <- pure x
        k' <- if k < d0
          then if k < d_pat then pure k else trie_m_fail
          else pure # k + d_dif
        trie_m_exact # PatN k'
      Lam _ t e -> do
        PatKind PtLam <- pure x
        rw_match_m_type t
        rw_match_m_term d_pat d0 e
      App a b -> do
        PatKind PtApp <- pure x
        rw_match_m_term d_pat d0 a
        rw_match_m_term d_pat d0 b
      _ -> trie_m_fail

rw_match_m :: HCS => Term -> RwMatchM RwMatchRes
rw_match_m e = do
  PatDepth d_pat <- trie_m_next
  let d0 = _term_depth e
  rw_match_m_term d_pat d0 e

  xs <- trie_m_val
  
  trie_m_lift # fmap filter_justs # mapM <~ xs #
    \info -> try # ctx_query # do
      types <- mapM <~ _rw_types info # \i -> do
        j <- ctx_mvars_map_get_type i
        e <- gets_ctx # map_get j . _ctx_type_asgns
        case e of
          Just e -> pure (j, e)
          Nothing -> do
            err [dfsc "Unassigned type metavariable after rewriting"]
      
      terms <- mapM <~ _rw_terms info # \i -> do
        j <- ctx_mvars_map_get_term d0 i
        e <- gets_ctx # map_get j . _ctx_term_asgns
        case e of
          Just e -> pure (i, e)
          Nothing -> do
            -- ifm_dbg # do
            --   tracem # _rw_terms info
            --   tracem i
            err [dfsc "Unassigned term metavariable after rewriting"]
      
      return (info, (Map.fromList types, Map.fromList terms))

rw_match_exact :: HCS => RwMap -> Term -> Tactic RwMatchRes
rw_match_exact rw_map e = do
  res <- fmap (fromMaybe []) # ctx_query # trie_m_run (rw_match_m e) rw_map
  -- tracem res
  return res

-- rw_match_exact_fst :: HCS => RwMap -> Term -> Tactic RwMatchRes
-- rw_match_exact_fst rw_map e = do
--   xs <- rw_match_exact rw_map e
--   return # headm xs

rw_match_term_val :: HCS => RwMap -> Term -> NthRw
rw_match_term_val rw_map e = case _term_val e of
  Lam mn t e -> do
    d <- pure # _term_depth e - 1
    res <- rw_match' rw_map e
    case res of
      Nothing -> pure Nothing
      Just (info, p, f, match) -> lift # do
        -- dbg <- pure False
        -- ifm dbg logb
        -- 
        -- sd <- pure # \e -> let
        --   de = _term_depth e
        --   dif = de - d
        --   sgn = ite (dif > 0) "+" "-"
        --   in put_in_brackets # show de {-if de == 0 then "0" else
        --     unwords # ["d"] ++ ite (dif == 0) [] [sgn, show # abs dif]-}
        -- 
        -- s_type <- pure # \name t -> ifm dbg # do
        --   s1 <- show_type t
        --   tracem # Msg [name, " : ", s1]
        -- 
        -- s_term <- pure # \name e -> ifm dbg # do
        --   s1 <- show_term e
        --   tracem # Msg [name, " := ", s1, " ", sd e]
        -- 
        -- s_proof <- pure # \name p -> ifm dbg # do
        --   s <- pure # _proof_stat p
        --   s1 <- show_term s
        --   tracem # Msg [name, " : ", s1, " ", sd s]
        -- 
        -- ifm dbg # tracem d
        -- 
        -- s_proof "p" p

        f <- res_term f
        tf <- pure # _term_type f
        tfp <- pure # mk_func tf mk_prop_t

        App e _ <- pure # _term_val # _proof_stat p
        TermRef _ [tp] <- pure # _term_val e
        Func tL _ <- pure # _type_val tp

        -- s_type "tp" tp
        -- s_type "tL" tL
        -- s_type "tf" tf
        -- s_type "tfp" tfp
        -- 
        -- fn <- pure ctx_mk_type_mvar

        f <- pure # mk_lam tL # mk_app mk_prop_t
          (mk_term_var (d + 3) tfp (d + 1)) #
          lift_term' (d + 1) 1 f
        -- s_term "f" f

        p <- ctx_univ_e p f
        -- s_proof "p1" p

        s <- pure # _proof_stat p

        p <- pure # mk_univ_i tfp p
        -- s_proof "p2" p
        -- 
        -- ifm dbg # tracem p

        p <- pure # mk_univ_i t p
        -- s_proof "p3" p

        f1 <- ctx_term_part_simple s [0, 1, 1]
        f1 <- pure # mk_lam t # lift_term (-1) f1
        
        -- s_term "f1" f1

        f2 <- ctx_term_part_simple s [1, 1]
        f2 <- pure # mk_lam t # lift_term (-1) f2
          
        -- s_term "f2" f2

        -- da <- ctx_term_depth
        -- modify_ctx # \ctx -> ctx {_ctx_terms_num = 0}
        p1 <- ctx_mk_proof_ref' namei_fn_ext_subst [t, tf]
        -- modify_ctx # \ctx -> ctx {_ctx_terms_num = da}
        -- s_proof "s" p1

        p1 <- ctx_univs_e p1 [f1, f2]
        -- s_proof "s1" p1

        p <- ctx_imp_e p1 p
        -- s_proof "s2" p

        -- ifm dbg logb

        f <- pure # mk_term_var (d + 1) (mk_func t tf) d

        -- ifm (d == 1) # do
        --   err ["#"]

        return # Just (info, p, f, match)
  App a b -> do
    -- n <- get
    -- case n of
    --   0 -> lift pfail
    --   _ -> put # n - 1
    
    -- n0 <- get
    -- tracem ("n0", n0)

    res1 <- rw_match' rw_map a

    -- n1 <- get
    -- tracem ("n1", n1)
    
    res2 <- if isJust res1 then pure Nothing else
      rw_match' rw_map b
    
    -- n2 <- get
    -- lift # do
    --   sa <- show_term a
    --   sb <- show_term b
    --   tracem (n0, n1, n2, Lit # map snd sa, Lit # map snd sb, isJust res1, isJust res2)
    
    case res1 of
      Just (info, p, f, match) -> do
        f <- lift # ctx_mk_app f (lift_term 1 b)
        return # Just (info, p, f, match)
      Nothing -> case res2 of
        Just (info, p, f, match) -> do
          f <- lift # ctx_mk_app (lift_term 1 a) f
          return # Just (info, p, f, match)
        Nothing -> pure Nothing
  _ -> pure Nothing

rw_match_aux :: HCS => RwMap -> Term -> RwMatchRes -> NthRw
rw_match_aux rw_map e matches = do
  -- lift # ifm_dbg # tracem # len matches
  st <- lift get
  dflt <- pure # rw_match_term_val rw_map e
  case matches of
    [] -> do
      -- lift # ifm_dbg # ctx_trace_term e
      dflt
    ((info, match@(match_t, match_e)) : matches) -> do
      res <- lift # ctx_try # do
        
        let d = _term_depth e
        p <- rw_info_get_proof info
        let f = mk_term_var (d + 1) (_term_type e) d

        -- tracem # _rw_types info
        -- tracem # match_t
        
        -- args <- pure # flip map (_rw_terms info) # \i ->
        --   map_get i match
        -- p <- ctx_univs_e p args
        
        -- tracem "<<<"
        -- tracem match
        -- ctx_trace_proof_stat p
        p <- pure # subst_type_mvars_in_proof match_t p
        p <- ctx_all_imps_e_tc p
        -- ctx_trace_proof_stat p

        p <- (\f -> foldM f p # _rw_terms info) # \p i -> do
          Just e <- pure # map_get i match_e
          p <- ctx_univ_e p e
          p <- ctx_all_imps_e_tc p
          -- ctx_trace_proof_stat p
          return p
        -- tracem ">>>"
        
        return # Just (info, p, f, match)
      case res of
        Right res -> do
          -- n <- get
          -- tracem n
          -- lift # ctx_trace_term e

          modify dec
          n <- get
          if n == 0 then pure res else do
            lift # put st
            dflt
        Left msg -> do
          -- lift # ctx_trace msg
          rw_match_aux rw_map e matches

rw_match' :: HCS => RwMap -> Term -> NthRw
rw_match' rw_map e = do
  n <- get
  if n == 0 then pure Nothing else do
    -- ctx <- lift get_ctx
    matches <- lift # rw_match_exact rw_map e
    -- lift # put_ctx ctx
    rw_match_aux rw_map e matches

rw_match :: HCS => RwMap -> N -> Prop -> Term -> NthRw
rw_match rw_map h_num g e = if h_num == 0
  then if not g then pure Nothing else
    rw_match' rw_map e
  else do
    let [a, b] = snd # split_app e
    res1 <- rw_match rw_map 0 True a
    case res1 of
      Just (info, p, f, match) -> do
        f <- lift # ctx_mk_imp f (lift_term 1 b)
        return # Just (info, p, f, match)
      Nothing -> do
        res2 <- rw_match rw_map (h_num - 1) g b
        case res2 of
          Just (info, p, f, match) -> do
            f <- lift # ctx_mk_imp (lift_term 1 a) f
            return # Just (info, p, f, match)
          Nothing -> pure Nothing

tac_rw_map :: HCS => N -> Prop -> Maybe N -> RwMap -> Tactic (Maybe RwInfo)
tac_rw_map h_num g n rw_map = do
  n <- pure # fromMaybe 0 n
  
  -- kp <- ctx_term_depth
  s <- ctx_get_goal_stat
  
  -- let keys = trie_keys rw_map
  -- e <- pure # fromJust # flip find keys # \k ->
  --   case _rw_proof_info # head # Set.toList # fromJust # trie_lookup k rw_map of
  --     Left name -> drop 6 name == "true_imp"
  -- tracem e
  
  -- ifm_dbg # do
  --   tracem # Msg []
  --   tracem # head # trie_keys rw_map
  --   tracem # Msg []
  --   tracem s
  --   tracem # Msg []
  --   -- case _rw_proof # head # Set.toList # Set.unions # trie_elems rw_map of
  --   --   Just p -> tac_show_proof p
  
  -- tracem "begin"
  -- tracem # n + 1
  res <- runStateT (rw_match rw_map h_num g s) # n + 1
  -- tracem res
  -- tracem "end"
  case res of
    (Just (info, p, f, match), 0) -> do
      t <- ctx_mk_type_mvar
      f <- pure # mk_lam t f
      
      -- tracem (kp, _term_depth f, _term_depth # _proof_stat p)
      
      -- ctx_trace_proof_stat p
      p <- ctx_univ_e p f
      p1 <- ctx_mk_proof_mvar'
      p <- ctx_imp_e p p1
      -- ctx_trace_proof_stat p
      tac_refine p
      
      return # Just info
    _ -> do
      return Nothing

tac_rw_1 :: HCS => Maybe N -> N -> Prop -> N -> Proof -> Tactic ()
tac_rw_1 n h_num g p_index p = do
  p <- tac_res_proof_tc p
  
  -- tracem "<<<"
  -- ctx_trace_proof_stat p

  let p_info = Right p_index
  (pat, info) <- rw_norm_pat True p_info p
  
  -- ctx_trace_proof_stat # fromJust # _rw_proof info
  -- tracem pat
  -- tracem ">>>"
  
  let rw_map = trie_list_singleton pat info
  res <- tac_rw_map h_num g n rw_map
  ifm (isNothing res) # tac_err "rewrite" [dfsc "pattern not found"]

tac_rw :: HCS => Maybe N -> Maybe N -> [Proof] -> Tactic (Maybe N)
tac_rw at n ps = tac_at' at # \at -> do
  -- ifm_dbg # tracem # len ps
  let (h_num, g) = tac_at_to_h_num_g at
  zipWithM_ (tac_rw_1 n h_num g) [0..] ps

tac_simp_aux' :: HCS => N -> Prop -> RwMap -> Tactic [RwInfo]
tac_simp_aux' h_num g rw_map = do
  -- res <- tac_rw_map h_num g (Just 0) rw_map
  res <- ctx_try # tac_rw_map h_num g (Just 0) rw_map
  res <- case res of
    Left msg -> do
      -- ctx_trace msg
      return Nothing
    Right res -> pure res
  case res of
    Nothing -> pure []
    Just info -> do
      -- p <- res_proof # fromJust # _rw_proof info
      -- ctx_trace_proof_stat p
      list <- tac_simp_aux' h_num g rw_map
      return # info : list

tac_simp_aux :: HCS => Prop -> Prop -> Maybe N -> [Proof] -> Tactic ()
tac_simp_aux shw only at ps = do
  ctx0 <- get_ctx
  
  rw_map <- (\f -> zipWithM f ps [0..]) # \p i -> do
    (pat, info) <- rw_norm_pat True (Right i) p
    return # trie_list_singleton pat info
  rw_map <- pure # trie_list_unions rw_map

  -- tracem # Lit # pure # show_map show show #
  --   Map.map <~ (trie_to_map rw_map) # \x ->
  --     Set.map <~ x # \x ->
  --       _rw_proof_info x
  
  rw_map <- if only then pure rw_map else do
    rw_map0 <- gets_th _th_simp_map
    return # trie_list_union rw_map0 rw_map
  
  let (h_num, g) = tac_at_to_h_num_g at
  res <- tac_simp_aux' h_num g rw_map
  ifm (null res) # tac_err "simp" [dfsc "failed to simplify"]
  ifm shw # do
    ps <- mapM <~ res # \rw_info -> do
      s <- case _rw_proof_info rw_info of
        Left i -> do
          name <- id_to_name i
          simp_name <- id_to_name namei_simp
          name <- case extract_middle usc (usc' : simp_name) name of
            Nothing -> pure name
            Just name1 -> pure name1
          i <- name_to_id name
          
          -- d <- ctx_term_depth
          -- let is = _rw_terms rw_info
          -- ts <- mapM (const ctx_mk_type_mvar) is
          -- es <- mapM <~ zip ts is # \(t, i) -> do
          --   res_term # mk_term_mvar d t i
          -- p <- ctx_mk_proof_ref' i # map mk_type_var # _rw_types rw_info
          -- p <- ctx_univs_e p es
          -- p <- res_proof p
          
          let ts = map mk_type_var # _rw_types rw_info
          p <- ctx_mk_proof_ref' i ts
          
          sp <- show_term # _proof_stat p
          return # concat
            [ [("proof", name), dfsc " : "]
            , sp
            ]
        Right i -> pure [dfsc "?"]
      return # dfsc "\n" : dfsc tab : s
    
    -- asms <- gets_ctx _ctx_asms
    -- tracem asms
    
    goals <- th_to_elab # show_goal # ctx_to_goal ctx0
    raise # concat
      [ goals, [dfsc "\n\n"]
      , dfsc "simp:" : concat ps
      ]

tac_simp :: HCS => Prop -> Prop -> Maybe N -> [Proof] -> Tactic (Maybe N)
tac_simp shw only at ps = do
  (_, i) <- tac_at at # \at -> tac_simp_aux shw only at ps
  return i

tac_subst_aux :: HCS => Prop -> [N] -> Tactic ()
tac_subst_aux symm is = do
  mp <- gets_ctx _ctx_proofs
  js <- pure # Set.toList # Set.difference
    (Map.keysSet mp) (Set.fromList is)
  let ats = Nothing : map Just js
  ps <- mapM <~ is # \i -> do
    p <- ctx_mk_proof_var i
    if symm then ctx_proof_symm p else pure p
  mapM_ <~ ats # \at -> ctx_try' # tac_simp False True at ps
  mapM_ tac_clear_asm_1 is

tac_subst :: HCS => [N] -> Tactic ()
tac_subst = tac_subst_aux False

tac_subst' :: HCS => [N] -> Tactic ()
tac_subst' = tac_subst_aux True

tac_subst_adv :: HCS => [N] -> Tactic ()
tac_subst_adv is = tac_subst is >> tac_try_triv_or_refl

tac_subst_adv' :: HCS => [N] -> Tactic ()
tac_subst_adv' is = tac_subst' is >> tac_try_triv_or_refl

unfold_match' :: HCS => Id -> N -> N -> N -> Term -> Tactic (Term, N)
unfold_match' name n k d e = case _term_val e of
  TermRef name1 ts -> if name1 /= name then pure (e, n) else do
    t <- ctx_mk_type_mvar
    f <- pure # if n == 0 then mk_term_var d t k else e
    return (f, n - 1)
  Lam m t e -> do
    (f, n) <- unfold_match' name n k (d + 1) e
    return (mk_lam_aux m t f, n)
  App a b -> do
    (fa, n) <- unfold_match' name n k d a
    (fb, n) <- unfold_match' name n k d b
    f <- ctx_mk_app fa fb
    return (f, n)
  _ -> pure (e, n)

unfold_match :: HCS => Id -> N -> N -> N -> Prop -> Term -> Tactic (Maybe Term)
unfold_match name n d h_num g e = if h_num == 0
  then if not g then pure Nothing else do
    (f, n) <- unfold_match' name n d (d + 1) e
    return # if n < 0 then Just f else Nothing
  else do
    let [a, b] = snd # split_app e
    f1 <- unfold_match name n d 0 True a
    f2 <- unfold_match name n d (h_num - 1) g b
    if isNothing f1 && isNothing f2 then pure Nothing else do
      f1 <- pure # fromMaybe a f1
      f2 <- pure # fromMaybe b f2
      f <- ctx_mk_imp f1 f2
      return # Just f

tac_unfold_aux' :: HCS => Id -> N -> N -> Prop -> Proof -> Tactic ()
tac_unfold_aux' name n h_num g p = do
  -- tracem name
  d <- ctx_term_depth
  s <- ctx_get_goal_stat
  match <- unfold_match name n d h_num g # lift_term 1 s
  case match of
    Nothing -> do
      name <- id_to_name name
      tac_err "unf" [dfsc "term ", ("term", name), dfsc " not found"]
    Just f -> do
      -- ctx_trace_term f
      t <- ctx_mk_type_mvar
      f <- pure # mk_lam t f
      p <- ctx_univ_e p f
      p1 <- ctx_mk_proof_mvar'
      p <- ctx_imp_e p p1
      -- ctx_trace_proof_stat p
      tac_refine p

tac_unfold_aux :: HCS => Maybe N -> Id -> N -> Tactic ()
tac_unfold_aux at name n = do
  let (h_num, g) = tac_at_to_h_num_g at
  unf_name <- get_priv_name' namei_unf name
  ref <- maybe (pure Nothing) ctx_get_proof_ref' unf_name
  if isNothing ref then do
    -- ifm_dbg # tracem name
    Just name' <- get_def_thm_name' name
    p <- ctx_mk_proof_ref name'
    tac_unfold_aux' name n h_num g p
  else do
    Just unf_name <- pure unf_name
    p <- ctx_mk_proof_ref unf_name
    tac_rw_1 (Just n) h_num g 0 p

tac_unfold :: HCS => Maybe N -> N -> Id -> Tactic (Maybe N)
tac_unfold at n name = tac_at' at # \at -> tac_unfold_aux at name n

tac_unfold_asm :: HCS => N -> Id -> Tactic N
tac_unfold_asm i name = do
  Just i <- tac_unfold (Just i) 0 name
  return i

tac_unfold_goal :: HCS => Id -> Tactic ()
tac_unfold_goal name = tac_unfold Nothing 0 name >> nop

tac_tau' :: HCS => Maybe N -> Maybe Name -> N -> Term -> Tactic ()
tac_tau' at mn n x = do
  s <- case at of
    Nothing -> ctx_get_goal_stat
    Just name -> ctx_get_proof_var name
  case term_find_binder_arg namei_tau n s of
    Nothing -> tac_err' "tau"
    Just f -> do
      p <- ctx_mk_proof_ref namei_tau_ind
      p <- ctx_univs_e p [f, x]
      p1 <- ctx_mk_proof_mvar'
      p <- ctx_imp_e p p1
      if _proof_stat p == s
        then tac_refine p
        else tac_have' mn p

tac_tau :: HCS => Maybe N -> Maybe Name -> N -> Term -> Tactic ()
tac_tau at mn n x = tac_tau' at mn n x <|> do
  tac_unfold at n namei_mk
  tac_tau at mn n x

tac_the :: HCS => Maybe N -> Maybe Name -> N -> Term -> Tactic ()
tac_the at mn n x = do
  s <- case at of
    Nothing -> ctx_get_goal_stat
    Just name -> ctx_get_proof_var name
  case term_find_binder_arg namei_the n s of
    Nothing -> tac_err' "the"
    Just f -> do
      p <- ctx_mk_proof_ref namei_the_ind
      p <- ctx_univs_e p [f, x]
      p1 <- ctx_mk_proof_mvar'
      p <- ctx_imp_e p p1
      if _proof_stat p == s
        then tac_refine p
        else tac_have' mn p

tac_refl :: HCS => Tactic ()
tac_refl = do
  s <- ctx_get_goal_stat
  let (target, args) = split_app s
  case _term_val target of
    TermRef i _ -> do
      if len args /= 2 then err [dfsc "tac_refl [2]"] else do
        let [a, b] = args
        if a /= b then err [dfsc "tac_refl [3]"] else do
          name <- id_to_name i
          i' <- register_name # name ++ "_refl"
          p <- ctx_mk_proof_ref i'
          p <- ctx_univ_e p a
          tac_refine p
    _ -> err [dfsc "tac_refl [1]"]

tac_triv :: HCS => Tactic ()
tac_triv = do
  p <- ctx_mk_proof_ref namei_triv
  tac_refine p

tac_try_triv_or_refl :: HCS => Tactic ()
tac_try_triv_or_refl = void # try # tac_triv <|> tac_refl

tac_try_triv_or_asm_ex :: HCS => Tactic ()
tac_try_triv_or_asm_ex = void # try # tac_triv <|> tac_asm_ex

tac_mk_have :: HCS => Maybe Name -> Proof -> (N -> Tactic ()) -> Tactic ()
tac_mk_have mn p tac = tac_have mn p >>= tac

tac_cases_get_name :: HCS => [Name] -> Tactic (Name, [Name])
tac_cases_get_name names = case names of
  [] -> tac_err "cases" [dfsc "Too few term names provided"]
  (name : names) -> pure (name, names)

tac_cases_get_names :: HCS => N -> [Name] -> Tactic ([Name], [Name])
tac_cases_get_names n names = if n == 0 then pure ([], names) else do
  (x, names) <- tac_cases_get_name names
  (xs, names) <- tac_cases_get_names (n - 1) names
  return (x : xs, names)

tac_cases_asm_1' :: HCS => Prop -> N -> Proof -> [Name] -> Tactic ([N], [Name])
tac_cases_asm_1' is_fst i h names = do
  let dflt = pfail
  let hs = _proof_stat h
  mn <- ctx_get_asm_name i
  f1 <- pure # do
    s <- ctx_get_goal_stat
    let (target, args) = split_app hs
    case _term_val target of
      TermRef ref_name ps ->
        if ref_name == namei_or then if not is_fst then dflt else do
          p <- ctx_mk_proof_ref namei_of_or
          p <- ctx_univs_e p args
          p <- ctx_imp_e p h
          p <- ctx_univ_e p s
          ps <- replicateM 2 # do
            p <- ctx_mk_proof_mvar'
            s <- ctx_mk_stat_mvar
            j <- gets_ctx _ctx_proof_depth
            return # mk_proof_clear [] [i] #
              mk_imp_i' mn i j s p
          p <- ctx_imps_e p ps
          tac_refine p
          return ([], names)
        else if ref_name == namei_and then do
          p <- ctx_mk_proof_ref namei_of_and
          p <- ctx_univs_e p args
          p <- ctx_imp_e p h
          p <- ctx_univ_e p s
          p1 <- ctx_mk_proof_mvar'
          p <- ctx_imp_e p p1
          tac_refine p
          tac_clear_asm_1 i
          (name1, names) <- tac_cases_get_name names
          (name2, names) <- tac_cases_get_name names
          [i1, i2] <- mapM <~ [Just name1, Just name2] # tac_imp_i_dep <~ i
          return ([i1, i2], names)
        else if ref_name == namei_iff then do
          p <- ctx_mk_proof_ref namei_of_iff
          p <- ctx_univs_e p args
          p <- ctx_imp_e p h
          p <- ctx_univ_e p s
          p1 <- ctx_mk_proof_mvar'
          p <- ctx_imp_e p p1
          tac_refine p
          tac_clear_asm_1 i
          (name1, names) <- tac_cases_get_name names
          [i1, i2] <- mapM <~ [mn, Just name1] # tac_imp_i_dep <~ i
          return ([i1, i2], names)
        else if ref_name == namei_exi then do
          let [f] = args
          p <- ctx_mk_proof_ref namei_of_exi
          p <- ctx_univ_e p f
          p <- ctx_imp_e p h
          p <- ctx_univ_e p s
          p1 <- ctx_mk_proof_mvar'
          p <- ctx_imp_e p p1
          tac_refine p
          ([e_name, h_name], names) <- tac_cases_get_names 2 names
          tac_univ_i e_name
          tac_clear_asm_1 i
          
          -- ctx0 <- get_ctx
          -- asms <- gets_ctx _ctx_asms
          -- tracem (asms, h_name, i)
          
          i1 <- tac_imp_i_dep (Just h_name) i
          
          -- ctx_query # do
          --   elab_norm ctx0
          --   asms <- gets_ctx _ctx_asms
          --   tracem (asms, h_name, i)
          
          -- s1 <- ctx_show_goals
          return ([i1], names)
        else if ref_name == namei_exiu then do
          let [f] = args
          p <- ctx_mk_proof_ref namei_of_exiu
          p <- ctx_univ_e p f
          p <- ctx_imp_e p h
          p <- ctx_univ_e p s
          p1 <- ctx_mk_proof_mvar'
          p <- ctx_imp_e p p1
          tac_refine p
          (name1, names) <- tac_cases_get_name names
          tac_univ_i name1
          tac_clear_asm_1 i
          (name1, names) <- tac_cases_get_name names
          [i1, i2] <- mapM <~ [mn, Just name1] # tac_imp_i_dep <~ i
          return ([i1, i2], names)
        else dflt
      _ -> dflt
  f2 <- pure # do
    Just (s1, s2) <- pure # term_to_imp hs
    p1 <- ctx_mk_proof_mvar_tc s1
    h <- ctx_imp_e h p1
    tac_cases_asm_1' is_fst i h names
  f3 <- pure # do
    h <- norm_proof h
    tac_cases_asm_1' is_fst i h names
  ctx_ors [f1, f2, f3]

tac_cases_asm_1 :: HCS => Prop -> N -> [Name] -> Tactic ([N], [Name])
tac_cases_asm_1 is_fst i names = do
  res <- ctx_get_proof_var' i
  case res of
    Nothing -> tac_err "tac_cases_asm_1"
      [ ("lit_str", usc' : show i)
      , dfsc " is not a local proof" ]
    Just hs -> do
      h <- ctx_mk_proof_var' hs i
      tac_cases_asm_1' is_fst i h names

tac_cases_1 :: HCS => Prop -> Prop -> [Name] -> [N] -> Tactic [Name]
tac_cases_1 rc is_fst names asms = case asms of
  [] -> pure names
  (i : asms) -> do
    res <- ctx_try # tac_cases_asm_1 is_fst i names
    (names, asms) <- case res of
      Right (asms_new, names) -> pure (names, asms_new ++ asms)
      Left msg -> if rc then pure (names, asms) else do
        Just e <- ctx_get_proof_var' i
        s1 <- show_term e
        -- ctx_trace msg
        tac_err "cases_1" #
          dfsc "Cannot destructure term:\n\n" : s1
    if not rc then pure names else
      tac_cases_1 True False names asms

tac_cases' :: HCS => Prop -> LocIdent -> [Name] -> Tactic ()
tac_cases' rc ident names = do
  ind <- pure # do
    pfail
    -- err # pure # dfsc "tac_cases' -> tac_ind' False ident names"
    -- tac_ind' False ident names
  case ident of
    Left i -> do
      t <- ctx_get_term_var i
      case _type_val t of
        PropT -> do
          (deps, _) <- ctx_calc_deps_for_idents [ident]
          let deps' = reverse deps
          let deps_num = len deps'
          let ds = [deps_num - 1, deps_num - 2 ..]
          tac_revert' # map Right deps' ++ [ident]
          p <- ctx_mk_proof_ref namei_prop_ind
          x <- ctx_mk_term_mvar
          p <- ctx_univ_e p x
          ps <- replicateM 2 # do
            p <- ctx_mk_proof_mvar'
            (\f -> foldM f p # zip deps' ds) # \p (i, di) -> do
              s <- ctx_mk_stat_mvar
              j <- gets_ctx _ctx_proof_depth
              mn <- ctx_get_asm_name i
              return # mk_proof_clear [] [i] #
                mk_imp_i_aux mn (Just i) (j + di) s p
          p <- ctx_imps_e p ps
          tac_refine p
        _ -> ind
    Right i -> (<|> ind) # do
      if rc then tac_rcases' i names else do
        names <- tac_cases_1 rc True names [i]
        ifmn (null names) # tac_err "cases" #
          (dfsc "Superfluos identifier names provided: " :) #
          intersperse (dfsc ", ") # map (mk_pair "term") names

tac_rcases' :: HCS => N -> [Name] -> Tactic ()
tac_rcases' i names = case names of
  [] -> tac_err "tac_rcases'" [dfsc "Empty names list"]
  [name2] -> do
    name1 <- get_asm_name i
    tac_rcases' i [name1, name2]
  [_, _] -> tac_cases (Right i) names
  (name1 : names) -> do
    let name2 = show # len names
    
    tac_cases (Right i) [name1, name2]
    
    -- (is, names') <- tac_cases_asm_1 True i [name1, name2]
    -- ifmn (null names') # tac_err "tac_rcases'" [dfsc "Superfluos names"]
    -- tracem is
    -- error ""
    
    deps <- gets_ctx # Map.toList . _ctx_asm_deps
    res <- pure # find <~ deps # \(_, (name, _)) -> name == name2
    i <- case res of
      Nothing -> error "tac_rcases'"
      Just (i, _) -> pure i
    
    tac_rcases' i names

tac_cases :: HCS => LocIdent -> [Name] -> Tactic ()
tac_cases = tac_cases' False

tac_rcases :: HCS => LocIdent -> [Name] -> Tactic ()
tac_rcases = tac_cases' True

tac_cases_have :: HCS => Maybe Name -> Proof -> [Name] -> Tactic ()
tac_cases_have mn p names = tac_mk_have mn p # \i ->
  tac_cases (Right i) names

tac_rcases_have :: HCS => Maybe Name -> Proof -> [Name] -> Tactic ()
tac_rcases_have mn p names = tac_mk_have mn p # \i ->
  tac_rcases (Right i) names

tac_split' :: HCS => Maybe Name -> Tactic ()
tac_split' mn = do
  s <- ctx_get_goal_stat
  let (target, args) = split_app s
  case _term_val target of
    TermRef name ps ->
      if name == namei_and then do
        p <- ctx_mk_proof_ref namei_and_of
        p <- ctx_univs_e p args
        ps <- replicateM 2 ctx_mk_proof_mvar'
        p <- ctx_imps_e p ps
        tac_refine p
      else if name == namei_iff then do
        p <- ctx_mk_proof_ref namei_iff_of
        p <- ctx_univs_e p args
        i <- ctx_get_avail_proof_id
        ps <- replicateM 2 # do
          p <- ctx_mk_proof_mvar'
          -- s <- ctx_mk_stat_mvar
          -- return # mk_imp_i i s p
          return p
        p <- ctx_imps_e p ps
        case mn of
          Just name -> tac_on_new_goals (tac_refine p) # do
            tac_imp_i name
          _ -> tac_refine p
      else do
        name <- id_to_name name
        tac_err "sp" [("term", name)]
    _ -> tac_err' "sp"

tac_split :: HCS => Maybe Name -> Tactic ()
tac_split mn = tac_split' mn <|> do
  tac_norm_goal
  tac_split mn

tac_exfalso :: HCS => Tactic ()
tac_exfalso = do
  s <- ctx_get_goal_stat
  dflt <- pure # do
    p <- ctx_mk_proof_ref namei_of_false
    p1 <- ctx_mk_proof_mvar'
    p <- ctx_imp_e p p1
    p <- ctx_univ_e p s
    tac_refine p
  case _term_val s of
    TermRef name _ -> ifm (name /= namei_false) dflt
    _ -> dflt

tac_contra :: HCS => Proof -> Proof -> Tactic ()
tac_contra p1 p2 = do
  tac_exfalso
  p <- ctx_mk_proof_ref namei_false_of
  p <- ctx_all_univs_e p
  p1 <- ctx_all_univs_imps_e p1
  p2 <- ctx_all_univs_imps_e p2
  ctx_ors # map <~ [(p1, p2), (p2, p1)] # \(p1, p2) -> do
    p <- ctx_imps_e p [p1, p2]
    tac_refine p

tac_by_contra_mk_neg :: HCS => Prop -> Tactic (Stat, Prop)
tac_by_contra_mk_neg strict = do
  s <- ctx_get_goal_stat
  return # stat_get_pos_info s
  -- res@(s, pos) <- pure # stat_get_pos_info s
  -- if not (strict && pos) then pure res else do
  --   tac_norm_goal'
  --   tac_by_contra_mk_neg strict

tac_by_contra_aux :: HCS => Prop -> Name -> Tactic ()
tac_by_contra_aux strict h = do
  (s, pos) <- tac_by_contra_mk_neg strict
  let name = ite pos namei_of_not_imp_false namei_not_of_imp_false
  p <- ctx_mk_proof_ref name
  p <- ctx_univ_e p s
  p1 <- ctx_mk_proof_mvar'
  p <- ctx_imp_e p p1
  tac_refine p
  i <- tac_imp_i h
  ifmn strict #
    ctx_try' # tac_push_neg # Just i

tac_by_contra :: HCS => Name -> Tactic ()
tac_by_contra = tac_by_contra_aux False

tac_by_contra' :: HCS => Name -> Tactic ()
tac_by_contra' = tac_by_contra_aux True

tac_cpos :: HCS => N -> Tactic ()
tac_cpos i = do
  mn <- ctx_get_asm_name i
  s <- ctx_get_goal_stat
  res <- ctx_get_proof_var' i
  case res of
    Nothing -> tac_err' "cpos"
    Just hs -> do
      h <- ctx_mk_proof_var' hs i
      (hs, hs_pos) <- pure # stat_get_pos_info hs
      (s, s_pos) <- pure # stat_get_pos_info s
      let c1 = ite hs_pos 'p' 'n'
      let c2 = ite s_pos 'p' 'n'
      ref_name <- name_to_id # concat ["cpos_", [c1, c2], "'"]
      p <- ctx_mk_proof_ref ref_name
      
      -- do
      --   p <- ctx_univ_e p hs
      --   p <- ctx_univ_e p s
      --   show_term (_proof_stat p) >>= (tracem . Msg . pure)
      
      p <- ctx_univs_e p [hs, s]
      p1 <- ctx_mk_proof_mvar'
      p <- ctx_imps_e p [h, p1]
      tac_refine p
      tac_clear_asm_1 i
      i <- tac_imp_i_dep mn i
      -- ctx_try' # tac_push_neg # Just i
      -- ctx_try' # tac_push_neg Nothing
      nop

tac_left_or_right :: HCS => Prop -> Tactic ()
tac_left_or_right is_right = do
  name <- pure # ite is_right namei_or_inr namei_or_inl
  s <- ctx_get_goal_stat
  p <- ctx_mk_proof_ref name
  args <- replicateM 2 ctx_mk_term_mvar
  p <- ctx_univs_e p args
  p1 <- ctx_mk_proof_mvar'
  p <- ctx_imp_e p p1
  tac_refine p

tac_left :: HCS => Tactic ()
tac_left = tac_left_or_right False

tac_right :: HCS => Tactic ()
tac_right = tac_left_or_right True

tac_close_goal :: HCS => Tactic a -> Tactic a
tac_close_goal tac = do
  i <- get_max_ctx_id
  res <- tac
  ctxs <- get_ctxs
  [] <- pure # filter <~ ctxs # \ctx ->
    _ctx_id ctx > i && not (_ctx_is_tc ctx)
  return res

tac_asm_ex :: HCS => Tactic ()
tac_asm_ex = do
  is <- gets_ctx # Map.keys . _ctx_proofs
  dflt <- pure # tac_err' "asm"
  ctx_ors # (dflt:) # map <~ is # \i -> do
    p <- ctx_mk_proof_var i
    tac_close_goal # tac_exact p

tac_asm :: HCS => Tactic ()
tac_asm = do
  is <- gets_ctx # Map.keys . _ctx_proofs
  dflt <- pure # tac_err' "asm"
  ctx_ors # map <~ [tac_exact, tac_apply] # \tac -> do
    ctx_ors # (dflt:) # map <~ is # \i -> do
      p <- ctx_mk_proof_var i
      tac_close_goal # tac p

tac_have_aux :: HCS => Bit -> Maybe N -> Maybe Name -> Proof -> Tactic N
tac_have_aux mode i0 mn p1 = do
  i <- ctx_get_avail_proof_id
  let s' = _proof_stat p1
  s <- ctx_get_goal_stat
  case mode of
    0 -> do
      p <- ctx_mk_proof_ref namei_of_imp_of
      p <- ctx_univs_e p [s', s]
      p2 <- ctx_mk_proof_mvar s
      p2 <- pure # mk_imp_i_aux mn i0 i s' p2
      p <- ctx_imps_e p [p1, p2]
      tac_refine p
      return i
    1 -> do
      p <- ctx_mk_proof_ref namei_imp_refl
      s_imp <- ctx_mk_imp s' s
      p <- ctx_univ_e p s_imp
      p2 <- ctx_mk_proof_mvar s
      p2 <- pure # mk_imp_i_aux mn i0 i s' p2
      p <- ctx_imps_e p [p2, p1]
      tac_refine p
      return i
    _ -> error ""

tac_have_dep :: HCS => N -> Maybe Name -> Proof -> Tactic N
tac_have_dep i0 = tac_have_aux 0 # Just i0

tac_have :: HCS => Maybe Name -> Proof -> Tactic N
tac_have = tac_have_aux 0 Nothing

tac_have' :: HCS => Maybe Name -> Proof -> Tactic ()
tac_have' mp p = tac_have mp p >> nop

tac_have_stat :: HCS => Maybe Name -> Stat -> Tactic ()
tac_have_stat mn s = do
  p <- ctx_mk_proof_mvar s
  tac_have' mn p

tac_suffices :: HCS => Maybe Name -> Proof -> Tactic N
tac_suffices = tac_have_aux 1 Nothing

tac_suffices' :: HCS => Maybe Name -> Proof -> Tactic ()
tac_suffices' mn p = tac_suffices mn p >> nop

tac_suffices_stat :: HCS => Maybe Name -> Stat -> Tactic ()
tac_suffices_stat mn s = do
  p <- ctx_mk_proof_mvar s
  tac_suffices' mn p

tac_refine_or_have :: HCS => Maybe Name -> Proof -> Tactic ()
tac_refine_or_have mn p = do
  p <- res_proof p
  s <- ctx_get_goal_stat
  if terms_eq (_proof_stat p) s
    then do
      Nothing <- pure mn
      tac_refine p
    else tac_have' mn p

tac_elim :: HCS => Proof -> Tactic ()
tac_elim p = do
  s <- ctx_get_goal_stat
  p1 <- ctx_mk_proof_ref namei_of_false
  p <- ctx_imp_e p1 p
  p <- ctx_univ_e p s
  tac_refine p

tac_use_tc :: HCS => Tactic ()
tac_use_tc = do
  s <- ctx_get_goal_stat
  (name, args) <- ctx_term_head s
  ifm (name == namei_and) # do
    [s1, s2] <- pure args
    p <- ctx_mk_proof_ref namei_and_of
    p <- ctx_univs_e p args
    p1 <- tac_tc_synth s1
    p2 <- ctx_mk_proof_mvar s2
    p <- ctx_imps_e p [p1, p2]
    tac_refine p

tac_use_aux :: HCS => Term -> Proof -> Tactic ()
tac_use_aux x p1 = do
  s <- ctx_get_goal_stat
  let (target, args) = split_app s
  case _term_val target of
    TermRef name ps ->
      if name == namei_and then do
        p <- ctx_mk_proof_ref namei_and_of
        p <- ctx_all_univs_e p
        p2 <- ctx_mk_proof_mvar'
        p <- ctx_imps_e p [p1, p2]
        tac_refine p
      else if name == namei_exi then do
        let [f] = args
        p <- ctx_mk_proof_ref namei_exi_of
        p <- ctx_univs_e p [f, x]
        p <- ctx_imp_e p p1
        tac_refine p
        -- ctx_try' tac_use_tc
      else if name == namei_exiu then do
        let [f] = args
        p <- ctx_mk_proof_ref namei_exiu_of
        p <- ctx_univs_e p [f, x]
        p2 <- ctx_mk_proof_mvar'
        p <- ctx_imps_e p [p1, p2]
        tac_refine p
        -- ctx_try' tac_use_tc
      else if name == namei_nempty then do
        let [a] = args
        p <- ctx_mk_proof_ref namei_nempty_of
        p <- ctx_univs_e p [a, x]
        p <- ctx_imp_e p p1
        tac_refine p
      else do
        name <- id_to_name name
        tac_err "use" [("term", name)]
    _ -> tac_err "use" [dfsc "[1]"]

tac_use' :: HCS => Term -> Proof -> Tactic ()
tac_use' x p = tac_use_aux x p <|> do
  p <- ctx_all_univs_e p
  tac_use_aux x p

tac_use :: HCS => Term -> Tactic ()
tac_use x = do
  p1 <- ctx_mk_proof_mvar'
  tac_use' x p1

tac_comm :: HCS => Maybe N -> N -> Tactic (Maybe N)
tac_comm at n = tac_at' at # \at -> do
  s <- tac_at_get_stat at
  let (h_num, g) = tac_at_to_h_num_g at
  (name, [a, b]) <- ctx_term_head s
  Just thm_name <- get_priv_name' namei_comm name
  p <- ctx_mk_proof_ref # thm_name
  p <- ctx_univs_e p [a, b]
  tac_rw_1 (Just n) h_num g 0 p

tac_symm :: HCS => Maybe N -> Tactic (Maybe N)
tac_symm at = tac_comm at 0

tac_work_on_goal :: HCS => N -> Tactic a -> Tactic a
tac_work_on_goal i tac = do
  pst <- get_pst
  let ctxs = _pst_ctxs pst
  let ctxs_num = len ctxs
  let fn = _pst_fn pst
  
  ids <- elab_get_ctx_ids
  i <- pure # fi # fromJust # elemIndex i ids
  
  if i < ctxs_num then nop else tac_err "work_on_goal"
    [dfsc "There is no goal with index ", ("lit_num", show i)]
  
  let (ctxs1, ctx : ctxs2) = splitAt (fi i) ctxs
  
  put_pst # init_pst ctx
  res <- tac
  
  pst_new <- get_pst
  let ctxs_new = _pst_ctxs pst_new
  let ctxs_new_num = len ctxs_new
  let fn_new = _pst_fn pst_new
  
  put_pst # ProofState
    { _pst_ctxs = ctxs1 ++ ctxs_new ++ ctxs2
    , _pst_fn = \ps -> let
      (ps1, ps') = splitAt (fi i) ps
      (ps_new, ps2) = splitAt (fi # ctxs_new_num) ps'
      in fn # ps1 ++ fn_new ps_new : ps2
    }
  
  return res

tac_work_on_goals :: HCS => [N] -> Tactic () -> Tactic ()
tac_work_on_goals is m = mapM_ (\i -> tac_work_on_goal i m) is

tac_move_goal :: HCS => N -> N -> Tactic ()
tac_move_goal i j = modify_pst # \pst -> ProofState
  { _pst_ctxs = list_move i j # _pst_ctxs pst
  , _pst_fn = \ps -> _pst_fn pst # list_move j i ps
  }

tac_by_cases :: HCS => Name -> Stat -> Tactic ()
tac_by_cases name s' = do
  s <- ctx_get_goal_stat
  p <- ctx_mk_proof_ref namei_of_em
  p <- ctx_univs_e p [s', s]
  i <- ctx_get_avail_proof_id
  ps <- replicateM 2 # do
    p <- ctx_mk_proof_mvar'
    s <- ctx_mk_stat_mvar
    return # mk_imp_i (Just name) i s p
  p <- ctx_imps_e p ps
  tac_refine p

tac_ext_1 :: HCS => Name -> Maybe Name -> Tactic ()
tac_ext_1 name h = do
  s <- ctx_get_goal_stat
  let (target, args) = split_app s
  case _term_val target of
    TermRef name1 ps ->
      if name1 == namei_eq then case _type_val # head' undefined ps of
        Func t1 t2 -> do
          p <- ctx_mk_proof_ref' namei_fn_ext [t1, t2]
          p <- ctx_univs_e p args
          p1 <- ctx_mk_proof_mvar'
          p <- ctx_imp_e p p1
          tac_refine p
        SetT -> do
          p <- ctx_mk_proof_ref namei_ax_ext
          p <- ctx_univs_e p args
          p1 <- ctx_mk_proof_mvar'
          p <- ctx_imp_e p p1
          tac_refine p
        _ -> do
          name <- id_to_name namei_eq
          tac_err "ext_1" [("term", name)]
      else tac_err "ext_1" [dfsc "[2]"]
    _ -> tac_err "ext_1" [dfsc "[1]"]
  tac_univ_i name

tac_ext_at :: HCS => [Name] -> Maybe Name -> Tactic ()
tac_ext_at names h = do
  mapM_ (flip tac_ext_1 h) names
  case h of
    Just _ -> nop
    Nothing -> do
      s <- ctx_get_goal_stat
      (name, ps, _) <- ctx_term_head_with_types s
      ifm (name == namei_eq) # do
        let [t] = ps
        ifm (_type_val t == PropT) # do
          p <- ctx_mk_proof_ref namei_eq_of_iff
          p <- ctx_all_univs_imps_e p
          tac_refine p

tac_ext :: HCS => [Name] -> [Maybe Name] -> Tactic ()
tac_ext names hs = mapM_ (tac_ext_at names) hs

tac_pme :: HCS => Maybe N -> Maybe Name -> N -> Id -> Term -> Tactic ()
tac_pme at mn n name e = do
  thm_name <- mk_def_thm_name name
  res <- get_thm' thm_name
  case res of
    Nothing -> do
      Nothing <- pure at

      ps <- (<|> pure []) # do
        s <- ctx_get_goal_stat
        Just ps <- pure # term_find_ref_ps name n s
        return ps

      thm_name <- mk_desc_thm_name name
      p <- ctx_mk_proof_ref' thm_name ps
      p <- ctx_univ_e p e
      p1 <- ctx_mk_proof_mvar'
      p <- ctx_imp_e p p1
      tac_refine_or_have mn p
    Just ref -> do
      tac_unfold at n name
      
      s <- ctx_term_part_simple <~ [0, 1, 1] #
        term_wout_univs # _thm_stat ref
      s <- pure # term_wout_lams s
      
      dflt <- pure # tac_err_show "tac_pme" s
      
      (target, args) <- pure # split_app s
      case _term_val target of
        TermRef f_name _ ->
          if f_name == namei_tau then tac_tau at mn n e
          else if f_name == namei_mk then do
            tac_unfold at n f_name
            tac_tau at mn n e
          else dflt
        _ -> dflt

tac_on_new_goals :: HCS => Tactic a -> Tactic b -> Tactic ()
tac_on_new_goals tac1 tac2 = do
  is <- ctx_get_new_ids tac1
  tac_work_on_goals is # tac2 >> nop

tac_spec :: HCS => N -> Proof -> Tactic ()
tac_spec i p = do
  mn <- ctx_get_asm_name i
  tac_have_dep i mn p `tac_on_new_goals`
    tac_clear_asm_1 i

tac_push_neg_aux :: HCS => RwMap -> Maybe N -> Tactic Prop
tac_push_neg_aux rw_map at = do
  let (h_num, g) = tac_at_to_h_num_g at
  res <- tac_simp_aux' h_num g rw_map
  return # not # null res

tac_push_neg' :: HCS => RwMap -> Maybe N -> Tactic ()
tac_push_neg' rw_map at = do
  ok <- tac_push_neg_aux rw_map at
  ifmn ok # do
    s <- tac_at_get_stat at
    (f_name, [s]) <- ctx_term_head s
    True <- pure # f_name == namei_not
    name <- ctx_term_head_name s
    tac_unfold_aux at name 0
    tac_push_neg' rw_map at

tac_push_neg :: HCS => Maybe N -> Tactic (Maybe N)
tac_push_neg at = do
  rw_map <- ctx_rw_map_filter namei_not
  tac_at' at (tac_push_neg' rw_map) <|>
    tac_err "push_neg" [dfsc "Failed to simplify"]

tac_let :: HCS => Name -> Name -> Term -> Tactic ()
tac_let h name e = do
  p <- ctx_mk_proof_ref namei_exi_eq
  p <- ctx_univ_e p e
  i <- tac_have (Just h) p
  tac_cases (Right i) [name, h]

tac_trans_find_proof :: HCS => Term -> Term -> [Maybe Proof] -> Tactic Proof
tac_trans_find_proof a b ps = do
  res <- (>>= liftm concat) # mapM <~ ps # \p -> case p of
    Nothing -> pure []
    Just p -> do
      (_, [e1, e2]) <- ctx_term_head # _proof_stat p
      if terms_eq e1 a && terms_eq e2 b then pure [p]
      else if terms_eq e1 b && terms_eq e2 a
        then ctx_proof_symm p >>= liftm pure
        else pure []
  case res of
    [p] -> pure p
    _ -> ctx_mk_proof_mvar'

tac_trans :: HCS => Maybe Term -> Maybe Proof -> Maybe Proof -> Tactic ()
tac_trans e p1 p2 = do
  (rel_name, [a, c]) <- ctx_get_goal_stat >>= ctx_term_head
  b <- case e of
    Just b -> pure b
    Nothing -> do
      Just p1 <- pure p1
      (_, [e1, e2]) <- ctx_term_head # _proof_stat p1
      return # if terms_eq e1 a || terms_eq e1 c then e2 else e1
  ps <- pure [p1, p2]
  p1 <- tac_trans_find_proof a b ps
  p2 <- tac_trans_find_proof b c ps
  Just thm_name <- get_priv_name' namei_trans rel_name
  p <- ctx_mk_proof_ref thm_name
  p <- ctx_univs_e p [a, b, c]
  p <- ctx_imps_e p [p1, p2]
  tac_refine p

tac_trans_term :: HCS => Term -> Tactic ()
tac_trans_term e = tac_trans (Just e) Nothing Nothing

tac_trans_proof :: HCS => Proof -> Tactic ()
tac_trans_proof p = tac_trans Nothing (Just p) Nothing

tac_trans_proofs :: HCS => Proof -> Proof -> Tactic ()
tac_trans_proofs p1 p2 = tac_trans Nothing (Just p1) (Just p2)

-- tac_ite :: HCS => Maybe N -> N -> Tactic (Maybe N)
-- tac_ite at n = tac_at' at # \at -> do
--   -- ifm (isJust at) # err [dfsc "tac_ite"]
--   s <- tac_at_get_stat at
--   let d = _term_depth s
--   let d1 = d + 1
--   (e, f) <- ctx_abstract' (Just n) <~ s # \e -> isJust # do
--     True <- pure # _term_depth e == d1
--     ("ite", args) <- term_head e
--     3 <- pure # len args
--     nop
--   (_, [cnd, a, b]) <- ctx_term_head e
--   p <- ctx_mk_proof_ref "ite_ind"
--   p <- ctx_univs_e p [f, cnd, a, b]
--   i <- ctx_get_avail_proof_id
--   ps <- replicateM 2 # do
--     p <- ctx_mk_proof_mvar'
--     s <- ctx_mk_stat_mvar
--     return # mk_imp_i i s p
--   p <- ctx_imps_e p ps
--   tac_refine p

tac_ite' :: HCS => Name -> N -> Tactic ()
tac_ite' h n = do
  s <- ctx_get_goal_stat
  let d = _term_depth s
  let d1 = d + 1
  (e, f) <- ctx_abstract' (Just n) <~ s # \e -> isJust # do
    True <- pure # _term_depth e == d1
    (f_name, args) <- term_head e
    True <- pure # f_name == namei_ite
    3 <- pure # len args
    nop
  (_, [cnd, a, b]) <- ctx_term_head e
  p <- ctx_mk_proof_ref namei_ite_ind
  p <- ctx_univs_e p [f, cnd, a, b]
  i <- ctx_get_avail_proof_id
  ps <- replicateM 2 # do
    p <- ctx_mk_proof_mvar'
    s <- ctx_mk_stat_mvar
    return # mk_imp_i (Just h) i s p
  p <- ctx_imps_e p ps
  tac_refine p

tac_ite :: HCS => Maybe N -> Name -> N -> Tactic ()
tac_ite at h n = case at of
  Nothing -> tac_ite' h n
  Just i -> do
    mn <- ctx_get_asm_name i
    tac_revert_asm_1 i
    tac_on_new_goals (tac_ite' h n) #
      tac_imp_i_dep mn i

tac_cong' :: HCS => Maybe N -> [Name] -> Term -> Term -> Tactic (Proof, [Name])
tac_cong' depth names lhs rhs = if terms_eq lhs rhs
  then do
    p <- ctx_mk_proof_ref namei_eq_refl
    p <- ctx_univ_e p lhs
    return (p, names)
  else do
    dflt <- pure # case _type_val # _term_type lhs of
      PropT -> do
        p <- ctx_mk_proof_ref namei_eq_of_iff
        p <- ctx_univs_e p [lhs, rhs]
        s <- ctx_mk_term_ref namei_iff
        s <- ctx_mk_apps [s, lhs, rhs]
        p1 <- ctx_mk_proof_mvar s
        p <- ctx_imp_e p p1
        return (p, names)
      _ -> do
        s <- ctx_mk_term_ref namei_eq
        s <- ctx_mk_apps [s, lhs, rhs]
        p <- ctx_mk_proof_mvar s
        return (p, names)
    let (target1, args1) = split_app lhs
    let (target2, args2) = split_app rhs
    let is_lam1 = term_is_lam target1
    let is_lam2 = term_is_lam target2
    if depth == Just 0 then dflt else do
      depth <- pure # depth >>= \depth -> pure # depth - 1
      if is_lam1 || is_lam2 then do
        p <- ctx_mk_proof_ref namei_fn_ext
        p <- ctx_univs_e p [lhs, rhs]
        (name : names) <- pure names
        Lam _ t lhs <- pure # _term_val # eta_expand lhs
        Lam _ _ rhs <- pure # _term_val # eta_expand rhs
        (p1, names) <- with_new_ctx # do
          ctx_enter_lam name t
          tac_cong' depth names lhs rhs
        p1 <- res_proof p1
        p1 <- pure # mk_univ_i' name t p1
        p <- ctx_imp_e p p1
        return (p, names)
      else if not # terms_eq target1 target2 then dflt else do
        p <- ctx_mk_proof_ref namei_eq_refl
        p <- ctx_univ_e p target1
        let args = zip args1 args2
        (\f -> foldM f (p, names) args) # \(p, names) (a, b) -> do
          p1 <- ctx_mk_proof_ref namei_cong
          es <- replicateM 2 ctx_mk_term_mvar
          p1 <- ctx_univs_e p1 # es ++ [a, b]
          (p_arg, names) <- tac_cong' depth names a b
          p1 <- ctx_imps_e p1 [p, p_arg]
          return (p1, names)

tac_cong :: HCS => Maybe N -> [Name] -> Tactic ()
tac_cong depth names = do
  s <- ctx_get_goal_stat
  (rel_name, [lhs, rhs]) <- ctx_term_head s
  ifm (rel_name /= namei_eq) # do
    rel <- ctx_mk_term_ref rel_name
    p <- ctx_mk_proof_ref namei_refl_of_eq
    p <- ctx_univs_e p [rel, lhs, rhs]
    Just name' <- get_priv_name' rel_name namei_refl
    refl_rel <- ctx_mk_proof_ref name'
    p1 <- ctx_mk_proof_mvar'
    p <- ctx_imps_e p [refl_rel, p1]
    tac_refine p
  (p, names) <- tac_cong' depth names lhs rhs
  ifmn (null names) # tac_err "cong" #
    (dfsc "Superfluos identifier names provided: " :) #
    intersperse (dfsc ", ") # map (mk_pair "term") names
  tac_refine p

ctx_get_tcs :: HCS => Tactic RwMap
ctx_get_tcs = do
  tcs <- gets_ctx _ctx_tcs
  case tcs of
    Just tcs -> pure tcs
    Nothing -> do
      is <- gets_ctx # map snd . _ctx_asms
      (\f -> foldM f trie_empty is) # \tcs i -> do
        let p_info = Right (-1)
        -- let p_info = Right undefined
        p <- ctx_mk_proof_var i
        (pat, info) <- rw_norm_pat False p_info p
        -- info <- pure # info {_rw_proof = Nothing}
        return # trie_list_insert pat info tcs

ctx_all_imps_e_tc :: HCS => Proof -> Tactic Proof
ctx_all_imps_e_tc p = do
  let s = _proof_stat p
  case term_to_imp s of
    Just (s1, s2) -> do
      p1 <- tac_tc_synth s1
      p <- ctx_imp_e p p1
      ctx_all_imps_e_tc p
    Nothing -> pure p

tac_res_stat_using_asms :: HCS => Stat -> Tactic Stat
tac_res_stat_using_asms s = do
  s <- res_term s
  if e_res s then pure s else do
    hs <- ctx_get_proof_list
    ctx_ors # map <~ hs # \(hi, s') -> do
      add_term_eq s s'
      return s'

tac_res_proof_tc :: HCS => Proof -> Tactic Proof
tac_res_proof_tc p = do
  asgns <- ctx_query # do
    tac_have Nothing p
    gets_ctx _ctx_term_asgns
  modify_ctx # \ctx -> ctx {_ctx_term_asgns = asgns}

  -- (type_asgns, term_asgns) <- ctx_query # do
  --   tac_have p
  --   type_asgns <- gets_ctx _ctx_type_asgns
  --   term_asgns <- gets_ctx _ctx_term_asgns
  --   return (type_asgns, term_asgns)
  -- modify_ctx # \ctx -> ctx
  --   { _ctx_type_asgns = type_asgns
  --   , _ctx_term_asgns = term_asgns
  --   }
  
  p <- res_proof p
  return p

tac_tc_fn :: HCS => RwMatch -> [N] {--> [Term]-} -> Proof -> Tactic Proof
tac_tc_fn match@(match_t, match_e) is {-tcs-} p = do
  let s = _proof_stat p
  case term_to_univ_or_imp s of
    Just (Left (t, s)) -> do
      (i : is) <- pure is
      Just e <- pure # map_get i match_e
      p <- ctx_univ_e p e
      tac_tc_fn match is {-tcs-} p
    Just (Right (s1, s2)) -> do
      p1 <- tac_tc_synth s1
      p <- ctx_imp_e p p1
      tac_tc_fn match is {-tcs-} p
    Nothing -> pure p

tac_tc_synth' :: HCS => RwMap -> Stat -> Tactic Proof
tac_tc_synth' tcs s = do
  xs <- rw_match_exact tcs s
  -- tracem xs
  ctx_ors # map <~ xs # \(info, match) -> do
    p <- case _rw_proof_info info of
      Left name -> ctx_mk_proof_ref name
      Right i -> do
        -- ctx_mk_proof_var i
        Just p <- pure # _rw_proof info
        return p
    p <- pure # subst_type_mvars_in_proof (fst match) p
    tac_tc_fn match (_rw_terms info) p

tac_tc_synth :: HCS => Stat -> Tactic Proof
tac_tc_synth s = do
  -- s <- tac_res_stat_using_asms s
  -- ifm_dbg # ctx_trace_term s
  -- s <- res_term s
  ifmn (e_res s) # do
    s1 <- show_term s
    ctx_trace #
      [ dfsc "[tac_tc_synth] ", dfsc "Statement not resolved\n\n"
      , dfsc # indent' 1] ++ s1
  tcs1 <- ctx_get_tcs
  tcs2 <- gets_th _th_tcs
  p <- tac_tc_synth' tcs1 s <|> tac_tc_synth' tcs2 s
  return p

tac_tc' :: HCS => Stat -> Tactic ()
tac_tc' s = do
  res <- ctx_try # tac_tc_synth s
  case res of
    Right p -> tac_refine p
    Left msg -> do
      -- s <- ctx_get_goal_stat
      s1 <- show_term s
      tac_err "tc" #
        [ dfsc "Unable to synthesize type class instance for\n\n"
        , dfsc # indent' 1] ++ s1
        -- ++ [dfsc "\n\n"] ++ msg

tac_tc :: HCS => Tactic ()
tac_tc = do
  s <- ctx_get_goal_stat
  tac_tc' s

tac_tc_adv :: HCS => Maybe Stat -> Tactic ()
tac_tc_adv s = case s of
  Nothing -> tac_tc
  Just s -> do
    p <- tac_tc_synth s
    tac_have Nothing p >> nop

-- tac_ind_filter_asms' :: HCS => N -> [(N, Stat)] -> [(N, Stat)]
-- tac_ind_filter_asms' i hs = filter <~ hs # \(_, s) -> let
--   (target, args) = split_app s
--   check = \e -> case _term_val e of
--     TermVar j -> j == i
--     _ -> False
--   in case _term_val target of
--     TermRef f_name _ ->
--       if f_name == namei_mem then check # args !! 0
--       else case lastm args of
--         Just e -> check e
--         Nothing -> False
--     _ -> False
-- 
-- tac_ind_filter_asms :: HCS => N -> Tactic [(N, Stat)]
-- tac_ind_filter_asms i = do
--   hs <- ctx_get_proof_list
--   return # tac_ind_filter_asms' i hs
-- 
-- tac_ind_intro' :: HCS => [N] -> [Name] -> Tactic [Name]
-- tac_ind_intro' deps names = do
--   s <- ctx_get_goal_stat
--   case term_to_univ s of
--     Nothing -> do
--       -- let imps_num = term_calc_imps_and_tc_univs_num s
--       let imps_num = term_calc_imps_num s
--       tac_imps_i_n # imps_num - len deps
--       mapM_ <~ deps # \hi -> tac_imps_i_n_aux (Just hi) (Just 1)
--       -- mapM_ <~ deps # \hi -> tac_imps_i_n_aux Nothing (Just 1)
--       return names
--     Just _ -> case names of
--       [] -> pure []
--       (name : names') -> do
--         tac_univ_i name
--         tac_ind_intro' deps names'
-- 
-- tac_ind_intro :: HCS => [N] -> [N] -> [Name] -> Tactic ()
-- tac_ind_intro ids deps names = do
--   -- ifm_dbg # do
--   --   tracem ids
--   --   tracem deps
--   --   tracem names
--   case ids of
--     [] -> nop
--     (i : ids') -> case names of
--       [] -> nop
--       _ -> do
--         names <- tac_work_on_goal i # tac_ind_intro' deps names
--         tac_ind_intro ids' deps names
-- 
-- tac_ind' :: HCS => Prop -> LocIdent -> [Name] -> Tactic ()
-- tac_ind' is_ind ident names = do
--   hs <- ctx_get_proof_list
--   names <- if not # null names then pure names else case ident of
--     Left i -> do
--       mp <- gets_ctx # map_inv . _ctx_term_names
--       return # maybe_to_list # map_get i mp
--     _ -> pure []
--   -- ifm_dbg # do
--   --   ctx_trace_goals
--   --   tracem is_ind
--   --   tracem ident
--   --   tracem names
--   ctx_ors # map <~ hs # \(hi, s) -> do
--     (fn_name, args) <- ctx_term_head s
--     if fn_name == namei_mem then do
--       [x, y] <- pure args
--       TermVar i <- pure # _term_val x
-- 
--       case ident of
--         Left i' -> do
--           True <- pure # i' == i
--           nop
--         _ -> nop
-- 
--       Just (deps, _) <- ctx_calc_deps_for_ident i
--       deps' <- pure # filter (hi /=) deps
--       deps <- pure # hi : deps'
-- 
--       -- asms <- mapM <~ deps # \i -> do
--       --   s <- ctx_get_proof_var i
--       --   return (i, s)
-- 
--       -- ifm_dbg # do
--       --   ctx_trace_goals
--       --   tracem deps
-- 
--       tac_revert # map Right deps
-- 
--       -- p <- ctx_mk_proof_ref "of_tc_univ"
--       -- p <- ctx_all_univs_e p
--       -- p1 <- ctx_mk_proof_mvar'
--       -- p <- ctx_imp_e p p1
--       -- tac_refine p
-- 
--       tac_revert [Left i]
-- 
--       let (y_target, y_args) = split_app y
-- 
--       case _term_val y_target of
--         TermRef set_name _ -> do
--           let f = ite is_ind _schemes_ind _schemes_cs
--           thm_name <- th_get_scheme f set_name
-- 
--           p <- ctx_mk_proof_ref thm_name
-- 
--           s <- ctx_get_goal_stat
--           t <- ctx_mk_type_mvar
--           f <- ctx_term_part_simple s [1, 0, 1]
--           f <- pure # mk_lam t f
--           
--           p <- ctx_univ_e p f
-- 
--           -- tracem "<<<"
-- 
--           -- ctx_trace_term y
--           -- tracem # len y_args
--           -- ctx_trace_proof_stat p
--           
--           p <- (\f -> foldM f p y_args) # \p _ -> do
--             e <- ctx_mk_term_mvar
--             ctx_univ_e p e
--           -- ctx_trace_proof_stat p
-- 
--           p <- ctx_all_imps_e p
--           -- ctx_trace_proof_stat p
-- 
--           -- tracem ">>>"
-- 
--           -- tracem "a"
--           ids <- ctx_get_new_ids # tac_refine p
--           -- tracem "b"
-- 
--           tac_ind_intro ids deps names
--         TermVar tc_i -> do
--           ctx_ors # map <~ hs # \(hj, s_tc) -> do
--             tc_name <- ctx_term_head_name s_tc
-- 
--             let f = ite is_ind _schemes_ind_tc _schemes_cs_tc
--             tc_thm <- th_get_scheme f tc_name
-- 
--             p <- ctx_mk_proof_ref tc_thm
--             p <- ctx_all_univs_e p
--             p1 <- ctx_mk_proof_var hj
--             p <- ctx_imp_e p p1
-- 
--             s <- ctx_get_goal_stat
-- 
--             t <- ctx_mk_type_mvar
--             f <- ctx_term_part_simple s [1, 0, 1]
--             f <- pure # mk_lam t f
-- 
--             p <- ctx_univ_e p f
--             p <- ctx_all_imps_e p
-- 
--             ids <- ctx_get_new_ids # tac_refine p
-- 
--             tac_ind_intro ids deps names
--         _ -> tac_err "tac_ind" [dfsc "[2]"]
--     else do
--       fn_name <- id_to_name fn_name
--       tac_err "tac_ind" [dfsc "[1] ", ("term", fn_name)]
-- 
-- tac_ind :: HCS => LocIdent -> [Name] -> Tactic ()
-- tac_ind = tac_ind' True

-----

tac_dbg :: HCS => Tactic ()
tac_dbg = enter_debug_mode

tac_test :: HCS => Tactic ()
tac_test = do
  nop

-----

itac_refine :: HCS => Tactic ()
itac_refine = do
  s <- ctx_get_goal_stat
  p <- tac_parse_proof s
  tac_refine p

itac_exact :: HCS => Tactic ()
itac_exact = do
  p <- tac_parse_proof'
  tac_exact p

itac_apply :: HCS => Tactic ()
itac_apply = do
  p <- tac_parse_proof'
  tac_on_new_goals (tac_apply p) tac_try_triv_or_asm_ex

tac_intro_term :: HCS => Tactic TacVar
tac_intro_term = do
  name <- parse_term_var_name
  tac_univ_i name
  return # TVar name

tac_intro_proof :: HCS => Tactic TacVar
tac_intro_proof = do
  name <- parse_proof_var_name
  tac_imp_i name
  return # PVar name

itac_intro :: HCS => Tactic ()
itac_intro = do
  xs <- p_all # choice [tac_intro_term, tac_intro_proof]
  recon_push_1 # TacIntro xs

itac_clear :: HCS => Tactic ()
itac_clear = do
  idents <- p_all' ctx_parse_ident
  tac_clear idents

itac_revert :: HCS => Tactic ()
itac_revert = do
  idents <- p_all' ctx_parse_ident
  tac_revert idents

itac_unfold :: HCS => Tactic ()
itac_unfold = do
  n <- p_nat'
  names <- p_all parse_term_ref_id
  at <- tac_parse_at
  tac_ats at # \at -> (\f -> foldM f at names) # \at name ->
    tac_unfold at n name

parse_rw_arg_term :: HCS => Elab Proof
parse_rw_arg_term = do
  rev <- p_prop # p_tok "←"
  name <- ctx_parse_term_ref_id
  ps <- parse_type_params
  p <- ctx_ors # map <~ [namei_unf, namei_def] # \pname -> do
    Just thm_name <- get_priv_name' pname name
    ctx_mk_proof_ref' thm_name ps
  if rev then ctx_proof_symm p else pure p

parse_rw_arg_proof :: HCS => Elab Proof
parse_rw_arg_proof = do
  p <- tac_parse_proof'
  -- ifm_dbg # do
  --   p <- res_proof p
  --   ctx_trace_proof_stat p
  proof_mvars_to_univs p

parse_rw_arg :: HCS => Elab Proof
parse_rw_arg = parse_rw_arg_term <++ parse_rw_arg_proof

parse_rw_args :: HCS => Elab [Proof]
parse_rw_args = (pure [] ++>) # p_list' parse_rw_arg

itac_rw_or_rwa :: HCS => Prop -> Tactic ()
itac_rw_or_rwa asm = do
  n <- p_nat'
  ps <- parse_rw_args
  at <- tac_parse_at
  tac_ats at # \at -> tac_rw at (Just n) ps
  if asm then tac_asm else tac_try_triv_or_refl

itac_rw :: HCS => Tactic ()
itac_rw = do
  itac_rw_or_rwa False

itac_rwa :: HCS => Tactic ()
itac_rwa = do
  itac_rw_or_rwa True

itac_simp_or_simpa :: HCS => Prop -> Tactic ()
itac_simp_or_simpa asm = do
  shw <- p_prop # p_tok "?"
  only <- p_prop # p_scope "tactic" # parse_word "only"
  ps <- parse_rw_args
  at <- tac_parse_at
  tac_ats at # \at -> tac_simp shw only at ps
  if asm then tac_asm else tac_try_triv_or_refl

itac_simp :: HCS => Tactic ()
itac_simp = itac_simp_or_simpa False

itac_simpa :: HCS => Tactic ()
itac_simpa = itac_simp_or_simpa True

itac_subst :: HCS => Tactic ()
itac_subst = do
  at <- tac_parse_at'
  is <- pure # map fromJust # filter isJust at
  False <- pure # null is
  tac_subst_adv is

itac_subst' :: HCS => Tactic ()
itac_subst' = do
  at <- tac_parse_at'
  is <- pure # map fromJust # filter isJust at
  False <- pure # null is
  tac_subst' is

itac_tau :: HCS => Tactic ()
itac_tau = do
  n <- p_nat'
  x <- tac_parse_term
  at <- tac_parse_at
  mn <- tac_parse_with_1'
  tac_ats at # \at -> tac_tau at mn n x

itac_the :: HCS => Tactic ()
itac_the = do
  n <- p_nat'
  x <- tac_parse_term
  at <- tac_parse_at
  mn <- tac_parse_with_1'
  tac_ats at # \at -> tac_the at mn n x

itac_refl :: HCS => Tactic ()
itac_refl = do
  tac_at_goal tac_refl

itac_triv :: HCS => Tactic ()
itac_triv = do
  tac_at_goal tac_triv

itac_cases_have :: HCS => Tactic ()
itac_cases_have = do
  p <- tac_parse_proof'
  names <- tac_parse_with
  tac_cases_have (Just "_itac_cases_have") p names

itac_rcases_have :: HCS => Tactic ()
itac_rcases_have = do
  p <- tac_parse_proof'
  names <- tac_parse_with
  tac_rcases_have (Just "_itac_rcases_have") p names

itac_cases :: HCS => Tactic ()
itac_cases = (<|> itac_cases_have) # do
  idents <- p_all ctx_parse_ident
  names <- tac_parse_with
  
  parse_itac_end
  mapM_ <~ idents # \ident -> do
    -- names <- case ident of
    --   Left _ -> pure names
    --   Right i -> do
    --     name <- get_asm_name i
    --     return # snoc names name
    
    tac_cases ident names

itac_rcases :: HCS => Tactic ()
itac_rcases = (<|> itac_rcases_have) # do
  ident <- ctx_parse_ident
  names <- tac_parse_with
  
  -- names <- case ident of
  --   Left _ -> pure names
  --   Right i -> do
  --     name <- get_asm_name i
  --     return # snoc names name
  
  parse_itac_end
  tac_rcases ident names

itac_split :: HCS => Tactic ()
itac_split = do
  mn <- tac_parse_with_1'
  tac_split mn

itac_exfalso :: HCS => Tactic ()
itac_exfalso = do
  tac_exfalso

itac_contra :: HCS => Tactic ()
itac_contra = do
  p1 <- tac_parse_proof_1'
  p2 <- tac_parse_proof_1'
  tac_contra p1 p2

itac_by_contra :: HCS => Tactic ()
itac_by_contra = do
  name <- tac_parse_with_1
  tac_by_contra name

itac_by_contra' :: HCS => Tactic ()
itac_by_contra' = do
  name <- tac_parse_with_1
  tac_by_contra' name

itac_cpos :: HCS => Tactic ()
itac_cpos = do
  name <- parse_proof_index
  tac_cpos name

itac_left :: HCS => Tactic ()
itac_left = do
  tac_left
  (>> nop) # p_maybe # do
    s <- ctx_get_goal_stat
    p <- tac_parse_proof s
    tac_refine p

itac_right :: HCS => Tactic ()
itac_right = do
  tac_right
  (>> nop) # p_maybe # do
    s <- ctx_get_goal_stat
    p <- tac_parse_proof s
    tac_refine p

itac_asm :: HCS => Tactic ()
itac_asm = do
  tac_asm

itac_have_stat :: HCS => Tactic ()
itac_have_stat = do
  s <- tac_parse_stat
  name <- tac_parse_with_1
  tac_have_stat (Just name) s

itac_have_proof :: HCS => Tactic ()
itac_have_proof = do
  p <- tac_parse_proof'
  name <- tac_parse_with_1
  tac_have' (Just name) p

itac_have :: HCS => Tactic ()
itac_have = do
  -- s' <- ctx_mk_stat_mvar ++> do
  --   p_maybe # p_tok ":"
  --   res <- try # tac_parse_stat
  --   case res of
  --     Right s -> pure s
  --     _ -> pfail
  -- p <- ctx_mk_proof_mvar s' ++> do
  --   p_tok ":="
  --   tac_parse_proof s'
  -- tac_have' p
  itac_have_stat <|> itac_have_proof

-- itac_suffices_stat :: HCS => Name -> Tactic ()
-- itac_suffices_stat name = do
--   s <- tac_parse_stat
--   tac_suffices_stat (Just name) s

-- itac_suffices_proof :: HCS => Name -> Tactic ()
-- itac_suffices_proof name = do
--   p <- tac_parse_proof'
--   tac_suffices' (Just name) p

itac_suffices :: HCS => Tactic ()
itac_suffices = do
  -- name <- parse_proof_var_name
  -- itac_suffices_stat name <|> itac_suffices_proof name
  s <- tac_parse_stat
  name <- tac_parse_with_1
  tac_suffices_stat (Just name) s

itac_elim :: HCS => Tactic ()
itac_elim = do
  s <- ctx_mk_term_ref namei_false
  p <- tac_parse_proof s
  tac_elim p

itac_use_fn :: HCS => Tactic ()
itac_use_fn = do
  f <- pure # do
    tac_norm_goal
    itac_use_fn
  (<|> f) # do
    s <- ctx_get_goal_stat
    (target, args) <- pure # split_app s
    TermRef name ps <- pure # _term_val target
    if name == namei_and then do
      p <- tac_parse_proof'
      tac_use' undefined p
    else if name == namei_exi then do
      x <- tac_parse_term
      tac_use x
    else if name == namei_exiu then do
      x <- tac_parse_term
      tac_use x
    else if name == namei_nempty then do
      x <- tac_parse_term
      tac_use x
    else pfail

itac_use :: HCS => Tactic ()
itac_use = do
  p_list' # (<|> itac_use_fn) # do
    p <- tac_parse_proof'
    tac_apply p
  tac_try_triv_or_refl

itac_use_proof_fn' :: HCS => Term -> Proof -> Tactic ()
itac_use_proof_fn' x p = do
  tac_use' x p
  tac_try_triv_or_refl

itac_use_proof_fn :: HCS => Term -> Proof -> Tactic ()
itac_use_proof_fn x p = itac_use_proof_fn' x p <|> do
  tac_norm_goal
  itac_use_proof_fn x p

itac_use_proof :: HCS => Tactic ()
itac_use_proof = do
  x <- ctx_mk_term_mvar
  p <- tac_parse_proof'
  itac_use_proof_fn x p

itac_symm :: HCS => Tactic ()
itac_symm = do
  at <- tac_parse_at
  mapM_ tac_symm at

itac_comm :: HCS => Tactic ()
itac_comm = do
  n <- p_nat'
  at <- tac_parse_at
  mapM_ (tac_comm <~ n) at

itac_iter :: HCS => Tactic ()
itac_iter = do
  n <- p_nat
  ctx_iter n parse_itac

itac_work_on_goal' :: HCS => N -> Tactic () -> Tactic ()
itac_work_on_goal' i tac = tac_work_on_goal i tac

-- itac_work_on_goals' :: HCS => [N] -> Tactic () -> Tactic ()
-- itac_work_on_goals' is tac = case is of
--   [] -> nop
--   (i : is) -> do
--     st <- plift get
--     itac_work_on_goal' i tac
--     mapM_ <~ is # \i -> do
--       plift # put st
--       itac_work_on_goal' i tac

itac_work_on_goals' :: HCS => [N] -> Tactic () -> Tactic ()
itac_work_on_goals' is tac = do
  st <- plift get
  mapM_ <~ is # \i -> do
    plift # put st
    itac_work_on_goal' i tac

itac_work_on_goal :: HCS => Tactic ()
itac_work_on_goal = do
  i <- p_nat
  ids <- elab_get_ctx_ids
  itac_work_on_goal' (fromJust # list_get i ids) parse_itacs

itac_work_on_goals :: HCS => Tactic ()
itac_work_on_goals = do
  is <- p_list' p_nat
  ids <- elab_get_ctx_ids
  let ids' = map (\i -> fromJust # list_get i ids) is
  itac_work_on_goals' ids' parse_itacs

itac_move_goal :: HCS => Tactic ()
itac_move_goal = do
  i <- p_nat
  j <- p_nat
  tac_move_goal i j

itac_by_cases :: HCS => Tactic ()
itac_by_cases = do
  s <- tac_parse_stat
  name <- tac_parse_with_1
  tac_by_cases name s

itac_ext :: HCS => Tactic ()
itac_ext = do
  names <- p_all parse_term_var_name
  hs <- tac_parse_with
  tac_ext names # if null hs
    then [Nothing] else map Just hs

itac_pme :: HCS => Tactic ()
itac_pme = do
  n <- p_nat'
  name <- parse_term_ref_id
  at <- tac_parse_at
  [at] <- pure at
  e <- tac_parse_term
  mn <- tac_parse_with_1'
  tac_pme at mn n name e

itac_spec :: HCS => Tactic ()
itac_spec = do
  p <- tac_parse_proof'

  -- ctx_trace_proof_stat p
  -- s <- p_query p_rest
  -- ctx_trace [dfsc s]
  
  -- ifm_dbg # ctx_trace_proof_stat p
  -- ifm_dbg # tracem # _proof_target_asm p
  Just i <- pure # _proof_target_asm p
  -- ifm_dbg # tracem i
  tac_spec i p

itac_push_neg :: HCS => Tactic ()
itac_push_neg = do
  at <- tac_parse_at
  tac_ats at tac_push_neg

itac_let :: HCS => Tactic ()
itac_let = do
  name <- parse_term_var_name
  p_tok ":="
  e <- tac_parse_term
  h <- tac_parse_with_1
  tac_let h name e

itac_norm :: HCS => Tactic ()
itac_norm = do
  at <- tac_parse_at'
  mapM_ tac_norm at

itac_trans_term :: HCS => Tactic ()
itac_trans_term = do
  e <- tac_parse_term
  tac_trans_term e

itac_trans_asm :: HCS => Tactic ()
itac_trans_asm = do
  i <- parse_proof_index
  p <- ctx_mk_proof_var i
  tac_trans_proof p

itac_trans_proof :: HCS => Tactic ()
itac_trans_proof = itac_trans_asm <|> do
  -- ps <- p_all parse_proof_var
  -- case ps of
  --   [p] -> tac_trans_proof p
  --   [p1, p2] -> tac_trans_proofs p1 p2
  p <- tac_parse_proof'
  tac_trans_proof p

itac_trans :: HCS => Tactic ()
itac_trans = itac_trans_term <|> itac_trans_proof

itac_ite :: HCS => Tactic ()
itac_ite = do
  n <- p_nat'
  at <- tac_parse_at
  name <- tac_parse_with_1
  tac_ats at # \at -> tac_ite at name n

itac_cong :: HCS => Tactic ()
itac_cong = do
  depth <- p_maybe p_nat
  names <- p_all' parse_term_var_name
  tac_cong depth names

itac_tc :: HCS => Tactic ()
itac_tc = do
  s <- p_maybe tac_parse_stat
  tac_tc_adv s

-- itac_ind :: HCS => Tactic ()
-- itac_ind = do
--   ident <- ctx_parse_ident
--   names <- tac_parse_with
--   tac_ind ident names

itac_imp_e :: HCS => Tactic ()
itac_imp_e = do
  s2 <- ctx_get_goal_stat
  s1 <- tac_parse_stat
  s12 <- ctx_mk_imp s1 s2
  p <- ctx_mk_proof_mvar s12
  p1 <- ctx_mk_proof_mvar s1
  p <- ctx_imp_e p p1
  tac_refine p

itac_univ_e :: HCS => Tactic ()
itac_univ_e = do
  f <- tac_parse_term_1
  x <- tac_parse_term_1
  s <- ctx_mk_univ f
  p <- ctx_mk_proof_mvar s
  p <- ctx_univ_e p x
  tac_refine p

-----

itac_dbg :: HCS => Tactic ()
itac_dbg = do
  parse_word "dbg"
  tracem "DEBUG"
  -- tac_dbg

itac_test :: HCS => Tactic ()
itac_test = do
  parse_word "test"
  tac_test

parse_itac_1 :: HCS => Tactic ()
parse_itac_1 = do
  n <- get_ctxs_num
  ctx0 <- get_ctx'
  pt <- plift get
  itac_special <++ do
    pt <- plift get
    name <- parse_tac_name
    ifm (n == 0) # do
      plift # put pt
      s <- p_rest
      err [dfsc "No more goals\n\n", dfsc s]
      -- error "No more goals"
    ifm (n /= 1) # err [dfsc "Multiple goals"]
    -- putStrLn name
    
    tacs <- ifm_recon ctx_get_tacs
    
    case name of
      "sorry" -> do
        plift # put pt
        itac_sorry
      _ -> case map_get name itac_parsers of
        Nothing -> err [dfsc "Unknown tactic ", ("tactic", name)]
        Just itac -> do
          itac <++ tac_err' name
    
    ifm_recon # do
      tacs1 <- ctx_get_tacs
      ifm (tacs1 == tacs) # do
        s <- ctx_show_tacs
        err
          [ dfsc "No reconstruction info for tactic "
          , ("tactic", name)
          , dfsc "\n\n"
          , dfsc s
          ]
    
  Just ctx0 <- pure ctx0

  -- tracem # Lit []
  -- fn <- pure # do
  --   get_ctxs >>= \ctxs -> tracem # map _ctx_asms ctxs
  --   get_ctxs >>= \ctxs -> tracem # map _ctx_asm_deps ctxs
  --   get_ctxs >>= \ctxs -> tracem # map _ctx_proofs_hidden ctxs
  -- fn

  elab_norm ctx0

  -- tracem # Lit ["---"]
  -- fn

parse_itac :: HCS => Tactic ()
parse_itac = do
  ids <- elab_get_ctx_ids
  parse_itac_1
  tok <- p_maybe # p_tok ";"
  case tok of
    Nothing -> nop
    Just _ -> do
      ids1 <- elab_get_ctx_ids
      itac_work_on_goals' (ids1 \\ ids) parse_itac

parse_itacs :: HCS => Tactic ()
parse_itacs = parse_itac_1 ++>
  (p_list_braces True False True parse_itac >> nop)

parse_itac_sep :: HCS => Tactic ()
parse_itac_sep = do
  p_all' itac_comment
  p_tok ","
  p_all' itac_comment
  nop

itac_comment :: HCS => Tactic ()
itac_comment = do
  p_tok "--"
  p_take_while # \c -> c /= '\r' && c /= '\n'
  nop

itac_sorry :: HCS => Tactic ()
itac_sorry = do
  "sorry" <- p_scope "sorry" parse_name
  s <- ctx_get_goal_stat
  p <- ctx_mk_proof_ref namei_sorry
  p <- ctx_univ_e p s
  tac_refine p

itac_display_goals :: HCS => Tactic ()
itac_display_goals = do
  p_tok "|"
  
  -- asms <- gets_ctx _ctx_asms
  -- tracem asms
  
  ctx_display_goals

itac_group :: HCS => Tactic ()
itac_group = do
  p_tok "("
  p_sep' parse_itac parse_itac_sep
  p_maybe parse_itac_sep
  p_tok ")"
  nop

itac_block_wout_open_brace :: HCS => Tactic ()
itac_block_wout_open_brace = do
  pst <- get_pst
  (ctx : ctxs) <- pure # _pst_ctxs pst
  put_pst # init_pst ctx
  p_sep' parse_itac parse_itac_sep
  p_maybe parse_itac_sep
  p_tok "}" <++ p_eof
  p <- ctx_pst_get_proof
  put_pst # ProofState
    { _pst_ctxs = ctxs
    , _pst_fn = \ps -> _pst_fn pst # p : ps
    }

itac_block :: HCS => Tactic ()
itac_block = do
  p_tok "{"
  itac_block_wout_open_brace

itac_repeat :: HCS => Tactic ()
itac_repeat = do
  p_tok "*"
  ctx_repeat parse_itac

itac_close_goal :: HCS => Tactic ()
itac_close_goal = do
  p_tok "."
  tac_close_goal parse_itac

parse_itac_end :: HCS => Tactic ()
parse_itac_end = do
  True <- p_query # p_prop # choice
    [ trimmed p_eof
    , p_tok ","
    , p_tok ";"
    , p_tok "}"
    ]
  nop

-----

itac_special :: HCS => Tactic ()
itac_special = choice
  [ itac_dbg
  , itac_test
  -- , itac_sorry
  , itac_display_goals
  , itac_group
  , itac_block
  , itac_repeat
  , itac_close_goal
  ]

itac_parsers :: HCS => Map Name (Tactic ())
itac_parsers = Map.fromList
  [ ("refine", itac_refine)
  , ("ex", itac_exact)
  , ("ap", itac_apply)
  , ("in", itac_intro)
  , ("clear", itac_clear)
  , ("revert", itac_revert)
  , ("unf", itac_unfold)
  , ("rw", itac_rw)
  , ("rwa", itac_rwa)
  , ("simp", itac_simp)
  , ("simpa", itac_simpa)
  , ("subst", itac_subst)
  , ("subst'", itac_subst')
  , ("tau", itac_tau)
  , ("the", itac_the)
  , ("refl", itac_refl)
  , ("triv", itac_triv)
  , ("cs", itac_cases)
  , ("csr", itac_rcases)
  , ("sp", itac_split)
  , ("exfalso", itac_exfalso)
  , ("contra", itac_contra)
  , ("byc", itac_by_contra)
  , ("byc'", itac_by_contra')
  , ("cpos", itac_cpos)
  , ("left", itac_left)
  , ("right", itac_right)
  , ("asm", itac_asm)
  , ("hv", itac_have)
  , ("sfc", itac_suffices)
  , ("elim", itac_elim)
  , ("use", itac_use)
  , ("use'", itac_use_proof)
  , ("symm", itac_symm)
  , ("comm", itac_comm)
  , ("iter", itac_iter)
  , ("work_on_goal", itac_work_on_goal)
  , ("work_on_goals", itac_work_on_goals)
  , ("move_goal", itac_move_goal)
  , ("by_cases", itac_by_cases)
  , ("ext", itac_ext)
  , ("pme", itac_pme)
  , ("spec", itac_spec)
  , ("pneg", itac_push_neg)
  , ("let", itac_let)
  , ("nm", itac_norm)
  , ("trans", itac_trans)
  , ("ite", itac_ite)
  , ("cong", itac_cong)
  , ("tc", itac_tc)
  -- , ("ind", itac_ind)
  , ("imp_e", itac_imp_e)
  , ("univ_e", itac_univ_e)
  ]