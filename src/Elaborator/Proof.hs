module Elaborator.Proof where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Maybe
import Data.Either
import Data.List
import Data.Char
import Control.Monad
import Control.Monad.State
import Prelude hiding (print, putStrLn)

import Util
import Sctr
import Bit
import Tree
import Trie
import Parser

import Elaborator.Data
import Elaborator.Util
import Elaborator.Parser
import Elaborator.Type
import Elaborator.Term
import Elaborator.Goal

import qualified Kernel as K

proof_subst_proof_mvars_list' :: HCS => Proof -> State [Proof] Proof
proof_subst_proof_mvars_list' p = do
  let s0 = _proof_stat p
  case _proof_val p of
    ProofMvar is_tc i -> do
      (p : ps) <- get
      put ps
      return p
    ImpI mn i0 i s p -> do
      p <- proof_subst_proof_mvars_list' p
      return # mk_imp_i_aux mn i0 i s p
    UnivI mn t p -> do
      p <- proof_subst_proof_mvars_list' p
      return # mk_univ_i_aux mn t p
    ImpE p1 p2 -> do
      p1 <- proof_subst_proof_mvars_list' p1
      p2 <- proof_subst_proof_mvars_list' p2
      return # mk_imp_e s0 p1 p2
    UnivE p e -> do
      p <- proof_subst_proof_mvars_list' p
      return # mk_univ_e s0 p e
    WithStat p -> error ""
    ProofClear e_names p_names p -> do
      p <- proof_subst_proof_mvars_list' p
      return # mk_proof_clear e_names p_names p
    _ -> pure p

proof_subst_proof_mvars_list :: HCS => [Proof] -> Proof -> Proof
proof_subst_proof_mvars_list ps p = evalState
  (proof_subst_proof_mvars_list' p) ps

-- ctx_proof_prepare_app :: HCS => Proof -> Elab Proof
-- ctx_proof_prepare_app p = if _proof_expl p then pure p else do
--   p <- ctx_all_univs_e p
--   return p

-----

ctx_univ_e' :: HCS => Type -> Term -> Proof -> Term -> Elab Proof
ctx_univ_e' t f p e = do
  add_type_eq t # _term_type e
  (f, e) <- pure # lift_terms f e
  s <- ctx_mk_app f e
  s <- res_term s
  p <- res_proof p
  e <- res_term e
  return # mk_univ_e s p e

ctx_univ_e :: HCS => Proof -> Term -> Elab Proof
ctx_univ_e p e = do
  p <- res_proof p
  p <- pure # lift_proof_to (_term_depth e) p
  (t, f) <- case term_to_univ # _proof_stat p of
    Just res -> pure res
    Nothing -> tac_err_show "ctx_univ_e" # _proof_stat p
  p <- ctx_univ_e' t f p e
  return p

ctx_univs_e :: HCS => Proof -> [Term] -> Elab Proof
ctx_univs_e = foldM ctx_univ_e

ctx_imp_e' :: HCS => Prop -> Stat -> Stat -> Proof -> Proof -> Elab Proof
ctx_imp_e' is_tc s1 s2 p p' = do
  -- sa <- fmap (>>= snd) # ctx_show s1
  -- sb <- fmap (>>= snd) # ctx_show # _proof_stat p'
  -- putStrLn sa
  -- putStrLn sb
  -- putStrLn ""
  
  add_term_eq s1 # _proof_stat p'

  s2 <- res_term s2
  p <- res_proof p
  p' <- res_proof p'

  let expl = _proof_expl p

  p <- if not is_tc then pure p else do
    p1 <- ctx_mk_proof_ref namei_of_tc_univ
    p1 <- ctx_univs_e p1 [s1, s2]
    ctx_imp_e p1 p

  p <- pure # mk_imp_e s2 p p'
  return # if is_tc then p {_proof_expl = expl} else p

ctx_imp_e :: HCS => Proof -> Proof -> Elab Proof
ctx_imp_e p p' = do
  p <- res_proof p
  p' <- res_proof p'
  
  (p, p') <- pure # lift_proofs p p'

  -- let s1 = _proof_stat p
  -- let s2 = _proof_stat p'
  -- let d1 = _term_depth s1
  -- let d2 = _term_depth s2
  -- if d1 == d2 then nop else do
  --   ctx_show_proof p
  --   ctx_show_proof p'
  --   error # show (d1, d2)
  
  let s1 = _proof_stat p
  let s2 = _proof_stat p'
  
  -- putStrLn ""
  -- ctx_show_term s1
  -- ctx_show_term s2
  -- putStrLn ""
  
  -- s <- fmap (>>= snd) # ctx_show s2
  -- ifm (last' undefined s == 'a') # error ""
  
  -- tracem (_term_depth s1, _term_depth s2)
  (s1, s2) <- pure # lift_terms s1 s2
  -- tracem (_term_depth s1, _term_depth s2)
  -- tracem # Lit [""]
  
  let stat = _proof_stat p
  res <- pure # term_to_imp stat
  (s1, s2) <- case res of
    Just res -> pure res
    Nothing -> tac_err_show "ctx_imp_e" stat
  
  -- tracem # Lit ["<<<"]
  -- tracem (_term_depth s1, _term_depth s2, _term_depth # _proof_stat p')
  -- ctx_trace_term s1
  -- ctx_trace_term s2
  -- ctx_trace_proof_stat p'
  
  -- tracem "---"
  p <- ctx_imp_e' False s1 s2 p p'
  -- ctx_trace_proof_stat p
  -- tracem # Lit [">>>\n"]
  
  return p

ctx_imps_e :: HCS => Proof -> [Proof] -> Elab Proof
ctx_imps_e = foldM ctx_imp_e

ctx_all_univs_imps_e' :: HCS => Prop -> Prop -> Proof -> Elab Proof
ctx_all_univs_imps_e' univs imps p = do
  let s = _proof_stat p
  s1 <- show_term s
  case term_to_univ s of
    Just (t, _) -> if not univs then pure p else do
      x <- ctx_mk_term_mvar' t
      p <- ctx_univ_e p x
      ctx_all_univs_imps_e' univs imps p
    _ -> if not imps then pure p else do
      res <- pure # term_to_imp s
      case res of
        Just (s, _) -> do
          x <- ctx_mk_proof_mvar_tc' False s
          p <- ctx_imp_e p x
          ctx_all_univs_imps_e' univs imps p
        _ -> pure p

ctx_all_univs_e :: HCS => Proof -> Elab Proof
ctx_all_univs_e = ctx_all_univs_imps_e' True False

ctx_all_imps_e :: HCS => Proof -> Elab Proof
ctx_all_imps_e = ctx_all_univs_imps_e' False True

ctx_all_univs_imps_e :: HCS => Proof -> Elab Proof
ctx_all_univs_imps_e = ctx_all_univs_imps_e' True True

-----

proof_mvars_to_univs :: HCS => Proof -> Elab Proof
proof_mvars_to_univs p = do
  p <- res_proof p
  
  let mvars = Map.toList # get_term_mvars_in_proof p
  let n = len mvars
  
  let s = _proof_stat p
  let d = _term_depth s
  let d1 = d + n
  p <- pure # lift_proof n p
  
  xs <- (\f -> zipWithM f mvars [0..]) # \(k, t) i -> do
    let e = mk_term_var d1 t (d + i)
    asgn_term k e
  
  p <- res_proof # (\f -> foldr f p mvars) # \(_, t) p ->
    mk_univ_i t p
  
  return p

-----

parse_proof_mvar :: HCS => Stat -> Elab Proof
parse_proof_mvar s = do
  p_scope "proof" p_usc
  ctx_mk_proof_mvar s

ctx_proof_symm :: HCS => Proof -> Elab Proof
ctx_proof_symm p = do
  let s = _proof_stat p
  let (target, args) = split_app s
  
  f <- pure # tac_err_show "ctx_proof_symm" s
  
  case _term_val target of
    TermRef name ps ->
      if name == namei_univ then do
        dflt <- pure # do
          p1 <- ctx_mk_proof_ref namei_subst_symm
          [arg] <- pure args
          f <- pure # \xs -> do
            f <- ctx_term_part_simple arg xs
            ifm (term_has_term_var (_term_depth f - 1) f) # do
              err [dfsc "proof_symm"]
            return # lift_term (-1) f
          x <- f [0, 0, 1, 1]
          y <- f [0, 1, 1]
          p1 <- ctx_univs_e p1 [x, y]
          p <- ctx_imp_e p1 p
          return p
        
        dflt <|> do
          p <- ctx_all_univs_e p
          ctx_proof_symm p
      else do
        Just name' <- get_priv_name' namei_symm name
        p1 <- ctx_mk_proof_ref name'
        p1 <- ctx_univs_e p1 args
        p <- ctx_imp_e p1 p
        return p
    _ -> f

parse_proof_group :: HCS => Stat -> Elab Proof
parse_proof_group s = p_parens # parse_proof s

-- parse_proof_clear :: HCS => Stat -> Elab Proof
-- parse_proof_clear s = do
--   p_tok "!"
--   names <- p_all parse_name
--   p_tok ","
--   ctx0 <- get_ctx
--   ctx_clear_names names
--   p <- parse_proof s
--   modify_ctx # \ctx -> ctx
--     { _ctx_terms = _ctx_terms ctx0
--     , _ctx_proofs = _ctx_proofs ctx0
--     }
--   return # mk_proof_clear names p

parse_proof_symm :: HCS => Stat -> Elab Proof
parse_proof_symm s = do
  p_tok "←"
  s' <- ctx_mk_stat_mvar
  p <- parse_proof_1 s'
  p <- res_proof p
  p <- ctx_proof_symm p
  add_term_eq s # _proof_stat p
  res_proof p

parse_proof_conj_elem :: HCS => Elab [Bit]
parse_proof_conj_elem = do
  b <- choice
    [ p_bit
    , p_char '+' >> pure 1
    , p_char '-' >> pure 0
    ]
  n <- p_nat <++ pure 1
  return # replicate (fi n) b

parse_proof_conj :: HCS => Proof -> Elab Proof
parse_proof_conj p = pure p ++> do
  p_tok "."
  xs <- p_all parse_proof_conj_elem
  p <- proof_conj_cs p # concat xs
  return p

parse_proof_group_conj :: HCS => Stat -> Elab Proof
parse_proof_group_conj s = do
  p <- parse_proof_group s
  parse_proof_conj p

parse_proof_1' :: HCS => Stat -> Elab Proof
parse_proof_1' s = do
  s <- res_term s
  -- if_dbg # do
  --   p_query p_rest >>= tracem . Lit . pure
  p <- trimmed # choice
    [ parse_proof_mvar s
    , parse_proof_ident
    , parse_proof_group s
    ]
  -- if_dbg # do
  --   p_query p_rest >>= tracem . Lit . pure
  --   ctx_trace_proof_stat p
  parse_proof_conj p

parse_proof_1 :: HCS => Stat -> Elab Proof
parse_proof_1 s = do
  s <- res_term s
  parse_proof_symm s <++ parse_proof_1' s

parse_proof_args_group' :: HCS => Elab ([N], Stat)
parse_proof_args_group' = do
  is <- p_all parse_proof_index
  p_tok ":"
  s <- parse_stat
  mapM_ <~ is # \i -> ctx_enter_proof_lam Nothing i s
  return (is, s)

parse_proof_args_group :: HCS => Elab ([N], Stat)
parse_proof_args_group =
  p_parens parse_proof_args_group' <++ do
    i <- parse_proof_index
    s <- ctx_mk_stat_mvar
    ctx_enter_proof_lam Nothing i s
    return ([i], s)

parse_proof_univ_i :: HCS => Stat -> Elab Proof
parse_proof_univ_i s = do
  ctx <- get_ctx
  (names, t) <- parse_binder_args_type'
  let args_num = len names
  
  s <- res_term s
  s1 <- ctx_mk_stat_mvar
  
  m <- pure # (\f -> iterM args_num (s, []) f) # \(s, ts) -> do
    (t', s) <- term_to_univ' s
    return (s, t : ts)
  
  case m of
    Just (s, ts) -> do
      mapM_ (add_type_eq t) ts
      add_term_eq s s1
    Nothing -> add_term_eq s # iter args_num s1 # mk_univ t
  
  s1 <- res_term s1
  
  -- ss <- show_term s
  -- tracem # Lit # map snd ss
  
  p <- parse_proof_univ_i s1 ++> do
    p_tok ","
    parse_proof_aux s1
  p <- res_proof p
  t <- res_type t
  
  modify_ctx # \ctx' -> ctx'
    { _ctx_terms = _ctx_terms ctx
    , _ctx_term_depth = _ctx_term_depth ctx
    }
  
  return # (\f -> foldr f p names) # \name p ->
    mk_univ_i' name t p

parse_proof_imp_i :: HCS => Stat -> Elab Proof
parse_proof_imp_i s = do
  ctx <- get_ctx
  
  -- p_query p_rest >>= putStrLn
  
  (is, sa) <- parse_proof_args_group
  let args_num = len is
  
  s1 <- ctx_mk_stat_mvar
  add_term_eq s # iter args_num s1 # mk_imp sa
  
  p <- parse_proof_imp_i s1 ++> do
    p_tok ","
    parse_proof_aux s1
  p <- res_proof p
  sa <- res_term sa
  
  modify_ctx # \ctx' -> ctx'
    { _ctx_proof_depth = _ctx_proof_depth ctx
    , _ctx_proofs = _ctx_proofs ctx
    }
  
  return # (\f -> foldr f p is) # \i p ->
    mk_imp_i Nothing i sa p

parse_proof_lam :: HCS => Stat -> Elab Proof
parse_proof_lam s = do
  p <- choice
    [ p_tok sym_lam >> parse_proof_univ_i s
    , p_tok sym_cap_lam >> parse_proof_imp_i s
    ]
  add_term_eq s # _proof_stat p
  res_proof p

parse_proof_arg_sep :: HCS => Elab ()
parse_proof_arg_sep = p_tok sym_appr

parse_proof_arg_term :: HCS => Elab Term
parse_proof_arg_term = do
  ctx_set_term_parser_state 0
  e <- choice
    [ parse_term_1
    , p_tok sym_appr >> parse_term_aux
    ]
  return e

parse_proof_arg_proof :: HCS => Stat -> Elab Proof
parse_proof_arg_proof s = parse_proof_1 s ++> do
  parse_proof_arg_sep
  parse_proof_aux s

parse_proof_app_end :: HCS => Prop -> Proof -> Elab Proof
parse_proof_app_end can_end p = do
  True <- pure can_end
  return p

parse_proof_app' :: HCS => Prop -> Proof -> Stat -> Elab Proof
parse_proof_app' can_end p s = do
  let p0 = p
  -- p <- res_proof p
  (<|> parse_proof_app_end can_end p) # do
    -- if_dbg # ctx_trace_proof_stat p
    -- res <- ctx_try # proof_to_univ_or_imp p
    -- case res of
    --   Left msg -> do
    --     if_dbg # ctx_trace msg
    --   _ -> nop
    (p, info) <- proof_to_univ_or_imp p
    -- if_dbg # ctx_trace_proof_stat p
    case info of
      Left (t, s) -> do
        -- d <- ctx_term_depth
        -- s <- pure # lift_term_to d s
        
        -- sp <- ctx_show p
        -- sps <- ctx_show # _proof_stat p
        -- st <- ctx_show t
        -- ss <- ctx_show s
        -- putStrLn # sp >>= snd
        -- putStrLn # sps >>= snd
        -- putStrLn # "-- UNIV"
        -- putStrLn # st >>= snd
        -- putStrLn # ss >>= snd
        -- putStrLn ""
        
        f1 <- pure # do
          e <- parse_proof_arg_term
          case _term_val e of
            TermMvar _ -> pfail
            _ -> nop
          add_type_eq t # _term_type e
          return (e, True)
        
        f2 <- pure # do
          e <- ctx_mk_term_mvar' t
          return (e, False)
        
        (e, can_end) <- f1 <|> f2

        let d = max (_term_depth s) (_term_depth e)
        e <- pure # lift_term_to d e
        p <- ctx_univ_e p e

        -- if_dbg # do
        --   ctx_trace_proof_stat p
        --   ctx_trace_term e
        
        parse_proof_app' can_end p s
      Right (s1, s2) -> do
        -- sp <- ctx_show p
        -- sps <- ctx_show # _proof_stat p
        -- st <- ctx_show s1
        -- ss <- ctx_show s2
        -- putStrLn # sp >>= snd
        -- putStrLn # sps >>= snd
        -- putStrLn # "-- IMP"
        -- putStrLn # st >>= snd
        -- putStrLn # ss >>= snd
        -- putStrLn ""
        
        f1 <- pure # do
          p1 <- parse_proof_arg_proof s1
          dflt <- pure # do
            p <- ctx_imp_e_norm_adv' p s1 p1
            return (p, True)
          case _proof_val p1 of
            ProofMvar _ _ -> do
              p1 <- ctx_mk_proof_mvar_tc s1
              p <- ctx_imp_e p p1
              return (p, True)
            _ -> dflt
        
        f2 <- pure # do
          p1 <- ctx_mk_proof_mvar_tc s1
          p <- ctx_imp_e p p1
          return (p, False)

        (p1, can_end) <- f1 <|> f2
        
        -- if_dbg # do
        --   tracem "<<<"
        --   ctx_trace_proof_stat p
        --   ctx_trace_proof_stat p1
        --   tracem ">>>"
        
        -- putStrLn "---"
        -- print # _term_depth s2
        -- ctx_show s2 >>= putStrLn . (>>= snd)
        -- s2 <- res_term s2
        -- print # _term_depth s2
        -- ctx_show s2 >>= putStrLn . (>>= snd)
        -- putStrLn "---"

        parse_proof_app' can_end p1 s2

parse_proof_app :: HCS => Proof -> Stat -> Elab Proof
parse_proof_app p s = do
  let i = _proof_target_asm p
  p <- parse_proof_app' True p s
  return # p {_proof_target_asm = i}

parse_proof_app_1 :: HCS => Stat -> Elab Proof
parse_proof_app_1 s = do
  s <- res_term s
  p <- parse_proof_1 s ++> do
    s' <- ctx_mk_stat_mvar
    parse_proof_group_conj s'
  parse_proof_app p s

parse_proof' :: HCS => Stat -> Elab Proof
parse_proof' s = do
  p <- parse_proof_app_1 s
  -- add_term_eq s # _proof_stat p
  let i = _proof_target_asm p
  p <- res_proof p
  
  -- s <- ctx_show p
  -- putStrLn # s >>= snd
  
  return # p {_proof_target_asm = i}

parse_proof_aux :: HCS => Stat -> Elab Proof
parse_proof_aux s =
  parse_proof' s ++>
  parse_proof_lam s

parse_proof :: HCS => Stat -> Elab Proof
parse_proof = parse_proof_aux

-----

ctx_show_goals :: HCS => Tactic Sctr
ctx_show_goals = do
  pst <- get_pst
  let ctxs = _pst_ctxs pst
  let goals = map ctx_to_goal ctxs
  th_to_elab # show_goals goals

ctx_display_goals :: HCS => Elab a
ctx_display_goals = do
  s1 <- ctx_show_goals
  raise s1

ctx_trace_goals :: HCS => Elab ()
ctx_trace_goals = do
  Left s <- ctx_try ctx_display_goals
  ctx_trace s

ctx_pst_get_proof :: HCS => Elab Proof
ctx_pst_get_proof = do
  pst <- get_pst
  if null # _pst_ctxs pst
    then pure # pst_get_proof pst
    else ctx_display_goals

ctx_show_proof :: HCS => Proof -> Elab ()
ctx_show_proof p = ctx_show_term # _proof_stat p

-----

res_proof :: HCS => Proof -> Elab Proof
res_proof p = if e_res p then pure p else do
  let expl = _proof_expl p
  p <- do
    s0 <- res_term # _proof_stat p
    case _proof_val p of
      ProofMvar is_tc i -> pure # mk_proof_mvar' is_tc s0 i
      ProofRef name ps -> do
        ps <- mapM res_type ps
        return # mk_proof_ref s0 name ps expl
      ProofVar name -> pure # mk_proof_var s0 name expl
      ImpI mn i0 i s p -> do
        s <- res_term s
        p <- res_proof p
        return # mk_imp_i_aux mn i0 i s p
      UnivI mn t p -> do
        t <- res_type t
        p <- res_proof p
        return # mk_univ_i_aux mn t p
      ImpE p1 p2 -> do
        p1 <- res_proof p1
        p2 <- res_proof p2
        return # mk_imp_e s0 p1 p2
      UnivE p e -> do
        p <- res_proof p
        e <- res_term e
        return # mk_univ_e s0 p e
      WithStat p -> res_proof p
      ProofClear e_names p_names p -> do
        p <- res_proof p
        return # mk_proof_clear e_names p_names p
  return # p {_proof_expl = expl}

-----

subst_type_mvars_in_proof :: HCS => Map N Type -> Proof -> Proof
subst_type_mvars_in_proof mp p = let
  s' = subst_type_mvars_in_term mp # _proof_stat p
  p' = case _proof_val p of
    ProofRef name ps -> ProofRef name #
      map (subst_type_mvars_in_type mp) ps
    ImpI mn i0 i s p -> ImpI mn i0 i
      (subst_type_mvars_in_term mp s)
      (subst_type_mvars_in_proof mp p)
    UnivI name t p -> UnivI name
      (subst_type_mvars_in_type mp t)
      (subst_type_mvars_in_proof mp p)
    ImpE p1 p2 -> ImpE
      (subst_type_mvars_in_proof mp p1)
      (subst_type_mvars_in_proof mp p2)
    UnivE p e -> UnivE
      (subst_type_mvars_in_proof mp p)
      (subst_type_mvars_in_term mp e)
    WithStat p -> WithStat
      (subst_type_mvars_in_proof mp p)
    ProofClear e_names p_names p -> ProofClear
      e_names p_names (subst_type_mvars_in_proof mp p)
    p -> p
  in p
    { _proof_stat = s'
    , _proof_val = p'
    }

modify_terms_in_proof :: HCS => (Term -> Term) -> Proof -> Proof
modify_terms_in_proof f p = let
  s' = f # _proof_stat p
  p' = case _proof_val p of
    ImpI mn i0 i s p -> ImpI mn i0 i (f s) (modify_terms_in_proof f p)
    UnivI name t p -> UnivI name t (modify_terms_in_proof f p)
    ImpE p1 p2 -> ImpE (modify_terms_in_proof f p1)
      (modify_terms_in_proof f p2)
    UnivE p e -> UnivE (modify_terms_in_proof f p) (f e)
    WithStat p -> _proof_val # modify_terms_in_proof f p
    ProofClear e_names p_names p -> ProofClear e_names p_names #
      modify_terms_in_proof f p
    p -> p
  in p
    { _proof_stat = s'
    , _proof_val = p'
    }

subst_term_mvars_in_proof :: HCS => Map N Term -> Proof -> Proof
subst_term_mvars_in_proof mp = modify_terms_in_proof #
  subst_term_mvars_in_term mp

proof_to_kproof' :: HCS => {-Map Name N ->-} N -> N -> Map N N -> Proof -> K.Proof
proof_to_kproof' {-loc-} de dp mp p = case _proof_val p of
  ProofRef name ps -> K.ProofRef {-(localize_name_mp loc name)-} name #
    map type_to_ktype ps
  ProofVar name -> K.ProofVar #
    dp - (fromJust # Map.lookup name mp) - 1
  ImpI _ _ i s p -> K.ImpI (term_to_kterm' {-loc-} de s) #
    proof_to_kproof' {-loc-} de (dp + 1) (map_insert i dp mp) p
  UnivI _ t p -> K.UnivI (type_to_ktype t) #
    proof_to_kproof' {-loc-} (de + 1) dp mp p
  ImpE p1 p2 -> K.ImpE (proof_to_kproof' {-loc-} de dp mp p1) #
    proof_to_kproof' {-loc-} de dp mp p2
  UnivE p e -> K.UnivE (proof_to_kproof' {-loc-} de dp mp p) #
    term_to_kterm' {-loc-} de e
  ProofClear _ _ p -> proof_to_kproof' {-loc-} de dp mp p
  _ -> error # show p

proof_to_kproof :: HCS => Proof -> Theory K.Proof
proof_to_kproof p = do
  -- loc <- gets _th_local_names
  return # proof_to_kproof' {-loc-} 0 0 Map.empty p

kproof_to_proof' :: HCS => Map N Type -> Map N Stat -> K.Proof -> Elab Proof
kproof_to_proof' mpe mpp p = do
  let de = map_size mpe
  let dp = map_size mpp
  -- su <- pure # \s -> Term
  --   { _term_depth = d
  --   , _term_type = mk_prop_t
  --   , _term_val = _term_val s
  --   , _term_res = True
  --   }
  case p of
    K.ProofRef i ps -> th_to_elab # do
      ps <- mapM ktype_to_type ps
      
      thm <- k_to_th # K.th_get_thm i
      s <- kterm_to_term # K._thm_stat thm
      s <- pure # subst_type_vars_in_term ps s
      s <- pure # lift_term_to de s
      
      return # mk_proof_ref s i ps True
      
    K.ProofVar i -> do
      i <- pure # dp - i - 1
      Just s <- pure # map_get i mpp
      
      -- putStrLn "\n---"
      -- print # (de, _term_depth s)
      -- putStrLn "---\n"
      s <- pure # lift_term_to de s
      
      return # mk_proof_var s i True
    
    K.ImpI s p -> do
      s <- th_to_elab # kterm_to_term' mpe s
      p <- kproof_to_proof' mpe (map_insert dp s mpp) p
      return # mk_imp_i Nothing dp s p
    
    K.UnivI t p -> do
      -- print ("univ_i", t, p)
      
      t <- th_to_elab # ktype_to_type t
      p <- kproof_to_proof' (map_insert de t mpe) mpp p
      return # mk_univ_i t p
    
    K.ImpE p1 p2 -> do
      p1 <- kproof_to_proof' mpe mpp p1
      p2 <- kproof_to_proof' mpe mpp p2
      
      -- print ("imp_e", p1, p2)
      
      -- let sp = _proof_stat p1
      -- (fn, args) <- pure # split_app sp
      -- 
      -- case _term_val fn of
      --   TermRef i _ -> ifm (i /= namei_imp) # error # show sp
      --   _ -> error # show sp
      -- 
      -- [_, s] <- pure args
      
      ctx_imp_e p1 p2
    
    K.UnivE p e -> do
      p <- kproof_to_proof' mpe mpp p
      e <- th_to_elab # kterm_to_term' mpe e
      
      -- let sp = _proof_stat p
      -- (fn, args) <- pure # split_app sp
      -- 
      -- case _term_val fn of
      --   TermRef i _ -> ifm (i /= namei_univ) # error # show sp
      --   _ -> error # show sp
      -- 
      -- [f] <- pure args
      -- (s, _) <- reduce_term # mk_app_safe f e
      
      ctx_univ_e p e

kproof_to_proof :: HCS => K.Proof -> Theory Proof
kproof_to_proof p = do
  fmap fst # elab_to_th #
    kproof_to_proof' Map.empty Map.empty p

-----

-- localize_kproof' :: HCS => Map Name N -> K.Proof -> K.Proof
-- localize_kproof' loc p = case p of
--   K.ProofRef name ps -> let
--     name' = localize_name_mp loc name
--     in K.ProofRef name' ps
--   K.ImpI s p -> K.ImpI (localize_kterm' loc s) #
--     localize_kproof' loc p
--   K.UnivI t p -> K.UnivI t # localize_kproof' loc p
--   K.ImpE p1 p2 -> K.ImpE (localize_kproof' loc p1) #
--     localize_kproof' loc p2
--   K.UnivE p e -> K.UnivE (localize_kproof' loc p) #
--     localize_kterm' loc e
--   _ -> p
-- 
-- localize_kproof :: HCS => K.Proof -> Theory K.Proof
-- localize_kproof p = do
--   loc <- gets _th_local_names
--   return # localize_kproof' loc p

-----

get_type_mvars_in_proof :: HCS => Proof -> Set N
get_type_mvars_in_proof p = Set.union
  (get_type_mvars_in_term # _proof_stat p) #
  case _proof_val p of
    ProofRef name ps -> Set.unions #
      map get_type_mvars_in_type ps
    ImpI _ _ _ s p -> Set.union
      (get_type_mvars_in_term s)
      (get_type_mvars_in_proof p)
    UnivI _ t p -> Set.union
      (get_type_mvars_in_type t)
      (get_type_mvars_in_proof p)
    ImpE p1 p2 -> Set.union
      (get_type_mvars_in_proof p1)
      (get_type_mvars_in_proof p2)
    UnivE p e -> Set.union
      (get_type_mvars_in_proof p)
      (get_type_mvars_in_term e)
    WithStat p -> get_type_mvars_in_proof p
    ProofClear _ _ p -> get_type_mvars_in_proof p
    _ -> Set.empty

get_term_mvars_in_proof :: HCS => Proof -> Map N Type
get_term_mvars_in_proof p = Map.union
  (get_term_mvars_in_term # _proof_stat p) #
  case _proof_val p of
    ImpI _ _ _ s p -> Map.union
      (get_term_mvars_in_term s)
      (get_term_mvars_in_proof p)
    UnivI _ _ p -> get_term_mvars_in_proof p
    ImpE p1 p2 -> Map.union
      (get_term_mvars_in_proof p1)
      (get_term_mvars_in_proof p2)
    UnivE p e -> Map.union
      (get_term_mvars_in_proof p)
      (get_term_mvars_in_term e)
    WithStat p -> get_term_mvars_in_proof p
    ProofClear _ _ p -> get_term_mvars_in_proof p
    _ -> Map.empty

-----

get_type_mvars' :: HCS => [Type] -> [Term] -> [Proof] -> Set N
get_type_mvars' ts es ps = Set.unions # concat
  [ map get_type_mvars_in_type ts
  , map get_type_mvars_in_term es
  , map get_type_mvars_in_proof ps
  ]

get_type_mvars :: HCS => [Type] -> [Term] -> [Proof] -> Map N ()
get_type_mvars ts es ps = set_to_map_unit #
  get_type_mvars' ts es ps

get_term_mvars :: HCS => [Term] -> [Proof] -> Map N Type
get_term_mvars es ps = Map.unions # concat
  [ map get_term_mvars_in_term es
  , map get_term_mvars_in_proof ps
  ]

-- proof_mvars_to_goals' :: HCS => Map Name (N, Type) -> Map N Name ->
--   Map Name Term -> Proof -> [Goal]
-- proof_mvars_to_goals' es es' ps p = case _proof_val p of
--   ProofMvar i -> pure # Goal
--       { _goal_id = -1
--       , _goal_types = Map.empty
--       , _goal_terms = es
--       , _goal_terms' = es'
--       , _goal_proofs = ps
--       , _goal_stat = _proof_stat p
--       }
--   ImpI name s p -> proof_mvars_to_goals'
--     es es' (map_insert name s ps) p
--   UnivI mn t p -> case mn of
--     Nothing -> proof_mvars_to_goals' es es' ps p
--     Just name -> let
--       d = _term_depth (_proof_stat p) - 1
--       in proof_mvars_to_goals' (map_insert name (d, t) es)
--         (map_insert d name es') ps p
--   ImpE p1 p2 -> proof_mvars_to_goals' es es' ps p1 ++
--     proof_mvars_to_goals' es es' ps p2
--   UnivE p e -> proof_mvars_to_goals' es es' ps p
--   WithStat p -> proof_mvars_to_goals' es es' ps p
--   ProofClear names p -> let
--     es_new = foldr Map.delete es names
--     ps_new = foldr Map.delete ps names
--     in proof_mvars_to_goals' es_new es' ps_new p
--   _ -> []

-- proof_mvars_to_goals :: HCS => Proof -> [Goal]
-- proof_mvars_to_goals = proof_mvars_to_goals'
--   Map.empty Map.empty Map.empty

proof_mvars_to_goals :: HCS => Proof -> [Goal]
proof_mvars_to_goals p = []

ctx_parse_ident :: HCS => Elab LocIdent
ctx_parse_ident = p_either parse_term_index parse_proof_index

-----

-- proof_binder_to_expr :: HCS => Set Name -> Map N Name -> Op ->
--   Name -> Stat -> Proof -> Elab Expr
-- proof_binder_to_expr names mp op name s p = do
--   se <- term_to_expr names mp s
--   sp <- proof_to_expr names mp p
--   expr_mk_binder scope_dflt "proof" op name se sp

-----

-- term_to_expr' :: HCS => Set Name -> Map N Name -> Term -> Elab Expr
-- term_to_expr' names mp e = do
--   par <- gets_th _th_term_par
--   case _term_val e of
--     TermMvar i -> expr_mk_atom [("term", "?" ++ show i)]
--     TermRef name ps -> do
--       let s = [("term", name)]
--       p <- gets_pp _pp_term_ref_types
--       st <- if null ps || not p then pure [] else do
--         st <- mapM type_to_expr ps
--         return # [dfsc ".{"] ++
--           intercalate [dfsc ", "] (map _expr_val st) ++
--           [dfsc "}"]
--       expr_mk_atom # s ++ st
--     TermVar i -> case map_get' i mp of
--       Just name -> expr_mk_atom [("term", name)]
--       Nothing -> expr_mk_atom [(scope_err, "?" ++ show i)]
--     Lam _ t e -> do
--       let op = map_get undefined sym_lam # _term_par_names par
--       binder_to_expr names mp op t e
--     App a b -> app_to_expr names mp a b

proof_to_expr :: HCS => Set Name -> Map N Name -> Proof -> Elab Expr
proof_to_expr names mp p = do
  par <- gets_th _th_term_par
  case _proof_val p of
    ProofMvar _ i -> expr_mk_atom [("proof", "?" ++ show i)]
    ProofRef i ps -> do
      name <- id_to_name i
      let s = [dfsc "@", ("proof", name)]
      p <- gets_pp _pp_proof_ref_types
      st <- if null ps || not p then pure [] else do
        st <- mapM type_to_expr ps
        return # [dfsc ".{"] ++
          intercalate [dfsc ", "] (map _expr_val st) ++
          [dfsc "}"]
      expr_mk_atom # s ++ st
    ProofVar i -> do
      name <- proof_index_to_name i
      expr_mk_atom [("proof", name)]
    ImpI mn _ i s p -> do
      let binder = [dfsc sym_cap_lam]
      name <- pure # case mn of
        Just name -> name
        Nothing -> usc' : show i
      ss <- term_to_expr names mp s
      sp <- proof_to_expr names mp p
      let group = ExprBinderGroupImpI [[("proof", name)]] # _expr_val ss
      expr_mk_binder binder group sp
    UnivI _ t p -> do
      let binder = [dfsc sym_lam]
      (name, names) <- pure # term_mk_name names
      mp <- pure # map_push name mp
      st <- type_to_expr t
      sp <- proof_to_expr names mp p
      let group = ExprBinderGroupUnivI [[("term", name)]] # _expr_val st
      expr_mk_binder binder group sp
    ImpE p1 p2 -> app_to_expr proof_to_expr proof_to_expr names mp p1 p2
    UnivE p e -> app_to_expr proof_to_expr term_to_expr names mp p e
    ProofClear _ _ p -> proof_to_expr names mp p
    p -> err [dfsc "proof_to_expr: ", dfsc # show p]

show_proof' :: HCS => Set Name -> Map N Name -> Proof -> Elab Sctr
show_proof' names mp p = do
  s <- proof_to_expr names mp p
  return # _expr_val s

show_proof :: HCS => Proof -> Elab Sctr
show_proof p = do
  mp <- gets_ctx # map_inv . _ctx_term_names
  -- ref_names <- elab_get_names_str
  names <- pure # --Set.union ref_names #
    Set.fromList # Map.elems mp
  show_proof' names mp p

res_show_proof' :: HCS => Set Name -> Map N Name -> Proof -> Elab Sctr
res_show_proof' names mp p = do
  p <- res_proof p
  show_proof' names mp p

res_show_proof :: HCS => Proof -> Elab Sctr
res_show_proof p = do
  p <- res_proof p
  show_proof p

instance CtxShow Proof where
  ctx_show = show_proof

-----

-- ctx_norm_proof_names_fn :: HCS => Map N N -> N -> [N] -> [N]
-- ctx_norm_proof_names_fn deps i hs = fromMaybe (snoc hs i) # do
--   j <- map_get' i deps
--   (index : _) <- pure # reverse # findIndices <~ hs # \k ->
--     k == j || map_get' k deps == Just j
--   return # insert_at (fi index + 1) i hs

ctx_norm_proof_names_fn :: HCS => [(Name, N)] -> [(N, (Name, N))] -> [(Name, N)]
ctx_norm_proof_names_fn asms deps = let
  (ds, deps') = partition <~ deps # \(_, (_, i)) ->
    any ((i ==) . snd) asms
  in if null ds then ite (null deps') asms (error "") else let
    asms' = asms >>= \asm@(name, i) -> asm : do
      (j, (name, i')) <- ds
      asrt # i' == i
      return (name, j)
    in ctx_norm_proof_names_fn asms' deps'

ctx_norm_proof_names :: HCS => Ctx -> Ctx -> Ctx
ctx_norm_proof_names ctx0 ctx =
  if _ctx_asms_normed ctx then ctx else run # do
    -- let dp0 = _ctx_proof_depth ctx0
    -- let dp = _ctx_proof_depth ctx
    let term_names = _ctx_term_names ctx
    let proofs = _ctx_proofs ctx
    let hidden_es = _ctx_terms_hidden ctx
    let hidden_ps = _ctx_proofs_hidden ctx

    -- hs <- pure # Map.elems # _ctx_asms ctx0
    -- hs <- pure # (\f -> foldl' f hs [dp0 .. dp - 1]) # \hs i ->
    --   ctx_norm_proof_names_fn deps i hs
    -- hs <- pure # filter <~ hs # \i ->
    --   not # Set.member i hidden_ps
    -- hs <- pure # Map.fromList # zip [0..] hs

    -- asms_new <- pure # do
    --   (j, (name, Nothing)) <- deps_list
    --   return (name, j)
    -- let asms = _ctx_asms ctx ++ asms_new
    -- hs <- pure # do
    --   h@(name, i) <- asms
    --   hs0 <- pure # do
    --     asrt # not # Set.member i hidden_ps
    --     return h
    --   hs1 <- pure # do
    --     (j, (name, Just i')) <- deps_list
    --     asrt # i' == i
    --     return (name, j)
    --   hs0 ++ hs1

    asms <- pure # snoc' (undefined, -1) # _ctx_asms ctx
    deps <- pure # map <~ Map.toList (_ctx_asm_deps ctx) #
      \(i, (name, dep)) -> (i, (name, fromMaybe (-1) dep))
    asms_new <- pure # filter <~ ctx_norm_proof_names_fn asms deps #
      \(_, i) -> not # i == -1 || Set.member i hidden_ps
    
    return # ctx
      { _ctx_proofs = Set.fold Map.delete proofs hidden_ps
      , _ctx_term_names = Map.filter <~ term_names # \i ->
        not # Set.member i hidden_es
      , _ctx_proofs_hidden = Set.empty
      , _ctx_terms_hidden = Set.empty
      , _ctx_asm_deps = Map.empty
      , _ctx_asms = asms_new
      , _ctx_asms_normed = True
      }

ctx_clear_unused_terms :: HCS => Ctx -> Ctx
ctx_clear_unused_terms ctx = let
  ss = maybe_to_list (_ctx_goal_stat ctx) ++
    Map.elems (_ctx_proofs ctx)
  used = Set.unions # map term_get_free_vars ss
  es = Map.filterWithKey <~ _ctx_terms ctx # \i _ ->
    Set.member i used
  in ctx {_ctx_terms = es}

ctx_norm' :: HCS => Ctx -> Ctx -> Ctx
ctx_norm' ctx0 ctx = run # do
  ctx <- pure # ctx_norm_proof_names ctx0 ctx
  ctx <- pure # ctx_clear_unused_terms ctx
  return # ctx {_ctx_tcs = Nothing}

ctx_norm :: HCS => Ctx -> Elab ()
ctx_norm ctx0 = modify_ctx # ctx_norm' ctx0

elab_norm :: HCS => Ctx -> Elab ()
elab_norm ctx0 = do
  pst <- gets _elab_pst
  let ctxs = map (ctx_norm' ctx0) # _pst_ctxs pst
  modify # \elab -> elab {_elab_pst = pst {_pst_ctxs = ctxs}}

-----

ctx_trace_proof_stat :: HCS => Proof -> Elab ()
ctx_trace_proof_stat p = ctx_trace_term # _proof_stat p

unfold_get_proof :: HCS => Id -> Tactic (Prop, Proof)
unfold_get_proof name = do
  unf_name <- get_priv_name' namei_unf name
  ref <- maybe (pure Nothing) ctx_get_proof_ref' unf_name
  if isNothing ref then do
    Just name' <- get_def_thm_name' name
    p <- ctx_mk_proof_ref name'
    return (False, p)
  else do
    Just unf_name <- pure unf_name
    p <- ctx_mk_proof_ref unf_name
    return (True, p)

norm_proof :: HCS => Proof -> Tactic Proof
norm_proof p = do
  let s = _proof_stat p
  let d = _term_depth s
  let (target, args) = split_app s
  let t = _term_type target
  TermRef name _ <- pure # _term_val target
  (custom, p1) <- unfold_get_proof name
  if not custom then do
    p1 <- ctx_proof_symm p1
    f <- pure # mk_term_var (d + 1) t d
    f <- pure # mk_apps_safe # f : args
    f <- pure # mk_lam t f
    p1 <- ctx_univ_e p1 f
    ctx_imp_e p1 p
  else do
    p1 <- ctx_univs_e p1 args
    p1 <- ctx_proof_symm p1
    t <- pure mk_prop_t
    f <- pure # mk_term_var (d + 1) t d
    f <- pure # mk_lam t f
    p1 <- ctx_univ_e p1 f
    p <- ctx_imp_e p1 p
    return p

proof_to_univ_or_imp :: HCS => Proof ->
  Tactic (Proof, Either (Type, Term) (Term, Term))
proof_to_univ_or_imp p = do
  let s = _proof_stat p
  case term_to_univ_or_imp s of
    Just info -> pure (p, info)
    Nothing -> do
      -- if_dbg # do
      --   tracem "<<<"
      --   ctx_trace_term s
      --   res <- ctx_try # norm_proof p
      --   case res of
      --     Left msg -> do
      --       ctx_trace msg
      --     _ -> nop
      p <- norm_proof p
      -- if_dbg # do
      --   ctx_trace_proof_stat p
      --   tracem ">>>"
      proof_to_univ_or_imp p

-----

proof_cong_refl :: HCS => Term -> Term -> Tactic Proof
proof_cong_refl lhs rhs = do
  add_term_eq lhs rhs
  lhs <- res_term lhs
  let d = _term_depth lhs
  let t = _term_type lhs
  lhs <- pure # lift_term_to (d + 1) lhs
  let tf = mk_func t mk_prop_t
  let f = mk_term_var (d + 1) tf d
  let s = mk_app_safe f lhs
  p <- ctx_mk_proof_ref namei_imp_refl
  p <- ctx_univ_e p s
  p <- pure # mk_univ_i tf p
  return p

proof_cong_norm_trans_combine :: HCS => Term -> Term -> Proof -> Tactic Proof
proof_cong_norm_trans_combine lhs rhs p = do
  let s = _proof_stat p
  rhs' <- ctx_term_part_simple s [1, 0, 0, 1, 1]
  rhs' <- pure # lift_term (-1) rhs'
  
  p1 <- proof_cong_norm' lhs rhs'

  p2 <- ctx_mk_proof_ref namei_subst_trans
  p2 <- ctx_univs_e p2 [lhs, rhs', rhs]
  p2 <- ctx_imps_e p2 [p1, p]

  return p2

proof_cong_norm_trans_aux1 :: HCS => Term -> Term -> N -> Term -> Id ->
  [Term] -> Proof -> Tactic Proof
proof_cong_norm_trans_aux1 lhs rhs d target name args p = do
  let tf = _term_type target
  let e1 = mk_term_var (d + 2) tf # d + 1
  f <- pure # mk_apps_safe #
    e1 : map (lift_term 2) args

  let tf' = mk_func (_term_type f) mk_prop_t
  let e2 = mk_term_var (d + 2) tf' d
  f <- pure # mk_app_safe e2 f
  f <- pure # mk_lam tf f
  
  p <- ctx_univ_e p f
  p <- pure # mk_univ_i tf' p

  proof_cong_norm_trans_combine lhs rhs p

proof_cong_norm_trans_aux2 :: HCS => Term -> Term -> N -> Term -> Id ->
  [Term] -> Proof -> Tactic Proof
proof_cong_norm_trans_aux2 lhs rhs d target name args p = do
  -- do
  --   ctx_trace_term lhs
  --   ctx_trace_term rhs
  --   ctx_trace_proof_stat p
  --   mapM_ ctx_trace_term args
  --   ctx_catch # ctx_univs_e p args
  p <- ctx_univs_e p args
  p <- proof_cong_norm_trans_combine lhs rhs p
  return p

proof_cong_norm_trans :: HCS => Term -> Term -> N -> Term -> Id ->
  [Term] -> Tactic Proof
proof_cong_norm_trans lhs rhs d target name args = do
  (custom, p) <- unfold_get_proof name
  f <- pure # \g -> g lhs rhs d target name args p
  p <- if custom
    then f proof_cong_norm_trans_aux1
    else f proof_cong_norm_trans_aux2
  return p

proof_cong_norm' :: HCS => Term -> Term -> Tactic Proof
proof_cong_norm' lhs rhs = proof_cong_refl lhs rhs <|> do
  let d = _term_depth lhs
  let (target1, args1) = split_app lhs
  let (target2, args2) = split_app rhs
  let is_lam1 = term_is_lam target1
  let is_lam2 = term_is_lam target2
  if is_lam1 || is_lam2 then do
    pfail
    -- error ""
  else case (_term_val target1, _term_val target2) of
    (TermRef name1 ps1, TermRef name2 ps2) -> do
      if name1 == name2 then do
        pfail
        -- ctx_trace_term lhs
        -- ctx_trace_term rhs
        -- error ""
      else do
        lvl1 <- ctx_get_term_level name1
        lvl2 <- ctx_get_term_level name2
        -- if_dbg # do
        --   tracem name1
        --   tracem name2
        --   tracem lvl1
        --   tracem lvl2
        if lvl1 >= lvl2 then do
          -- do
          --   ctx_trace_term lhs
          --   ctx_trace_term rhs
          --   tracem (name1, name2)
          --   tracem (lvl1, lvl2)
          --   ctx_catch # proof_cong_norm_trans rhs lhs d target1 name1 args1
          p <- proof_cong_norm_trans rhs lhs d target1 name1 args1
          p <- ctx_proof_symm p
          return p
        else do
            -- if_dbg # tracem "a"
          p <- proof_cong_norm_trans lhs rhs d target2 name2 args2
          return p
    _ -> do
      pfail
      -- ctx_trace_term lhs
      -- ctx_trace_term rhs
      -- error ""

proof_cong_norm :: HCS => Term -> Term -> Tactic Proof
proof_cong_norm lhs rhs = do
  (lhs, rhs) <- pure # lift_terms lhs rhs
  -- ctx_catch # proof_cong_norm' lhs rhs
  p <- proof_cong_norm' lhs rhs
  let d = _term_depth # _proof_stat p
  let t = mk_prop_t
  let f = mk_lam t # mk_term_var (d + 1) t d
  ctx_univ_e p f

ctx_imp_e_norm_exact :: HCS => Proof -> Term -> Proof -> Tactic Proof
ctx_imp_e_norm_exact p s p1 = do
  let s1 = _proof_stat p1
  -- do
  --   ctx_trace_term s1
  --   ctx_trace_term s
  p2 <- proof_cong_norm s1 s
  p <- res_proof p
  p2 <- ctx_imp_e p2 p1
  ctx_imp_e p p2

ctx_imp_e_norm_univ_e :: HCS => Proof -> Term -> Proof -> Tactic Proof
ctx_imp_e_norm_univ_e p s p1 = do
  (p1, info) <- proof_to_univ_or_imp p1
  case info of
    Left (t, s1) -> do
      let d = _term_depth s1
      e <- ctx_mk_term_mvar' t
      e <- pure # lift_term_to d e
      p1 <- ctx_univ_e p1 e
      ctx_imp_e_norm' p s p1
    Right (s1, s2) -> pfail

ctx_imp_e_norm' :: HCS => Proof -> Term -> Proof -> Tactic Proof
ctx_imp_e_norm' p s p1 = ctx_ors
  [ ctx_imp_e p p1
  , ctx_imp_e_norm_exact p s p1
  , ctx_imp_e_norm_univ_e p s p1
  ]

ctx_imp_e_norm :: HCS => Proof -> Proof -> Tactic Proof
ctx_imp_e_norm p p1 = do
  let s = _proof_stat p
  Just (s, _) <- pure # term_to_imp s
  -- do
  --   ctx_trace_proof_stat p
  --   ctx_trace_proof_stat p1
  ctx_imp_e_norm' p s p1

ctx_imp_e_norm_adv' :: HCS => Proof -> Term -> Proof -> Tactic Proof
ctx_imp_e_norm_adv' p s p1 = ctx_imp_e_norm' p s p1 <|> do
  -- tracem "<<<"
  -- ctx_trace_proof_stat p
  -- ctx_trace_proof_stat p1
  -- tracem ">>>"
  (p1, info) <- proof_to_univ_or_imp p1
  case info of
    Left (t, s) -> do
      e <- ctx_mk_term_mvar' t
      
      let d = max (_term_depth s) (_term_depth e)
      e <- pure # lift_term_to d e
      
      p1 <- ctx_univ_e p1 e
      ctx_imp_e_norm_adv' p s p1
    Right (s1, s2) -> do
      pfail

ctx_imp_e_norm_adv :: HCS => Proof -> Proof -> Tactic Proof
ctx_imp_e_norm_adv p p1 = do
  let s = _proof_stat p
  Just (s, _) <- pure # term_to_imp s
  ctx_imp_e_norm_adv' p s p1

-----

proof_conj_cs_1 :: HCS => Proof -> Bit -> Elab Proof
proof_conj_cs_1 p b = do
  name <- pure # case b of
    0 -> namei_and_left
    1 -> namei_and_right
    _ -> error ""
  p1 <- ctx_mk_proof_ref name
  p1 <- ctx_all_univs_e p1

  -- tracem "<<<"
  -- ctx_trace_proof_stat p1
  -- ctx_trace_proof_stat p
  -- tracem ">>>"
  
  ctx_imp_e_norm p1 p

proof_conj_cs :: HCS => Proof -> [Bit] -> Elab Proof
proof_conj_cs = foldM proof_conj_cs_1

-----

-- proof_get_target :: HCS => Proof -> Proof
-- proof_get_target p = case _proof_val p of
--   ImpE p _ -> proof_get_target p
--   UnivE p _ -> proof_get_target p
--   _ -> p

-- proof_max_type_var :: HCS => Proof -> N
-- proof_max_type_var p = term_max_type_var # _proof_stat p