{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Elaborator.Term where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Maybe
import Data.Either
import Data.List
import Data.Char
import Data.Foldable
import Control.Monad
import Control.Monad.State
import Prelude hiding (print, putStrLn)

import Util
import Sctr
import Bit
import Tree
import Trie
import Parser

import Elaborator.Data
import Elaborator.Util
import Elaborator.Parser
import Elaborator.Type

import qualified Kernel as K

subst_type_mvar_in_term :: HCS => N -> Type -> Term -> Term
subst_type_mvar_in_term i t1 e = let
  t' = subst_type_mvar_in_type i t1 # _term_type e
  e' = case _term_val e of
    TermRef name ps -> TermRef name #
      map (subst_type_mvar_in_type i t1) ps
    Lam mn t e -> Lam mn (subst_type_mvar_in_type i t1 t) #
      subst_type_mvar_in_term i t1 e
    App a b -> App (subst_type_mvar_in_term i t1 a)
      (subst_type_mvar_in_term i t1 b)
    e -> e
  in e
    { _term_type = t'
    , _term_val = e'
    }

subst_type_mvars_in_term :: HCS => Map N Type -> Term -> Term
subst_type_mvars_in_term mp e = let
  t' = subst_type_mvars_in_type mp # _term_type e
  e' = case _term_val e of
    TermRef name ps -> TermRef name #
      map (subst_type_mvars_in_type mp) ps
    Lam mn t e -> Lam mn (subst_type_mvars_in_type mp t) #
      subst_type_mvars_in_term mp e
    App a b -> App (subst_type_mvars_in_term mp a)
      (subst_type_mvars_in_term mp b)
    e -> e
  in e
    { _term_type = t'
    , _term_val = e'
    }

subst_type_vars_in_term :: HCS => [Type] -> Term -> Term
subst_type_vars_in_term ts e = let
  d = (_term_depth e)
  t = subst_type_vars_in_type ts # _term_type e
  in case _term_val e of
    TermRef name ps -> mk_term_ref d t name #
      map (subst_type_vars_in_type ts) ps
    Lam mn t e -> mk_lam_aux mn (subst_type_vars_in_type ts t) #
      subst_type_vars_in_term ts e
    App a b -> mk_app t (subst_type_vars_in_term ts a)
      (subst_type_vars_in_term ts b)
    WithType t e -> mk_with_type t # subst_type_vars_in_term ts e
    _ -> e {_term_type = t}

-----

ctx_mk_term_ref_aux :: HCS => Id -> Maybe [Type] -> Elab Term
ctx_mk_term_ref_aux name ts = do
  th <- get_th
  res <- get_def' name
  ref <- case res of
    Nothing -> do
      name <- id_to_name name
      err [("tactic", "ctx_mk_term_ref_aux"), dfsc ": ", ("term", name)]
    Just ref -> pure ref
  ts <- case ts of
    Just ts -> pure ts
    Nothing -> ctx_mk_type_mvars # _def_ps_num ref
  let t = subst_type_vars_in_type ts # _def_type ref
  d <- ctx_term_depth
  return # mk_term_ref d t name ts

ctx_mk_term_ref' :: HCS => Id -> [Type] -> Elab Term
ctx_mk_term_ref' name ts = ctx_mk_term_ref_aux name # Just ts

ctx_mk_term_ref :: HCS => Id -> Elab Term
ctx_mk_term_ref name = ctx_mk_term_ref_aux name Nothing

ctx_mk_app :: HCS => Term -> Term -> Elab Term
ctx_mk_app a b = do
  let ta = _term_type a
  let tb = _term_type b
  f <- pure # do
    t <- ctx_mk_type_mvar
    
    -- s0 <- show_type t
    -- s1 <- show_type ta
    -- s2 <- show_type tb
    -- s3 <- gets_ctx _ctx_type_asgns
    -- print # Lit ["\n", s0, "\n", s1, "\n", s2, "\n", show s3]
    
    add_type_eq ta # mk_func tb t
    
    return t
  t <- case _type_val ta of
    Func t1 t2 -> if ta == t1 then pure t2 else f
    _ -> f
  -- res_term # mk_app t a b
  return # mk_app t a b

ctx_mk_apps :: HCS => [Term] -> Elab Term
ctx_mk_apps es = case es of
  (e : es) -> foldM ctx_mk_app e es
  _ -> error ""

ctx_mk_binder_aux :: HCS => Id -> Maybe Name -> Type -> Term -> Elab Term
ctx_mk_binder_aux bname mn t e = do
  binder <- ctx_mk_term_ref bname
  let lam = mk_lam_aux mn t e
  ctx_mk_app (lift_term_to (_term_depth lam) binder) lam

ctx_mk_binder' :: HCS => Id -> Name -> Type -> Term -> Elab Term
ctx_mk_binder' bname name = ctx_mk_binder_aux bname # Just name

ctx_mk_binder :: HCS => Id -> Type -> Term -> Elab Term
ctx_mk_binder bname = ctx_mk_binder_aux bname Nothing

ctx_mk_binders' :: HCS => Id -> [(Name, Type)] -> Term -> Elab Term
ctx_mk_binders' bname args e = case args of
  [] -> pure e
  ((name, t) : args)-> do
    e <- ctx_mk_binders' bname args e
    ctx_mk_binder' bname name t e

ctx_mk_binders :: HCS => Id -> [Type] -> Term -> Elab Term
ctx_mk_binders bname ts e = case ts of
  [] -> pure e
  (t:ts)-> do
    e <- ctx_mk_binders bname ts e
    ctx_mk_binder bname t e

-----

parse_term_kw :: HCS => Name -> Elab ()
parse_term_kw name = p_scope "term_kw" # parse_word name

parse_type_params :: HCS => Elab [Type]
parse_type_params = pure [] ++> do
  p_tok ".{"
  ps <- p_sepf parse_type # p_tok ","
  p_tok "}"
  return ps

parse_type_params' :: HCS => Prop -> Elab [Type]
parse_type_params' p = if p then parse_type_params else pure []

parse_term_mvar :: HCS => Elab Term
parse_term_mvar = do
  p_scope "term" p_usc
  ctx_mk_term_mvar

parse_term_var :: HCS => Elab Term
parse_term_var = do
  name <- parse_term_var_name
  Just (i, t) <- ctx_get_term_var_by_name' name
  d <- ctx_term_depth
  return # mk_term_var d t i

parse_term_ref_aux :: HCS => Prop -> Elab Term
parse_term_ref_aux p_params = do
  i <- parse_term_ref_id
  ref <- ctx_get_term_ref i
  let ps_num = _def_ps_num ref
  let t = _def_type ref
  ps <- parse_type_params' p_params
  let n = len ps
  let dif = ps_num - n
  if not # dif < 0 then nop else error ""
  ps <- if dif == 0 then pure ps else do
    ps' <- ctx_mk_type_mvars dif
    return # ps ++ ps'
  t <- pure # subst_type_vars_in_type ps t
  d <- ctx_term_depth
  return # mk_term_ref d t i ps

parse_term_ref' :: HCS => Elab Term
parse_term_ref' = parse_term_ref_aux False

parse_term_ref :: HCS => Elab Term
parse_term_ref = parse_term_ref_aux True

parse_term_ident_aux' :: HCS => Elab Term
parse_term_ident_aux' = parse_term_var <++ parse_term_ref

parse_term_ident_aux :: HCS => Elab Term
parse_term_ident_aux = do
  res <- p_either (p_scope "term" parse_ops) parse_term_ident_aux'
  case res of
    Left [op] -> case _op_type op of
      Const -> do
        e <- ctx_mk_term_ref # _op_name op
        return e
      _ -> pfail
    Right e -> pure e
    _ -> error ""

parse_term_ident' :: HCS => Prop -> Elab Term
parse_term_ident' is_let = if not is_let
  then parse_term_ident_aux else do
    res <- ctx_try parse_term_ident_aux
    case res of
      Left msg -> pfail
      Right e -> do
        False <- p_prop # p_tok ":="
        return e

parse_term_ident :: HCS => Elab Term
parse_term_ident = parse_term_ident' False

parse_term_group :: HCS => Elab Term
parse_term_group = p_parens # with_new_ctx_stack # do
  e <- parse_term
  pure e ++> do
    p_tok ":"
    t <- parse_type
    add_type_eq t # _term_type e
    t <- res_type t
    return # e {_term_type = t}

parse_term_compr :: HCS => Elab Term
parse_term_compr = do
  name <- parse_term_var_name
  p_tok "|"
  ctx_set_term_parser_state 0
  fn <- ctx_mk_term_ref namei_compr
  ctx_enter_lam name mk_set_t
  e <- parse_term
  ctx_mk_app fn # mk_lam mk_set_t e

parse_term_set_filter :: HCS => Elab Term
parse_term_set_filter = do
  name <- parse_term_var_name
  p_scope "op" # p_tok sym_mem
  x <- parse_term
  p_tok "|"
  ctx_set_term_parser_state 0
  fn <- ctx_mk_term_ref namei_filter
  ctx_enter_lam name mk_set_t
  e <- parse_term
  ctx_mk_apps [fn, mk_lam mk_set_t e, x]

parse_term_set_fin :: HCS => Elab Term
parse_term_set_fin = do
  xs <- p_sepf' parse_term # do
    p_tok ","
    ctx_set_term_parser_state 0
  fn <- ctx_mk_term_ref namei_insert
  e <- ctx_mk_term_ref namei_emp
  (\f -> foldrM f e xs) # \x e ->
    ctx_mk_apps [fn, x, e]

parse_term_set :: HCS => Elab Term
parse_term_set = with_new_ctx # p_braces # choice
  [ parse_term_compr
  , parse_term_set_filter
  , parse_term_set_fin
  ]

parse_term_let_in :: HCS => Elab Term
parse_term_let_in = do
  parse_term_kw "in"
  e <- parse_term
  res_term e

parse_term_let' :: HCS => Elab Term
parse_term_let' = parse_term_let_in <++ do
  name <- parse_term_var_name
  p_tok ":="
  ctx_set_term_parser_state 0
  x <- parse_term' True
  t <- ctx_mk_type_mvar
  ctx_enter_lam name t
  ctx_set_term_parser_state 0
  e <- parse_term_let'
  t <- res_type t
  let f = mk_lam t e
  let_fn <- ctx_mk_term_ref namei_let_fn
  ctx_mk_apps [let_fn, x, f]

parse_term_let :: HCS => Elab Term
parse_term_let = do
  parse_term_kw "let"
  e <- with_new_ctx parse_term_let'
  d <- ctx_term_depth
  return # lift_term_to d e

parse_term_1' :: HCS => Prop -> Elab Term
parse_term_1' is_let = do
  0 <- gets_ctx _ctx_term_parser_state
  choice
    [ parse_term_part
    , parse_term_mvar
    , parse_term_ident' is_let
    , parse_term_group
    , parse_term_set
    , parse_term_let
    ]

parse_term_1 :: HCS => Elab Term
parse_term_1 = parse_term_1' False

parse_stat_1 :: HCS => Elab Stat
parse_stat_1 = do
  s <- parse_term_1
  add_type_eq mk_prop_t # _term_type s
  return s

parse_stat :: HCS => Elab Stat
parse_stat = do
  s <- parse_term
  add_type_eq mk_prop_t # _term_type s
  res_term s

parse_binder_args_type_aux :: HCS => Elab ([Name], Type)
parse_binder_args_type_aux = do
  names <- p_all parse_term_var_name
  p_tok ":"
  t <- parse_type
  mapM_ <~ names # \name ->
    ctx_enter_lam name t
  return (names, t)

parse_binder_args_type' :: HCS => Elab ([Name], Type)
parse_binder_args_type' =
  p_parens parse_binder_args_type_aux <++ do
    name <- parse_term_var_name
    t <- ctx_mk_type_mvar
    ctx_enter_lam name t
    return ([name], t)

parse_binder_args_type :: HCS => Elab [BinderArg]
parse_binder_args_type = do
  (names, t) <- parse_binder_args_type'
  return # map Left # zip names # repeat t

parse_binder_args_tc :: HCS => Elab [BinderArg]
parse_binder_args_tc = do
  p_tok "["
  e <- parse_term
  p_tok "]"
  return [Right e]

parse_binder_args_mem :: HCS => Elab [BinderArg]
parse_binder_args_mem = p_parens # do
  names <- p_all parse_term_var_name
  p_scope "op" # p_tok "∈"
  ctx_set_term_parser_state 0

  e <- parse_term
  xs <- mapM <~ names # \name -> do
    let t = mk_set_t
    d <- gets_ctx _ctx_term_depth
    ctx_enter_lam name t
    e1 <- ctx_mk_term_var d
    e2 <- ctx_mk_term_ref namei_mem
    e2 <- ctx_mk_apps [e2, e1, e]
    return [Left (name, t), Right e2]
  return # concat xs

parse_binder_args_group :: HCS => Elab [BinderArg]
parse_binder_args_group = with_new_ctx_stack # choice
  [ parse_binder_args_type
  , parse_binder_args_tc
  , parse_binder_args_mem
  ]

parse_binder_args :: HCS => Elab [BinderArg]
parse_binder_args = do
  xs <- p_all' parse_binder_args_group
  return # concat xs

parse_binder :: HCS => Prop -> Id -> Elab Term
parse_binder is_let name = do
  (args, e) <- with_new_ctx # do
    args <- parse_binder_args
    p_scope "op" # p_tok ","
    e <- parse_term_aux' is_let
    return (args, e)
  -- if name == sym_lam
  --   then pure # mk_lams' args e
  --   else ctx_mk_binders' name args e
  let bname = if name == namei_lam then Nothing else Just name
  ctx_mk_binders_adv bname args e

parse_op_aux :: HCS => Map Char (Trie Char [Op]) -> Elab [Op]
parse_op_aux mp = do
  char <- pget
  case Map.lookup char mp of
    Nothing -> pfail
    Just mm -> parse_ops' mm

parse_ops' :: HCS => Trie Char [Op] -> Elab [Op]
parse_ops' mm = do
  let val = _trie_val mm
  let mp = _trie_map mm
  let xs = maybe [] (\a -> [pure a]) val
  choice # parse_op_aux mp : xs

parse_ops :: HCS => Elab [Op]
parse_ops = do
  par <- get_par
  trimmed # parse_ops' # _term_par_syms par

parse_op :: HCS => Prop -> Elab Op
parse_op is_let = do
  [op] <- p_scope "op" # parse_ops
  case _op_type op of
    Const -> error "parse_op Const"
    Prefix -> ctx_push_op op
    Infix dir -> do
      case dir of
        Nothing -> error ""
        Just 0 -> do
          let pri = _op_pri op
          ctx_apply_op_while # \op -> _op_pri op >= pri
        Just 1 -> do
          let pri = _op_pri op
          ctx_apply_op_while # \op -> _op_pri op > pri
        _ -> error ""
      ctx_push_op op
    Binder -> do
      e <- parse_binder is_let # _op_name op
      ctx_push_term e
    a -> error # show a
  return op

show_stacks :: HCS => Elab ()
show_stacks = do
  ctx <- get_ctx
  print # Lit ["term_stack: ", show # _ctx_term_stack ctx]
  print # Lit ["op_stack: ", replicate 2 ' ', show # _ctx_op_stack ctx]

parse_term_aux' :: HCS => Prop -> Elab Term
parse_term_aux' is_let = trimmed # do
  -- logb' '>'
  -- ctx_trace
  
  res <- p_maybe # p_either
    (p_all # trimmed # parse_term_1' is_let)
    (parse_op is_let)
  
  e <- case res of
    Nothing -> do
      -- term_stack <- gets_ctx _ctx_term_stack
      -- op_stack <- gets_ctx _ctx_op_stack
      -- tracem "START"
      -- tracem term_stack
      -- print # map _op_name op_stack
      
      -- tracem ">>>"
      ctx_apply_op_while # const True
      -- tracem "<<<"

      -- term_stack <- gets_ctx _ctx_term_stack
      -- op_stack <- gets_ctx _ctx_op_stack
      -- tracem term_stack
      -- print # map _op_name op_stack
      -- tracem "END"

      ctx_pop_term
    Just res -> do
      -- ctx_trace
      -- print # Lit [show_either show show res]
      -- print # Lit []
      -- show_stacks
      
      case res of
        Left es -> do
          e <- ctx_mk_apps es
          ctx_push_term e
          ctx_set_term_parser_state 1
        Right op -> do
          st <- gets_ctx _ctx_term_parser_state
          let tp = _op_type op
          st1 <- pure # prop_to_bit # not #
            tp == Prefix || tp == Binder
          -- tracem (_op_name op, st, st1)
          True <- pure # st == st1
          ctx_set_term_parser_state 0

      -- print # Lit []
      -- show_stacks
      -- logb' '<'

      parse_term_aux' is_let

  return e

parse_term_aux :: HCS => Elab Term
parse_term_aux = parse_term_aux' False

parse_term' :: HCS => Prop -> Elab Term
parse_term' = parse_term_aux'

parse_term :: HCS => Elab Term
parse_term = parse_term_aux' False

ctx_apply_op_while :: HCS => (Op -> Prop) -> Elab ()
ctx_apply_op_while cond = do
  ops <- gets_ctx _ctx_op_stack
  if null ops then nop else do
    op <- ctx_pop_op
    if not # cond op then ctx_push_op op else do
      let name = _op_name op
      let is_appr = name == namei_appr
      f <- if is_appr then pure undefined else ctx_mk_term_ref name
      
      -- if not is_appr then nop else tracem op
      
      e <- case _op_type op of
        Prefix -> do
          a <- ctx_pop_term

          -- dd <- th_to_elab # gets _th_dbg
          -- if not dd then nop else do
          --   s1 <- th_to_elab # show_term f
          --   s2 <- th_to_elab # show_term a
          --   mp <- gets_ctx _ctx_terms
          --   print # Lit # pure # unlines' ["", s1, show # _term_depth f, s2]
          --   tracem mp

          ctx_mk_app f a
        Infix _ -> do
          -- if not # name == "or" then nop else do
          --   a <- gets_ctx _ctx_terms
          --   dbg a
          b <- ctx_pop_term
          a <- ctx_pop_term
          if is_appr
            then ctx_mk_app a b
            else ctx_mk_apps [f, a, b]
        a -> error # show a
      ctx_push_term e
      ctx_apply_op_while cond

-----

term_has_var :: HCS => N -> Term -> Prop
term_has_var i e = case _term_val e of
  TermVar j -> j == i
  Lam _ _ e -> term_has_var i e
  App a b -> term_has_var i a || term_has_var i b
  _ -> False

-----

eta_expand' :: HCS => Term -> (Type, Term)
eta_expand' e = case _term_val e of
  Lam _ t e -> (t, e)
  _ -> let
    Func t _ = _type_val # _term_type e
    i = _term_depth e
    d = i + 1
    in (t, mk_app_safe (lift_term 1 e) # mk_term_var d t i)

eta_expand :: HCS => Term -> Term
eta_expand e = if term_is_lam e then e else let
  (t, e') = eta_expand' e
  in mk_lam t e'

-----

get_type_mvars_in_term :: HCS => Term -> Set N
get_type_mvars_in_term e = Set.union
  (get_type_mvars_in_type # _term_type e) #
  case _term_val e of
    TermRef name ps -> Set.unions #
      map get_type_mvars_in_type ps
    Lam _ t e -> Set.union
      (get_type_mvars_in_type t)
      (get_type_mvars_in_term e)
    App a b -> Set.union
      (get_type_mvars_in_term a)
      (get_type_mvars_in_term b)
    WithType t e -> Set.union
      (get_type_mvars_in_type t)
      (get_type_mvars_in_term e)
    _ -> Set.empty

get_term_mvars_in_term :: HCS => Term -> Map N Type
get_term_mvars_in_term e = case _term_val e of
  TermMvar i -> Map.singleton i # _term_type e
  Lam _ _ e -> get_term_mvars_in_term e
  App a b -> Map.union
    (get_term_mvars_in_term a)
    (get_term_mvars_in_term b)
  WithType _ e -> get_term_mvars_in_term e
  _ -> Map.empty

split_app :: HCS => Term -> (Term, [Term])
split_app e = case _term_val e of
  App a b -> let
    (target, args) = split_app a
    in (target, snoc args b)
  _ -> (e, [])

-----

-- single_to_ast :: HCS => ExprVal -> Elab Expr
-- single_to_ast info = do
--   mx <- elab_get_term_pri
--   return # mk_ast mx mx info
-- 
-- un_op_to_ast :: HCS => Op -> Expr -> Elab Expr
-- un_op_to_ast op opnd = do
--   mx <- elab_get_term_pri
--   mx' <- elab_get_app_pri
--   let pri = _op_pri op
--   let sym = _op_sym op
--   let pri_left = _ast_pri_left opnd
--   let pri_right = _ast_pri_right opnd
--   (opnd, pri_right) <- pure # if pri > pri_left
--     then (mk_ast_group mx opnd, mx)
--     else (opnd, pri_right)
--   return # mk_ast mx' (min pri pri_right) # AstPrefix sym opnd
-- 
-- bin_op_to_ast :: HCS => Op -> Expr -> Expr -> Elab Expr
-- bin_op_to_ast op opnd1 opnd2 = do
--   mx <- elab_get_term_pri
--   let Infix dir = _op_type op
--   let pri = _op_pri op
--   let sym = _op_sym op
--   let pri_left1 = _ast_pri_left opnd1
--   let pri_right1 = _ast_pri_right opnd1
--   let pri_left2 = _ast_pri_left opnd2
--   let pri_right2 = _ast_pri_right opnd2
--   case dir of
--     Nothing -> error ""
--     Just 0 -> do
--       (opnd1, pri_left1) <- pure # if pri > pri_right1
--         then (mk_ast_group mx opnd1, mx)
--         else (opnd1, pri_left1)
--       (opnd2, pri_right2) <- pure # if pri >= pri_left2
--         then (mk_ast_group mx opnd2, mx)
--         else (opnd2, pri_right2)
--       return # mk_ast (min pri pri_left1) (min pri pri_right2) #
--         AstInfix sym opnd1 opnd2
--     Just 1 -> do
--       (opnd1, pri_left1) <- pure # if pri >= pri_right1
--         then (mk_ast_group mx opnd1, mx)
--         else (opnd1, pri_left1)
--       (opnd2, pri_right2) <- pure # if pri > pri_left2
--         then (mk_ast_group mx opnd2, mx)
--         else (opnd2, pri_right2)
--       return # mk_ast (min pri pri_left1) (min pri pri_right2) #
--         AstInfix sym opnd1 opnd2

app_to_expr :: HCS => ToExprFn a -> ToExprFn b -> Set Name -> Map N Name ->
  a -> b -> Elab Expr
app_to_expr f g names mp a b = do
  s1 <- f names mp a
  s2 <- g names mp b
  op <- elab_get_op_app
  expr_mk_bin_op scope_dflt op s1 s2

term_app_to_expr :: HCS => Set Name -> Map N Name -> Term -> Term -> Elab Expr
term_app_to_expr = app_to_expr term_to_expr term_to_expr

-- binder_to_expr :: HCS => Name -> Name -> ToExprFn a -> Set Name -> Map N Name ->
--   Op -> N -> Type -> a -> Elab Expr
-- binder_to_expr op_scope name_scope f names mp op d t x = do
--   (name, names) <- pure # term_mk_name names
--   mp <- pure # map_insert (d - 1) name mp
--   st <- type_to_expr t
--   s <- f names mp x
--   expr_mk_binder op_scope name_scope op name st s

-- term_binder_to_expr :: HCS => Set Name -> Map N Name -> Op ->
--   Type -> Term -> Elab Expr
-- term_binder_to_expr names mp op t e = binder_to_expr "op" "term"
--   term_to_expr names mp op (_term_depth e) t e

term_to_expr_binder_tc :: HCS => Set Name -> Map N Name -> String -> Term -> Term -> Elab Expr
term_to_expr_binder_tc names mp binder x y = do
  let f = term_to_expr names mp
  binder <- pure [("op", binder)]
  dflt <- pure # do
    sx <- f x
    return # ExprBinderGroupTc # _expr_val sx
  group <- case term_head x of
    Just (name, args) -> if name /= namei_mem then dflt else do
      args <- mapM f args
      [sa, sb] <- pure # map _expr_val args
      return # ExprBinderGroupTcMem sa sb
    _ -> dflt
  sy <- f y
  expr_mk_binder binder group sy

term_to_expr_binder_tc_args :: HCS => Set Name -> Map N Name -> String -> [Term] -> Elab Expr
term_to_expr_binder_tc_args names mp binder args = do
  [x, y] <- pure args
  term_to_expr_binder_tc names mp binder x y

term_ref_to_expr :: HCS => Id -> Maybe [Type] -> Elab Expr
term_ref_to_expr i ps = do
  name <- id_to_name i
  let s = [("term", name)]
  st <- case ps of
    Just ps@(_ : _) -> do
      st <- mapM type_to_expr ps
      return # [dfsc ".{"] ++
        intercalate [dfsc ", "] (map _expr_val st) ++
        [dfsc "}"]
    _ -> pure []
  expr_mk_atom # s ++ st

term_to_expr' :: HCS => Set Name -> Map N Name -> Term -> Elab Expr
term_to_expr' names mp e = do
  par <- gets_th _th_term_par
  case _term_val e of
    TermMvar i -> expr_mk_atom [("term", "?" ++ show i)]
    TermRef i ps -> do
      term_ref_to_expr i # Just ps
      -- term_ref_to_expr i Nothing
    TermVar i -> case map_get i mp of
      Just name -> expr_mk_atom [("term", name)]
      Nothing -> expr_mk_atom [(scope_err, usc' : show i)]
    Lam _ t e -> do
      -- let op = map_get undefined sym_lam # _term_par_names par
      -- term_binder_to_expr names mp op t e
      (name, names) <- pure # term_mk_name names
      mp <- pure # map_insert (_term_depth e - 1) name mp
      st <- type_to_expr t
      se <- term_to_expr names mp e
      let binder = [("op", sym_lam)]
      let group = ExprBinderGroupType [[("term", name)]] # _expr_val st
      expr_mk_binder binder group se
    App a b -> term_app_to_expr names mp a b
    WithType _ _ -> error ""

term_to_expr :: HCS => Set Name -> Map N Name -> Term -> Elab Expr
term_to_expr names mp e = do
  par <- gets_th _th_term_par
  mx <- elab_get_atom_pri
  let (target, args) = split_app e
  let n = len args
  let dflt = term_to_expr' names mp e
  if_pp <- pure # \f m -> do
    p <- gets_pp f
    ite p m dflt
  let args_m = mapM (term_to_expr names mp) args
  case _term_val target of
    TermRef name ps -> do
      dflt' <- pure # case Map.lookup name # _term_par_names par of
        Just op -> case _op_type op of
          Const -> if_pp _pp_term_sym #
            expr_mk_atom [("term", _op_sym op)]
          Prefix -> if_pp _pp_term_un_op #
            if n /= 1 then dflt else do
              [s] <- args_m
              expr_mk_un_op "op" op s
          Postfix -> if_pp _pp_term_un_op #
            error # show op
          Infix dir -> if_pp _pp_term_bin_op #
            if n == 0 then term_ref_to_expr name # Just ps
            else if n == 2 then do
              [s1, s2] <- args_m
              expr_mk_bin_op "op" op s1 s2
            else dflt
          Binder -> if n /= 1 then dflt else do
            let [arg] = args
            let Func t _ = _type_val # _term_type arg
            arg <- pure # if term_is_lam arg
              then arg else eta_expand arg
            Lam _ _ e <- pure # _term_val arg
            -- term_binder_to_expr names mp op t e
            (name, names) <- pure # term_mk_name names
            mp <- pure # map_insert (_term_depth e - 1) name mp
            st <- type_to_expr t
            se <- term_to_expr names mp e
            let binder = [("op", _op_sym op)]
            let group = ExprBinderGroupType [[("term", name)]] # _expr_val st
            expr_mk_binder binder group se
        Nothing -> if len args /= 2 then dflt else do
          f <- pure # \a -> term_to_expr_binder_tc_args names mp a args
          case name of
            -- "tc_univ" -> f sym_univ
            -- "tc_exi" -> f "∃"
            _ -> dflt
      pp_set_fin <- gets_pp _pp_set_fin
      if name == namei_emp then
        if not (pp_set_fin && len args == 0) then dflt' else do
          e <- dflt'
          return # e {_expr_info = Just (ExprFinset [])}
      else if name == namei_insert then
        if not (pp_set_fin && len args == 2) then dflt' else do
          [x, e] <- mapM (term_to_expr names mp) args
          case _expr_info e of
            Just (ExprFinset xs) -> do
              xs <- pure # _expr_val x : xs
              e <- expr_mk_atom # concat #
                [[(dfsc "{")], intercalate [(dfsc ", ")] xs, [(dfsc "}")]]
              return # e {_expr_info = Just (ExprFinset xs)}
            _ -> dflt'
      else dflt'
    _ -> dflt

show_term' :: HCS => Set Name -> Map N Name -> Term -> Elab Sctr
show_term' names mp e = do
  s1 <- term_to_expr names mp e
  return # _expr_val s1

show_term :: HCS => Term -> Elab Sctr
show_term e = do
  mp <- gets_ctx # map_inv . _ctx_term_names
  -- ref_names <- elab_get_names_str
  names <- pure # --Set.union ref_names #
    Set.fromList # Map.elems mp
  show_term' names mp e

show_term_1 :: HCS => Term -> Elab Sctr
show_term_1 e = do
  s <- show_term e
  dflt <- pure # pure # concat [[dfsc "("], s, [dfsc ")"]]
  fn <- pure # if elem space # s >>= snd then dflt else pure s
  case _term_val e of
    TermRef _ _ -> fn
    TermVar _ -> fn
    _ -> dflt

res_show_term' :: HCS => Set Name -> Map N Name -> Term -> Elab Sctr
res_show_term' names mp e = do
  e <- res_term e
  show_term' names mp e

res_show_term :: HCS => Term -> Elab Sctr
res_show_term e = do
  e <- res_term e
  show_term e

instance CtxShow Term where
  ctx_show = show_term

-----

term_to_kterm' :: HCS => {-Map Name N ->-} N -> Term -> K.Term
term_to_kterm' {-loc-} d e = case _term_val e of
  TermRef name ps ->  K.TermRef {-}(localize_name_mp loc name)-} name #
    map type_to_ktype ps
  TermVar i -> K.TermVar # d - i - 1
  Lam _ t e -> K.Lam (type_to_ktype t) # term_to_kterm' {-loc-} (d + 1) e
  App a b -> K.App (term_to_kterm' {-loc-} d a) # term_to_kterm' {-loc-} d b
  e -> error # show e

term_to_kterm :: HCS => Term -> Theory K.Term
term_to_kterm e = do
  -- loc <- gets _th_local_names
  return # term_to_kterm' {-loc-} 0 e

kterm_to_term' :: HCS => Map N Type -> K.Term -> Theory Term
kterm_to_term' mp e = do
  let d = map_size mp
  case e of
    K.TermRef name ps -> do
      def <- k_to_th # K.th_get_def name
      ps <- mapM ktype_to_type ps
      t <- ktype_to_type # K._def_type def
      t <- pure # subst_type_vars_in_type ps t
      return # mk_term_ref d t {-(unlocalize_name name)-} name ps
    K.TermVar i -> do
      i <- pure # d - i - 1
      case map_get i mp of
        Nothing -> error # show i
        Just t -> pure # mk_term_var d t i
    K.Lam t e -> do
      t <- ktype_to_type t
      e <- kterm_to_term' (map_insert d t mp) e
      return # mk_lam t e
    K.App a b -> do
      a <- kterm_to_term' mp a
      b <- kterm_to_term' mp b
      return # mk_app_safe a b

kterm_to_term :: HCS => K.Term -> Theory Term
kterm_to_term = kterm_to_term' Map.empty

-----

-- kterm_is_local :: HCS => K.Term -> Prop
-- kterm_is_local e = case e of
--   K.TermRef name _ -> kname_is_local name
--   K.Lam _ e -> kterm_is_local e
--   K.App a b -> kterm_is_local a || kterm_is_local b
--   _ -> False
-- 
-- localize_kterm' :: HCS => Map Name N -> K.Term -> K.Term
-- localize_kterm' loc e = case e of
--   K.TermRef name ps -> let
--     name' = localize_name_mp loc name
--     in K.TermRef name' ps
--   K.Lam t e -> K.Lam t # localize_kterm' loc e
--   K.App a b -> K.App (localize_kterm' loc a) #
--     localize_kterm' loc b
--   _ -> e
-- 
-- localize_kterm :: HCS => K.Term -> Theory K.Term
-- localize_kterm e = do
--   loc <- gets _th_local_names
--   return # localize_kterm' loc e

-----

term_part_simple :: HCS => Term -> [Bit] -> Maybe Term
term_part_simple e xs = case xs of
  [] -> pure e
  (x:xs) -> case _term_val e of
    Lam _ _ e -> do
      0 <- pure x
      term_part_simple e xs
    App a b -> do
      e <- pure # case x of
        0 -> a
        1 -> b
        _ -> error ""
      term_part_simple e xs
    _ -> Nothing

ctx_term_part_simple :: HCS => Term -> [Bit] -> Elab Term
ctx_term_part_simple e xs = case term_part_simple e xs of
  Just e -> pure e
  Nothing -> err [dfsc "term_part_simple"]

term_imp_info :: HCS => Term -> Maybe (Stat, Stat)
term_imp_info e = do
  App e b <- pure # _term_val e
  App e a <- pure # _term_val e
  TermRef namei_imp' [] <- pure # _term_val e
  True <- pure # namei_imp' == namei_imp
  return (a, b)

term_univ_info :: HCS => Term -> Maybe (Type, Term)
term_univ_info e = do
  App e f <- pure # _term_val e
  TermRef namei_univ' [t] <- pure # _term_val e
  True <- pure # namei_univ' == namei_univ
  return (t, f)

-----

lift_terms :: HCS => Term -> Term -> (Term, Term)
lift_terms a b = let
  d1 = _term_depth a
  d2 = _term_depth b
  d = max d1 d2
  in (lift_term_to d a, lift_term_to d b)

subst_term :: HCS => (TermVal -> Maybe Term) -> Term -> Term
subst_term f e = let
  t0 = _term_type e
  e0 = _term_val e
  in case f e0 of
    Just e_new -> lift_term_to (_term_depth e) e_new
    Nothing -> case e0 of
      Lam mn t e -> mk_lam_aux mn t # subst_term f e
      App a b -> mk_app t0 (subst_term f a) (subst_term f b)
      WithType t e -> mk_with_type t # subst_term f e
      _ -> e

subst_term_mvar_in_term :: HCS => N -> Term -> Term -> Term
subst_term_mvar_in_term i x = subst_term # \e -> case e of
  TermMvar j -> if j == i then pure x else Nothing
  _ -> Nothing

subst_term_mvars_in_term :: HCS => Map N Term -> Term -> Term
subst_term_mvars_in_term mp = subst_term # \e -> case e of
  TermMvar i -> Map.lookup i mp
  _ -> Nothing

subst_term_var_in_term :: HCS => N -> Term -> Term -> Term
subst_term_var_in_term i x = subst_term # \e -> case e of
  TermVar j -> if j == i then Just x else Nothing
  _ -> Nothing

-----

term_has_term_mvar :: HCS => N -> Term -> Prop
term_has_term_mvar i e = case _term_val e of
  TermMvar j -> j == i
  Lam _ _ e -> term_has_term_mvar i e
  App a b -> term_has_term_mvar i a || term_has_term_mvar i b
  WithType _ e -> term_has_term_mvar i e
  _ -> False

term_has_any_term_mvar :: HCS => N -> Term -> Elab Prop
term_has_any_term_mvar d e = case _term_val e of
  TermMvar i -> do
    d' <- ctx_get_term_mvar_depth i
    return # d' > d
  Lam _ _ e -> term_has_any_term_mvar d e
  App a b -> do
    res <- term_has_any_term_mvar d a
    if res then pure True else
      term_has_any_term_mvar d b
  WithType _ e -> term_has_any_term_mvar d e
  _ -> pure False

term_get_some_term_mvar' :: HCS => N -> Term -> Elab (Maybe N)
term_get_some_term_mvar' d e = case _term_val e of
  TermMvar i -> do
    d' <- ctx_get_term_mvar_depth i
    return # do
      True <- pure # d' > d
      return i
  Lam _ _ e -> term_get_some_term_mvar' d e
  App a b -> do
    res <- term_get_some_term_mvar' d a
    case res of
      Just _ -> pure res
      _ -> term_get_some_term_mvar' d b
  WithType _ e -> term_get_some_term_mvar' d e
  _ -> pure Nothing

term_get_some_term_mvar :: HCS => N -> Term -> Elab N
term_get_some_term_mvar d e = do
  Just i <- term_get_some_term_mvar' d e
  return i

-----

beta_reduce :: HCS => Term -> Elab Term
beta_reduce e = case _term_val e of
  Lam mn t e -> do
    e <- beta_reduce e
    return # mk_lam_aux mn t e
  App a b -> do
    let d = _term_depth e
    a <- beta_reduce a
    b <- beta_reduce b
    let dflt = mk_app (_term_type e) a b
    res <- term_has_any_term_mvar d a
    -- SAFE REDUCE
    if res then pure dflt else case _term_val a of
      Lam _ _ e -> beta_reduce # lift_term (-1) #
        subst_term_var_in_term (_term_depth a) b e
      _ -> pure dflt
  WithType _ e -> beta_reduce e
  _ -> pure e

eta_reduce :: HCS => Term -> Elab Term
eta_reduce e = case _term_val e of
  Lam mn t e -> do
    e <- eta_reduce e
    let e_dflt = mk_lam_aux mn t e
    let i = _term_depth e_dflt
    case _term_val e of
      App a b -> case _term_val b of
        TermVar j -> do
          has <- term_has_any_term_mvar i a
          return # if j == i && not
            ( term_has_term_var i a
            -- SAFE REDUCE
            || has
            ) then lift_term (-1) a else e_dflt
        _ -> pure e_dflt
      _ -> pure e_dflt
  App a b -> do
    a <- eta_reduce a
    b <- eta_reduce b
    return # mk_app (_term_type e) a b
  _ -> pure e

reduce_term :: HCS => Term -> Elab Term
reduce_term e = do
  e <- beta_reduce e
  eta_reduce e

-----

put_on_hold :: HCS => N -> Term -> Term -> Elab ()
put_on_hold i x e = do
  let eq = (x, e)
  mp <- gets_ctx _ctx_eqs_on_hold
  let eqs = eq : fromMaybe [] (Map.lookup i mp)
  modify_ctx # \ctx -> ctx
    {_ctx_eqs_on_hold = map_insert i eqs mp}

reinsert_on_hold :: HCS => N -> Elab ()
reinsert_on_hold i = do
  mp <- gets_ctx _ctx_eqs_on_hold
  case Map.lookup i mp of
    Nothing -> nop
    Just eqs -> do
      modify_ctx # \ctx -> ctx {_ctx_eqs_on_hold = Map.delete i mp}
      mapM_ (uncurry add_term_eq) eqs

get_term_mvar :: HCS => N -> Elab (Maybe Term)
get_term_mvar i = do
  ctx <- get_ctx
  let mp = _ctx_term_asgns ctx
  case Map.lookup i mp of
    Nothing -> pure Nothing
    Just e -> do
      e' <- res_term e
      if e == e' then nop else do
        put_ctx # ctx {_ctx_term_asgns = map_insert i e' mp}
        reinsert_on_hold i
      return # Just e'

res_term' :: HCS => Term -> Elab Term
res_term' e = if e_res e then pure e else do
  -- e <- pure # reduce_term e
  let d = _term_depth e
  t <- res_type # _term_type e
  case _term_val e of
    TermMvar i -> do
      res <- get_term_mvar i
      return # case res of
        Nothing -> mk_term_mvar d t i
        Just e -> do
          -- lift_term_to (max d # _term_depth e) e
          lift_term_to d e
    TermRef name ps -> do
      ps <- mapM res_type ps
      return # mk_term_ref d t name ps
    TermVar i -> pure # mk_term_var d t i
    Lam _ t e -> do
      t <- res_type t
      e <- res_term' e
      return # mk_lam t e
    App a b -> do
      a <- res_term' a
      b <- res_term' b
      -- (a, b) <- pure # lift_terms a b
      return # mk_app t a b
    WithType _ e -> res_term' e
      -- e <- res_term e
      -- return # mk_with_type t e

res_term :: HCS => Term -> Elab Term
res_term e = do
  -- s1 <- show_term e
  -- print # Lit ["(", show e, ", ", s1, ")"]
  
  e1 <- res_term' e
  -- print # e_res e
  
  ifm (_term_depth e1 /= _term_depth e) # do
    s1 <- fmap (>>= snd) # ctx_show e
    s2 <- fmap (>>= snd) # ctx_show e1
    error # unlines' ["", s1, s2]
  
  e2 <- reduce_term e1
  ifm (_term_depth e2 /= _term_depth e1) # error ""
  
  return e2

asgn_term :: HCS => N -> Term -> Elab ()
asgn_term i e = if term_has_term_mvar i e
  then do
    let d = _term_depth e
    let t = _term_type e
    x <- ctx_get_term_mvar t i
    put_on_hold i x e
  else do
    -- if_dbg # ifm (i == 205) # do
    --   j <- gets_ctx _ctx_id
    --   tracem j
    --   ctx_trace_term e
    mp <- gets_ctx _ctx_term_asgns
    modify_ctx # \ctx -> ctx {_ctx_term_asgns = map_insert i e mp}
    reinsert_on_hold i

add_term_eq :: HCS => Term -> Term -> Elab ()
add_term_eq lhs rhs = do
  -- n <- gets_ctx _ctx_mvars_num
  -- tracem n
  
  (lhs, rhs) <- pure # lift_terms lhs rhs
  
  let d1 = _term_depth lhs
  let d2 = _term_depth rhs
  ifm (d1 /= d2) # trace' (d1, d2) # error ""
  
  add_type_eq (_term_type lhs) (_term_type rhs)
  lhs <- res_term lhs
  rhs <- res_term rhs
  
  -- s1 <- show_term lhs
  -- s2 <- show_term rhs
  -- print # Lit # [s1, " --- ", s2]

  -- let d1 = _term_depth lhs
  -- let d2 = _term_depth rhs
  -- if d1 == d2 then nop else err ["..."]
  
  if lhs == rhs then nop else
    case (_term_val lhs, _term_val rhs) of
      (TermMvar i, _) -> asgn_term i rhs
      (_, TermMvar i) -> asgn_term i lhs
      (Lam _ t _, _) -> term_eq_eta t lhs rhs
      (_, Lam _ t _) -> term_eq_eta t lhs rhs
      _ -> do
        let (target1, args1) = split_app lhs
        let (target2, args2) = split_app rhs
        mismatch <- pure # do
          s1 <- res_show_term lhs
          s2 <- res_show_term rhs
          err # [dfsc "Term mismatch:\n\n", dfsc tab] ++
            s1 ++ [dfsc # '\n' : tab] ++ s2 ++ error ""
        case (_term_val target1, _term_val target2) of
          (TermMvar i, _) -> put_on_hold i lhs rhs
          (_, TermMvar i) -> put_on_hold i lhs rhs
          (TermRef name1 ps1, TermRef name2 ps2) ->
            if name1 /= name2 then mismatch else do
              zipWithM_ add_type_eq ps1 ps2
              zipWithM_ add_term_eq args1 args2
          (TermVar i, TermVar j) ->
            if i /= j then mismatch else do
              zipWithM_ add_term_eq args1 args2
          -- SAFE REDUCE
          (Lam _ _ _, _) -> do
            i <- term_get_some_term_mvar d1 lhs
            put_on_hold i lhs rhs
          (_, Lam _ _ _) -> do
            i <- term_get_some_term_mvar d1 rhs
            put_on_hold i lhs rhs
          _ -> mismatch

term_eq_eta :: HCS => Type -> Term -> Term -> Elab ()
term_eq_eta t lhs rhs = do
  let d = _term_depth lhs
  let x = mk_term_var (d + 1) t d
  lhs <- ctx_mk_app (lift_term 1 lhs) x
  rhs <- ctx_mk_app (lift_term 1 rhs) x
  add_term_eq lhs rhs

-----

term_to_univ :: HCS => Term -> Maybe (Type, Term)
term_to_univ e = do
  App e f <- pure # _term_val e
  TermRef namei_univ' [t] <- pure # _term_val e
  True <- pure # namei_univ' == namei_univ
  return (t, f)

term_to_univ' :: HCS => Term -> Maybe (Type, Term)
term_to_univ' e = do
  (t, f) <- term_to_univ e
  return # eta_expand' f

-- term_to_imp_or_tc_univ_aux :: HCS => Prop -> Term -> Maybe (Prop, Term, Term)
-- term_to_imp_or_tc_univ_aux tc_enabled e = case term_head e of
--   Just (name, [a, b]) ->  case name of
--     "imp" -> Just (False, a, b)
--     "tc_univ" -> do
--       True <- pure tc_enabled
--       return (True, a, b)
--     _ -> Nothing
--   _ -> Nothing
-- 
-- term_to_imp_or_tc_univ' :: HCS => Term -> Maybe (Prop, Term, Term)
-- term_to_imp_or_tc_univ' = term_to_imp_or_tc_univ_aux True
-- 
-- term_to_imp_or_tc_univ :: HCS => Term -> Elab (Maybe (Prop, Term, Term))
-- term_to_imp_or_tc_univ e = do
--   tc_enabled <- gets_th _th_tc_enabled
--   return # term_to_imp_or_tc_univ_aux tc_enabled e

term_to_imp :: HCS => Term -> Maybe (Term, Term)
term_to_imp e = do
  (name, [a, b]) <- term_head e
  True <- pure # name == namei_imp
  return (a, b)

term_to_univ_or_imp :: HCS => Term -> Maybe (Either (Type, Term) (Term, Term))
term_to_univ_or_imp e = maybe_either
  (term_to_univ e) (term_to_imp e)

-----

ctx_lift_term :: HCS => Term -> Elab Term
ctx_lift_term e = do
  d <- ctx_term_depth
  return # lift_term_to d e

-----

terms_eq :: HCS => Term -> Term -> Prop
terms_eq a b = let
  d = max (_term_depth a) (_term_depth b)
  in lift_term_to d a == lift_term_to d b

abstract_aux :: HCS => N -> (Term -> Prop) -> Term -> State (Maybe N, Maybe Term) Term
abstract_aux i f e = do
  (n, e0) <- get
  if n == Just (-1) then pure e else do
    let t = _term_type e
    if f e then do
      let e' = mk_term_var (_term_depth e) t i
      case n of
        Just n -> case e0 of
          Nothing -> if n == 0
            then do
              put (Just (-1), Just e)
              return # e'
            else do
              put (Just (n - 1), Nothing)
              return e
          Just e0 -> pure e
        Nothing -> case e0 of
          Nothing -> do
            put (Nothing, Just e)
            return e'
          Just e0 -> pure # if terms_eq e0 e then e' else e
    else case _term_val e of
        Lam mn t e -> do
          e' <- abstract_aux i f e
          return # mk_lam_aux mn t e'
        App a b -> do
          a' <- abstract_aux i f a
          b' <- abstract_aux i f b
          return # mk_app t a' b'
        WithType _ _ -> error ""
        _ -> pure e

abstract' :: HCS => Maybe N -> (Term -> Prop) -> Term -> Maybe (Term, Term)
abstract' n f e = do
  let d = _term_depth e
  (e, (_, Just e0)) <- pure # runState (abstract_aux d f # lift_term 1 e)
    (n, Nothing)
  return (lift_term (-1) e0, mk_lam (_term_type e0) e)

abstract :: HCS => (Term -> Prop) -> Term -> Maybe (Term, Term)
abstract = abstract' Nothing

ctx_abstract' :: HCS => Maybe N -> (Term -> Prop) -> Term -> Elab (Term, Term)
ctx_abstract' n f e = case abstract' n f e of
  Nothing -> tac_err_show "ctx_abstract" e
  Just x -> pure x

ctx_abstract :: HCS => (Term -> Prop) -> Term -> Elab (Term, Term)
ctx_abstract = ctx_abstract' Nothing

ctx_abstract_ref :: HCS => Term -> Id -> Elab ([Type], Term)
ctx_abstract_ref e name = do
  (e0, fn) <- ctx_abstract <~ e # \e -> case _term_val e of
    TermRef name1 _ -> name == name1
    _ -> False
  TermRef _ ps <- pure # _term_val e0
  return (ps, fn)

ctx_abstract_var :: HCS => Term -> N -> Elab (Term, Term)
ctx_abstract_var e i = do
  (x, fn) <- ctx_abstract <~ e # \e -> case _term_val e of
    TermVar j -> i == j
    _ -> False
  x <- ctx_lift_term x
  return (x, fn)

-----

-- norm_term_mvars' :: HCS => Term -> State (Map N N) Term
-- norm_term_mvars' e = do
--   let d = _term_depth e
--   let norm_type = norm_type_mvars' # _term_type e
--   case _term_val e of
--     TermMvar i -> do
--       t <- norm_type
--       j <- norm_mvar i
--       return # mk_term_mvar d t j
--     TermRef name ps -> do
--       t <- norm_type
--       ps <- mapM norm_type_mvars' ps
--       return # mk_term_ref d t name ps
--     TermVar i -> do
--       t <- norm_type
--       return # mk_term_var d t i
--     Lam mn t e -> do
--       t <- norm_type_mvars' t
--       e <- norm_term_mvars' e
--       return # mk_lam_aux mn t e
--     App a b -> do
--       a <- norm_term_mvars' a
--       b <- norm_term_mvars' b
--       return # mk_app_safe a b
--     _ -> error # show e

-- norm_term_mvars :: HCS => Term -> Term
-- norm_term_mvars e = evalState (norm_term_mvars' e) Map.empty

-----

term_to_pat' :: HCS => Term -> PatBuilder ()
term_to_pat' e = case _term_val e of
  TermMvar i -> do
    pat_push # PatKind PtTermMvar
    pat_push # PatN i
    type_to_pat' # _term_type e
  TermRef name ps -> do
    pat_push' [PatKind PtTermRef, PatId name]
    mapM_ type_to_pat' ps
  TermVar i -> pat_push' [PatKind PtTermVar, PatN i]
  Lam _ t e -> do
    pat_push # PatKind PtLam
    type_to_pat' t
    term_to_pat' e
  App a b -> do
    pat_push # PatKind PtApp
    term_to_pat' a
    term_to_pat' b
  _ -> error # show e

term_to_pat :: HCS => Term -> Pat
term_to_pat e = PatDepth (_term_depth e) :
  build_pat_fn term_to_pat' e

-----

-- match_term' :: HCS => N -> Term -> PatM a ()
-- match_term' d e = do
--   k <- pat_get_kind
--   case k of
--     PtTermMvar -> do
--       i <- pat_get_n
--       match_type' # _term_type e
--       pat_asgn_term i e
--     PtTermRef -> do
--       TermRef name ps <- pure # _term_val e
--       -- ptm <- pat_get_ptm
--       pat_get_str_lit name
--       mapM_ match_type' ps
--     PtTermVar -> do
--       TermVar i <- pure # _term_val e
--       -- tracem ("--->", d, i)
--       mp <- gets _pat_st_lam_mp
--       case Map.lookup i mp of
--         Just j -> pat_get_n_lit j
--         Nothing -> do
--           if i == d then error "match_term'" else nop
--           if i > d then lift [] else pat_get_n_lit # i - d
--     PtLam -> do
--       Lam _ t e' <- pure # _term_val e
--       match_type' t
-- 
--       mp <- gets _pat_st_lam_mp
--       let d1 = _term_depth e
--       let d2 = map_size mp --d1 - d - 1
--       let mp' = map_insert d1 d2 mp
--       -- tracem ("#", d, d1, d2)
--       modify # \st -> st {_pat_st_lam_mp = mp'}
-- 
--       match_term' d e'
-- 
--       modify # \st -> st {_pat_st_lam_mp = mp}
--     PtApp -> do
--       App a b <- pure # _term_val e
--       match_term' d a
--       match_term' d b
--     _ -> error # show k
-- 
-- match_term :: HCS => PatMap a -> N -> Term -> [(a, Map N Type, Map N Term)]
-- match_term ptm d e = do
--   st <- init_pat_sts ptm
--   st <- execStateT (match_term' d e) st
--   let ptm = _pat_st_ptm st
--   case _trie_val ptm of
--     Nothing -> []
--     Just a -> pure (a, _pat_st_types st, _pat_st_terms st)

-----

-- lams_to_mvars' :: HCS => Term -> Elab (Term, [Term])
-- lams_to_mvars' e = case _term_val e of
--   Lam _ t e -> do
--     let d = _term_depth e - 1
--     x <- ctx_mk_term_mvar' t
--     (e, mvars) <- lams_to_mvars' # --reduce_term #
--       subst_term_var_in_term d x e
--     return (e, x : mvars)
--   _ -> pure (e, [])
-- 
-- lams_to_mvars :: HCS => Term -> Elab (Term, [Term])
-- lams_to_mvars e = ctx_with_new_mvars # lams_to_mvars' e

term_get_intro_names :: HCS => Term -> [Name]
term_get_intro_names e = let
  (target, args) = split_app e
  in case _term_val target of
    TermRef name _ ->
      if name == namei_univ then case _term_val # head' undefined args of
        Lam (Just name) _ e -> name : term_get_intro_names e
        _ -> []
      else if name == namei_imp then term_get_intro_names # args !! 1
        -- "tc_univ" -> term_get_intro_names # args !! 1
      else []
    _ -> []

term_find_binder_arg_aux :: HCS => Id -> Term -> State N (Maybe Term)
term_find_binder_arg_aux name e = case _term_val e of
  Lam _ _ e -> term_find_binder_arg_aux name e
  App a b -> do
    dflt <- pure # do
      a <- term_find_binder_arg' name a
      case a of
        Nothing -> term_find_binder_arg' name b
        _ -> pure a
    case _term_val a of
      TermRef name1 _ -> if name1 /= name then dflt else do
        n <- get
        put # n - 1
        if n == 0 then pure # Just b else do dflt
      _ -> dflt
  _ -> pure Nothing

term_find_binder_arg' :: HCS => Id -> Term -> State N (Maybe Term)
term_find_binder_arg' name e = do
  mf <- term_find_binder_arg_aux name e
  return # do
    f <- mf
    return # lift_term_to (_term_depth e) f

term_find_binder_arg :: HCS => Id -> N -> Term -> Maybe Term
term_find_binder_arg name n e = evalState (term_find_binder_arg' name e) n

type_vars_to_mvars_in_term :: HCS => Term -> Term
type_vars_to_mvars_in_term e = let
  d = _term_depth e
  t = type_vars_to_mvars_in_type # _term_type e
  in case _term_val e of
    TermMvar i -> mk_term_mvar d t i
    TermRef name ps -> mk_term_ref d t name #
      map type_vars_to_mvars_in_type ps
    TermVar i -> mk_term_var d t i
    Lam mn t' e -> mk_lam_aux mn
      (type_vars_to_mvars_in_type t')
      (type_vars_to_mvars_in_term e)
    App a b -> mk_app t
      (type_vars_to_mvars_in_term a)
      (type_vars_to_mvars_in_term b)
    WithType t e -> mk_with_type
      (type_vars_to_mvars_in_type t)
      (type_vars_to_mvars_in_term e)

ctx_mk_imp :: HCS => Stat -> Stat -> Elab Stat
ctx_mk_imp a b = do
  add_type_eq mk_prop_t # _term_type a
  add_type_eq mk_prop_t # _term_type b
  a <- res_term a
  b <- res_term b
  return # mk_imp a b

ctx_mk_univ :: HCS => Term -> Elab Term
ctx_mk_univ f = do
  univ <- ctx_mk_term_ref namei_univ
  ctx_mk_app univ f

ctx_mk_term_var' :: HCS => Type -> N -> Elab Term
ctx_mk_term_var' t i = do
  d <- ctx_term_depth
  return # mk_term_var d t i

ctx_mk_term_var :: HCS => N -> Elab Term
ctx_mk_term_var i = do
  t <- ctx_mk_type_mvar
  ctx_mk_term_var' t i

ctx_show_term :: HCS => Term -> Elab ()
ctx_show_term e = do
  s1 <- show_term e
  print # Lit # map snd s1

max_term_var_in_term' :: HCS => N -> Term -> N
max_term_var_in_term' d0 e = case _term_val e of
  TermMvar _ -> -1
  TermRef _ _ -> -1
  TermVar i -> if i < d0 then i else -1
  Lam _ _ e -> max_term_var_in_term' d0 e
  App a b -> max (max_term_var_in_term' d0 a) (max_term_var_in_term' d0 b)
  WithType _ e -> max_term_var_in_term' d0 e

max_term_var_in_term :: HCS => Term -> N
max_term_var_in_term e = max_term_var_in_term' (_term_depth e) e

term_wout_lams :: HCS => Term -> Term
term_wout_lams e = case _term_val e of
  Lam _ _ e -> term_wout_lams e
  _ -> e

term_wout_univs :: HCS => Term -> Term
term_wout_univs e = case term_to_univ' e of
  Nothing -> e
  Just (_, f) -> term_wout_univs f

ctx_get_term_level :: HCS => Id -> Elab N
ctx_get_term_level name = do
  ref <- ctx_get_term_ref name
  return # _def_level ref

term_calc_level :: HCS => Term -> Elab N
term_calc_level e = case _term_val e of
  TermRef name _ -> ctx_get_term_level name
  Lam _ _ e -> term_calc_level e
  App a b -> do
    lvl1 <- term_calc_level a
    lvl2 <- term_calc_level b
    return # max lvl1 lvl2
  WithType _ e -> term_calc_level e
  _ -> pure (-1)

-----

stat_get_pos_info :: HCS => Stat -> (Stat, Prop)
stat_get_pos_info s = let dflt = (s, True) in
  case _term_val s of
    App a b -> case _term_val a of
      TermRef name _ -> if name == namei_not
        then (b, False) else dflt
      _ -> dflt
    _ -> dflt

term_refs_num :: HCS => Id -> Term -> N
term_refs_num name e = case _term_val e of
  TermRef name1 _ -> ite (name1 == name) 1 0
  Lam _ _ e -> term_refs_num name e
  App a b -> term_refs_num name a + term_refs_num name b
  WithType _ a -> term_refs_num name a
  _ -> 0

-----

term_head_with_types_fst :: HCS => Term -> Maybe ((Id, N), [Type], [Term])
term_head_with_types_fst e = do
  let (target, args) = split_app e
  TermRef name ps <- pure # _term_val target
  return ((name, 0), ps, args)

term_head_with_types_last :: HCS => Term -> Maybe ((Id, N), [Type], [Term])
term_head_with_types_last e = case term_to_univ_or_imp e of
  Just (Left (_, f)) -> term_head_with_types_last f
  Just (Right (a, b)) -> do
    ((name, n), ps, args) <- term_head_with_types_last b
    n <- pure # n + term_refs_num name a
    return ((name, n), ps, args)
  _ -> term_head_with_types_fst e

term_head_with_types' :: HCS => Prop -> Term -> Maybe ((Id, N), [Type], [Term])
term_head_with_types' last = ite last term_head_with_types_last term_head_with_types_fst

term_head_with_types :: HCS => Term -> Maybe (Id, [Type], [Term])
term_head_with_types e = do
  ((name, _), ps, args) <- term_head_with_types' False e
  return (name, ps, args)

term_head_fst :: HCS => Term -> Maybe ((Id, N), [Term])
term_head_fst e = do
  (a, _, b) <- term_head_with_types_fst e
  return (a, b)

term_head_last :: HCS => Term -> Maybe ((Id, N), [Term])
term_head_last e = do
  (a, _, b) <- term_head_with_types_last e
  return (a, b)

term_head' :: HCS => Prop -> Term -> Maybe ((Id, N), [Term])
term_head' last e = do
  (a, _, b) <- term_head_with_types' last e
  return (a, b)

term_head :: HCS => Term -> Maybe (Id, [Term])
term_head e = do
  (a, _, b) <- term_head_with_types e
  return (a, b)

term_head_name' :: HCS => Prop -> Term -> Maybe (Id, N)
term_head_name' last e = do
  (name_n, _) <- term_head' last e
  return name_n

term_head_name :: HCS => Term -> Maybe Id
term_head_name e = do
  (name, _) <- term_head e
  return name

ctx_term_head_with_types :: HCS => Term -> Elab (Id, [Type], [Term])
ctx_term_head_with_types e = do
  Just res <- pure # term_head_with_types e
  return res

ctx_term_head :: HCS => Term -> Elab (Id, [Term])
ctx_term_head e = do
  (a, _, b) <- ctx_term_head_with_types e
  return (a, b)

ctx_term_head_name' :: HCS => Prop -> Term -> Elab (Id, N)
ctx_term_head_name' last e = do
  Just res <- pure # term_head_name' last e
  return res

ctx_term_head_name :: HCS => Term -> Elab Id
ctx_term_head_name e = do
  Just res <- pure # term_head_name e
  return res

-----

term_get_free_vars :: HCS => Term -> Set N
term_get_free_vars e = case _term_val e of
  TermVar i -> Set.singleton i
  Lam _ _ e -> Set.delete (_term_depth e) #
    term_get_free_vars e
  App a b -> Set.union
    (term_get_free_vars a)
    (term_get_free_vars b)
  WithType _ e -> term_get_free_vars e
  _ -> Set.empty

-----

ref_to_proof :: HCS => Id -> [Type] -> Thm -> Elab Proof
ref_to_proof name ps ref = do
  let ps_num = _thm_ps_num ref
  let s = _thm_stat ref
  let n = len ps
  let dif = ps_num - n
  ifm (dif < 0) # error ""
  ps <- if dif == 0 then pure ps else do
    ps' <- ctx_mk_type_mvars dif
    return # ps ++ ps'
  s <- ctx_lift_term # subst_type_vars_in_term ps s
  return # mk_proof_ref s name ps False

ctx_mk_proof_ref' :: HCS => Id -> [Type] -> Elab Proof
ctx_mk_proof_ref' name ps = do
  ref <- ctx_get_proof_ref name
  ref_to_proof name ps ref

ctx_mk_proof_ref :: HCS => Id -> Elab Proof
ctx_mk_proof_ref name = ctx_mk_proof_ref' name []

ctx_mk_proof_var' :: HCS => Stat -> N -> Elab Proof
ctx_mk_proof_var' s i = do
  d <- ctx_term_depth
  s <- pure # lift_term_to d s
  return # mk_proof_var s i False

ctx_mk_proof_var :: HCS => N -> Elab Proof
ctx_mk_proof_var i = do
  s <- ctx_get_proof_var i
  ctx_mk_proof_var' s i

parse_proof_var :: HCS => Elab Proof
parse_proof_var = do
  i <- parse_proof_index
  Just s <- ctx_get_proof_var' i
  p <- ctx_mk_proof_var' s i
  return # p {_proof_target_asm = Just i}

parse_proof_ref' :: HCS => Id -> Elab Proof
parse_proof_ref' i = do
  res <- get_thm' i
  case res of
    Nothing -> pfail
    Just ref -> do
      ps <- parse_type_params
      ref_to_proof i ps ref

parse_proof_ref :: HCS => Elab Proof
parse_proof_ref = do
  i <- parse_proof_ref_id
  parse_proof_ref' i

parse_proof_ident :: HCS => Elab Proof
parse_proof_ident = parse_proof_var <++ parse_proof_ref

-----

parse_term_part_goal_stat :: HCS => Elab Term
parse_term_part_goal_stat = do
  p_tok "⊢"
  ctx_get_goal_stat

parse_term_part_index' :: HCS => N -> Elab [N]
parse_term_part_index' dn = if dn == 0 then pure [] else do
  d <- p_digit
  ds <- parse_term_part_index' # dn - 1
  return # d : ds

parse_term_part_index :: HCS => N -> Elab N
parse_term_part_index dn = do
  ds <- parse_term_part_index' dn
  return # (\f -> foldl' f 0 ds) # \n d -> n * 10 + d

parse_term_part' :: HCS => Term -> Elab Term
parse_term_part' e = (p_ws >> pure e) ++> case _term_val e of
  Lam _ t e -> do
    e <- parse_term_part' e
    return # mk_lam t e
  App _ _ -> do
    let (_, args) = split_app e
    let args_num = len args
    let dn = len # show # args_num - 1
    i <- parse_term_part_index dn
    Just e <- pure # list_get i args
    parse_term_part' e
  _ -> pfail

parse_term_part :: HCS => Elab Term
parse_term_part = do
  s <- parse_term_part_goal_stat <++ do
    p <- parse_proof_ident
    return # _proof_stat p
  p_tok "|"
  parse_term_part' s

-----

ctx_trace_term :: HCS => Term -> Elab ()
ctx_trace_term e = do
  s1 <- show_term e
  ctx_trace s1

-----

ctx_mk_binders_adv' :: HCS => Maybe Id -> Either (Name, Type) Term -> Term -> Elab Term
ctx_mk_binders_adv' bname arg e = case arg of
  Left (name, t) -> case bname of
    Nothing -> pure # mk_lam' name t e
    Just bname -> ctx_mk_binder' bname name t e
  Right c -> do
    bname <- case bname of
      Nothing -> pure namei_tc_lam
      Just bname ->
        if bname == namei_univ then pure namei_imp
        else if bname == namei_exi then pure namei_and
        else pfail
    -- mk_priv_name' bname "tc"
    e1 <- ctx_mk_term_ref bname
    ctx_mk_apps [e1, c, e]

ctx_mk_binders_adv :: HCS => Maybe Id -> [Either (Name, Type) Term] -> Term -> Elab Term
ctx_mk_binders_adv bname args e = foldrM (ctx_mk_binders_adv' bname) e args

-----

term_calc_imps_num :: HCS => Term -> N
term_calc_imps_num e = case term_to_imp e of
  Nothing -> 0
  Just (_, e) -> 1 + term_calc_imps_num e

-- term_calc_imps_and_tc_univs_num :: HCS => Term -> N
-- term_calc_imps_and_tc_univs_num e = case term_to_imp_or_tc_univ' e of
--   Nothing -> 0
--   Just (_, _, e) -> 1 + term_calc_imps_and_tc_univs_num e

term_find_ref_ps_list :: HCS => Id -> Term -> [[Type]]
term_find_ref_ps_list name e = case _term_val e of
  TermRef name1 ps -> do
    True <- pure # name == name1
    return ps
  Lam _ _ e -> term_find_ref_ps_list name e
  App a b -> term_find_ref_ps_list name a ++
    term_find_ref_ps_list name b
  WithType _ e -> term_find_ref_ps_list name e
  _ -> []

term_find_ref_ps :: HCS => Id -> N -> Term -> Maybe [Type]
term_find_ref_ps name n e = list_get n #
  term_find_ref_ps_list name e