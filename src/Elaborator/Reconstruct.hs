module Elaborator.Reconstruct where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Maybe
import Data.Either
import Data.List
import Data.Char
import Control.Monad
import Control.Monad.State
import Prelude hiding (putStrLn, print)

import Util
import Sctr
import Bit
import Tree
import Trie
import Bimap
import Trimap
import Serializer
import Parser

import Elaborator.Util
import Elaborator.Data
import Elaborator.Parser
import Elaborator.Type
import Elaborator.Term
import Elaborator.Proof
import Elaborator.Theory
import Elaborator.Goal
import Elaborator.ProofCompressor

import qualified Kernel as K

show_tac_proof' :: TacProof -> Elab ([String], Prop)
show_tac_proof' p = case p of
  TacProofRef i -> do
    name <- id_to_name i
    return ([name], False)
  TacProofVar i -> do
    name <- pure # 'h' : nat_to_sub i
    return ([show # PVar name], False)
  TacProofTerm e -> do
    s <- fmap (map snd) # show_term_1 e
    return (s, False)
  TacProofApp p1 p2 -> do
    (s1, h1) <- show_tac_proof' p1
    (s2, h2) <- show_tac_proof' p2
    s2 <- pure # if not h2 then s2 else
      ["("] ++ s2 ++ [")"]
    return (s1 ++ [" "] ++ s2, True)

show_tac_proof :: TacProof -> Elab [String]
show_tac_proof p = fmap fst # show_tac_proof' p

show_tac_rw :: TacRw -> Elab [String]
show_tac_rw rw = case rw of
  TacRwRef i -> do
    name <- id_to_name i
    return [name]

show_tac_rws :: [TacRw] -> Elab [String]
show_tac_rws rws = case rws of
  [rw] -> show_tac_rw rw
  _ -> do
    ss <- mapM show_tac_rw rws
    return # ["["] ++ intercalate [", "] ss ++ ["]"]

show_tac1 :: Tac1 -> Elab [String]
show_tac1 tac1 = case tac1 of
  TacIntro vs -> do
    names <- mapM <~ vs # \v -> do
      let name = show v
      case v of
        TVar _ -> do
          -- t <- ctx_mk_type_mvar
          t <- pure undefined
          ctx_enter_lam name t
        PVar _ -> do
          -- Maybe Name -> N -> Stat -> Elab ()
          i <- ctx_get_avail_proof_id
          s <- pure undefined
          ctx_enter_proof_lam (Just name) i s
      return name
    return ["in ", unwords names]
  TacImpE s -> do
    ss <- show_term s
    return # concat [["imp_e "], map snd ss]
  TacUnivE f x -> do
    sf <- fmap (map snd) # show_term_1 f
    sx <- fmap (map snd) # show_term_1 x
    return # concat [["univ_e "], sf, [" "], sx]
  TacApply h_ex p -> fmap ((ite h_ex "ex" "ap" ++ " ") :) # show_tac_proof p
  TacTc -> pure ["tc"]
  TacTriv -> pure ["triv"]
  TacRefl -> pure ["refl"]
  TacRw rws -> do
    s <- show_tac_rws rws
    return # "rw " : s
  TacSimp rws -> do
    s <- if null rws then pure [] else do
      s <- show_tac_rws rws
      return # " " : s
    return # "simp" : s
  TacOther s -> pure [s]

show_tac' :: N -> Prop -> Prop -> Tac -> Elab [String]
show_tac' indt is_top is_block tac = do
  let tac1 = _tac_tac1 tac
  let tacs = _tac_tacs tac
  if is_block then do
    ctx <- get_ctx
    s <- show_tac' (indt + 1) False False tac
    put_ctx ctx
    s <- pure # concat
      [ [indent indt "{\n"], s
      , ["\n", indent indt "}", ite is_top "" ","]
      ]
    return s
  else do
    s <- show_tac1 tac1
    s <- pure # concat [[indent indt ""], s, [","]]
    ss <- case tacs of
      [] -> pure []
      [tac] -> do
        s <- show_tac' indt False False tac
        return [s]
      _ -> do
        tacs' <- pure # init' tacs
        tac <- pure # last' undefined tacs
        ss <- mapM (show_tac' indt False True) tacs'
        s <- show_tac' indt False False tac
        return # snoc ss s
    s <- pure # intercalate ["\n"] # s : ss
    return s

show_tac :: Tac -> Elab String
show_tac tac = do
  pst0 <- get_pst
  ctx_init
  s <- show_tac' 0 True True tac
  put_pst pst0
  return # concat s

proof_to_tac_aux :: Proof -> Elab Tac
proof_to_tac_aux p = do
  let sp = _proof_stat p
  case _proof_val p of
    ProofRef i _ -> pure # Tac (TacApply True # TacProofRef i) []
    ProofVar i -> pure # Tac (TacApply True # TacProofVar i) []
    ImpI _ _ i _ p1 -> do
      let name = new_proof_index_to_name i
      tac1 <- proof_to_tac_aux p1
      return # Tac (TacIntro [PVar name]) [tac1]
    UnivI _ _ p1 -> do
      i <- pure # _term_depth sp
      let name = new_term_index_to_name i
      tac1 <- proof_to_tac_aux p1
      return # Tac (TacIntro [TVar name]) [tac1]
    ImpE p1 p2 -> do
      tac1 <- proof_to_tac_aux p1
      tac2 <- proof_to_tac_aux p2
      sp1 <- res_term # _proof_stat p1
      s <- case term_to_imp sp1 of
        Just (s, _) -> pure s
        Nothing -> do
          s <- show_term sp1
          tac_err "proof_to_tac_aux.ImpE" s
      return # Tac (TacImpE s) [tac1, tac2]
    UnivE p1 x -> do
      tac1 <- proof_to_tac_aux p1
      sp1 <- res_term # _proof_stat p1
      f <- case term_to_univ sp1 of
        Just (_, f) -> pure f
        Nothing -> do
          s <- show_term sp1
          tac_err "proof_to_tac_aux.ImpE" s
      return # Tac (TacUnivE f x) [tac1]
    pv -> error # show pv

get_priv_name_rc :: Id -> Id -> Elab (Maybe (Maybe Id))
get_priv_name_rc k j = th_to_elab # th_sim_dflt_ids [k, j] # do
  mp <- gets _th_rels
  return # do
    set <- map_get k # _trimap3 mp
    return # do
      (i, _) <- find ((j ==) . snd) # Set.toList set
      return i

is_priv_name :: Id -> Id -> Elab Prop
is_priv_name i j = th_to_elab # th_has_id_rel j i

-- simp_tac :: Tac -> Elab Tac
-- simp_tac (Tac tac1 tacs) = do
--   tacs <- mapM simp_tac tacs
--   let r = simp_tac
--   
--   let r_tac1 = pure # Tac tac1 []
--   let r_tc = pure # Tac TacTc []
--   let r_triv = pure # Tac TacTriv []
--   let r_refl = pure # Tac TacRefl []
--   let dflt = pure # Tac tac1 tacs
--   
--   f1 <- pure # \x p tacs -> do
--     let dflt1 = r # Tac (TacApply False p) tacs
--     case x of
--       Nothing -> dflt1
--       Just x -> case _type_val # _term_type x of
--         -- Func _ _ -> r # Tac (TacApply True # TacProofApp p # TacProofTerm x) tacs
--         -- _ -> dflt1
--         _ -> r # Tac (TacApply True # TacProofApp p # TacProofTerm x) tacs
--   
--   let tac_info = (tac1, tacs)
--   
--   stage_3 <- pure # case tac_info of
--     _ -> dflt
--   
--   stage_2 <- pure # case tac_info of
--     (TacApply True (TacProofRef i), []) -> do
--       h <- is_priv_name namei_tc i
--       if h then r_tc else stage_3
--     (TacApply False (TacProofApp (TacProofRef i) (TacProofTerm _)), tacs) -> do
--       res <- get_priv_name_rc namei_def i
--       case res of
--         Nothing -> stage_3
--         Just (Just i) -> r # Tac (TacRw [TacRwRef i]) tacs
--         _ -> error ""
--     _ -> stage_3
--   
--   stage_1 <- pure # case tac_info of
--     (TacIntro xs, [Tac (TacIntro ys) tacs]) ->
--       r # Tac (TacIntro # xs ++ ys) tacs
--     (TacApply True p1, (Tac (TacApply True p2) [] : tacs)) ->
--       r # Tac (TacApply True (TacProofApp p1 p2)) tacs
--     (TacUnivE _ x, Tac (TacApply True p) [] : tacs) -> f1 (Just x) p tacs
--     (TacUnivE _ x, Tac (TacApply False p) [] : tacs) -> f1 (Just x) p tacs
--     (TacImpE _, Tac (TacApply _ p) [] : tacs) -> f1 Nothing p tacs
--     (TacUnivE _ _, [Tac TacTc []]) -> r_tc
--     (TacImpE _, [Tac TacTc [], Tac TacTc []]) -> r_tc
--     (TacImpE _, [Tac TacTc [], Tac (TacApply _ (TacProofVar _)) []]) -> r_tc
--     
--     (TacApply _ p, [Tac (TacApply True (TacProofVar _)) []]) -> r_tac1
--     -- (_, [Tac (TacApply True (TacProofVar _)) []]) -> r_tac1
--     
--     (TacApply True (TacProofRef 43), []) -> r_triv
--     (TacApply False p, [Tac TacTriv []]) -> r_tac1
--     -- (TacApply ex (TacProofApp p (TacProofTerm x)), []) ->
--     --   case _type_val # _term_type x of
--     --     Func _ _ -> stage_2
--     --     _ -> r # Tac (TacApply ex p) []
--     (TacIntro [PVar i], [Tac (TacApply True (TacProofVar j)) []]) ->
--       if i /= j then stage_2 else do
--         h <- th_to_elab # kth_has_id namei_imp_refl
--         if h then r_refl else stage_2
--     (TacApply True (TacProofApp (TacProofRef i) _), []) -> do
--       h <- is_priv_name namei_refl i
--       if h then r_refl else stage_2
--     (TacRw _, [Tac TacRefl []]) -> r_tac1
--     -- (TacImpE _, [tac, Tac TacTc []]) -> pure tac
--     -- (TacApply False p1, (Tac (TacApply True p2) [] : tacs)) ->
--     --   r # Tac (TacApply True # TacProofApp p1 p2) tacs
--     _ -> stage_2
--   
--   stage_1

proof_to_tac' :: Proof -> Elab Tac
proof_to_tac' = proof_to_tac_aux

kproof_to_tac' :: K.Proof -> Elab Tac
kproof_to_tac' p = do
  p <- th_to_elab # kproof_to_proof p
  proof_to_tac' p

proof_to_tac :: Proof -> Elab Tac
proof_to_tac = proof_to_tac'

kproof_to_tac :: K.Proof -> Elab Tac
kproof_to_tac = kproof_to_tac'

-----

tac_push :: Tac -> Tac -> Tac
tac_push tac (Tac tac1 tacs) = Tac tac1 # snoc tacs tac

tacs_to_tac' :: Tac -> [Tac] -> Tac
tacs_to_tac' tac tacs = case tacs of
  [] -> tac
  (tac_prev : rest) -> tacs_to_tac' (tac_push tac tac_prev) rest

tacs_to_tac :: [Tac] -> Tac
tacs_to_tac tacs = case tacs of
  [] -> error ""
  (tac : tacs) -> tacs_to_tac' tac tacs

ctx_get_tacs :: Elab [Tac]
ctx_get_tacs = gets_ctx _ctx_tacs

ctx_get_tac :: Elab Tac
ctx_get_tac = fmap tacs_to_tac ctx_get_tacs

new_term_index_to_name :: N -> Name
new_term_index_to_name i = [fromJust # list_get i ['a' ..]]

new_proof_index_to_name :: N -> Name
new_proof_index_to_name i = 'h' : nat_to_sub i

ctx_show_tacs :: Elab String
ctx_show_tacs = ctx_get_tac >>= show_tac

recon_push_1 :: Tac1 -> Elab ()
recon_push_1 tac = ifm_recon # modify_ctx # \ctx -> ctx
  {_ctx_tacs = Tac tac [] : _ctx_tacs ctx}