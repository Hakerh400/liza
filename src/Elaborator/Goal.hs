module Elaborator.Goal where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Maybe
import Data.Either
import Data.List
import Data.Char
import Control.Monad
import Control.Monad.State

import Util
import Sctr
import Elaborator.Util
import Elaborator.Data
import Elaborator.Type
import Elaborator.Term
import Elaborator.Parser

import qualified Kernel as K

-- init_goal :: N -> Map Name N -> Term -> Goal
-- init_goal i ps stat = Goal
--   { _goal_id = i
--   , _goal_types = ps
--   , _goal_terms = Map.empty
--   , _goal_terms' = Map.empty
--   , _goal_proofs = Map.empty
--   , _goal_stat = stat
--   }

goal_get_asm :: Name -> Goal -> Elab Term
goal_get_asm name goal = case find ((== name) . fst) # _goal_proofs goal of
  Just (_, stat) -> pure stat
  Nothing -> err [dfsc "Assumption ", ("proof", name), dfsc " not found"]

goal_get_names :: Goal -> Set Name
goal_get_names goal = Set.unions
  [ Map.keysSet # _goal_types goal
  , Map.keysSet # _goal_terms goal
  , Set.fromList # map fst # _goal_proofs goal
  ]

-- goal_to_expr_ctx :: Goal -> ExprCtx
-- goal_to_expr_ctx goal = Map.fromList # concat
--   [ map (\name -> (name, ExprType)) # Set.toList # _goal_types goal
--   , map (\(name, _) -> (name, ExprTerm)) # Map.toList # _goal_terms goal
--   , map (\(name, _) -> (name, ExprProof)) # Map.toList # _goal_proofs goal
--   ]

goal_has_name :: Name -> Goal -> Prop
goal_has_name name goal = Set.member name # goal_get_names goal

goal_has_type :: Name -> Goal -> Prop
goal_has_type name goal = Map.member name # _goal_types goal

goal_has_term :: Name -> Goal -> Prop
goal_has_term name goal = Map.member name # _goal_terms goal

goal_has_proof :: Name -> Goal -> Prop
goal_has_proof name goal = any ((== name) . fst) # _goal_proofs goal

goal_get_term :: Name -> Goal -> Maybe Type
goal_get_term name goal = do
  (_, t) <- Map.lookup name # _goal_terms goal
  return t

goal_get_proof :: Name -> Goal -> Maybe Term
goal_get_proof name goal = fmap snd #
  find ((== name) . fst) # _goal_proofs goal

-- goal_get_avail_term_name :: Goal -> Name
-- goal_get_avail_term_name goal = let
--   names = Map.keysSet # _goal_terms goal
--   in get_avail_of_set term_var_to_str names
-- 
-- goal_get_avail_proof_name :: Goal -> Name
-- goal_get_avail_proof_name goal = let
--   names = Map.keysSet # _goal_proofs goal
--   in get_avail_of_set proof_var_to_str names

-----

goal_init_ctx :: Goal -> Ctx
goal_init_ctx goal = init_ctx' # _goal_id goal
  -- { _ctx_types = _goal_types goal
  -- , _ctx_terms_num = 0
  -- , _ctx_terms = Map.empty
  -- , _ctx_proofs = Map.empty
  -- , _ctx_term_stack = []
  -- , _ctx_op_stack = []
  -- , _ctx_mvars_num = 0
  -- , _ctx_type_asgns = Map.empty
  -- , _ctx_term_asgns = Map.empty
  -- , _ctx_eqs_on_hold = Map.empty
  -- }

-----

show_goal_types :: Map Name N -> Elab Sctr
show_goal_types names = pure [] {-if Map.null names
  then pure ""
  else pure # concat ["Types: ", unwords # Map.keys names]-}

show_term_in_goal :: Goal -> Term -> Elab Sctr
show_term_in_goal goal e = do
  -- names <- elab_get_names_str
  names <- pure # --Set.union names #
    Map.keysSet # _goal_terms goal
  res_show_term' names (_goal_terms' goal) e

show_goal_elem :: Name -> (a -> Elab Sctr) -> Name -> a -> Elab Sctr
show_goal_elem scope f name a = do
  -- s1 <- pure # case name of
  --   ('*' : name) -> [dfsc "*", (scope, name)]
  --   _ -> [(scope, name)]
  s1 <- pure [(scope, name)]
  s2 <- f a
  return # concat [s1, [dfsc " : "], s2]

show_goal_elems :: Name -> (a -> Elab Sctr) -> [(Name, a)] -> Elab Sctr
show_goal_elems scope f xs = do
  -- f_aux <- pure # \(a, _) (b, _) -> case compare (len a) (len b) of
  --   EQ -> compare a b
  --   res -> res
  -- xs <- pure # sortBy f_aux xs
  
  xs <- mapM (uncurry # show_goal_elem scope f) xs
  return # intercalate [dfsc "\n"] xs

show_goal_terms :: Map Name (N, Type) -> Elab Sctr
show_goal_terms mp = show_goal_elems "term" <~ Map.toList mp #
  \(_, t) -> res_show_type t

show_goal_proofs :: Goal -> Elab Sctr
show_goal_proofs goal = show_goal_elems "proof"
  (show_term_in_goal goal) # _goal_proofs goal

show_goal_stat :: Goal -> Elab Sctr
show_goal_stat goal = do
  s1 <- show_term_in_goal goal # _goal_stat goal
  return # dfsc "⊢ " : s1

show_goal' :: Goal -> Elab Sctr
show_goal' goal = do
  -- let i = _goal_id goal
  -- s <- pure # if i == -1 then "" else
  --   concat ["[", show # _goal_id goal, "]"]
  xs <- sequence
    [ -- show_goal_types # _goal_types goal
      -- show_goal_terms # _goal_terms goal
      show_goal_proofs goal
    , show_goal_stat goal
    ]
  return # intercalate [dfsc "\n\n"] # filter (not . null) xs

show_goal :: Goal -> Theory Sctr
show_goal goal = do
  (a, _) <- elab_to_th' (goal_init_ctx goal) # show_goal' goal
  return a

show_goals :: [Goal] -> Theory Sctr
show_goals goals = do
  par <- gets _th_term_par
  msgs <- mapM show_goal goals
  let sep = dfsc # put_in2 "\n\n" # replicate 5 '-'
  return # intercalate [sep] msgs

display_goals :: [Goal] -> Elab a
display_goals goals = do
  msg <- th_to_elab # show_goals goals
  raise msg

ctx_to_goal :: Ctx -> Goal
ctx_to_goal ctx = let
  terms = _ctx_terms ctx
  es = Map.fromList # do
    (name, i) <- Map.toList # _ctx_term_names ctx
    Just t <- pure # map_get i terms
    return (name, (i, t))
  Just s = _ctx_goal_stat ctx
  in Goal
    { _goal_id = _ctx_id ctx
    , _goal_types = _ctx_types ctx
    , _goal_terms = es
    , _goal_terms' = map_inv # Map.map fst es
    , _goal_proofs = ctx_get_asm_list' ctx
    , _goal_stat = s
    }