{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Elaborator.Theory where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Maybe
import Data.Either
import Data.List
import Data.Char
import Control.Monad
import Control.Monad.State
import Control.Monad.Trans.Except
import Prelude hiding (print, putStrLn)

import Util
import Sctr
import Bit
import Tree
import Trie
import Bimap
import Trimap
import Serializer

import Elaborator.Util
import Elaborator.Data
import Elaborator.Parser
import Elaborator.Type
import Elaborator.Term
import Elaborator.Proof
import Elaborator.Ser

import qualified Kernel as K

th_mk_def :: N -> N -> Type -> Def
th_mk_def lvl ps_num t = Def
  { _def_ps_num = ps_num
  , _def_type = t
  , _def_level = lvl
  }

th_init_defs :: Map Id Def
th_init_defs = Map.fromList
  [ mk_pair namei_imp # th_mk_def 0 0 #
    mk_func mk_prop_t # mk_func mk_prop_t mk_prop_t
  , mk_pair namei_univ # th_mk_def 0 1 #
    mk_func (mk_func (mk_type_var 0) mk_prop_t) mk_prop_t
  ]

init_th :: K.Theory -> Bimap Id Name -> [OpDef] -> TheoryT
init_th kth mp_id_name op_defs = TheoryT
  { _th_kth = kth
  , _th_defs = th_init_defs
  , _th_thms = Map.empty
  , _th_simp_map = trie_empty
  , _th_tcs = trie_empty
  , _th_schemes = init_schemes
  , _th_op_defs = op_defs
  , _th_term_par = op_defs_to_parser op_defs
  , _th_next_index = 2
  , _th_sim_index = Nothing
  , _th_mp_id_name = mp_id_name
  , _th_mp_id_index = bimap_from_list # zip <~ [0..] #
    [namei_imp, namei_univ]
  , _th_rels = trimap_empty
  , _th_mp_e_e = bimap_empty
  , _th_mp_e_stat = bimap_empty
  , _th_mp_e_p = bimap_empty
  , _th_mp_p_p = bimap_empty
  -- , _th_mp_p_sctr = Map.empty
  , _th_recon = False
  }

-----

th_assert_no_id :: Id -> Theory ()
th_assert_no_id i = do
  th_asrt_no_sim_index
  h <- kth_has_id i
  ifm h # do
    -- xs <- pure # if K.th_has_def kth name
    --   then Map.keys # K.th_get_defs kth
    --   else Map.keys # K.th_get_thms kth
    -- tracem # Lit # pure # unwords xs
    name <- th_id_to_name i
    -- error # show (i, name)
    err [dfsc "Name ", ("lit_str", name), dfsc " already exists"]

th_assert_no_name :: Name -> Theory ()
th_assert_no_name name = do
  i <- th_name_to_id name
  th_assert_no_id i

th_put_kth' :: K.Theory -> Theory ()
th_put_kth' thk = modify # \th -> th {_th_kth = thk}

th_put_kth :: Maybe K.Theory -> Theory ()
th_put_kth mth = do
  case mth of
    Nothing -> err [dfsc "Rejected by the kernel"]
    Just th -> th_put_kth' th

get_kth :: Elab K.Theory
get_kth = gets_th _th_kth

put_kth' :: K.Theory -> Elab ()
put_kth' kth = th_to_elab # th_put_kth' kth

put_kth :: Maybe K.Theory -> Elab ()
put_kth mth = th_to_elab # th_put_kth mth

-- reset_mvars :: Theory ()
-- reset_mvars = modify # \th -> th {_th_mvars_num = 0}

modify_term_par :: (TermParser -> TermParser) -> Theory ()
modify_term_par f = modify # \th -> th {_th_term_par = f # _th_term_par th}

-- th_add_const :: Id -> String -> Theory ()
-- th_add_const name sym = do
--   let op_def = OpDef name sym Const Nothing
--   res <- gets _th_elab_res
--   res <- pure # res
--     { _res_op_defs = snoc (_res_op_defs res) op_def
--     }
--   modify # \th -> th
--     { _th_elab_res = res
--     , _th_term_par = term_par_add_const name sym # _th_term_par th
--     , _th_op_defs = snoc (_th_op_defs th) op_def
--     }
-- 
-- th_add_op :: OpType -> Id -> String -> Rel -> Theory ()
-- th_add_op op_type name sym rel = do
--   let op_def = OpDef name sym op_type # Just (_op_name # _rel_op rel, _rel_ord rel)
--   res <- gets _th_elab_res
--   res <- pure # res
--     { _res_op_defs = snoc (_res_op_defs res) op_def
--     }
--   modify # \th -> th
--     { _th_elab_res = res
--     , _th_term_par = term_par_add_op op_type name sym rel # _th_term_par th
--     , _th_op_defs = snoc (_th_op_defs th) op_def
--     }

th_add_simp :: Prop -> Pat -> RwInfo -> Theory ()
th_add_simp local pat info = do
  th_asrt_no_sim_index
  modify # \th -> th
    { _th_simp_map = trie_list_insert pat info # _th_simp_map th
    }

th_add_tc :: Prop -> Pat -> RwInfo -> Theory ()
th_add_tc local pat info = do
  th_asrt_no_sim_index
  modify # \th -> th
    { _th_tcs = trie_list_insert pat info # _th_tcs th
    }

-- th_add_loc_name' :: Id -> Theory ()
-- th_add_loc_name' i = do
--   th <- get
--   let loc = _th_local_names th
--   loc' <- pure # Map.alter (pure . maybe 0 inc) name loc
--   let n = map_get undefined name loc'
--   let res = _th_elab_res th
--   put # th
--     { _th_local_names = loc'
--     , _th_elab_res = res
--       { _res_local_names = map_insert name n #
--         _res_local_names res
--       }
--     }
-- 
-- th_add_loc_name :: Id -> Theory Name
-- th_add_loc_name i = do
--   th_add_loc_name' i
--   th_localize_name i

th_next_index :: HCS => Id -> Theory N
th_next_index i = do
  th_asrt_no_sim_index
  
  index <- gets _th_next_index
  mp <- gets _th_mp_id_index
  
  -- ifm (bimap_has i mp) # error ""
  -- ifm (bimap_has' index mp) # error ""
  
  modify # \th -> th
    { _th_next_index = index + 1
    , _th_mp_id_index = bimap_insert i index mp
    }
  
  return index

th_add_def :: HCS => Id -> Def -> Theory ()
th_add_def i def = do
  th_asrt_no_sim_index
  
  th_next_index i
  kdef <- k_to_th # K.th_get_def i
  
  modify # \th -> th
    { _th_defs = map_insert i def # _th_defs th
    }

th_add_unf :: HCS => Id -> Def -> Theory ()
th_add_unf i def = do
  th_asrt_no_sim_index
  
  -- mp <- gets _th_mp_id_index
  -- asrt # bimap_has i mp
  
  kdef <- k_to_th # K.th_get_def i
  
  modify # \th -> th
    { _th_defs = map_insert i def # _th_defs th
    }

th_add_thm :: HCS => Id -> Thm -> Theory ()
th_add_thm i thm = do
  ifm (i /= -1) #do
    th_asrt_no_sim_index
    th_next_index i
  
  kthm <- k_to_th # K.th_get_thm i
  modify # \th -> th
    { _th_thms = map_insert i thm # _th_thms th
    }

th_show :: (CtxShow a) => a -> Theory Sctr
th_show a = fmap fst # elab_to_th # ctx_show a

th_show' :: (CtxShow a) => TheoryT -> a -> IO (Either Sctr Sctr)
th_show' th a = runExceptT # evalStateT (th_show a) th

-----

type MpId = Bimap_nn Id Id
type MpId2 = (MpId, MpId)
type ThMpAddTerm = State MpId ()
type ThMpAddProof = State MpId2 ()

th_mp_add_lift :: ThMpAddTerm -> ThMpAddProof
th_mp_add_lift f = modify # \(a, b) -> (execState f a, b)

th_mp_add_e' :: Id -> K.Term -> ThMpAddTerm
th_mp_add_e' i e = case e of
  K.TermRef j _ -> modify # bimap_insert j i
  K.Lam _ e -> th_mp_add_e' i e
  K.App a b -> th_mp_add_e' i a >> th_mp_add_e' i b
  _ -> nop

th_mp_add_p' :: Id -> K.Proof -> ThMpAddProof
th_mp_add_p' i p = case p of
  K.ProofRef j _ -> modify # \(a, b) ->
    mk_pair a # bimap_insert j i b
  K.ImpI s p -> do
    th_mp_add_lift # th_mp_add_e' i s
    th_mp_add_p' i p
  K.UnivI _ p -> th_mp_add_p' i p
  K.ImpE p1 p2 -> th_mp_add_p' i p1 >> th_mp_add_p' i p2
  K.UnivE p e -> do
    th_mp_add_p' i p
    th_mp_add_lift # th_mp_add_e' i e
  _ -> nop

th_mp_add_e :: Id -> K.Term -> MpId -> MpId
th_mp_add_e i e = execState (th_mp_add_e' i e)

th_mp_add_proof :: Id -> K.Proof -> MpId2 -> MpId2
th_mp_add_proof i p = execState (th_mp_add_p' i p)

th_mp_add_e_e :: Id -> K.Term -> Theory ()
th_mp_add_e_e ie e = modify # \th -> th
  {_th_mp_e_e = th_mp_add_e ie e # _th_mp_e_e th}

th_mp_add_e_stat :: Id -> K.Term -> Theory ()
th_mp_add_e_stat ip s = modify # \th -> th
  {_th_mp_e_stat = th_mp_add_e ip s # _th_mp_e_stat th}

th_mp_add_p :: Id -> K.Proof -> Theory ()
th_mp_add_p ip p = do
  -- ifm (ip == 1578) # do
  --   print p
  --   mp1 <- pure bimap_empty
  --   mp2 <- pure bimap_empty
  --   (mp1, mp2) <- pure # th_mp_add_proof ip p (mp1, mp2)
  --   mapM_ <~ [mp1, mp2] # \mp -> do
  --     print # map fst # Map.toList # _bimapl_nn mp
  --   error ""
  mp1 <- gets _th_mp_e_p
  mp2 <- gets _th_mp_p_p
  (mp1, mp2) <- pure # th_mp_add_proof ip p (mp1, mp2)
  modify # \th -> th {_th_mp_e_p = mp1, _th_mp_p_p = mp2}