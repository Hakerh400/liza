{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Elaborator.Util where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Maybe
import Data.Either
import Data.List
import Data.Char
import Data.Functor.Identity
import Control.Monad
import Control.Monad.State
import Control.Monad.Trans.Except
import Prelude hiding (print, putStrLn)
import Control.DeepSeq

import Util
import Sctr
import Bit
import Tree
import Trie
import Bimap
import Trimap
import JSON
import Parser
import Elaborator.Data
import Control.Monad.Trans.Maybe

import qualified Kernel as K

[ _namei_aux
  , namei_imp
  , namei_univ
  , namei_not
  , namei_exi
  , namei_lam
  , namei_app
  , namei_appr
  , namei_compr
  , namei_filter
  , namei_insert
  , namei_emp
  , namei_let_fn
  , namei_mem
  , namei_tc_lam
  , namei_and
  , namei_of_tc_univ
  , namei_subst_symm
  , namei_symm
  , namei_desc
  , namei_def
  , namei_unf
  , namei_imp_refl
  , namei_subst_trans
  , namei_and_left
  , namei_and_right
  , namei_subst_of'
  , namei_eq
  , namei_fn_arg
  , namei_eq_subst
  , namei_iff_subst
  , namei_true
  , namei_false
  , namei_iff
  , namei_not_subst
  , namei_fn_arg_subst
  , namei_fn_ext_subst
  , namei_comm
  , namei_tau
  , namei_tau_ind
  , namei_the
  , namei_the_ind
  , namei_mk
  , namei_triv
  , namei_or
  , namei_of_or
  , namei_of_and
  , namei_of_iff
  , namei_of_exi
  , namei_exiu
  , namei_of_exiu
  , namei_prop_ind
  , namei_and_of
  , namei_iff_of
  , namei_of_false
  , namei_false_of
  , namei_of_not_imp_false
  , namei_not_of_imp_false
  , namei_or_inl
  , namei_or_inr
  , namei_of_imp_of
  , namei_exi_of
  , namei_exiu_of
  , namei_nempty
  , namei_nempty_of
  , namei_of_em
  , namei_fn_ext
  , namei_eq_of_iff
  , namei_ax_ext
  , namei_trans
  , namei_exi_eq
  , namei_ite
  , namei_ite_ind
  , namei_eq_refl
  , namei_cong
  , namei_refl_of_eq
  , namei_refl
  , namei_sorry
  , namei_simp
  , namei_tc
  , namei_comm_of_symm'
  , namei_eq_subst'
  , namei_iff_subst'
  , namei_not_subst'
  ] = [0 .. 83] :: HCS => [Id]

namei_expand :: Id
namei_expand = 1902

sym_arrow_right = "→"
sym_fn_type = sym_arrow_right
sym_appr = "#"
sym_lam = "λ"
sym_cap_lam = "Λ"
sym_univ = "∀"
sym_imp = sym_fn_type
sym_mem = "∈"

loc_char = '|'

comment_prefix :: HCS => String
comment_prefix = replicate 2 '-'

subscript_start :: HCS => N
subscript_start = 0x2080

all_type_names :: HCS => [Name]
all_type_names = all_combs' greek_letters_low

all_term_names :: HCS => [Name]
all_term_names = all_combs' letters_low

-- proof_name_to_id :: HCS => Name -> Maybe N
-- proof_name_to_id name = do
--   ('h' : cs@(c : _)) <- pure name
--   True <- pure # all is_sub_digit cs
--   return # sub_to_nat cs

-- proof_id_to_name :: HCS => N -> Name
-- proof_id_to_name i = 'h' : nat_to_sub i

-----

-- digit_to_subscript :: HCS => N -> Char
-- digit_to_subscript d = chr # fi # subscript_start + d
-- 
-- subscript_to_digit :: HCS => Char -> N
-- subscript_to_digit c = fi (ord c) - subscript_start
-- 
-- var_index_to_str :: HCS => N -> String
-- var_index_to_str i = if i == 0 then "" else
--   map (digit_to_subscript . read . (:[])) # show i
-- 
-- str_to_var_index :: HCS => String -> N
-- str_to_var_index s = if null s then 0 else
--   read # map (head . show . subscript_to_digit) s
-- 
-- type_var_to_str :: HCS => N -> String
-- type_var_to_str i = concat ["t", var_index_to_str i]
-- 
-- term_var_to_str :: HCS => N -> String
-- term_var_to_str i = [list_get i ['a'..]]
-- 
-- proof_var_to_str :: HCS => N -> String
-- proof_var_to_str i = concat ["h", var_index_to_str i]
-- 
-- str_to_type_var :: HCS => String -> N
-- str_to_type_var ('t':s) = str_to_var_index s

-----

-- th_get :: HCS => Theory TheoryT
-- th_get = gets snd
-- 
-- th_gets :: HCS => (TheoryT -> a) -> Theory a
-- th_gets f = do
--   s <- th_get
--   return # f s
-- 
-- th_put :: HCS => TheoryT -> Theory ()
-- th_put s = modify # \(p, _) -> (p, s)
-- 
-- th_modify :: HCS => (TheoryT -> TheoryT) -> Theory ()
-- th_modify f = modify # \(p, s) -> (p, f s)
-- 
-- elab_get :: HCS => Elab ElabT
-- elab_get = gets snd
-- 
-- elab_gets :: HCS => (ElabT -> a) -> Elab a
-- elab_gets f = do
--   s <- elab_get
--   return # f s
-- 
-- elab_put :: HCS => ElabT -> Elab ()
-- elab_put s = modify # \(p, _) -> (p, s)
-- 
-- elab_modify :: HCS => (ElabT -> ElabT) -> Elab ()
-- elab_modify f = modify # \(p, s) -> (p, f s)

-----

avail_names_list :: HCS => [Name]
avail_names_list = map (\i -> 'a' : show i) [0..]

get_avail_name_for_set :: HCS => Set Name -> Name
get_avail_name_for_set names = fromJust #
  find (\name -> not # Set.member name names) avail_names_list

show_params' :: HCS => [String] -> String
show_params' ts = if null ts then "" else
  concat [".{", intercalate ", " ts, "}"]

show_params :: HCS => Name -> [String] -> String
show_params name ts = concat [name, show_params' ts]

maybe_set_to_set :: HCS => (Ord a) => Maybe (Set a) -> Set a
maybe_set_to_set = fromMaybe Set.empty

group_args :: HCS => (Eq b) => [(a, b)] -> [([a], b)]
group_args args = case args of
  [] -> []
  ((name, val) : args) -> let
    (args1, args2) = span (\(_, val') -> val' == val) args
    res = group_args args2
    in (name : map fst args1, val) : res

builtin_term_names :: HCS => Set Id
builtin_term_names = Set.fromList
  [namei_imp, namei_univ]

is_builtin_term :: HCS => Id -> Prop
is_builtin_term name = Set.member name builtin_term_names

filter_kth_defs :: HCS => Map Id K.Def -> Map Id K.Def
filter_kth_defs = Map.filterWithKey # \name _ -> not # is_builtin_term name

-- kth_get_non_builtin_defs :: HCS => Theory (Map Id K.Def)
-- kth_get_non_builtin_defs = k_to_th # do
--   defs <- K.th_get_defs
--   return # filter_kth_defs defs

kth_has_id :: HCS => Id -> Theory Prop
kth_has_id i = th_sim_dflt_id i # k_to_th # K.th_has_id i

instance MonadFail (Either a) where
  fail = error

type_of :: HCS => Term -> Type
type_of = _term_type

stat_of :: HCS => Proof -> Stat
stat_of = _proof_stat

get_debug_mode :: HCS => Elab Prop
get_debug_mode = noimpl --gets_th _th_dbg

set_debug_mode :: HCS => Prop -> Elab ()
set_debug_mode p = noimpl --modify_th # \th -> th {_th_dbg = p}

enter_debug_mode :: HCS => Elab ()
enter_debug_mode = noimpl --do
  -- -- par <- gets _parser
  -- -- th_err # pure # unlines #
  -- --   map (unwords . map show . reverse) #
  -- --   inorder # _par_tree par
  -- tracem "DEBUG"
  -- set_debug_mode True

ifm_dbg :: HCS => Elab a -> Elab ()
ifm_dbg m = noimpl --do
  -- d <- get_debug_mode
  -- ifm d m

exit_debug_mode :: HCS => Elab ()
exit_debug_mode = noimpl --set_debug_mode False

-- th_dbg :: HCS => (Show a) => a -> Theory ()
-- th_dbg a = get_debug_mode >>= \p ->
--   if p then th_err [show a] else nop

th_err :: HCS => Sctr -> Theory a
th_err msg = {-gets _name >>= \name ->
  err # [quote name, "\n\n"] ++ msg-}
  err msg

-----

th_modify_rels :: HCS => (ThRels -> ThRels) -> Theory ()
th_modify_rels f = do
  th_asrt_no_sim_index
  mp <- gets # f . _th_rels
  
  -- () <- seq <~ nop # _trimap1 mp
  -- () <- seq <~ nop # _trimap2 mp
  -- () <- seq <~ nop # _trimap3 mp
  
  modify # \th -> th {_th_rels = mp}

th_add_id_rel :: HCS => HCS => Id -> Id -> Id -> Theory ()
th_add_id_rel a b c = do
  -- ifm (b /= namei_unf) #
  -- print (a, b, c)
  th_modify_rels # trimap_insert_new a b c
  -- putStrLn "DONE\n"
  -- error ""

th_remove_id_rel :: HCS => Id -> Id -> Id -> Theory ()
th_remove_id_rel a b c = th_modify_rels # trimap_remove a b c

th_has_id_rel :: HCS => Id -> Id -> Theory Prop
th_has_id_rel a b = th_sim_dflt_ids [a, b] #
  gets # trimap_has a b . _th_rels

th_get_id_rel' :: HCS => Id -> Id -> Theory (Maybe Id)
th_get_id_rel' a b = th_sim_dflt_ids [a, b] #
  gets # trimap_get a b . _th_rels

th_get_id_rel :: HCS => Id -> Id -> Theory Id
th_get_id_rel a b = do
  res <- th_get_id_rel' a b
  case res of
    Just c -> pure c
    Nothing -> do
      xs <- mapM th_id_to_name [a, b]
      error # unwords xs

th_replace_id_rel' :: HCS => Id -> Id -> Id -> Theory ()
th_replace_id_rel' a b c = do
  c0 <- th_get_id_rel' a b
  case c0 of
    Nothing -> nop
    Just c0 -> th_remove_id_rel a b c0
  th_add_id_rel a b c

th_replace_id_rel :: HCS => Id -> Id -> Id -> Theory ()
th_replace_id_rel a b c = do
  c0 <- th_get_id_rel a b
  th_remove_id_rel a b c0
  th_add_id_rel a b c

-----

register_priv_name' :: HCS => HCS => Id -> Id -> Name -> Elab Id
register_priv_name' i j name = do
  k <- register_name name
  th_to_elab # th_add_id_rel j i k
  return k

register_priv_name :: HCS => HCS => Id -> Id -> Name -> Elab Id
register_priv_name i j name = do
  -- names <- mapM id_to_name [j, i]
  -- putStrLn # unwords names
  
  has <- th_to_elab # th_has_id_rel j i
  ifm has # error ""
  
  register_priv_name' i j name

mk_priv_name_aux1 :: HCS => Prop -> Name -> Name -> Name
mk_priv_name_aux1 h name1 name2 = let
  name1' = case name1 of
    ('`' : s) -> s
    s -> if not h then s else error s
  s = intercalate usc [name2, name1']
  in if not h then s else case s of
    ('\x5F' : _) -> s
    _ -> usc' : s

-- mk_priv_name_aux' :: HCS => Prop -> Id -> Id -> Elab (Maybe Id)
-- mk_priv_name_aux' i j = do
--   si <- id_to_name i
--   sj <- id_to_name j
--   s <- pure # mk_priv_name_aux1 b si sj
--   has <- has_name s
--   
--   -- ifmn has trace_stack
--   
--   ifm' has # do
--     i <- register_priv_name i j s
--     return # Just i

mk_priv_name_aux :: HCS => HCS => Prop -> Id -> Id -> Elab Id
mk_priv_name_aux b i j = do
  si <- id_to_name i
  sj <- id_to_name j
  s <- pure # mk_priv_name_aux1 b si sj
  register_priv_name i j s

-- mk_priv_name_no_usc' :: HCS => Id -> Id -> Elab (Maybe Id)
-- mk_priv_name_no_usc' = mk_priv_name_aux' False

mk_priv_name_no_usc :: HCS => HCS => Id -> Id -> Elab Id
mk_priv_name_no_usc = mk_priv_name_aux False

mk_priv_name :: HCS => HCS => Id -> Id -> Elab Id
mk_priv_name = mk_priv_name_aux True

mk_desc_thm_name :: HCS => HCS => Id -> Elab Id
mk_desc_thm_name = mk_priv_name namei_desc

mk_def_thm_name :: HCS => HCS => Id -> Elab Id
mk_def_thm_name = mk_priv_name namei_def

get_priv_name' :: HCS => HCS => Id -> Id -> Elab (Maybe Id)
get_priv_name' i j = th_to_elab # th_get_id_rel' j i

get_priv_name :: HCS => HCS => Id -> Id -> Elab Id
get_priv_name i j = th_to_elab # th_get_id_rel j i

get_desc_thm_name' :: HCS => HCS => Id -> Elab (Maybe Id)
get_desc_thm_name' = get_priv_name' namei_desc

get_def_thm_name' :: HCS => HCS => Id -> Elab (Maybe Id)
get_def_thm_name' = get_priv_name' namei_def

-----

-- th_mk_mvar :: HCS => Theory N
-- th_mk_mvar = do
--   i <- gets _th_mvars_num
--   modify # \th -> th {_th_mvars_num = i + 1}
--   return i

-- instance VarCnt Theory where
--   cnt_next = th_mk_mvar

-----

mk_type_mvar :: HCS => N -> Type
mk_type_mvar i = Type
  { _type_val = TypeMvar i
  , _type_res = False
  }

mk_type_var :: HCS => N -> Type
mk_type_var i = Type
  { _type_val = TypeVar i
  , _type_res = True
  }

mk_prop_t :: HCS => Type
mk_prop_t = Type
  { _type_val = PropT
  , _type_res = True
  }

mk_set_t :: HCS => Type
mk_set_t = Type
  { _type_val = SetT
  , _type_res = True
  }

mk_func :: HCS => Type -> Type -> Type
mk_func a b = Type
  { _type_val = Func a b
  , _type_res = e_res a && e_res b
  }

-----

mk_term_mvar :: HCS => N -> Type -> N -> Term
mk_term_mvar d t i = Term
  { _term_depth = d
  , _term_type = t
  , _term_val = TermMvar i
  , _term_res = False
  }

mk_term_ref :: HCS => N -> Type -> Id -> [Type] -> Term
mk_term_ref d t name ps = Term
  { _term_depth = d
  , _term_type = t
  , _term_val = TermRef name ps
  , _term_res = e_res t && all e_res ps
  }

mk_term_var :: HCS => N -> Type -> N -> Term
mk_term_var d t i = if i >= d then trace' (d, i) # error "" else Term
  { _term_depth = d
  , _term_type = t
  , _term_val = TermVar i
  , _term_res = e_res t
  }

mk_lam_aux :: HCS => Maybe Name -> Type -> Term -> Term
mk_lam_aux mn t e = e
  { _term_depth = case _term_depth e of
      0 -> error "mk_lam_aux"
      n -> n - 1
  , _term_type = mk_func t # _term_type e
  , _term_val = Lam mn t e
  , _term_res = e_res t && e_res e
  }

mk_lam' :: HCS => Name -> Type -> Term -> Term
mk_lam' name = mk_lam_aux # Just name

mk_lam :: HCS => Type -> Term -> Term
mk_lam = mk_lam_aux Nothing

mk_lams' :: HCS => [(Name, Type)] -> Term -> Term
mk_lams' args e = foldr (\(name, t) e -> mk_lam' name t e) e args

mk_lams :: HCS => [Type] -> Term -> Term
mk_lams ts e = foldr mk_lam e ts

-----

lift_term' :: HCS => N -> N -> Term -> Term
lift_term' d0 dif e = if dif == 0 then e else let
  t = _term_type e
  de = _term_depth e
  d = de + dif
  in if d < 0 then error "" else
    case _term_val e of
      TermMvar i -> mk_term_mvar d t i
      TermRef name ps -> mk_term_ref d t name ps
      TermVar i -> let
        j = if i < d0 then i else i + dif
        in mk_term_var d t j
      Lam mn t e -> mk_lam_aux mn t # lift_term' d0 dif e
      App a b -> mk_app t (lift_term' d0 dif a) (lift_term' d0 dif b)
      WithType t e -> mk_with_type t # lift_term' d0 dif e

lift_term :: HCS => N -> Term -> Term
lift_term dif e = lift_term' (_term_depth e) dif e

lift_term_to :: HCS => N -> Term -> Term
lift_term_to d e = let
  d0 = _term_depth e
  dif = d - d0
  in lift_term' d0 dif e

-----

lift_proof' :: HCS => N -> N -> Proof -> Proof
lift_proof' d0 dif p = if dif == 0 then p else let
  s0 = _proof_stat p
  -- de = _term_depth s0
  -- d = de + dif
  s = lift_term' d0 dif s0
  in case _proof_val p of
    ProofMvar is_tc i -> mk_proof_mvar' is_tc s i
    ProofRef name ps -> mk_proof_ref s name ps # _proof_expl p
    ProofVar i -> mk_proof_var s i # _proof_expl p
    ImpI mn i0 i s1 p -> mk_imp_i_aux mn i0 i (lift_term' d0 dif s1) (lift_proof' d0 dif p)
    UnivI mn t p -> mk_univ_i_aux mn t (lift_proof' d0 dif p)
    ImpE p1 p2 -> mk_imp_e s (lift_proof' d0 dif p1) (lift_proof' d0 dif p2)
    UnivE p e -> mk_univ_e s (lift_proof' d0 dif p) (lift_term' d0 dif e)
    _ -> error # show p

lift_proof :: HCS => N -> Proof -> Proof
lift_proof dif p = let
  s = _proof_stat p
  in lift_proof' (_term_depth s) dif p

lift_proof_to :: HCS => N -> Proof -> Proof
lift_proof_to d p = let
  s = _proof_stat p
  d0 = _term_depth s
  dif = d - d0
  in lift_proof' d0 dif p

lift_proofs :: HCS => Proof -> Proof -> (Proof, Proof)
lift_proofs p1 p2 = let
  d1 = _term_depth # _proof_stat p1
  d2 = _term_depth # _proof_stat p2
  d = max d1 d2
  in (lift_proof_to d p1, lift_proof_to d p2)

-----

mk_app :: HCS => Type -> Term -> Term -> Term
mk_app t a b = let
  d1 = _term_depth a
  d2 = _term_depth b
  d = max d1 d2
  a' = lift_term_to d a
  b' = lift_term_to d b
  in Term --if d1 /= d2 then trace' (d1, d2, a, b) # error # show u else Term
    { _term_depth = d
    , _term_type = t
    , _term_val = App a' b'
    , _term_res = e_res t && e_res a && e_res b
    }

mk_app_safe :: HCS => HCS => Term -> Term -> Term
mk_app_safe a b = let
  d1 = _term_depth a
  d2 = _term_depth b
  d = max d1 d2
  a' = lift_term_to d a
  b' = lift_term_to d b
  f = \t1 t2 -> error # unlines'
    [ "\n\nmk_app_safe", show a, show b
    , show t1, show t2, show # _term_type b, "" ]
  in --if _term_depth b /= d then trace' u # error "" else
    case _type_val # _term_type a of
      Func t1 t2 -> if _term_type b /= t1
        then f t1 t2 else Term
          { _term_depth = d
          , _term_type = t2
          , _term_val = App a' b'
          , _term_res = e_res a && e_res b
          }
      a -> error # show a

mk_apps_safe :: HCS => [Term] -> Term
mk_apps_safe = foldl1' mk_app_safe

-- `e` must be resolved
mk_apps_res :: HCS => Type -> TermVal -> [Term] -> Term
mk_apps_res t e args = case args of
  [] -> error ""
  (arg:_) -> mk_apps_safe # (:args) # Term
    { _term_depth = _term_depth arg
    , _term_type = t
    , _term_val = e
    , _term_res = e_res t
    }

mk_with_type :: HCS => Type -> Term -> Term
mk_with_type t e = Term
  { _term_depth = _term_depth e
  , _term_type = t
  , _term_val = WithType t e
  , _term_res = e_res t && e_res e
  }

mk_not :: HCS => Term -> Term
mk_not a = mk_apps_res
  (mk_func mk_prop_t mk_prop_t)
  (TermRef namei_not []) [a]

mk_imp :: HCS => Term -> Term -> Term
mk_imp a b = mk_apps_res
  (mk_func mk_prop_t # mk_func mk_prop_t mk_prop_t)
  (TermRef namei_imp []) [a, b]

mk_binder_ref :: HCS => Id -> Type -> Term -> Term
mk_binder_ref name t f = let
  t' = mk_func (mk_func t mk_prop_t) mk_prop_t
  u = mk_term_ref (_term_depth f) t' name [t]
  in mk_app_safe u f

mk_univ' :: HCS => Type -> Term -> Term
mk_univ' = mk_binder_ref namei_univ

mk_univ :: HCS => Type -> Term -> Term
mk_univ t e = mk_univ' t # mk_lam t e

mk_exi :: HCS => Type -> Term -> Term
mk_exi t e = mk_binder_ref namei_exi t # mk_lam t e

lam_to_univ :: HCS => Term -> Term
lam_to_univ f = case _term_val f of
  Lam _ t _ -> mk_univ' t f
  _ -> error ""

-----

mk_proof_mvar' :: HCS => Prop -> Stat -> N -> Proof
mk_proof_mvar' is_tc s i = Proof
  { _proof_stat = s
  , _proof_val = ProofMvar is_tc i
  , _proof_expl = True
  , _proof_res = False
  , _proof_target_asm = Nothing
  }

mk_proof_mvar :: HCS => Stat -> N -> Proof
mk_proof_mvar = mk_proof_mvar' False

mk_proof_ref :: HCS => Stat -> Id -> [Type] -> Prop -> Proof
mk_proof_ref s name ps expl = Proof
  { _proof_stat = s
  , _proof_val = ProofRef name ps
  , _proof_expl = expl
  , _proof_res = e_res s && all e_res ps
  , _proof_target_asm = Nothing
  }

mk_proof_var :: HCS => Stat -> N -> Prop -> Proof
mk_proof_var s i expl = Proof
  { _proof_stat = s
  , _proof_val = ProofVar i
  , _proof_expl = expl
  , _proof_res = e_res s
  , _proof_target_asm = Nothing
  }

mk_univ_i_aux :: HCS => Maybe Name -> Type -> Proof -> Proof
mk_univ_i_aux mn t p = let
  s = _proof_stat p
  s' = mk_univ t s
  in Proof
  { _proof_stat = s'
  , _proof_val = UnivI mn t p
  , _proof_expl = True
  , _proof_res = e_res s' && e_res t && e_res p
  , _proof_target_asm = Nothing
  }

mk_univ_i' :: HCS => Name -> Type -> Proof -> Proof
mk_univ_i' name = mk_univ_i_aux # Just name

mk_univ_i :: HCS => Type -> Proof -> Proof
mk_univ_i = mk_univ_i_aux Nothing

mk_imp_i_aux :: HCS => Maybe Name -> Maybe N -> N -> Stat -> Proof -> Proof
mk_imp_i_aux mn i0 i s p = Proof
  { _proof_stat = mk_imp s # _proof_stat p
  , _proof_val = ImpI mn i0 i s p
  , _proof_expl = True
  , _proof_res = e_res s && e_res p
  , _proof_target_asm = Nothing
  }

mk_imp_i' :: HCS => Maybe Name -> N -> N -> Stat -> Proof -> Proof
mk_imp_i' mn i0 i s p = mk_imp_i_aux mn (Just i0) i s p

mk_imp_i :: HCS => Maybe Name -> N -> Stat -> Proof -> Proof
mk_imp_i mn i s p = mk_imp_i_aux mn Nothing i s p

mk_univ_e :: HCS => Stat -> Proof -> Term -> Proof
mk_univ_e s p e = Proof
  { _proof_stat = s
  , _proof_val = UnivE p e
  , _proof_expl = _proof_expl p
  , _proof_res = e_res s && e_res p && e_res e
  , _proof_target_asm = Nothing
  }

mk_imp_e :: HCS => HCS => Stat -> Proof -> Proof -> Proof
mk_imp_e s p1 p2 = let
  d1 = _term_depth # _proof_stat p1
  d2 = _term_depth # _proof_stat p2
  d = _term_depth s
  cnd = d == max d1 d2
  in if not cnd then error "" else Proof
    { _proof_stat = s
    , _proof_val = ImpE p1 p2
    , _proof_expl = True
    , _proof_res = e_res s && e_res p1 && e_res p2
    , _proof_target_asm = Nothing
    }

mk_proof_clear :: HCS => [N] -> [N] -> Proof -> Proof
mk_proof_clear e_ids p_ids p = Proof
  { _proof_stat = _proof_stat p
  , _proof_val = ProofClear e_ids p_ids p
  , _proof_expl = True
  , _proof_res = e_res p
  , _proof_target_asm = Nothing
  }

-----

pst_get_ctx :: HCS => ProofState -> Ctx
pst_get_ctx pst = case _pst_ctxs pst of
  (ctx : _) -> ctx
  _ -> error ""

pst_put_ctx :: HCS => Ctx -> ProofState -> ProofState
pst_put_ctx ctx pst = pst
  {_pst_ctxs = ctx : tail' (_pst_ctxs pst)}

pst_modify_ctx :: HCS => (Ctx -> Ctx) -> ProofState -> ProofState
pst_modify_ctx f pst = case _pst_ctxs pst of
  (ctx : ctxs) -> pst {_pst_ctxs = f ctx : ctxs}
  _ -> pst

-----

get_th :: HCS => Elab TheoryT
get_th = gets _elab_th

put_th :: HCS => TheoryT -> Elab ()
put_th th = modify # \elab -> elab {_elab_th = th}

get_pst :: HCS => Elab ProofState
get_pst = gets _elab_pst

get_ctx' :: HCS => Elab (Maybe Ctx)
get_ctx' = do
  pst <- get_pst
  return # list_to_maybe # _pst_ctxs pst

get_ctx :: HCS => Elab Ctx
get_ctx = do
  ctx <- get_ctx'
  case ctx of
    Just ctx -> pure ctx
    _ -> do
      -- error "get_ctx: No more goals"
      err [dfsc "No more goals"]

gets_th :: HCS => (TheoryT -> a) -> Elab a
gets_th f = do
  th <- get_th
  return # f th

modify_th :: HCS => (TheoryT -> TheoryT) -> Elab ()
modify_th f = modify # \elab -> elab
  {_elab_th = f # _elab_th elab}

-----

th_asrt_no_sim_index :: HCS => Theory ()
th_asrt_no_sim_index = do
  res <- gets _th_sim_index
  ifm (isJust res) # error ""

th_sim_index_has_id :: HCS => Id -> Theory Prop
th_sim_index_has_id i = do
  th <- get
  return # case _th_sim_index th of
    Nothing -> True
    Just n -> isJust # do
      index <- bimap_get i # _th_mp_id_index th
      asrt # index < n
      nop

th_sim_dflt_id :: HCS => (Inhabited a) => Id -> Theory a -> Theory a
th_sim_dflt_id i m = do
  h <- th_sim_index_has_id i
  ifm' h m

th_sim_dflt_ids :: HCS => (Inhabited a) => [Id] -> Theory a -> Theory a
th_sim_dflt_ids is m = case is of
  [] -> m
  (i : is) -> th_sim_dflt_id i # th_sim_dflt_ids is m

th_sim_dflt_name :: HCS => (Inhabited a) => Name -> Theory a -> Theory a
th_sim_dflt_name name m = do
  res <- gets # bimap_get' name . _th_mp_id_name
  case res of
    Nothing -> dflt
    Just i -> th_sim_dflt_id i m

th_sim_dflt_names :: HCS => (Inhabited a) => [Name] -> Theory a -> Theory a
th_sim_dflt_names names m = case names of
  [] -> m
  (name : names) -> th_sim_dflt_name name # th_sim_dflt_names names m

-----

asrt_no_sim_index :: HCS => Elab ()
asrt_no_sim_index = th_to_elab th_asrt_no_sim_index

sim_index_has_id :: HCS => Id -> Elab Prop
sim_index_has_id i = th_to_elab # th_sim_index_has_id i

sim_dflt_id :: HCS => (Inhabited a) => Id -> Elab a -> Elab a
sim_dflt_id i m = do
  h <- sim_index_has_id i
  ifm' h m

sim_dflt_ids :: HCS => (Inhabited a) => [Id] -> Elab a -> Elab a
sim_dflt_ids is m = case is of
  [] -> m
  (i : is) -> sim_dflt_id i # sim_dflt_ids is m

-----

th_get_schemes :: HCS => Elab TheorySchemes
th_get_schemes = gets_th _th_schemes

th_put_schemes :: HCS => TheorySchemes -> Elab ()
th_put_schemes schemes = do
  asrt_no_sim_index
  modify_th # \th -> th
    { _th_schemes = schemes
    -- , _th_elab_res = (_th_elab_res th) {_res_schemes = schemes}
    }

th_modify_schemes :: HCS => (TheorySchemes -> TheorySchemes) -> Elab ()
th_modify_schemes f = do
  schemes <- th_get_schemes
  th_put_schemes # f schemes

th_get_scheme' :: HCS => (TheorySchemes -> Map Id Id) -> Id -> Elab (Maybe Id)
th_get_scheme' f i = sim_dflt_id i # do
  schemes <- th_get_schemes
  return # map_get i # f schemes

th_get_scheme :: HCS => (TheorySchemes -> Map Id Id) -> Id -> Elab Id
th_get_scheme f name = do
  Just name <- th_get_scheme' f name
  return name

gets_ctx :: HCS => (Ctx -> a) -> Elab a
gets_ctx f = do
  ctx <- get_ctx
  return # f ctx

put_pst :: HCS => ProofState -> Elab ()
put_pst pst = modify # \elab -> elab {_elab_pst = pst}

modify_pst :: HCS => (ProofState -> ProofState) -> Elab ()
modify_pst f = modify # \elab -> elab {_elab_pst = f # _elab_pst elab}

put_ctx :: HCS => Ctx -> Elab ()
put_ctx ctx = modify # \elab -> elab
  {_elab_pst = pst_put_ctx ctx # _elab_pst elab}

modify_ctx :: HCS => (Ctx -> Ctx) -> Elab ()
modify_ctx f = modify # \elab -> elab
  {_elab_pst = pst_modify_ctx f # _elab_pst elab}

ctx_term_depth :: HCS => Elab N
ctx_term_depth = gets_ctx _ctx_term_depth

-----

-- elab_get_names :: HCS => Elab (Set Id)
-- elab_get_names = do
--   th <- get_th
--   return # Set.union
--     (Map.keysSet # _th_defs th)
--     (Map.keysSet # _th_thms th)

-- elab_get_names_str :: HCS => Elab (Set Name)
-- elab_get_names_str = do
--   set <- elab_get_names
--   set_map_m id_to_name set

elab_get_max_op_pri :: HCS => Elab N
elab_get_max_op_pri = do
  par <- gets_th _th_term_par
  return # _term_par_max_op_pri par

elab_get_app_pri :: HCS => Elab N
elab_get_app_pri = do
  pri <- elab_get_max_op_pri
  return # pri + 1

elab_get_atom_pri :: HCS => Elab N
elab_get_atom_pri = do
  pri <- elab_get_max_op_pri
  return # pri + 2

elab_get_op_app :: HCS => Elab Op
elab_get_op_app = do
  pri <- elab_get_app_pri
  return # Op
    { _op_name = namei_app
    , _op_sym = ""
    , _op_type = Infix (Just 0)
    , _op_pri = pri
    }

-----

-- bin_op_sym_sep :: HCS => Name -> String -> Sctr
-- bin_op_sym_sep scope sym = if null sym
--   then [dfsc " "]
--   else put_in2 [dfsc " "] [(scope, sym)]

-- bin_op_sym_sep :: HCS => String -> Sctr
-- bin_op_sym_sep = bin_op_sym_sep' "op"
-- 
-- bin_op_sep :: HCS => Op -> Sctr
-- bin_op_sep op = bin_op_sym_sep # _op_sym op

mk_expr_aux :: HCS => N -> N -> Maybe ExprInfo -> Sctr -> Expr
mk_expr_aux pri_left pri_right info s = Expr
  { _expr_val = s
  , _expr_pri_left = pri_left
  , _expr_pri_right = pri_right
  , _expr_info = info
  }

mk_expr :: HCS => N -> N -> Sctr -> Expr
mk_expr m n = mk_expr_aux m n Nothing

mk_expr' :: HCS => N -> N -> ExprInfo -> Sctr -> Expr
mk_expr' m n info = mk_expr_aux m n # Just info

-- expr_val_is_lam :: HCS => ExprVal -> Prop
-- expr_val_is_lam info = case info of
--   AstBinder sym _ _ -> sym == sym_lam
--   _ -> False
-- 
-- expr_is_lam :: HCS => Expr -> Prop
-- expr_is_lam expr = expr_val_is_lam # _expr_val expr

-- show_expr' :: HCS => Name -> N -> Set Name -> Map N Name -> Expr -> Elab Sctr
-- show_expr' scope d names mp expr = case _expr_val expr of
--   AstRef name -> pure [(scope, name)]
--   AstVar i -> case map_get i mp of
--     Nothing -> pure [(scope, concat [usc, show i])]
--     Just name -> pure [(scope, name)]
--   AstSymbol sym -> pure [("sym", sym)]
--   AstPrefix sym opnd -> do
--     s <- show_expr' scope d names mp opnd
--     return # ("op", sym) : s
--   AstInfix sym opnd1 opnd2 -> do
--     s1 <- show_expr' scope d names mp opnd1
--     s2 <- show_expr' scope d names mp opnd2
--     scope' <- pure # ite (scope == "term") "op" scope_dflt
--     return # s1 ++ bin_op_sym_sep scope' sym ++ s2
--   AstBinder sym st opnd -> do
--     -- st <- show_expr' "type" d names mp t
--     (name, names) <- pure # term_mk_name names
--     mp <- pure # map_insert d name mp
--     opnd <- if sym == sym_lam then pure opnd else do
--       -- if not # expr_is_lam opnd then error "" else do
--       AstBinder _ _ opnd <- pure # _expr_val opnd
--       return opnd
--     se <- show_expr' scope (d + 1) names mp opnd
--     return # [("op", sym), dfsc " (", ("term", name), dfsc " : "] ++
--       st ++ [dfsc "), "] ++ se
--   AstWithType t opnd -> error ""
--   AstGroup opnd -> do
--     s <- show_expr' scope d names mp opnd
--     return # [dfsc "("] ++ s ++ [dfsc ")"]
-- 
-- show_expr :: HCS => N -> Set Name -> Map N Name -> Expr -> Elab Sctr
-- show_expr = show_expr' "term"

expr_mk_atom :: HCS => Sctr -> Elab Expr
expr_mk_atom s = do
  mx <- elab_get_atom_pri
  return # mk_expr mx mx s

expr_mk_group :: HCS => Expr -> Elab Expr
expr_mk_group s = expr_mk_atom # [dfsc "("] ++ _expr_val s ++ [dfsc ")"]

expr_mk_group' :: HCS => Prop -> Expr -> Elab Expr
expr_mk_group' p s = if p then expr_mk_group s else pure s

expr_mk_un_op :: HCS => String -> Op -> Expr -> Elab Expr
expr_mk_un_op scope op s1 = do
  mx <- elab_get_app_pri
  let pri = _op_pri op
  let sym = _op_sym op
  s1 <- expr_mk_group' (pri > _expr_pri_left s1) s1
  return # mk_expr mx (min pri # _expr_pri_right s1) #
    [(scope, sym)] ++ _expr_val s1

expr_mk_bin_op :: HCS => String -> Op -> Expr -> Expr -> Elab Expr
expr_mk_bin_op scope op s1 s2 = do
  let Infix dir = _op_type op
  let pri = _op_pri op
  let sym = _op_sym op
  (r1, r2) <- pure # case dir of
    Nothing -> error ""
    Just 0 -> ((>), (>=))
    Just 1 -> ((>=), (>))
    _ -> error ""
  s1 <- expr_mk_group' (r1 pri # _expr_pri_right s1) s1
  s2 <- expr_mk_group' (r2 pri # _expr_pri_left s2) s2
  let pri1 = min pri # _expr_pri_left s1
  let pri2 = min pri # _expr_pri_right s2
  let sp = [dfsc " "]
  let sep = if null sym then sp else put_in2 sp [(scope, sym)]
  return # mk_expr pri1 pri2 # _expr_val s1 ++ sep ++ _expr_val s2

-- expr_mk_binder :: HCS => Name -> Name -> Op -> Name -> Expr -> Expr -> Elab Expr
-- expr_mk_binder op_scope name_scope op name st se = do
--   mx <- elab_get_app_pri
--   let pri = _op_pri op
--   let s_op = [(op_scope, _op_sym op)]
--   let s_st = _expr_val st
--   let s_se = _expr_val se
--   let s_name = [(name_scope, name)]
--   let dflt_group = ([s_name], s_st)
--   f <- pure # \groups se -> let
--     s_groups = do
--       (names, st) <- groups
--       let s_names = intercalate [dfsc " "] names
--       concat [[dfsc " ("], s_names, [dfsc " : "], st, [dfsc ")"]]
--     in concat [s_op, s_groups, [dfsc ", "], se]
--   dflt <- pure # do
--     let groups = [dflt_group]
--     let info = ExprBinder s_op groups s_se
--     return # mk_expr' mx pri info # f groups s_se
--   case _expr_info se of
--     Just (ExprBinder s_op' groups@(group@(names, s_st') : rest) s_se) ->
--       if s_op' /= s_op then dflt else do
--         groups <- pure # if s_st' == s_st
--           then (s_name : names, s_st) : rest
--           else dflt_group : groups
--         let info = ExprBinder s_op groups s_se
--         return # mk_expr' mx pri info # f groups s_se
--     _ -> dflt

expr_binder_group_parens_to_sctr :: HCS => [Sctr] -> Sctr -> Sctr -> Sctr
expr_binder_group_parens_to_sctr xs sep y = concat
  [[dfsc "("], intercalate [dfsc " "] xs, sep, y, [dfsc ")"]]

expr_binder_group_to_sctr :: HCS => ExprBinderGroup -> Sctr
expr_binder_group_to_sctr group = let
  f = \sep xs y -> expr_binder_group_parens_to_sctr xs sep y
  in case group of
    ExprBinderGroupType xs y -> f [dfsc " : "] xs y
    ExprBinderGroupTc e -> concat [[dfsc "["], e, [dfsc "]"]]
    ExprBinderGroupTcMem a b -> concat
      [[dfsc "["], a, [("op", " ∈ ")], b, [dfsc "]"]]
    ExprBinderGroupMem xs y -> f [("op", " ∈ ")] xs y
    ExprBinderGroupUnivI xs y -> f [dfsc " : "] xs y
    ExprBinderGroupImpI xs y -> f [dfsc " : "] xs y

expr_binder_groups_to_sctr :: HCS => [ExprBinderGroup] -> Sctr
expr_binder_groups_to_sctr groups = do
  group <- groups
  dfsc " " : expr_binder_group_to_sctr group

expr_info_binder_to_sctr :: HCS => Sctr -> [ExprBinderGroup] -> Sctr -> Sctr
expr_info_binder_to_sctr binder groups s = let
  s_groups = expr_binder_groups_to_sctr groups
  in concat [binder, s_groups, [dfsc ", "], s]

expr_info_to_sctr :: HCS => ExprInfo -> Sctr
expr_info_to_sctr info = case info of
  ExprBinder binder groups s -> expr_info_binder_to_sctr
    binder groups s
  _ -> error # show info

expr_mk_binder_fn :: HCS => Sctr -> [ExprBinderGroup] -> Expr -> Elab Expr
expr_mk_binder_fn binder groups expr = do
  dflt <- pure # do
    par <- gets_th _th_term_par
    mx <- elab_get_app_pri
    Just op_lam <- pure # map_get namei_lam # _term_par_names par
    let pri = _op_pri op_lam
    s <- pure # case _expr_info expr of
      Just (ExprBinder _ _ s) -> s
      _ -> _expr_val expr
    info <- pure # ExprBinder binder groups s
    let expr = expr_info_to_sctr info
    return # mk_expr' mx pri info expr
  case groups of
    (ExprBinderGroupMem [x] y : ExprBinderGroupMem xs y' : groups) ->
      if y' /= y then dflt else do
        groups <- pure # ExprBinderGroupMem (x : xs) y : groups
        expr_mk_binder_fn binder groups expr
    _ -> dflt

expr_mk_binder :: HCS => Sctr -> ExprBinderGroup -> Expr -> Elab Expr
expr_mk_binder binder group expr = do
  let f = \groups -> expr_mk_binder_fn binder groups expr
  dflt <- pure # expr_mk_binder_fn binder [group] #
    expr {_expr_info = Nothing}
  case _expr_info expr of
    Nothing -> dflt
    Just info -> case info of
      ExprBinder binder' groups@(group0 : groups') e ->
        if binder' /= binder then dflt else do
          dflt <- pure # group : groups
          f # case group of
            ExprBinderGroupType xs y -> case group0 of
              ExprBinderGroupType xs' y' ->
                if y' /= y then dflt else
                  ExprBinderGroupType (xs ++ xs') y : groups'
              ExprBinderGroupTcMem a b -> case xs of
                [x] -> if x /= a then dflt else
                  ExprBinderGroupMem [a] b : groups'
                _ -> dflt
              _ -> dflt
            _ -> dflt
      _ -> dflt

-----

term_mk_name :: HCS => Set Name -> (Name, Set Name)
term_mk_name = mk_avail_getter all_term_names

type_is_func :: HCS => Type -> Prop
type_is_func t = case _type_val t of
  Func _ _ -> True
  _ -> False

-- th_term_names :: HCS => Theory (Set Id)
-- th_term_names = gets # Map.keysSet . _th_defs

-- th_proof_names :: HCS => Theory (Set Id)
-- th_proof_names = gets # Map.keysSet . _th_thms

-- th_all_names :: HCS => Theory (Set Id)
-- th_all_names = do
--   names1 <- th_term_names
--   names2 <- th_proof_names
--   return # Set.union names1 names2

term_is_lam :: HCS => Term -> Prop
term_is_lam e = case _term_val e of
  Lam _ _ _ -> True
  _ -> False

init_pst :: HCS => Ctx -> ProofState
init_pst ctx = ProofState
  { _pst_ctxs = [ctx]
  , _pst_fn = head' undefined
  }

elab_get_ctx :: HCS => ElabT -> Ctx
elab_get_ctx elab = head' undefined # _pst_ctxs # _elab_pst elab

-- ctx_get_all_names :: HCS => Elab (Set Name)
-- ctx_get_all_names = do
--   ctx <- get_ctx
--   return # Set.unions
--     [ Map.keysSet # _ctx_types ctx
--     , Map.keysSet # _ctx_terms ctx
--     , Map.keysSet # _ctx_proofs ctx
--     ]

ctx_has_name :: HCS => Name -> Elab Prop
ctx_has_name name = error ""

ctx_assert_no_name :: HCS => Name -> Elab ()
ctx_assert_no_name name = do
  p <- ctx_has_name name
  if not p then nop else err
    [dfsc "Local name ", ("lit_str", name), dfsc " already exists"]

proof_name_to_index' :: HCS => N -> [N] -> N
proof_name_to_index' n ds = case ds of
  [] -> n
  (d:ds) -> if not # d >= 0 && d <= 9 then 0 else
    proof_name_to_index' (n * 10 + d) ds

proof_name_to_index :: HCS => Name -> N
proof_name_to_index name = case name of
  ('h':s) -> proof_name_to_index' 0 #
    map (\c -> fi (ord c) - 8320) s
  _ -> 0

term_has_term_var :: HCS => N -> Term -> Prop
term_has_term_var i e = case _term_val e of
  TermVar j -> j == i
  Lam _ _ e -> term_has_term_var i e
  App a b -> term_has_term_var i a || term_has_term_var i b
  WithType _ e -> term_has_term_var i e
  _ -> False

term_has_free_var :: HCS => N -> Term -> Prop
term_has_free_var i e = if _term_depth e <= i then False
  else case _term_val e of
    TermVar j -> j == i
    Lam _ _ e -> term_has_free_var i e
    App a b -> term_has_free_var i a || term_has_free_var i b
    WithType _ e -> term_has_free_var i e
    _ -> False

-----

norm_mvar :: HCS => N -> State (Map N N) N
norm_mvar i = do
  mp <- get
  case Map.lookup i mp of
    Nothing -> do
      let j = map_size mp
      put # map_insert i j mp
      return j
    Just j -> pure j

-----

build_pat :: HCS => PatBuilder () -> Pat
build_pat m = reverse # execState m []

build_pat_fn :: HCS => (a -> PatBuilder ()) -> a -> Pat
build_pat_fn f x = build_pat # f x

pat_push :: HCS => PatElem -> PatBuilder ()
pat_push x = modify (x:)

pat_push' :: HCS => [PatElem] -> PatBuilder ()
pat_push' xs = modify (reverse xs ++)

-----

-- init_pat_sts :: HCS => PatMap a -> [PatState a]
-- init_pat_sts ptm = do
--   (x, ptm) <- Map.toList # _trie_map ptm
--   case x of
--     PatDepth d -> pure # PatState
--       { _pat_st_ptm = ptm
--       , _pat_st_depth = d
--       , _pat_st_types = Map.empty
--       , _pat_st_terms = Map.empty
--       , _pat_st_lam_mp = Map.empty
--       }
--     _ -> error # show x
-- 
-- pat_fail :: HCS => PatM a b
-- pat_fail = lift []
-- 
-- pat_get_ptm :: HCS => PatM a (PatMap a)
-- pat_get_ptm = gets _pat_st_ptm
-- 
-- pat_put_ptm :: HCS => PatMap a -> PatM a ()
-- pat_put_ptm ptm = modify # \st -> st {_pat_st_ptm = ptm}
-- 
-- pat_get :: HCS => PatM a PatElem
-- pat_get = StateT # \st -> let
--   ptm = _pat_st_ptm st
--   mp = _trie_map ptm
--   in flip map (Map.toList mp) # \(x, ptm) ->
--     (x, st {_pat_st_ptm = ptm})
-- 
-- pat_get_kind :: HCS => PatM a PatKindT
-- pat_get_kind = do
--   x <- pat_get
--   case x of
--     PatKind k -> pure k
--     _ -> error ""
-- 
-- pat_get_lit :: HCS => PatElem -> PatM a ()
-- pat_get_lit x = do
--   ptm <- pat_get_ptm
--   let mp = _trie_map ptm
--   case Map.lookup x mp of
--     Nothing -> do
--       -- tracem (Map.keys mp, x)
--       pat_fail
--     Just ptm -> pat_put_ptm ptm
-- 
-- pat_get_n_lit :: HCS => N -> PatM a ()
-- pat_get_n_lit i = pat_get_lit # PatN i
-- 
-- pat_get_n :: HCS => PatM a N
-- pat_get_n = do
--   x <- pat_get
--   case x of
--     PatN i -> pure i
--     _ -> error ""
-- 
-- pat_get_str_lit :: HCS => String -> PatM a ()
-- pat_get_str_lit str = pat_get_lit # PatStr str
-- 
-- pat_asgn_type :: HCS => N -> Type -> PatM a ()
-- pat_asgn_type i t = do
--   mp <- gets _pat_st_types
--   case Map.lookup i mp of
--     Nothing -> modify # \st -> st
--       {_pat_st_types = map_insert i t mp}
--     Just t' -> if t' == t then nop else pat_fail
-- 
-- pat_asgn_term :: HCS => N -> Term -> PatM a ()
-- pat_asgn_term i e = do
--   mp <- gets _pat_st_terms
--   case Map.lookup i mp of
--     Nothing -> modify # \st -> st
--       {_pat_st_terms = map_insert i e mp}
--     Just e' -> if e' == e then nop else pat_fail

-----

-- pat_elem_add_offset :: HCS => N -> PatElem -> PatElem
-- pat_elem_add_offset offset pe = case pe of
--   PatN i -> PatN # i + offset
--   _ -> pe

pat_add_offset :: HCS => N -> Pat -> Pat
pat_add_offset offset pat = case pat of
  [] -> []
  PatKind PtTermVar : PatN i : pat ->
    PatKind PtTermVar : PatN (i + offset) :
      pat_add_offset offset pat
  x : pat -> x : pat_add_offset offset pat

pat_sub_offset :: HCS => N -> Pat -> Pat
pat_sub_offset offset = pat_add_offset (-offset)

-----

elab_mk_goal_id :: HCS => Elab N
elab_mk_goal_id = do
  elab <- get
  let i = _elab_goals_num elab
  put # elab {_elab_goals_num = i + 1}
  return i

elab_get_ctx_ids :: HCS => Elab [N]
elab_get_ctx_ids = do
  pst <- gets _elab_pst
  return # map _ctx_id # _pst_ctxs pst

ctx_get_id :: HCS => Elab N
ctx_get_id = do
  ctx <- get_ctx
  return # _ctx_id ctx

-- elab_ctx_index_to_id :: HCS => N -> Elab [N]
-- elab_ctx_index_to_id i = do
--   ids <- elab_get_ctx_ids
--   error ""

-- elab_ctx_ids_to_indices :: HCS => [N] -> Elab [N]
-- elab_ctx_ids_to_indices xs = do
--   ids <- elab_get_ctx_ids
--   let mp = list_to_map_inv ids
--   return # map (\i -> map_get i mp) xs

name_to_sub :: HCS => Name -> N -> Name
name_to_sub name i = name ++ nat_to_sub i

elab_get_avail_name :: HCS => Name -> Elab Name
elab_get_avail_name name = do
  asrt_no_sim_index
  -- kth <- gets_th _th_kth
  -- i <- pure # nat_find # \i -> i /= 0 && let
  --   name' = name_to_sub name i
  --   in not # kth_has_id name' kth
  mp <- gets_th _th_mp_id_name
  i <- pure # nat_find # \i -> i /= 0 && let
    name' = name_to_sub name i
    in not # bimap_has' name' mp
  return # name_to_sub name i

simp_opts :: HCS => SimpOpts
simp_opts = SimpOpts
  { _simp_single_pass = False
  }

ctx_set_depth :: HCS => N -> Elab ()
ctx_set_depth d = modify_ctx # \ctx -> ctx {_ctx_term_depth = d}

ctx_with_depth :: HCS => N -> Elab a -> Elab a
ctx_with_depth d m = do
  d0 <- ctx_term_depth
  ctx_set_depth d
  a <- m
  ctx_set_depth d0
  return a

rw_map_filter :: HCS => Id -> RwMap -> RwMap
rw_map_filter name = trie_filter_prefix
  [PatDepth 0, PatKind PtApp, PatKind PtTermRef, PatId name]

ctx_rw_map_filter :: HCS => Id -> Elab RwMap
ctx_rw_map_filter i = do
  rw_map <- gets_th _th_simp_map
  return # rw_map_filter i rw_map

-----

newtype ThIdent = ThIdent Name

class CtxShow a where
  ctx_show :: HCS => a -> Elab Sctr

instance CtxShow String where
  ctx_show s = pure [dfsc s]

ctx_err' :: HCS => Name -> Name -> Sctr -> Elab a
ctx_err' scope name msg = do
  err # (scope, name) : case msg of
    [] -> []
    msg -> dfsc ": " : msg

tac_err :: HCS => Name -> Sctr -> Elab a
tac_err = ctx_err' "tactic"

tac_err' :: HCS => Name -> Elab b
tac_err' name = tac_err name []

tac_err_show :: HCS => (CtxShow a) => Name -> a -> Elab b
tac_err_show name a = do
  msg <- ctx_show a
  tac_err name msg

kw_err :: HCS => Name -> Sctr -> Elab a
kw_err = ctx_err' "keyword"

kw_err' :: HCS => Name -> Elab b
kw_err' name = kw_err name []

kw_err_show :: HCS => (CtxShow a) => Name -> a -> Elab b
kw_err_show name a = do
  msg <- ctx_show a
  kw_err name msg

-----

pp_dflt :: HCS => PrettyPrinter
pp_dflt = PrettyPrinter
  { _pp_term_sym = True
  , _pp_term_un_op = True
  , _pp_term_bin_op = True
  , _pp_term_binder = True
  , _pp_term_binder_join = True
  , _pp_term_ref_types = False
  , _pp_proof_ref_types = False
  , _pp_set_fin = True
  }

pp_raw :: HCS => PrettyPrinter
pp_raw = PrettyPrinter
  { _pp_term_sym = False
  , _pp_term_un_op = False
  , _pp_term_bin_op = False
  , _pp_term_binder = False
  , _pp_term_binder_join = False
  , _pp_term_ref_types = True
  , _pp_proof_ref_types = True
  , _pp_set_fin = False
  }

-- get_pp :: HCS => Elab PrettyPrinter
-- get_pp = gets_th _th_pp
-- 
-- put_pp :: HCS => PrettyPrinter -> Elab ()
-- put_pp pp = modify_th # \th -> th {_th_pp = pp}
-- 
-- gets_pp :: HCS => (PrettyPrinter -> a) -> Elab a
-- gets_pp f = do
--   pp <- get_pp
--   return # f pp
-- 
-- modify_pp :: HCS => (PrettyPrinter -> PrettyPrinter) -> Elab ()
-- modify_pp f = modify_th # \th -> th {_th_pp = f # _th_pp th}
-- 
-- with_pp :: HCS => PrettyPrinter -> Elab a -> Elab a
-- with_pp pp m = do
--   pp0 <- get_pp
--   put_pp pp
--   res <- m
--   put_pp pp0
--   return res
-- 
-- with_pp_raw :: HCS => Elab a -> Elab a
-- with_pp_raw = with_pp pp_raw

gets_pp :: HCS => (PrettyPrinter -> a) -> Elab a
gets_pp f = pure # f pp_dflt

-----

init_schemes :: HCS => TheorySchemes
init_schemes = TheorySchemes
  { _schemes_ind = Map.empty
  , _schemes_ind_tc = Map.empty
  , _schemes_cs = Map.empty
  , _schemes_cs_tc = Map.empty
  }

schemes_union :: HCS => TheorySchemes -> TheorySchemes -> TheorySchemes
schemes_union s1 s2 = let
  f = \g -> Map.union (g s1) (g s2)
  in TheorySchemes
    { _schemes_ind = f _schemes_ind
    , _schemes_ind_tc = f _schemes_ind_tc
    , _schemes_cs = f _schemes_cs
    , _schemes_cs_tc = f _schemes_cs_tc
    }

schemes_unions :: HCS => [TheorySchemes] -> TheorySchemes
schemes_unions = foldr1 schemes_union

-----

get_ctxs_num :: HCS => Elab N
get_ctxs_num = do
  pst <- get_pst
  return # len # _pst_ctxs pst

ctx_get_proofs :: HCS => Elab (Map N Stat)
ctx_get_proofs = gets_ctx _ctx_proofs

ctx_get_proof_list :: HCS => Elab [(N, Stat)]
ctx_get_proof_list = do
  hs <- ctx_get_proofs
  return # Map.toList hs

-----

-- localize_name :: HCS => N -> Name -> Name
-- localize_name n name = concat [name, [loc_char], show n]
-- 
-- unlocalize_name :: HCS => Name -> Name
-- unlocalize_name name = head # split_at_elem loc_char name
-- 
-- th_localize_name_mp :: HCS => Map Name N -> Name -> Theory Name
-- th_localize_name_mp loc name = case map_get name loc of
--   Nothing -> name
--   Just n -> localize_name n name
-- 
-- localize_name_mp_inc :: HCS => Map Name N -> Name -> Name
-- localize_name_mp_inc loc name = localize_name <~ name #
--   maybe 0 inc # map_get name loc

th_localize_name :: HCS => Id -> Theory Id
th_localize_name i = do
  -- loc <- gets _th_local_names
  -- th_localize_name_mp loc name
  return i

-- th_localize_name_inc :: HCS => Name -> Theory Name
-- th_localize_name_inc name = do
--   loc <- gets _th_local_names
--   return # localize_name_mp_inc loc name
-- 
-- kname_is_local :: HCS => Name -> Prop
-- kname_is_local = elem loc_char
-- 
-- th_name_is_local :: HCS => Name -> Theory Prop
-- th_name_is_local name = do
--   loc <- gets _th_local_names
--   return # Map.member name loc

mk_type_params_sctr :: HCS => N -> Sctr
mk_type_params_sctr n = if n == 0 then [] else let
  ts = map (\a -> ("type", [a])) # take (fi n) greek_letters_low
  in [dfsc ".{"] ++ intersperse (dfsc ", ") ts ++ [dfsc "}"]

-----

ctx_set_term_parser_state :: HCS => Bit -> Elab ()
ctx_set_term_parser_state st = modify_ctx # \ctx -> ctx
  {_ctx_term_parser_state = st}

-----

-- th_to_elab :: HCS => Theory a -> Elab a
-- th_to_elab m = ParserM # StateT # \elab -> ExceptT # do
--   res <- runExceptT # runStateT m # _elab_th elab
--   case res of
--     Left msg -> raise msg
--     Right (a, th) -> pure (a, elab {_elab_th = th})

th_to_elab :: HCS => Theory a -> Elab a
th_to_elab m = do
  th <- get_th
  res <- elab_lift_m # runExceptT # runStateT m th
  case res of
    Left msg -> raise msg
    Right (a, th) -> do
      put_th th
      return a

th_id_to_name :: HCS => Id -> Theory Name
th_id_to_name i = do
  res <- th_sim_dflt_id i #
    gets # bimap_get i . _th_mp_id_name
  return # case res of
    Nothing -> error # show i
    Just name -> name

th_name_to_id' :: HCS => Name -> Theory (Maybe Id)
th_name_to_id' name = th_sim_dflt_name name # do
  mp <- gets _th_mp_id_name
  return # bimap_get' name mp

th_name_to_id :: HCS => Name -> Theory Id
th_name_to_id name = do
  res <- th_name_to_id' name
  case res of
    Nothing -> error name
    Just i -> pure i

th_register_name :: HCS => Name -> Theory Id
th_register_name name = do
  th_asrt_no_sim_index
  res <- th_name_to_id' name
  case res of
    Just i -> pure i
    Nothing -> do
      -- error name
      i <- gets # inc . fi . bimap_size . _th_mp_id_name
      -- res <- gets _th_elab_res
      -- res <- pure # res
      --   { _res_mp_id_name = bimap_insert i name # _res_mp_id_name res
      --   }
      modify # \th -> th
        { _th_mp_id_name = bimap_insert i name # _th_mp_id_name th
        -- , _th_elab_res = res
        }
      return i

id_to_name :: HCS => Id -> Elab Name
id_to_name = th_to_elab . th_id_to_name

name_to_id_aux :: HCS => Prop -> Name -> Elab Id
name_to_id_aux is_new name = do
  res <- th_to_elab # th_name_to_id' name
  case res of
    Just i -> pure i
    Nothing -> do
      True <- pure is_new
      register_name name

name_to_id :: HCS => Name -> Elab Id
name_to_id = name_to_id_aux False

name_to_id' :: HCS => Name -> Elab (Maybe Id)
name_to_id' = th_to_elab . th_name_to_id'

register_name :: HCS => Name -> Elab Id
register_name = th_to_elab . th_register_name

th_has_name :: HCS => Name -> Theory Prop
th_has_name name = th_sim_dflt_name name #
  gets # bimap_has' name . _th_mp_id_name

has_name :: HCS => Name -> Elab Prop
has_name = th_to_elab . th_has_name

proof_index_to_name :: HCS => N -> Elab Name
proof_index_to_name i = do
  asms <- gets_ctx _ctx_asms
  return # case find ((== i) . snd) asms of
    Nothing -> usc' : show i
    Just (name, _) -> name

ctx_get_asm_name :: HCS => N -> Elab (Maybe Name)
ctx_get_asm_name i = do
  asms <- gets_ctx _ctx_asms
  return # fmap fst # find <~ asms # \(_, j) -> j == i

th_lift_m :: HCS => ElabM a -> Theory a
th_lift_m = liftIO

elab_lift_m :: HCS => ElabM a -> Elab a
elab_lift_m = liftIO

k_to_th :: HCS => K.Kernel a -> Theory a
k_to_th m = do
  kth <- gets _th_kth
  res <- th_lift_m # runMaybeT # runStateT m kth
  case res of
    Nothing -> raise [dfsc "Kernel: invalid action"]
    Just (a, kth) -> do
      modify # \th -> th {_th_kth = kth}
      return a

k_to_elab :: HCS => K.Kernel a -> Elab a
k_to_elab = th_to_elab . k_to_th

instance ToJSON OpDef where
  to_json op = Obj # Map.fromList
    [ ("name", to_json # _op_def_name op)
    , ("sym", to_json # _op_def_sym op)
    -- , ("type", to_json # _op_def_type op)
    -- , ("rel", to_json # _op_def_rel op)
    ]

-----

th_get_def' :: Id -> Theory (Maybe Def)
th_get_def' i = th_sim_dflt_id i # gets #
  map_get i . _th_defs

th_get_thm' :: Id -> Theory (Maybe Thm)
th_get_thm' i = th_sim_dflt_id i # gets #
  map_get i . _th_thms

th_get_def :: Id -> Theory Def
th_get_def = from_just_m . th_get_def'

th_get_thm :: Id -> Theory Thm
th_get_thm = from_just_m . th_get_thm'

th_has_def :: Id -> Theory Prop
th_has_def = fmap isJust . th_get_def'

th_has_thm :: Id -> Theory Prop
th_has_thm = fmap isJust . th_get_thm'

-----

get_def' :: Id -> Elab (Maybe Def)
get_def' = th_to_elab . th_get_def'

get_thm' :: Id -> Elab (Maybe Thm)
get_thm' = th_to_elab . th_get_thm'

get_def :: Id -> Elab Def
get_def = th_to_elab . th_get_def

get_thm :: Id -> Elab Thm
get_thm = th_to_elab . th_get_thm

has_def :: Id -> Elab Prop
has_def = th_to_elab . th_has_def

has_thm :: Id -> Elab Prop
has_thm = th_to_elab . th_has_thm

-----

th_id_to_index' :: Id -> Theory (Maybe N)
th_id_to_index' i = th_sim_dflt_id i #
  gets # bimap_get i . _th_mp_id_index

th_id_to_index :: Id -> Theory N
th_id_to_index = from_just_m . th_id_to_index'

id_to_index' :: Id -> Elab (Maybe N)
id_to_index' = th_to_elab . th_id_to_index'

id_to_index :: Id -> Elab N
id_to_index = th_to_elab . th_id_to_index

ifm_recon :: (Inhabited a) => Elab a -> Elab a
ifm_recon fn = do
  h <- gets_th _th_recon
  ifm' h fn