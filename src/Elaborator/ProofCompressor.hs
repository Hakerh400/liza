module Elaborator.ProofCompressor
  ( compress_proof
  , decompress_proof
  ) where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Maybe
import Data.Either
import Data.List
import Data.Char
import Control.Monad
import Control.Monad.State

import Util
import Sctr
import Bit
import Tree
import Trie
import Parser
import Serializer

import Elaborator.Data
import Elaborator.Util
import Elaborator.Parser
import Elaborator.Type
import Elaborator.Term
import Elaborator.Goal
import Elaborator.Proof

import qualified Kernel as K

data SerInfo = SerInfo
  { _ser_types :: Map K.Type N
  , _ser_terms :: Map K.Term N
  } deriving (Eq, Ord, Show)

type S = StateT SerInfo Ser

init_ser_info :: SerInfo
init_ser_info = SerInfo
  { _ser_types = Map.empty
  , _ser_terms = Map.empty
  }

compress_proof :: K.Proof -> String
compress_proof p = let
  s = buf_to_str # ser_to_buf p
  in unlines' # split_into_same_groups 100 # '`' : s

s_list :: (a -> S ()) -> [a] -> S ()
s_list f xs = do
  lift # ser # length xs
  mapM_ f xs

ser_type :: K.Type -> S ()
ser_type t = do
  ts <- gets _ser_types
  case map_get t ts of
    Just i -> lift # do
      ser_0
      ser_bnat (map_size ts - 1) # fi i
    Nothing -> do
      ifmn (null ts) # lift ser_1
      case t of
        K.TypeVar i -> lift # do
          ser_bnat 3 0
          ser i
        K.Prop -> lift # ser_bnat 3 1
        K.SetT -> lift # ser_bnat 3 2
        K.Func a b -> do
          lift # ser_bnat 3 3
          ser_type a
          ser_type b
      modify # \st -> st {_ser_types = map_push_inv t ts}

ser_term :: K.Term -> S ()
ser_term e = do
  es <- gets _ser_terms
  case map_get e es of
    Just i -> lift # do
      ser_0
      ser_bnat (map_size es - 1) # fi i
    Nothing -> do
      ifmn (null es) # lift ser_1
      case e of
        K.TermRef i ps -> do
          lift # do
            ser_bnat 3 0
            ser i
          s_list ser_type ps
        K.TermVar i -> lift # do
          ser_bnat 3 1
          ser i
        K.Lam t e -> do
          lift # ser_bnat 3 2
          ser_type t
          ser_term e
        K.App a b -> do
          lift # ser_bnat 3 3
          ser_term a
          ser_term b
      modify # \st -> st {_ser_terms = map_push_inv e es}

ser_proof :: K.Proof -> S ()
ser_proof p = case p of
  K.ProofRef i ps -> do
    lift # do
      ser_bnat 5 0
      ser i
    s_list ser_type ps
  K.ProofVar i -> lift # do
    ser_bnat 5 1
    ser i
  K.ImpI s p -> do
    lift # ser_bnat 5 2
    ser_term s
    ser_proof p
  K.UnivI t p -> do
    lift # ser_bnat 5 3
    ser_type t
    ser_proof p
  K.ImpE p1 p2 -> do
    lift # ser_bnat 5 4
    ser_proof p1
    ser_proof p2
  K.UnivE p e -> do
    lift # ser_bnat 5 5
    ser_proof p
    ser_term e

dser_str' :: Dser String
dser_str' = do
  end <- gets # null . _dser_bits
  if end then pure "" else do
    n <- dser_bnat 93
    s <- dser_str'
    return # chr (33 + fi n) : s

dser_str :: Dser String
dser_str = do
  s <- dser_str'
  return # reversed <~ s # dropWhile (== '!')

buf_to_str :: Buf -> String
buf_to_str buf = from_right # dser_from_buf' dser_str buf

-----

data DserInfo = DserInfo
  { _dser_types :: Map N K.Type
  , _dser_terms :: Map N K.Term
  } deriving (Eq, Ord, Show)

type D = StateT DserInfo Dser

init_dser_info :: DserInfo
init_dser_info = DserInfo
  { _dser_types = Map.empty
  , _dser_terms = Map.empty
  }

decompress_proof :: String -> K.Proof
decompress_proof str = dser_from_buf #
  str_to_buf (concat # sanl # tail' str)

d_list :: D a -> D [a]
d_list f = do
  n <- lift dser
  replicateM n f

ser_str :: String -> Ser ()
ser_str str = case str of
  [] -> nop
  (c : cs) -> do
    ser_bnat 93 # fi (ord c) - 33
    ser_str cs

str_to_buf :: String -> Buf
str_to_buf str = from_right # ser_to_buf' # ser_str str

dser_type :: D K.Type
dser_type = do
  ts <- gets _dser_types
  is_new <- if null ts then pure True else lift dser_prop
  if not is_new then do
    i <- lift # dser_bnat # map_size ts - 1
    return # fromJust # map_get i ts
  else do
    k <- lift # dser_bnat 3
    t <- case k of
      0 -> lift dser >>= liftm K.TypeVar
      1 -> pure # K.Prop
      2 -> pure # K.SetT
      3 -> do
        a <- dser_type
        b <- dser_type
        return # K.Func a b
      _ -> error ""
    modify # \st -> st {_dser_types = map_push t ts}
    return t

dser_term :: D K.Term
dser_term = do
  es <- gets _dser_terms
  is_new <- if null es then pure True else lift dser_prop
  if not is_new then do
    i <- lift # dser_bnat # map_size es - 1
    return # fromJust # map_get i es
  else do
    k <- lift # dser_bnat 3
    e <- case k of
      0 -> do
        i <- lift dser
        ps <- d_list dser_type
        return # K.TermRef i ps
      1 -> do
        i <- lift dser
        return # K.TermVar i
      2 -> do
        t <- dser_type
        e <- dser_term
        return # K.Lam t e
      3 -> do
        a <- dser_term
        b <- dser_term
        return # K.App a b
      _ -> error ""
    modify # \st -> st {_dser_terms = map_push e es}
    return e

dser_proof :: D K.Proof
dser_proof = do
  k <- lift # dser_bnat 5
  case k of
    0 -> do
      i <- lift dser
      ps <- d_list dser_type
      pure # K.ProofRef i ps
    1 -> do
      i <- lift dser
      return # K.ProofVar i
    2 -> do
      s <- dser_term
      p <- dser_proof
      return # K.ImpI s p
    3 -> do
      t <- dser_type
      p <- dser_proof
      return # K.UnivI t p
    4 -> do
      p1 <- dser_proof
      p2 <- dser_proof
      return # K.ImpE p1 p2
    5 -> do
      p <- dser_proof
      e <- dser_term
      return # K.UnivE p e
    _ -> error ""

instance Serializable K.Proof where
  ser p = evalStateT (ser_proof p) init_ser_info
  dser = evalStateT dser_proof init_dser_info