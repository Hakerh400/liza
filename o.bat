@echo off
cls

cd src
ghc -O -outputdir "../build" -o "../pure.exe" -rtsopts=ignoreAll Main.hs
cd ..