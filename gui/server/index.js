'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('../assert');
const cp = require('child_process');
const EventEmitter = require('events');
const O = require('../js-util');
const config = require('../config');

const proj = config.project_name;

const cwd = __dirname;
const proj_dir = path.join(cwd, '../..');
const th_dir = path.join(proj_dir, 'theory');
const server_pth = path.join(proj_dir, proj);

class Server extends EventEmitter{
  constructor(){
    super();
    
    const server = cp.spawn(server_pth, {cwd: proj_dir});
    this.server = server;
    
    let msg = '';
    let str = '';
    
    server.stdout.on('data', buf => {
      const parts = (str + buf).split('.');
      
      str = parts.pop();
      
      for(const s of parts){
        const c = O.sfcc(Number(s));
        
        if(c !== '\n'){
          msg += c;
          continue;
        }
        
        this.on_msg(JSON.parse(msg));
        msg = '';
      }
    });
    
    server.stderr.on('data', buf => {
      this.on_err(buf.toString());
    });
    
    server.on('error', err => {
      this.on_err(err);
    });
    
    server.on('exit', status => {
      this.on_exit(status);
    });
  }
  
  exit(){
    this.server.kill();
  }
  
  send(msg){
    const s = [...`${JSON.stringify(msg)}\n`].
      map(a => `${O.cc(a)}.`).join('');
    
    this.server.stdin.write(s);
  };
  
  on_msg(msg){
    this.emit('msg', msg);
  };
  
  on_err(msg){
    this.emit('error', msg);
  }
  
  on_exit(status){
    this.emit('exit', status);
  }
}

module.exports = Server;