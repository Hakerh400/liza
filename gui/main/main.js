"use strict"

const fs = require('fs');
const path = require('path');
const assert = require('../assert');
const cp = require('child_process');
const O = require('../js-util');
const media = require('../../../node-projects/media');
const config = require('../config');
const Server = require('../server');
const storage = require('./storage');
const abbrevs = require('./abbrevs');
const unescape_str = require('./unescape-str');
const Interface = require('./interface');
const names_mp = require("./names")

return require('./mathlib');

const {min, max} = Math;
const {dflt_scope} = Interface;

const snippet_id = 1;

const proj = config.project_name;

const cwd = __dirname;
const proj_dir = path.join(cwd, '../..');
const data_dir = path.join(proj_dir, 'data');
const snippets_dir = path.join(data_dir, 'snippets');
const th_dir = path.join(proj_dir, 'theory');
const server_pth = path.join(proj_dir, proj);

const pure_ext = proj;
const snippet_ext = 'png';

const {g} = O.ceCanvas();
const {canvas} = g;

let server = null;
let intface = null;
let cur_file = null;

const main = () => {
  if(!fs.existsSync(data_dir))
    fs.mkdirSync(data_dir);
  
  storage.init();
  
  init_server();
  mk_open_file_intface();
  on_resize();
  
  if(!storage.has('initial_file'))
    storage.set('initial_file', null);
  
  const initial_file = storage.get('initial_file');
  
  if(initial_file !== null)
    intface.open_file(initial_file);
  
  // intface.cursors[0] = Interface.Cursor.single(9, 0)
  // intface.y_scroll = 0
  // intface.render()
  
  aels();
};

const aels = () => {
  O.ael("resize", on_resize)
  O.ael("keydown", on_key_down)
  O.ael("blur", on_blur)
  O.ael("error", on_err)
};

const on_resize = evt => {
  intface.resize(O.iw, O.ih);
};

const on_key_down = evt => {
  const {code, key} = evt;
  const flags = O.evtFlags(evt);

  const char_cnd = key.length === 1 && (flags & 5) === 0;
  const char = char_cnd ? key : null;
  
  if(intface.on_key_down(code, flags, char))
    return O.pd(evt);
};

const on_blur = evt => {
  intface.on_blur()
}

const on_err = evt => {
  intface.has_err = 1
  O.body.innerText = evt.error
}

const init_server = () => {
  server = new Server();
  
  server.on('msg', on_msg);
  
  server.on('error', err => {
    log('Server error: ', err);
  });
  
  server.on('exit', status => {
    log(`Server exited with status ${status}`);
  });
};

const send_msg = msg => {
  // log('SEND', msg);
  server.send(msg);
};

const on_msg = msg => {
  const [type, info] = msg;
  // log('READ', type, info);
  
  const {cmds, cmds_queue} = intface;
  
  cur_file = null;
  
  if(type === 'processing_file'){
    cur_file = info;
    
    intface.set_info([[dflt_scope, 'Processing file: ']].concat(info));
    intface.render();
    
    return;
  }
  
  if(type === 'scopes'){
    intface.add_scopes_to_cmd(info)
    intface.render()

    if(cmds_queue.length !== 0)
      run_cmds();
    
    return;
  }
  
  if(type === 'info'){
    cmds_queue.length = 0;
    
    intface.set_info(info);
    intface.render();

    return;
  }
  
  if(type === 'err_in_imports'){
    cmds_queue.length = 0;
    
    // const msg = cur_file !== null ? `Error in file: ${cur_file}\n\n` : '';
    
    intface.set_info(info);
    intface.render();
    
    return;
  }

  assert.fail(type);
};

const mk_intface = ctor_name => {
  if(intface !== null)
    intface.dispose();
  
  intface = new Interface[ctor_name](g);
};

const mk_open_file_intface = (pth='') => {
  mk_intface('Open_file');

  intface.pth = pth;
  intface.get_files_fn = get_file_list1;
  intface.open_file = open_file;

  intface.update_files();
};

const get_pth_info = pth => {
  pth = pth_split(pth);

  const name = pth.pop();
  const dir = path.join(th_dir, pth.join('/'));

  return [dir, name];
};

const get_file_list = pth => {
  const [dir, name] = get_pth_info(pth);
  
  if(!fs.existsSync(dir)) return;
  if(!fs.statSync(dir).isDirectory()) return;
  
  const files = fs.readdirSync(dir).map(name => {
    if(name.endsWith(`.${pure_ext}`))
      return name.slice(0, -(pure_ext.length + 1));
    
    if(name.includes('.')) return null;
    
    return `${name}/`;
  }).filter(m => {
    if(m === null) return 0;
    return m.includes(name);
  }).sort((m1, m2) => {
    return (m1 | 0) < (m2 | 0) ? -1 : 1
    // let p;
    // 
    // const i1 = m1.indexOf(name);
    // const i2 = m2.indexOf(name);
    // 
    // p = (i1 > i2) - (i1 < i2);
    // if(p) return p;
    // 
    // p = m2.endsWith('/') - m1.endsWith('/');
    // if(p) return p;
    // 
    // p = (m1 > m2) - (m1 < m2);
    // if(p) return p;
    // 
    // return 0;
  })
  
  return files;
};

const get_file_list1 = pth => {
  return get_file_list(pth) ?? [];
};

const open_file = pth => {
  const full_pth = path.join(th_dir, `${pth}.${pure_ext}`);
  
  if(!fs.existsSync(full_pth)){
    const dir = path.join(full_pth, '..');
    
    if(!fs.existsSync(dir))
      fs.mkdirSync(dir, {recursive: true});
    
    O.wfs(full_pth, '');
  }
  
  send_msg(['open_file', pth]);
  mk_intface('File');

  intface.pth = pth;
  intface.full_pth = full_pth;
  intface.read_file = read_file;
  intface.save_file = save_file;
  intface.close_file = close_file;
  intface.run_cmds_fn = run_cmds;
  intface.save_snippet = save_snippet;

  on_resize();
  intface.load();
  
  storage.set('initial_file', pth);
};

const close_file = pth => {
  send_msg(['close_file']);
  
  let dir = pth_get_dir(pth);
  if(dir !== '') dir += '/';
  
  mk_open_file_intface(dir);
  on_resize();
};

const read_file = pth => {
  return O.rfs(pth, 1);
};

const save_file = (pth, str) => {
  if(str === ''){
    if(fs.existsSync(pth))
      fs.unlinkSync(pth);

    return;
  }

  return O.wfs(pth, str);
};

const run_cmds = () => {
  let {cmds_queue} = intface
  let cmd = cmds_queue[0]
  
  send_msg(['elab_cmd', String(cmd.index + 1), cmd.to_str()])
};

const calc_snippet_id = () => {
  if(snippet_id !== null)
    return snippet_id;
  
  const reg = RegExp(`^([1-9]\\d*)\\.${snippet_ext}`);
  
  const get_id = name => {
    const match = name.match(reg);
    if(match === null) return null;
    
    return BigInt(match[1]);
  };
  
  const ids = O.sortAsc(fs.readdirSync(snippets_dir).
    map(name => get_id(name)).
    filter(a => a !== null));
  
  let id = 1;
  while(ids[id - 1] === id) id++;
  
  return id;
};

const save_snippet = img => {
  if(!fs.existsSync(snippets_dir))
    fs.mkdirSync(snippets_dir);
  
  const id = calc_snippet_id();
  const name = `${id}.${snippet_ext}`;
  const pth = path.join(snippets_dir, name);
  
  media.saveImage(pth, img).catch(log);
};

const indent = len => {
  return ' '.repeat(len);
};

const isDigit = c => c >= '0' && c <= '9';

const isSubDigit = c => c >= '₀' && c <= '₉';

const pth_split = pth => pth.split(/[\/\\]/);;

const pth_get_dir = pth => {
  const xs = pth_split(pth);
  return xs.slice(0, -1).join('/');
};

main();