'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('../assert');
const cp = require('child_process');
const O = require('../js-util');
const config = require('../config');
const abbrevs = require('./abbrevs');

const unescape_str = str => {
  return str;
  // return str.split('.').slice(1).map(a => O.sfcc(Number(a))).join('');
};

module.exports = unescape_str;