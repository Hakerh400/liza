'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('../assert');
const O = require('../js-util');

const cwd = __dirname;
const proj_dir = path.join(cwd, '../..');
const data_dir = path.join(proj_dir, 'data');
const storage_file = path.join(data_dir, 'storage.json');

let storage = {};

const init = async () => {
  if(fs.existsSync(storage_file))
    storage = JSON.parse(O.rfs(storage_file, 1));

  const proto = {
    getItem(key){
      return get(key);
    },

    setItem(key, val){
      set(key, val);
    },

    removeItem(key){
      remove(key);
    },

    clear(){
      clear();
    },
  };

  Object.defineProperty(window, 'localStorage', {
    value: new Proxy(proto, {
      get(t, key, obj){
        if(O.has(proto, key))
          return proto[key];
        
        return get(key);
      },

      set(t, key, val, obj){
        set(key, val);
        return 1;
      },
    }),
  });
};

const has = key => {
  return O.has(storage, key);
};

const get = key => {
  return storage[key];
};

const set = (key, val) => {
  storage[key] = val;
  save();
};

const remove = key => {
  delete storage[key];
  save();
};

const clear = () => {
  storage = {};
  save();
};

const save = () => {
  O.wfs(storage_file, JSON.stringify(storage));
};

module.exports = {
  init,
  has,
  get,
  set,
  remove,
  clear,
};