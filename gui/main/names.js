"use strict"

const fs = require("fs")
const path = require("path")
const assert = require("../assert")
const O = require("../js-util")
const config = require("../config")

const cwd = __dirname
const proj_dir = path.join(cwd, "../..")
const db_dir = path.join(proj_dir, "db")
const names_file = path.join(db_dir, "names.txt")

const ids_reg = /\x01(\d+)/g

let names_list
let mp_id_name
let mp_name_id

const update = () => {
  // log("update")
  
  names_list = O.sanl(O.rfs(names_file, 1).trim())
  mp_id_name = new Map()
  mp_name_id = new Map()

  names_list.forEach((name, i) => {
    i = BigInt(i + 1)
    mp_id_name.set(i, name)
    mp_name_id.set(name, i)
  })
}

const id_to_name = i => {
  assert(typeof i === "bigint")
  
  if(!mp_id_name.has(i)) update()
  
  if(!mp_id_name.has(i)){
    log(i)
    assert.fail()
  }

  return mp_id_name.get(i)
}

const name_to_id = name => {
  assert(typeof name === "string")
  
  // if(!mp_name_id.has(name)) update()
  if(!mp_name_id.has(name)){
    log(name)
    assert.fail()
  }
  
  return mp_name_id.get(name)
}

const str_ids_to_names = str => {
  update()
  return str.replace(ids_reg, (a, b) => id_to_name(BigInt(b)))
}

update()

module.exports = {
  names_list,
  mp_id_name,
  mp_name_id,
  id_to_name,
  name_to_id,
  str_ids_to_names,
  update,
}