'use strict';

const assert = require('../assert');
const O = require('../js-util');
const pos = require('./position');

const {min, max} = Math;

class Cursor{
  static merge(cursors){
    if(cursors.length === 1) return cursors[0];
    
    const [y1, x1] = pos.minimum(cursors.map(c => [c.y1, c.x1]));
    const [y2, x2] = pos.maximum(cursors.map(c => [c.y2, c.x2]));
    
    const dir = cursors.some(c => c.y === y2 && c.x === x2) ? 1 :
      cursors.some(c => c.y === y1 && c.x === x1) ? 0 : 1;
    
    return new Cursor(y1, x1, y2, x2, dir);
  }
  
  static single(y, x, x3=null){
    return new Cursor(y, x, y, x, null, x3);
  }

  static select(y1, x1, y2, x2, x3=null){
    let dir = 1;

    if(pos.lt(y2, x2, y1, x1)){
      [y1, x1, y2, x2] = [y2, x2, y1, x1];
      dir = 0;
    }
    
    return new Cursor(y1, x1, y2, x2, dir, x3);
  }
  
  constructor(y1, x1, y2=y1, x2=x1, dir=null, x3=null){
    assert(pos.le(y1, x1, y2, x2));
    
    if(dir === null) dir = 1;
    
    this.y1 = y1;
    this.x1 = x1;
    this.y2 = y2;
    this.x2 = x2;
    this.dir = dir;
    this.y = dir === 0 ? y1 : y2;
    this.x = dir === 0 ? x1 : x2;
    
    // if(!this.single) x3 = null;
    this.x3 = x3 ?? this.x;

    const from_to = [[y1, x1], [y2, x2]];
    this.from = from_to[dir ^ 1];
    this.to = from_to[dir];
  }
  
  get dy(){
    return this.y2 - this.y1;
  }
  
  get dx(){
    return this.x2 - this.x1;
  }
  
  get single(){
    return this.dy === 0 && this.dx === 0;
  }
  
  le(cur){
    return pos.le(this.y1, this.x1, cur.y1, cur.x1);
  }
  
  has_pos(y, x){
    return pos.between(y, x, this.y1, this.x1, this.y2, this.x2);
  }
  
  overlaps(cur, rec=1){
    if(this.single)
      return cur.has_pos(this.y, this.x);
    
    const cnd1 = pos.le(cur.y1, cur.x1, this.y1, this.x1);
    const cnd = cnd1 && pos.lt(this.y1, this.x1, cur.y2, cur.x2);
    
    if(cnd) return 1;
    if(rec) return cur.overlaps(this, 0);
  }
  
  shift(y1, x1, y2, x2){
    const chk = pos.le(y2, x2, y1, x1);
    const [ym, xm] = pos.min(y1, x1, y2, x2);
    
    if(pos.lt(this.y2, this.x2, ym, xm)) return this;
    
    const [y1_new, x1_new] = pos.shift(this.y1, this.x1, y1, x1, y2, x2, chk);
    const [y2_new, x2_new] = pos.shift(this.y2, this.x2, y1, x1, y2, x2, chk);
    
    // if(chk){
    //   log(this)
    //   log(chk, y1, x1, y2, x2, ym, xm)
    //   log(y1_new, x1_new, y2_new, x2_new, null, this.dir)
    //   log()
    // }
    
    return new Cursor(y1_new, x1_new, y2_new, x2_new, this.dir);
  }
}

module.exports = Cursor;