'use strict';

const fs = require('fs');
const path = require('path');
const O = require('../js-util');

// const LOCAL = O.url.startsWith('http://localhost/');
//
// const scriptSrc = LOCAL ?
//   dir('data/mathjax.js') :
//   'https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js';

const cwd = __dirname;
const proj_dir = path.join(cwd, '../..');
const data_dir = path.join(proj_dir, 'data');
const orig_mathjax_dir = path.join(proj_dir, '../mathjax');
const orig_mathjax_file = path.join(orig_mathjax_dir, 'tex-mml-chtml.js');
const mathjax_dir = path.join(data_dir, 'mathjax');
const mathjax_file = path.join(mathjax_dir, 'mathjax.js');

const init = async () => {
  const sem = new O.Semaphore(0);

  if(!fs.existsSync(data_dir)) fs.mkdirSync(data_dir);
  if(!fs.existsSync(mathjax_dir)) fs.mkdirSync(mathjax_dir);

  if(!fs.existsSync(mathjax_file)){
    const s = await O.rfs(orig_mathjax_file, 1)
    O.wfs(mathjax_file, s);
  }

  window.MathJax = {
    options: {
      // enableMenu: 0,
    },
    tex: {
      inlineMath: [['`', '`']],
      displayMath: [['``', '``']],
    },
    startup: {
      pageReady: () => {
        sem.signal();
      },
    },
  };

  const src = await O.rfs(mathjax_file, 1);
  Function(src)();

  // const script = ce('script');
  //
  // script.src = script_src;
  // O.head.appendChild(script);

  await sem.wait();
};

const math_to_div = async (str, display=0, fontSize=null) => {
  const div = ce('div');

  if(fontSize !== null)
    div.style.fontSize = `${fontSize}px`

  div.innerText = display ? `$$${str}$$` : `$${str}$`;
  await typeset(div);

  return div;
};

const typeset = async elems => {
  if(!O.isArr(elems))
    elems = [elems];

  await MathJax.typesetPromise(elems);
};

const ce = tag => {
  return O.doc.createElement(tag);
};

module.exports = {
  init,
  math_to_div,
  typeset,
};