'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('../assert');
const O = require('../js-util');

const cwd = __dirname;
const abbrevsFile = path.join(cwd, 'abbrevs.txt');

const arr2obj = arr => {
  const obj = O.obj();
  
  for(const entry of arr){
    const val = entry.pop();
    const keys = entry;
    
    for(const key of keys)
      obj[key] = val;
  }
  
  return obj;
};

const abbrevs = arr2obj(O.sanl(O.rfs(abbrevsFile, 1)).
  map(a => a.split(' ').reverse()));

module.exports = abbrevs;