'use strict';

const assert = require('../assert');
const O = require('../js-util');

const {min, max} = Math;

const pos = {
  eq(y1, x1, y2, x2){
    return y1 === y2 && x1 === x2;
  },

  le(y1, x1, y2, x2){
    if(y1 < y2) return 1;
    return y1 === y2 && x1 <= x2;
  },

  lt(y1, x1, y2, x2){
    if(y1 < y2) return 1;
    return y1 === y2 && x1 < x2;
  },

  between(y, x, y1, x1, y2, x2){
    return pos.le(y1, x1, y, x) && pos.le(y, x, y2, x2);
  },

  shift(y, x, y1, x1, y2, x2, chk=0){
    if(chk && pos.between(y, x, y2, x2, y1, x1))
      return [y2, x2];
    
    const dy = y2 - y1;
    const dx = x2 - x1;
    const y_new = y + dy;
    const x_new = y === y1 ? x + dx : x;
    
    return [y_new, x_new];
  },

  min(y1, x1, y2, x2){
    if(pos.le(y1, x1, y2, x2)) return [y1, x1];
    return [y2, x2];
  },
  
  max(y1, x1, y2, x2){
    if(pos.le(y1, x1, y2, x2)) return [y2, x2];
    return [y1, x1];
  },
  
  foldl(f, list){
    let p = null;
    
    for(const p1 of list){
      if(p === null){
        p = p1;
        continue;
      }
      
      p = f(...p, ...p1);
    }
    
    return p;
  },
  
  minimum(list){
    return pos.foldl(pos.min, list);
  },
  
  maximum(list){
    return pos.foldl(pos.max, list);
  },
};

module.exports = pos;