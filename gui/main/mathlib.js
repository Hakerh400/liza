"use strict"

const fs = require("fs")
const path = require("path")
const assert = require("../assert")
const cp = require("child_process")
const O = require("../js-util")
const config = require("../config")
const storage = require("./storage")
const Interface = require("./interface")
// const mathjax = require("./mathjax")

const {cols, scope_cols} = Interface

const main_th_dir_name = "theory"
const info_ext = "txt"
const refs_num_threshold = 20

const cwd = __dirname
const proj_dir = path.join(cwd, "../..")
const mathlib_dir = path.join(proj_dir, "data/mathlib")
const mathlib_index_file = path.join(mathlib_dir, "index.json")

const proj = config.project_name
const div = O.ceDiv(O.body, "main")

let hist1 = []
let hist2 = []

let mp_id_name = null
let mp_name_id = null
let mp_op_id = null

let cur_id = null

const main = async () => {
  let css = O.rfs(path.join(cwd, "style.css"), 1)
  let style = O.ce(O.head, "style")
  style.innerText = css
  
  O.body.classList.add("mathlib")
  
  let glob_info = JSON.parse(O.rfs(mathlib_index_file, 1))
  
  mp_id_name = new Map(glob_info.mp_id_name)
  mp_name_id = new Map(glob_info.mp_id_name.map((([a, b]) => [b, a])))
  mp_op_id = new Map(glob_info.ops.map((op => [op.sym, op.name])))
  
  storage.init()
  // await mathjax.init()
  
  O.ael("keydown", evt => {
    let {code} = evt
    let flags = O.evtFlags(evt)
    
    if(flags === 1 || flags === 4){
      if(code === "ArrowLeft"){
        if(hist1.length === 0) return
        let id = hist1.pop()
        hist2.push(cur_id)
        load_entry(id, 0)
        return
      }
      
      if(code === "ArrowRight"){
        if(hist2.length === 0) return
        let id = hist2.pop()
        hist1.push(cur_id)
        load_entry(id, 0)
        return
      }
      
      return
    }
  })
  
  // await load_entry("/core/defs", "_true_desc")
  await load_entry(storage.has("id") ? Number(storage.get("id")) : 1)
}

// const load_dir = async pth => {
//   set_title(`/${pth}`)
// 
//   const file = mk_pth(pth)
//   const src = await O.rfs(file, 1);
//   const info = parse_dir_info(src);
//   const e_entries = O.ceDiv(div, 'entries');
// 
//   for(const entry of info){
//     const e_entry = O.ceDiv(e_entries, 'entry');
// 
//     const {name, desc} = entry;
//     const desc1 = ` - ${desc}`;
// 
//     ce_link(e_entry, name, `${pth}/${name}`);
//     O.ceDiv(e_entry, 'entry-desc').innerText = desc1;
//   }
// }
// 
// const load_file = async pth => {
//   set_title(`/${pth}`);
// 
//   const file = mk_pth(pth);
//   const src = await O.rfs(file, 1);
//   const info = parse_file_info(O.sanll(src).slice(0, 2).join('\n\n'));
//   const e_entries = O.ceDiv(div, 'entries');
// 
//   for(const obj of info){
//     const e_entry = O.ceDiv(e_entries, 'entry');
//     
//     const {name, desc} = obj;
//     const desc1 = ` - ${desc}`;
// 
//     ce_link(e_entry, name, pth, name);
//     O.ceDiv(e_entry, 'entry-desc').innerText = desc1;
//   }
// };
// 
// const load_file_entry = async (pth, name) => {
//   const file = mk_pth(pth);
//   const src = await O.rfs(file, 1);
//   const info = parse_file_info(O.sanll(src).slice(0, 2).join('\n\n')).
//     find(a => a.name === name);
//   
//   const title = info.desc !== null ? info.desc : info.name;
//   set_title(title);
// 
//   let e;
// 
//   e = ce_entry(div, 'Name');
//   ce_code_inline(e, name);
// 
//   if(info.informal !== null){
//     e = ce_entry(div, 'Definition');
//     await ce_text(e, info.informal);
//   }
// 
//   if(info.parent !== null){
//     e = ce_entry(div, 'Parent');
//     ce_ref(e, info.parent);
//   }
// 
//   if(info.src !== null){
//     e = ce_entry(div, 'Source');
//     const label = `${pth}.${proj}:${info.src}`;
//     const pth1 = '#';
//     ce_link(e, label, pth1);
//   }
//   
//   e = ce_entry(div, 'Formal definiton');
//   ce_code_block(e, info.formal);
// 
//   e = ce_entry(div, 'Fully elaborated');
//   ce_code_block(e, info.fully_elabd);
// 
//   e = ce_entry(div, 'Depends on definitions');
//   ce_refs(e, info.defs);
// 
//   e = ce_entry(div, 'Aux definitions');
//   ce_refs(e, info.aux_defs);
// 
//   e = ce_entry(div, 'Aux theorems');
//   ce_refs(e, info.aux_thms);
// 
//   e = ce_entry(div, 'See also');
//   ce_refs(e, info.see_also);
// }

const load_entry = async (id, update_hist = 1) => {
  if(id === cur_id) return
  
  div.innerText = ""
  
  let mathlib_dir = path.join(cwd, "../../data/mathlib")
  let pth = path.join(mathlib_dir, `${id}.json`)
  let info = JSON.parse(O.rfs(pth, 1))
  
  const get = key => {
    if(!O.has(info, key)) return null
    return info[key]
  }
  
  let name = get("name")
  let title = get("title") || get("name")
  let kind = get("kind")
  
  set_title(title)
  
  let e
  
  e = ce_entry(div, "Name")
  ce_code_inline(e, name)
  
  e = ce_entry(div, "Kind")
  ce_code_inline(e, kind)
  
  e = ce_entry(div, "Source")
  ce_code_block(e, get("src"))
  
  let desc = get("desc")
  
  if(desc !== null){
    e = ce_entry(div, "Definition")
    await ce_text(e, desc)
  }
  
  e = ce_entry(div, "ID")
  ce_code_inline(e, id)
  
  e = ce_entry(div, "Index")
  ce_code_inline(e, get("index"))
  
  // e = ce_entry(div, "Type parameters")
  // ce_code_inline(e, get("ps_num"))
  
  let rels = [...get("rels1"), ...get("rels2")].map(a => a[1])
  
  e = ce_entry(div, "Closely related")
  ce_refs(e, rels)
  
  show_kind_info: {
    if(kind === "term"){
      e = ce_entry(div, "Unfold depth")
      ce_code_inline(e, get("unf_level"))
      
      e = ce_entry(div, "Appears in definitions")
      ce_refs(e, get("mp_e"))
      
      e = ce_entry(div, "Appears in statements")
      ce_refs(e, get("mp_stat"))
      
      e = ce_entry(div, "Appears in proofs")
      ce_refs(e, get("mp_p"))
      
      break show_kind_info
    }
    
    if(kind === "proof"){
      let axioms = get("axioms")
      
      e = ce_entry(div, "Axioms used")
      ce_refs(e, axioms.filter(a => a !== id))
      
      e = ce_entry(div, "Appears in proofs")
      ce_refs(e, get("mp_p"))
      
      break show_kind_info
    }
    
    assert.fail(kind)
  }
  
  /////
  
  if(update_hist && cur_id !== null){
    hist1.push(cur_id)
    hist2.length = 0
  }
  
  cur_id = id
  storage.set("id", String(id))
}

const id_to_name = id => {
  assert(mp_id_name.has(id))
  return mp_id_name.get(id)
}

const name_to_id = name => {
  assert(mp_name_id.has(name))
  return mp_name_id.get(name)
}

const op_to_id = sym => {
  assert(mp_op_id.has(sym))
  return mp_op_id.get(sym)
}

const ce_ref = (e, id) => {
  ce_link(e, id)
}

const ce_refs = (e, refs) => {
  if(refs.length === 0){
    ce_plain_text(e, "/")
    return
  }
  
  let n = refs_num_threshold
  let refs_num = refs.length
  
  for(let i = 0; i !== refs_num; i++){
    if(i !== 0) ce_plain_text(e, " | ")
    
    if(i === n){
      ce_plain_text(e, `... (${refs_num - n} more)`)
      break
    }
    
    let ref = refs[i]
    ce_ref(e, ref)
  }
}

const ce_text = async (e, text) => {
  for(const [type, info] of text)
    await ce_methods[type](e, info)
}

const ce_plain_text = (e, str) => {
  O.ceDiv(e, 'plain-text').innerText = str
}

const ce_emphasis = (e, str) => {
  e = O.ceDiv(e, 'emphasis')
  O.ce(e, 'i').innerText = str
}

const ce_math = async (e, str, type) => {
  str = `\`${str}\``
  O.ceDiv(e, `math math-${type}`).innerText = str
  await mathjax.typeset(e)
}

const ce_math_inline = (e, str) => {
  return ce_math(e, str, 'inline')
}

const ce_math_block = async (e, str) => {
  return ce_math(e, str, "block")
}

const ce_code_inline = (e, str) => {
  const e_code = O.ceDiv(e, "code code-inline")
  e_code.innerText = str
}

const ce_code_block = (e, sctr) => {
  let e_code = O.ceDiv(e, "code code-block")
  
  for(let [scope, str] of sctr){
    for(let s of O.match(str, /\n|\s+|\S+/g)){
      if(s === "\n"){
        O.ceBr(e_code)
        continue
      }

      const e = O.ceDiv(e_code, "scope")
      e.style.color = scope_cols[scope]
      e.innerText = s
      
      let name = s.trim()
      
      if(mp_name_id.has(name)){
        let id = name_to_id(name)
        e.classList.add("scope-ref")
        set_ref_evt(e, id)
        continue
      }
      
      if(mp_op_id.has(name)){
        if(scope === "op" || scope === "term"){
          let id = op_to_id(name)
          e.classList.add("scope-ref")
          set_ref_evt(e, id)
          continue
        }
        
        continue
      }
    }
  }
}

const ce_methods = {
  plain_text: ce_plain_text,
  emphasis: ce_emphasis,
  code: ce_code_inline,
  math: ce_math_inline,
};

const parse_ref = str => {
  const pth = str.split("/");
  const name = pth.pop();

  return {
    pth: pth.join("/"),
    name,
    label: name,
  }
};

const parse_refs = str => {
  str = str.slice(1, -1);
  if(str === "") return [];

  return str.split(', ').map(s => {
    return parse_ref(s);
  });
};

const ce_entry = (e, name = null, colon = 1) => {
  const e_entry = O.ceDiv(e, 'entry');

  if(name !== null){
    if(colon) name += ': ';
    O.ceDiv(e_entry, 'entry-name').innerText = name;
  }

  return e_entry
};

const set_title = title => {
  const e_title = O.ceDiv(div, 'title');
  e_title.innerText = title;
};

const parse_dir_info = src => {
  return O.sanl(src).map(line => {
    const [name, desc] = split_fst(line, '-');
    const is_file = !name.endsWith("/");

    return {name, desc, is_file};
  });
};

const parse_file_info = src => {
  const info_list = [];
  let info = null;

  const push = () => {
    if(info === null) return;

    info_list.push(info);
    info = null;
  };

  for(const line of O.sanl(src)){
    if(!line.startsWith(' ')){
      const [name, desc] = line.includes('-') ?
        split_fst(line, '-') : [line, null];

      push();
      info = O.obj();
      info.name = name;
      info.desc = desc;

      continue;
    }

    const [key, str] = split_fst(line, ':');
    info[key] = file_key_parsers[key](str);
  }

  push();

  return info_list;
};

const file_key_parsers = {
  scope(str){
    return str;
  },
  
  informal(str){
    if(str === "/") return null;
    return parse_text(str);
  },

  parent(str){
    if(str === "/") return null;
    return parse_ref(str);
  },

  src(str){
    if(str === "/") return null;
    return Number(str);
  },
  
  formal(str){
    if(str === "/") return null;
    return parse_sctr(str);
  },

  fully_elabd(str){
    return parse_sctr(str);
  },
  
  defs(str){
    return parse_refs(str);
  },

  aux_defs(str){
    return parse_refs(str);
  },

  aux_thms(str){
    return parse_refs(str);
  },

  see_also(str){
    return parse_refs(str);
  },
};

const parse_sctr = str => {
  const sctr = [];

  while(str !== ""){
    const [scope, str1] = split_fst(str, ' ');
    const match = str1.match(/("(?:\\.|[^"])*")\s*/);

    const s = JSON.parse(match[1]);
    str = str1.slice(match[0].length);

    sctr.push([scope, s]);
  }

  return sctr;
};

const parse_text = str => {
  const info = [];

  const push = (type, str) => {
    info.push([type, str]);
  };

  while(str.length !== 0){
    if(str[0] === '*'){
      str = str.slice(1);

      const [s1, s2] = split_fst(str, '*', 0);
      push('emphasis', s1);
      str = s2;

      continue;
    }

    if(str[0] === '`'){
      str = str.slice(1);

      const [s1, s2] = split_fst(str, '`', 0);
      push('math', s1);
      str = s2;

      continue;
    }

    const s1 = str.match(/^[^\*\`]+/)[0];
    push('plain_text', s1);

    str = str.slice(s1.length);
  }

  return info;
}

const set_ref_evt = (e, id) => {
  O.ael(e, "click", evt => {
    load_entry(id).catch(log)
  })
}

const ce_link = (parent, id) => {
  let link = O.ceDiv(parent, "link")
  link.innerText = id_to_name(id)

  set_ref_evt(link, id)

  return link
}

const mk_pth = pth => {
  return `${path.join(mathlib_dir, main_th_dir_name, pth)}.${info_ext}`
}

const split_fst = (str, delim, trim=1) => {
  const index = str.indexOf(delim)
  assert(index !== -1)
  
  return split_at(str, index, delim.length, trim)
}

const split_at = (str, i, n, trim=1) => {
  const parts = [str.slice(0, i), str.slice(i + n)]

  if(trim) return parts.map(a => a.trim())
  return parts
}

main()