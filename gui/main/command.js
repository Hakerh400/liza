'use strict';

const assert = require('../assert');
const O = require('../js-util');

const {min, max} = Math;

class Command{
  lines = [];
  
  constructor(index, y){
    this.index = index;
    this.y = y;
  }
  
  add_line(line){
    this.lines.push(line);
  }
  
  to_str(){
    return this.lines.join('\n').trimEnd();
  }
}

module.exports = Command;