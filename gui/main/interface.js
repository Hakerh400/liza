"use strict"

const assert = require('../assert');
const O = require('../js-util');
const abbrevs = require('./abbrevs');
const Command = require('./command');
const Cursor = require('./cursor');
const pos = require('./position');
const names_mp = require('./names');

const {min, max, floor, ceil} = Math;

const {
  id_to_name,
  name_to_id,
  str_ids_to_names,
} = names_mp

const tab_size = 2;
const line_nums_sp = 2;
const max_line_len = 120;
const pg_nav_fac = 0.6;

const snippet_draw_line_nums = 0;
const snippet_last_cmd = 1;
const snippet_offset = 5;
const snippet_line_len = 70;

const font_size = 15;
const parens = '()[]{}'.split('');
const dflt_col = '#d4d4d4';
const dflt_scope = 'dflt';

const show_cmd_info = 0;

const font_family = [
  '"DejaVu Sans Mono"',
  '"Consolas"',
  '"Courier New"',
  'monospace'
].join(', ');

const cols = {
  // bg: '#1e1e1e',
  bg: '#282c34',
  open_file_inp: '#070707',
  line_nums: '#858585',
  cursor: dflt_col,
  selected: '#264f78',
  vbar: dflt_col,
  scopes: {
    dflt: dflt_col,
    keyword: '#ff4488',
    lit_str: '#98c379',
    lit_num: '#98c379',
    term: '#61afef',
    proof: '#e5c07b',
    cmd_term: '#00ffff',
    cmd_proof: '#ffff00',
    op: '#c678dd',
    term_kw: '#c678dd',
    // term_kw: '#c586c0',
    tactic: '#00ff00',
    type: '#98c379',
    fn_type: dflt_col,
    comment: '#888888',
    attrib: '#98c379',
    sorry: '#ff0000',
    err: '#e06c75',
  },
};

const scope_cols = cols.scopes;
const snippet_offset2 = snippet_offset * 2;

const calc_ws_hs = (g=null) => {
  if(g === null)
    g = O.doc.createElement('canvas').getContext('2d');
  
  g.font = `${font_size}px ${font_family}`;

  const ws = g.measureText(' ').width;
  const hs = font_size + 2;
  
  return [ws, hs];
};

const indent = (n, s='') => {
  return `${' '.repeat(tab_size * n)}${s}`;
};

const get_open_paren = s => {
  const index = parens.indexOf(s);
  if(index === -1 || index % 2 !== 1) return null;

  return parens[index - 1];
};

const get_closed_paren = s => {
  const index = parens.indexOf(s);
  if(index === -1 || index % 2 !== 0) return null;

  return parens[index + 1];
};

const calc_lines_num_size = lines_num => {
  const n = String(lines_num).length;
  return max(n, 2) + 1;
};

const set_scope = (g, name) => {
  const col = scope_cols[name];
  if(!col) assert.fail(name);

  g.fillStyle = col;
};

class Interface{
  iw = null
  ih = null
  iwh = null
  ihh = null
  px = null
  py = null
  ws = null
  hs = null
  wsh = null
  hsh = null
  disposed = 0
  has_err = 0

  constructor(g=null){
    this.g = g;
    this.canvas = g !== null ? g.canvas : null;
  }

  resize(iw, ih){
    const {g, canvas} = this;

    this.iw = iw;
    this.ih = ih;
    this.iwh = iw / 2;
    this.ihh = ih / 2;

    canvas.width = iw;
    canvas.height = ih;

    g.textBaseline = 'middle';
    g.textAlign = 'left';
    
    [this.ws, this.hs] = calc_ws_hs(g);

    this.wsh = this.ws / 2;
    this.hsh = this.hs / 2;

    this.clear_pointer();
    this.resize1();
    this.render();
  }

  resize1(){}
  
  on_key_down(code, flags, char){
    const res = this.on_key_down1(code, flags, char);
    if(res) this.render();
    return res;
  }
  
  on_key_down1(code, flags, char){}

  on_blur(){}

  set_pointer(px, py){
    this.px = px;
    this.py = py;
  }

  clear_pointer(){
    this.set_pointer(null, null);
  }
  
  render(){
    if(this.disposed) return;
    if(this.g === null) return;
    
    this.render1();
  }
  
  render1(){}
  
  dispose(){
    this.disposed = 1;
  }
}

class Text_interface extends Interface{
  w = null;
  h = null;
  
  resize1(){
    const {iw, ih} = this;
    
    this.w = iw / this.ws - 1 | 0;
    this.h = ih / this.hs - 1 | 0;
  }
  
  draw_text(y, x, s){
    const {g, ws, hs} = this;
    g.fillText(s, ws * x, hs * (y + .5));
  }

  draw_cur(y, x){
    const {g, ws, hs} = this;
    
    g.fillStyle = cols.cursor;
    g.fillRect(ws * x + .5, hs * y + .5, 2, hs);
  }
  
  fill_rect(y1, x1, y2, x2){
    const {g, ws, hs} = this;
    
    g.fillRect(x1 * ws, y1 * hs, (x2 - x1 + 1) * ws, (y2 - y1 + 1) * hs);
  }
}

class Open_file extends Text_interface{
  pth = '';
  files = null;
  get_files_fn = null;
  open_file = null;
  close_file = null;

  update_files(){
    const {pth} = this;
    this.files = this.get_files_fn(pth);
    this.render();
  }

  on_key_down(code, flags, char){
    const {pth, files} = this;
    
    if(flags === 0){
      if(code === 'Enter'){
        const parts = pth.split('/');
        const name = O.last(parts);

        if(name === '') return 0;
        if(files.includes(`${name}/`)) return 0;
        
        this.open_file(pth);
        return 1;
      }
    }

    const res = this.on_key_down1(code, flags, char);
    if(res) this.update_files();
    return res;
  }

  on_key_down1(code, flags, char){
    const {pth, files} = this;
    const n = pth.length;

    if(char !== null){
      this.pth = pth + char;
      return 1;
    }

    if(flags === 0){
      if(code === 'Backspace'){
        if(n === 0) return 0;
        this.pth = pth.slice(0, n - 1);
        return 1;
      }

      if(code === 'Tab'){
        if(files.length === 0) return 0;
        this.pth = pth.replace(/(?:[^\/\\])*$/, files[0]);
        return 1;
      }

      return 0;
    }

    return 0;
  }

  render1(){
    const {
      g, iw, ih, w, h, ws, hs,
      pth, files,
    } = this;
    
    if(iw === null) return;
    
    g.fillStyle = cols.bg;
    g.fillRect(0, 0, iw, ih);

    g.fillStyle = cols.open_file_inp;
    this.fill_rect(1, 1, 1, w - 1);
    
    this.draw_cur(1, pth.length + 1);
    
    g.fillStyle = scope_cols[dflt_scope];
    this.draw_text(1, 1, pth);

    for(let i = 0; i !== files.length; i++)
      this.draw_text(i + 3, 1, files[i]);
  }
}

class File extends Text_interface{
  pth = null;
  full_pth = null;
  read_file = null;
  save_file = null;
  run_cmds_fn = null;
  save_snippet = null;
  lines_orig = [];
  lines = [""];
  cmds_queue = [];
  cmds = [];
  scopes = [];
  cursors = [Cursor.single(0, 0)];
  cursors_new = [];
  cur_index = null;
  modified = 0;
  y_scroll = 0;
  info = [];
  max_line_len = max_line_len;
  // see_line_y = null;
  draw_line_nums = 1
  elabd_fst_cmd = 0

  // see_line(y){
  //   const y0 = this.see_line_y;
  // 
  //   y = max(y, 0);
  // 
  //   // if(y0 === null || y < y0)
  // }

  clear_info(){
    this.info.length = 0;
  }

  set_info(msg){
    const {info} = this;
    
    info.length = 0;
    
    let line = []
    info.push(line);
    
    for(const [scope, str] of msg){
      const parts = O.sanl(str);
      
      for(let i = 0; i !== parts.length; i++){
        const part = parts[i];
        
        if(i !== 0){
          line = [];
          info.push(line);
        }
        
        if(part !== '')
          line.push([scope, part]);
      }
    }
  }
  
  set_info_str(str){
    this.set_info([[dflt_scope, str]]);
  }

  scroll_to(y){
    const {y_scroll} = this;

    y = max(y, 0);
    if(y_scroll === y) return;

    this.y_scroll = y;
    this.modified = 1;
  }

  scroll_by(dy){
    this.scroll_to(this.y_scroll + dy);
  }

  get_src_raw(){
    return this.lines.join("\n")
  }
  
  get_src(){
    const {lines_orig, lines} = this
    
    return [
      ...lines_orig,
      ...lines.slice(lines_orig.length),
    ].join("\n")
  }
  
  set_lines(lines1){
    const {lines} = this;
    
    lines.length = 0;
    
    for(const line of lines1)
      lines.push(line);
  }
  
  set_src(str){
    this.set_lines(O.sanl(str_ids_to_names(str)))
  }
  
  load(){
    const str = this.read_file(this.full_pth);

    this.set_src(str);
    this.render();
  }

  save(){
    if(this.has_err) return
    this.save_file(this.full_pth, this.get_src());
  }
  
  for_cursors(f){
    const {cursors, cursors_new} = this;
    
    assert(!this.modified);
    assert(this.cur_index === null);
    assert(cursors_new.length === 0);
    
    const c_len = cursors.length;
    
    for(let i = 0; i !== c_len; i++){
      this.cur_index = i;
      
      const cur = cursors[i];
      const res = f(cur);
      
      if(!res){
        cursors_new.push(cur);
        continue;
      }
      
      this.modified = 1;
      
      if(res instanceof Cursor){
        cursors_new.push(res);
        continue;
      }
      
      for(const cur_new of res)
        cursors_new.push(cur_new);
    }
    
    const cn_len = cursors_new.length;
    
    cursors_new.sort((cur1, cur2) => cur1.le(cur2) ? -1 : 1);
    cursors.length = 0;
    
    for(let i = 0; i !== cn_len;){
      const cursors_overlap = [cursors_new[i]];
      
      while(++i !== cn_len){
        const cur = cursors_new[i];
        if(!cursors_overlap.some(c => cur.overlaps(c))) break;
        cursors_overlap.push(cur);
      }
      
      cursors.push(Cursor.merge(cursors_overlap));
    }
    
    const {modified} = this;
    
    cursors_new.length = 0;
    this.cur_index = null;
    this.modified = 0;
    
    return modified;
  }
  
  shift_cursors(y1, x1, y2, x2){
    const {cursors, cursors_new} = this;
    const lists = [cursors, cursors_new];
    
    for(let list_i = 0; list_i !== 2; list_i++){
      const list = lists[list_i];
      const len = list.length;
      const offset = list_i === 0 ? this.cur_index + 1 : 0;
      
      for(let i = offset; i !== len; i++)
        list[i] = list[i].shift(y1, x1, y2, x2);
    }
  }
  
  get lines_num(){
    return this.lines.length;
  }
  
  get lines_num1(){
    return this.lines_num - 1;
  }
  
  get_line(y){
    if(y > this.lines_num1) return '';
    return this.lines[y];
  }
  
  get_line_len(y){
    return this.get_line(y).length;
  }

  get_line_indent(y){
    const line = this.get_line(y);
    const n = line.match(/^ */)[0].length;

    return n / tab_size | 0;
  }
  
  get_line_parts(y, x){
    const line = this.get_line(y);
    return [line.slice(0, x), line.slice(x)];
  }

  insert_line(y, line){
    const {lines, scopes} = this;
    const same = lines[y] === line;
    
    if(same){
      if(y < scopes.length){
        const scope = line.length !== 0 ? [[dflt_scope, 0, line.length]] : [];
        scopes.splice(y, 0, scope);
      }
    }
    
    lines.splice(y, 0, line);
    this.set_elab_line(same ? y + 1 : y);
  }

  insert_lines(y, lines){
    const n = lines.length;

    for(let i = 0; i !== n; i++)
      this.insert_line(y + i, lines[i]);
  }
  
  set_line(y, line){
    if(this.lines[y] === line) return;

    this.set_elab_line(y);
    this.lines[y] = line;
  }
  
  remove_lines(y, n){
    // const same = n === 1 && this.lines[y] === this.lines[y + 1];
    
    this.lines.splice(y, n);
    this.scopes.splice(y, n);
    this.set_elab_line(y);
  }
  
  remove_line(y){
    this.remove_lines(y, 1);
  }
  
  pos_dif(y1, x1, y2, x2){
    if(y1 === y2) return x2 - x1;
    
    let dif = this.get_line_len(y1) - x1 + x2;
    
    for(let y = y1 + 1; y < y2; y++)
      dif += this.get_line_len(y);
    
    return dif;
  }
  
  insert_text(y, x, str){
    this.modified = 1;
    
    const lines = O.sanl(str);
    const n = lines.length;
    const n1 = n - 1;
    const [s1, s2] = this.get_line_parts(y, x);
    
    if(n === 1){
      this.set_line(y, s1 + str + s2);
      this.shift_cursors(y, x, y, x + str.length);
      return;
    }

    this.set_line(y, s1 + lines[0]);
    this.insert_lines(y + 1, lines.slice(1, -1));
    this.insert_line(y + n1, lines[n1] + s2);
    this.shift_cursors(y, x, y + n1, lines[n1].length);
  }
  
  delete_text(y1, x1, y2, x2){
    if(pos.eq(y1, x1, y2, x2)) return;
    assert(pos.le(y1, x1, y2, x2));
    
    this.modified = 1;
    
    const s1 = this.get_line_parts(y1, x1)[0];
    const s2 = this.get_line_parts(y2, x2)[1];
    
    this.set_line(y1, s1 + s2);
    this.remove_lines(y1 + 1, y2 - y1);
    this.shift_cursors(y2, x2, y1, x1);
  }
  
  replace_text(y1, x1, y2, x2, str){
    this.delete_text(y1, x1, y2, x2);
    this.insert_text(y1, x1, str);
  }
  
  get_cmds_num(){
    return O.last(this.cmds, -1) + 1;
  }
  
  set_elab_line(y){
    const {lines_orig, lines, scopes, cmds} = this

    // if(y !== 0 && y < lines.length && lines[y - 1] === '')
    //   y++

    // log(y)
    // log(scopes)
    // log(cmds)
    
    if(y < cmds.length)
      cmds.length = cmds.indexOf(cmds[y])

    const y1 = min(y, cmds.length)
    
    if(y1 < scopes.length)
      scopes.length = y1
    
    if(y1 < lines_orig.length)
      lines_orig.length = y1

    return cmds.length
  }
  
  add_scopes(y, scopes_new_orig, has_cur=0, lines_new_num=null){
    const {lines_orig, lines, scopes, cursors} = this;

    assert(cursors.length === 1);
    if(has_cur) assert(lines_new_num !== null);
    
    // const y0 = scopes.length;
    // const dif0 = y - y0;
    //
    // for(let i = 0; i !== dif0; i++){
    //   const n = this.get_line_len(y0 + i);
    //   const scope = [];
    //
    //   if(n !== 0) scope.push([dflt_scope, 0, n]);
    //   scopes.push(scope);
    // }
    
    let s_orig = ""
    let scopes_new = scopes_new_orig.map(([scope, str]) => {
      s_orig += str
      return [scope, str_ids_to_names(str)]
    })
    let lines_new_orig = O.sanl(s_orig)

    let dif = 0;
    let line = '';
    let offset = 0;
    let sc = [];
    let line_index = 0;

    const push = () => {
      if(has_cur && line_index === lines_new_num - 1){
        const len = line.length;
        const line0 = lines[y];

        lines[y] = line + line0;
        sc.push([dflt_scope, len, len + line0.length]);

        this.for_cursors(cur => {
          return Cursor.single(cur.y, cur.x + line.length);
        });
      }else{
        lines.splice(y, 0, line);
      }
      
      lines_orig.push(lines_new_orig.shift() ?? "")
      scopes.push(sc)

      y++;
      dif++;
      line = '';
      offset = 0;
      sc = [];
      line_index++;
    };

    for(const [scope, str] of scopes_new){
      const parts = O.sanl(str);
      const s = parts.pop();

      for(const s of parts){
        line += s;
        sc.push([scope, offset, offset + s.length]);
        push();
      }

      line += s;
      sc.push([scope, offset, offset + s.length]);
      offset += s.length;
    }

    if(sc.length !== 0) push();
  }
  
  add_scope(y, scope, has_cur, lines_new_num){
    this.add_scopes(y, [scope], has_cur, lines_new_num);
  }
  
  add_scopes_to_cmd(scopes_new){
    const {cursors, scopes, cmds, cmds_queue, lines} = this;
    
    assert(cursors.length === 1);
    this.clear_info();

    if(scopes.length !== 0){
      const sc = O.last(scopes_new);
      sc[1] = sc[1].trimEnd();
    }
    
    const cur = cursors[0];
    const {x: cx, y: cy} = cur;
    const cmd = cmds_queue.shift();
    
    if(cmd.index === -1){
      this.elabd_fst_cmd = 1
      return
    }
    
    const y0 = cmd.y
    const cmd_index = cmd.index;
    const str = scopes_new.map(a => a[1]).join('');
    const lines_old = cmd.lines;
    const lines_new = O.sanl(str);
    const lines_new_num = lines_new.length;
    const lines_old_num = lines_old.length;
    const y1 = y0 + lines_new_num;

    let cy_dif = lines_new_num - lines_old_num;
    let has_cur = 0

    assert(scopes.length >= y0);
    if(scopes.length > y0) scopes.length = y0;

    assert(cmds.length >= y0);
    if(cmds.length > y0) cmds.length = y0;

    for(let i = 0; i !== lines_old_num; i++){
      if(y0 + i === cy){
        assert(i === lines_old_num - 1);
        
        lines[y0] = lines[y0].slice(cx);
        // cy_dif++;
        has_cur = 1;

        break;
      }
      
      lines.splice(y0, 1);
    }

    this.for_cursors(() => {
      const cy_new = cy + cy_dif;
      const cx_new = has_cur ? 0 : cx;

      return Cursor.single(cy_new, cx_new);
    });

    this.add_scopes(y0, scopes_new, has_cur, lines_new_num);

    const cur1 = cursors[0];
    
    let done = !has_cur;

    calc_done: {
      const {y, x} = cur1;

      if(!done){
        if(this.get_line_len(y) === x && this.get_line(y + 1) === '')
          done = 1;

        break calc_done;
      }
      
      const cnd = y === y0 + lines_new_num &&
        this.get_line_parts(y, x)[1].length !== 0;
      
      if(cnd) done = 0;
    }

    if(done){
      for(let i = 0; i !== lines_new_num; i++)
        cmds.push(cmd_index);
    }else{
      const is_thm = /\b(?:thm|example)\b/.test(str);
    
      if(is_thm)
        this.set_info([[dflt_scope, 'Goals accomplished! \u{1f389}']]);
    }

    for(const cmd of cmds_queue)
      cmd.y += cy_dif;

    /*const {cmds, cmds_queue} = this;
    const cmd = cmds_queue.shift();
    const cmd_index = cmd.index;
    const {lines, y} = cmd;
    const lines_num = lines.length;
    const str = lines.join('\n').trimRight();

    const y_end = y + lines_num - 1;
    const next_line_emp = this.get_line_len(y_end + 1) === 0;
    const last_line_len = this.get_line_len(y_end);

    let cmd_done = O.last(lines) === '' && last_line_len === 0;
    let extra_line = 0;

    if(!cmd_done && next_line_emp){
      cmd_done = 1;
      extra_line = 1;
    }
    
    if(str === 'snippet')
      this.render_snippet();
    
    this.clear_info();

    if(cmd_done){
      let n = lines_num;
      if(extra_line) n++;

      for(let i = 0; i !== n; i++)
        cmds.push(cmd_index);
   
      this.add_scopes(y, scopes);
    }else{
      const is_thm = /\b(?:thm|example)\b/.test(str);
      
      const hide_goals_msg = this.cursors[0].x === last_line_len && (
        y_end === this.lines_num1 ||
        next_line_emp
      );
      
      if(is_thm && !hide_goals_msg)
        this.set_info([[dflt_scope, 'Goals accomplished! \u{1f389}']]);
    }*/
  }

  on_blur(){
    this.save();
  }

  run_cmds(){
    const {cmds_queue} = this

    if(cmds_queue.length === 0)
      return
    
    if(!this.elabd_fst_cmd && cmds_queue[0].index === 0){
      cmds_queue.unshift({
        lines: ["--"],
        index: -1,
        y: -1,
        to_str(){
          return this.lines.join("\n")
        },
      })
    }
    
    this.run_cmds_fn()
  }
  
  elab(){
    const {cursors, cmds_queue} = this;
    const {y2, x2} = O.last(cursors);
    const y1 = this.set_elab_line(y2);
    
    this.clear_info();
    
    let cmd_index = this.get_cmds_num();
    let cmd = new Command(cmd_index, y1);

    const push_cmd = () => {
      if(cmd.to_str() === '') return 0;

      cmds_queue.push(cmd);
      return 1;
    };
    
    for(let y = y1; y <= y2; y++){
      const last = y === y2;
      
      let line = this.get_line(y);
      if(last) line = line.slice(0, x2);

      if(line === '')
        if(push_cmd(cmd))
          cmd = new Command(++cmd_index, y);
      
      cmd.add_line(line);
      if(last) push_cmd();
    }

    this.run_cmds();
  }
  
  elab_req(){
    this.save();
    this.elab();
  }

  on_key_down1(code, flags, char){
    const {
      w, h,
      cmds_queue, cmds, 
      copes, cursors, y_scroll,
    } = this;
    
    const alt = flags & 1;
    const shift = flags & 2;
    const ctrl = flags & 4;
    
    if(cmds_queue.length !== 0)
      return 0;
    
    if(flags === 4){
      if(code === 'Enter'){
        this.elab_req();
        return 1;
      }

      if(code === 'KeyW'){
        this.save();
        this.close_file(this.pth);
        return 1;
      }

      if(code === 'KeyR'){
        this.save();
        return 0;
      }
    }

    return this.for_cursors(cur => {
      const {y1, x1, y2, x2, x3, y, x, dir, single} = cur;

      const is_between_parens = (y0=y, x0=x) => {
        const [s1, s2] = this.get_line_parts(y0, x0);
        const paren = get_closed_paren(O.last(s1));

        if(paren === null) return 0;
        if(!s2.startsWith(paren)) return 0;

        return 1;
      }

      const set_text = (str, dx1=0, dx2=0, dy2=0) => {
        const lines = O.sanl(str);
        const n = lines.length;
        const x1n = x1 - dx1;

        this.replace_text(y1, x1n, y2, x2, str);

        if(n === 1)
          return Cursor.single(y1 - dy2, x1n + str.length - dx2);
        
        return Cursor.single(y1 + n - 1 - dy2, O.last(lines).length - dx2);
      };

      const set_text_abbrev = (str, always=1, s0='') => {
        const dflt0 = () => set_text(str + s0);
        
        const dflt = () => {
          if(!/^\d$/.test(s0)) return dflt0();
          if(!/(?:(?:^|[^a-zA-Z])[a-zA-Z]|[₀-₉])$/.test(s)) return dflt0();
          
          const s1 = O.sfcc(O.cc(s0) - O.cc('0') + O.cc('₀'));
          return set_text(str + s1);
        };

        if(!single) return dflt0();

        const s = this.get_line_parts(y, x)[0];
        const index = s.lastIndexOf('\\');
        if(index === -1) return dflt();

        const name0 = s.slice(index + 1);
        const name = name0 + s0;
        if(!O.has(abbrevs, name)) return dflt();

        const sym = abbrevs[name];
        const str1 = always ? sym + str : sym;

        return set_text(str1, name0.length + 1);
      };

      const single_or_select = (yn, xn, use_x3=0) => {
        const x3n = use_x3 ? x3 : null;

        if(!shift)
          return Cursor.single(yn, xn, x3n);
        
        return Cursor.select(...cur.from, yn, xn, x3n);
      };
      
      if(char !== null){
        if(single){
          if(char === ' ') return set_text_abbrev(char, 1);
          if(/\d/.test(char)) return set_text_abbrev('', 1, char);

          check_paren1: {
            const paren = get_closed_paren(char);
            if(paren === null) break check_paren1;
            
            const s = this.get_line_parts(y, x)[1];
            if(/^[^\s\)\]\}]/.test(s)) break check_paren1;
            
            return set_text(char + paren, 0, 1);
          }
          
          check_paren2: {
            const paren = get_open_paren(char);
            if(paren === null) break check_paren2;
            
            const s = this.get_line_parts(y, x)[1];
            if(!s.startsWith(char)) break check_paren2
            
            return Cursor.single(y, x + 1);
          }
        }

        return set_text(char);
      }
      
      if(code === 'ArrowLeft'){
        if(!(single || shift))
          return Cursor.single(y1, x1);
        
        if(x === 0){
          if(y === 0) return;
          
          const xn = this.get_line_len(y - 1);
          
          return single_or_select(y - 1, xn);
        }
        
        if(!ctrl) return single_or_select(y, x - 1);

        const s = this.get_line_parts(y, x)[0];
        const n = O.rev(s).match(/^(?:\w+|\W+)/)[0].length;
        
        return single_or_select(y, x - n);
      }
      
      if(code === 'ArrowRight'){
        if(!(single || shift))
          return Cursor.single(y2, x2);
        
        if(x === this.get_line_len(y)){
          if(y === this.lines_num1) return;
          return single_or_select(y + 1, 0);
        }
        
        if(!ctrl) return single_or_select(y, x + 1);

        const s = this.get_line_parts(y, x)[1];
        const n = s.match(/^(?:\w+|\W+)/)[0].length;
        
        return single_or_select(y, x + n);
      }

      if(flags === 0 || flags === 2){
        if(code === 'ArrowUp'){
          if(y === 0){
            if(shift || single) return;
            return Cursor.single(y, x, x3);
          }
          
          const yn = y - 1;
          const xn = min(x3, this.get_line_len(yn));

          return single_or_select(yn, xn, 1);
        }
        
        if(code === 'ArrowDown'){
          if(y === this.lines_num1){
            if(shift || single) return;
            return Cursor.single(y, x, x3);
          }
          
          const yn = y + 1;
          const xn = min(x3, this.get_line_len(yn));

          return single_or_select(yn, xn, 1);
        }
        
        if(code === 'Home'){
          const line = this.get_line(y);
          const indent = line.match(/^ */)[0].length;
          
          if(x !== indent)
            return single_or_select(y, indent);
          
          return single_or_select(y, 0);
        }
        
        if(code === 'End'){
          const line = this.get_line(y);
          return single_or_select(y, line.length);
        }
      }

      if(flags === 0 || flags === 4){
        if(code === 'Delete'){
          if(!single){
            this.delete_text(y1, x1, y2, x2);
            return Cursor.single(y1, x1);
          }
          
          if(x === this.get_line_len(y)){
            if(y === this.lines_num1) return;
            
            const yn = y + 1;
            const xn = 0;
            
            this.delete_text(y, x, yn, xn);
            return Cursor.single(y, x);
          }
          
          this.delete_text(y, x, y, x + 1);
          return Cursor.single(y, x);
        }
      }
      
      if(flags === 0){
        if(code === 'Backspace'){
          if(!single){
            this.delete_text(y1, x1, y2, x2);
            return Cursor.single(y1, x1);
          }
          
          if(x === 0){
            if(y === 0) return;
            
            const yn = y - 1;
            const xn = this.get_line_len(yn);
            
            this.delete_text(yn, xn, y, x);
            return Cursor.single(yn, xn);
          }

          const n = this.get_line_indent(y) * tab_size;
          const dx = x === n ? tab_size : 1;
          const xn = x - dx;
          const x_end = x + (is_between_parens() ? 1 : 0);

          this.delete_text(y, xn, y, x_end);
          return Cursor.single(y, xn);
        }

        if(code === 'Enter'){
          const n = this.get_line_indent(y1);
          const s = `\n${indent(n)}`;

          if(is_between_parens()){
            const s1 = `\n${indent(n + 1)}`;
            return set_text(s1 + s, 0, -tab_size, 1);
          }

          return set_text(s);
        }

        if(code === 'Tab'){
          return set_text_abbrev(indent(1), 0);
        }

        if(code === 'PageUp'){
          const dy = h * pg_nav_fac | 0;
          const y_new = max(y - dy, 0);
          const x_new = min(x, this.get_line_len(y_new));

          if(y_new < y_scroll)
            this.scroll_to(y_new - y + y_scroll);

          return Cursor.single(y_new, x_new, single ? x3 : null);
        }

        if(code === 'PageDown'){
          const dy = h * pg_nav_fac | 0;
          const y_new = min(y + dy, this.lines_num1);
          const x_new = min(x, this.get_line_len(y_new));

          if(y_new >= y_scroll + h)
            this.scroll_to(y_new - y + y_scroll);

          return Cursor.single(y_new, x_new, single ? x3 : null);
        }

        return;
      }

      if(flags === 4){
        if(code === 'KeyA'){
          const yn = this.lines_num1;
          const xn = this.get_line_len(yn);
          return Cursor.select(0, 0, yn, xn);
        }

        if(code === 'ArrowUp'){
          this.scroll_by(-1);
          return;
        }

        if(code === 'ArrowDown'){
          this.scroll_by(1);
          return;
        }

        return;
      }
    });
  }
  
  render_snippet(){
    const {
      g, w, h, ws, hs, cmds,
    } = this;
    
    const n = max(cmds.length - 1, 0);
    
    let lines = this.lines.slice(0, n);
    let scopes = this.scopes;
    
    if(lines.length === 0)
      lines.push('');
    
    extract_last_cmd: if(snippet_last_cmd){
      let i = lines.lastIndexOf('');
      if(i === -1) break extract_last_cmd;
      
      i++;
      
      lines = lines.slice(i);
      scopes = scopes.slice(i);
    }
    
    const max_line_len = snippet_line_len !== null ?
      snippet_line_len : lines.reduce((a, b) => max(a, b.length), 0);
    const line_nums_size = calc_lines_num_size(n);
    const line_nums_sp_total = snippet_draw_line_nums ?
      line_nums_size + line_nums_sp : 0;
    const w1 = line_nums_sp_total + max_line_len;
    const h1 = lines.length;
    
    const iw1 = ceil(w1 * ws);
    const ih1 = ceil(h1 * hs);
    
    const canvas1 = O.doc.createElement('canvas');
    const g1 = canvas1.getContext('2d');
    const obj = new File(g1);
    
    obj.cursors.length = 0;
    obj.max_line_len = max_line_len + 1;
    obj.set_lines(lines);
    obj.scopes = scopes;
    obj.draw_line_nums = snippet_draw_line_nums;
    
    obj.resize(iw1, ih1);
    
    const canvas2 = O.doc.createElement('canvas');
    const g2 = canvas2.getContext('2d');
    const iw2 = iw1 + snippet_offset2;
    const ih2 = ih1 + snippet_offset2;
    
    canvas2.width = iw2;
    canvas2.height = ih2;
    
    g2.fillStyle = cols.bg;
    g2.fillRect(0, 0, iw2, ih2);
    
    g2.drawImage(canvas1, snippet_offset, snippet_offset);
    this.save_snippet(g2);
  }

  render1(){
    this.y_scroll = min(this.y_scroll, this.lines_num1);

    const {
      g, iw, ih, w, h, ws, hs,
      max_line_len, lines, scopes, cmds,
      cursors, y_scroll, info, draw_line_nums,
    } = this;
    
    const h1 = h + 1;
    const lines_num = lines.length;
    const line_nums_size = calc_lines_num_size(lines_num);
    const line_nums_sp_total = line_nums_size + line_nums_sp;
    const ofx = draw_line_nums ? line_nums_sp_total : 0;
    const y_start = y_scroll;
    const y_end = y_start + h1;

    const has_y = y => {
      return y >= y_start && y <= y_end;
    };

    g.fillStyle = cols.bg;
    g.fillRect(0, 0, iw, ih);
    
    for(const cur of cursors){
      const {y1, x1, y2, x2, x3, y, x, dir} = cur;
      
      if(!cur.single){
        g.fillStyle = cols.selected;
        
        let x = x1;
        
        for(let y0 = y1; y0 <= y2; y0++){
          if(!has_y(y0)) continue;

          const y = y0 - y_start;
          const x_new = (y0 !== y2 ? w : x2) - 1;
          
          draw_selection: {
            if(x >= max_line_len) break draw_selection;
            const xm = min(x_new, max_line_len - 1);
            this.fill_rect(y, ofx + x, y, ofx + xm);
          }
          
          x = 0;
        }
      }
      
      if(has_y(y) && x < max_line_len)
        this.draw_cur(y - y_start, ofx + x);
    }

    for(let i = y_start; i <= y_end; i++){
      if(i >= lines_num) break;
      
      const y = i - y_start
      
      if(draw_line_nums){
        const s = String(i + 1).padStart(line_nums_size);
        let col = cols.line_nums;

        if(show_cmd_info){
          const cmd = i < cmds.length ? cmds[i] : null;

          if(cmd !== null)
            col = (cmd & 1) === 0 ? '#8f8' : '#2a2';
        }

        g.fillStyle = col;
        this.draw_text(y, 0, s);
      }
      
      const str = lines[i].slice(0, max_line_len);
      
      if(i >= scopes.length){
        g.fillStyle = scope_cols[dflt_scope];
        this.draw_text(y, ofx, str);
        continue;
      }
      
      for(const [name, x1, x2] of scopes[i]){
        if(x1 >= x2){
          // log(name, x1, x2);
          // assert.fail();
          continue;
        }
        
        set_scope(g, name);
        this.draw_text(y, ofx + x1, str.slice(x1, x2));
      }
    }

    draw_vbar: {
      const x = (ofx + max_line_len) * ws + .5;
      
      g.strokeStyle = cols.vbar;
      g.beginPath();
      g.moveTo(x, 0);
      g.lineTo(x, ih);
      g.stroke();
    }
    
    draw_info_view: {
      const info_len = info.length;
      const x0 = ofx + max_line_len + 1
      const d = w - x0;
      
      let line_i = 0;
      let str_i = 0;
      let char_i = 0;
      
      let y = 1;
      let x = x0;
      
      while(line_i !== info_len){
        if(x === w){
          y++;
          x = x0;
        }
        
        const line = info[line_i];
        
        if(str_i === line.length){
          line_i++;
          str_i = 0;
          char_i = 0;
          y++;
          x = x0;
          continue;
        }
        
        const [scope, str] = line[str_i];
        const str_len = str.length;
        
        if(char_i === str_len){
          str_i++;
          char_i = 0;
          continue;
        }
        
        const n = min(w - x, str_len - char_i);
        const s = str.slice(char_i, char_i + n);
        
        set_scope(g, scope);
        this.draw_text(y, x, s);
        
        char_i += n;
        x += n;
      }
    }
  }
}

module.exports = Object.assign(Interface, {
  Cursor,
  dflt_scope,
  cols,
  scope_cols,
  font_size,
  font_family,
  calc_ws_hs,
  
  Text_interface,
  Open_file,
  File,
});