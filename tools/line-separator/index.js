"use strict"

const fs = require("fs")
const path = require("path")
const assert = require("../../gui/assert")
const cp = require("child_process")
const O = require("../../gui/js-util")
const config = require("../../gui/config")

const proj = config.project_name

const cwd = __dirname
const proj_dir = path.join(cwd, "../..")
const db_dir = path.join(proj_dir, "db")
const th_dir = path.join(proj_dir, "theory")

const pure_ext = proj

const dirs = [
  db_dir,
  th_dir,
]

const exts = [
  "txt",
  pure_ext,
]

const main = () => {
  for(let dir of dirs){
    for(let base of fs.readdirSync(dir)){
      let {ext} = path.parse(base)
      
      if(!exts.some(ext1 => ext === `.${ext1}`))
        continue
      
      let pth = path.join(dir, base)
      let s = O.rfs(pth, 1)
      
      s = O.lf(s)
      
      O.wfs(pth, s)
    }
  }
}

main()