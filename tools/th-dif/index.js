'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('../../gui/assert');
const cp = require('child_process');
const O = require('../../gui/js-util');
const config = require('../../gui/config');

const proj = config.project_name;

const cwd = __dirname;
const proj_dir = path.join(cwd, '../..');
const th_dir1 = path.join(proj_dir, '../pure-old/pure/theory');
const th_dir2 = path.join(proj_dir, 'theory');

const pure_ext = proj;

const main = () => {
  const names1 = get_names(th_dir1);
  const names2 = get_names(th_dir2);

  const log_name = (tp, name) => {
    if(name.split('\x5F').includes('tc')) return;
    log(`${tp} ${name}`);
  };

  for(let i = 0; i !== 2; i++){
    if(i !== 0) O.logb();

    const xs1 = names1[i];
    const xs2 = names2[i];

    for(const name of xs1)
      if(!xs2.has(name))
        log_name('-', name);
    
    for(const name of xs2)
      if(!xs1.has(name))
        log_name('+', name);
  }
};

const get_names = th_dir => {
  const defs = new Set();
  const thms = new Set();
  const queue = [th_dir];

  while(queue.length !== 0){
    const dir = queue.shift();
    const names = fs.readdirSync(dir);

    for(const name of names){
      const pth = path.join(dir, name);
      
      if(fs.statSync(pth).isDirectory()){
        queue.push(pth);
        continue;
      }

      if(!name.endsWith(`.${pure_ext}`))
        continue;

      let str = O.rfs(pth, 1);

      for(const line of O.sanl(str)){
        const words = line.split(/(?:\s+|\.)/);
        const [cmd, name] = words;

        const add_name = xs => {
          if(!name){
            log(line);
            assert.fail();
          }

          xs.add(name);
        };

        if(cmd === 'def'){
          add_name(defs);
          continue;
        }

        if(cmd === 'thm'){
          add_name(thms);
          continue;
        }
      }
    }
  }

  return [defs, thms]
};

main();