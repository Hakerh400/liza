'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('../gui/assert');
const cp = require('child_process');
const O = require('../gui/js-util');

const tool = null;

if(tool === null) return;

const cwd = __dirname;
const tool_pth = path.join(cwd, tool);

const main = () => {
  require(tool_pth);
};

main();