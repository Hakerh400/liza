'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('../../gui/assert');
const cp = require('child_process');
const O = require('../../gui/js-util');
const config = require('../../gui/config');
const Server = require('../../gui/server');
const Interface = require('../../gui/main/interface');
const Cursor = require('../../gui/main/cursor');
const format = require('../../../node-projects/format');

const {floor, ceil} = Math;
const {cols, scope_cols, font_size, font_family} = Interface;

const monochrome = 1;
const frame = monochrome;
const baseline_workaround = 1;

const black = '#000';
const white = '#fff';

const snp_offset = 5;
const snp_line_len = 60;
const bg_col = monochrome ? white : cols.bg;

const proj = config.project_name;
const pure_ext = proj;

const cwd = __dirname;
const proj_dir = path.join(cwd, '../..');
const th_dir = path.join(proj_dir, 'theory');
const snps_dir = format.pth('-dw/a/snippets');
const info_file = path.join(snps_dir, 'info.txt');
const scopes_file = path.join(snps_dir, 'scopes.json');
const svg_dir = path.join(snps_dir, 'svg');
const svg_index_file = path.join(svg_dir, 'index.htm');

const test_file_name = 'test';
const dummy_file_name = '\x20';
const name_reg = new RegExp(`^(\\d+)\\.${pure_ext}$`);
const svg_ext = 'svg';
const svg_name_pad = 3;

const snp_offset2 = snp_offset * 2;

const main = async () => {
  const scopes = await extract_scopes();
  const snps_num = scopes.length;
  
  if(!fs.existsSync(svg_dir))
    fs.mkdirSync(svg_dir);
  
  O.wfs(svg_index_file, O.ftext(`
    <style>
    body{margin:8px;}
    object{
      display:block;
      margin-bottom:8px;
    }
    </style>
    ${O.ca(snps_num, i => {
      const name = calc_svg_name(i);
      return `<object data="${name}"></object>`;
    }).join('')}
  `));
  
  const [ws, hs] = Interface.calc_ws_hs();
  const width = ceil(snp_line_len * ws + snp_offset2);
  
  const svg_header = O.ftext(`
    <?xml version="1.0" encoding="UTF-8"?>
    <svg xmlns="http://www.w3.org/2000/svg"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      width="#w2" height="#h2">
    <style>
    svg{
      font-size:${font_size};
      font-family:${font_family};
    }
    ${O.keys(scope_cols).map(scope => {
      return `.scope-${scope}{fill:${
        monochrome ? black : scope_cols[scope]
      };}`;
    }).join('')}
    </style>
    <rect x="0.5" y="0.5" width="#w" height="#h" style="fill:${bg_col}"${
      monochrome ? ` stroke="${black}"` : ''
    }/>
  `);
  
  for(let snp_i = 0; snp_i !== snps_num; snp_i++){
    const lines = scopes[snp_i];
    const lines_num = lines.length;
    const height = ceil(lines_num * hs + snp_offset2);
    
    const name = calc_svg_name(snp_i);
    const pth = path.join(svg_dir, name);
    
    let src = svg_header.
      replace(/\#w2/g, width + 2).
      replace(/\#h2/g, height + 2).
      replace(/\#w/g, width).
      replace(/\#h/g, height);
    
    lines.forEach((line, line_i) => {
      const y_extra = baseline_workaround ? 12 : 0;
      let y = line_i * hs + snp_offset + y_extra;
      let x = snp_offset;
      
      for(const [scope, str] of line){
        for(let i = 0; i !== str.length; i++){
          src += O.ftext(`
            <text ${baseline_workaround ? '' :
            `dominant-baseline="hanging"`}
            class="scope-${scope}" x="${x}" y="${y}">
            ${html_escape(str[i])}
            </text>
            `);
          
          x += ws;
        }
      }
    })
    
    src += '</svg>';
    
    O.wfs(pth, src);
  }
};

const save_scopes = scopes => {
  O.wfs(scopes_file, JSON.stringify(scopes));
};

const extract_scopes_aux = async () => {
  if(fs.existsSync(scopes_file))
    return JSON.parse(O.rfs(scopes_file));
  
  const test_file = mk_th_pth(test_file_name);
  
  const [dep_names_list, dep_cmds_obj] = get_dep_cmds([
    'core/defs',
    'prop',
  ]);
  
  const server = new Server();
  const sem = new O.Semaphore(0);
  
  let cmd_scopes = null;
  
  server.on('msg', ([type, info]) => {
    if(type === 'scopes'){
      cmd_scopes = info;
      sem.signal();
      return;
    }
    
    if(0){
      log(type);
      log(info);
    }
  });
  
  server.on('error', log);
  
  const files = fs.readdirSync(snps_dir);
  const raw_snp_files = files.filter(a => a.endsWith(`.${pure_ext}`));
  const raw_snps_num = raw_snp_files.length;
  
  const snps_info = O.sanl(O.rfs(info_file, 1));
  const spec_snps_num = snps_info.length;
  const snps_num = raw_snps_num + spec_snps_num;
  
  const scopes = [];
  
  const names_to_src = deps => {
    return deps.flatMap(name => {
      assert(O.has(dep_cmds_obj, name));
      return dep_cmds_obj[name];
    }).join('\n\n');
  };
  
  const get_snp_info = snp_i => {
    if(snp_i < raw_snps_num){
      const name_i = raw_snp_files.findIndex(name => {
        const match = name.match(name_reg);
        if(match === null) return 0;
        
        return Number(match[1]) - 1 === snp_i;
      });
      
      assert(name_i !== -1);
      const name = raw_snp_files[name_i];
      
      const pth = path.join(snps_dir, name);
      
      let src = O.rfs(pth, 1);
      let test_src = null;
      
      const lines = O.sanl(src);
      let has_deps = 0;
      
      if(lines[0].startsWith('#')){
        const deps = lines.shift().split(' ').slice(1);
        
        while(lines[0].length === 0)
          lines.shift();
        
        test_src = names_to_src(deps);
        src = add_import_test(src, lines);
        has_deps = 1;
      }
      
      return {id: name, test_src, src, lines, has_deps};
    }
    
    const snp_j = snp_i - raw_snps_num;
    const id = `[${snp_i + 1}]`;
    
    const names = snps_info[snp_j].split(' ');
    const last_name = O.last(names);
    const last_name_i = dep_names_list.indexOf(last_name);
    assert(last_name_i !== -1);
    
    const deps = dep_names_list.slice(0, last_name_i).
      filter(name => !names.includes(name));
    const test_src = names_to_src(deps);
    
    let src = names_to_src(names);
    const lines = O.sanl(src);
    src = add_import_test(src, lines);
    
    return {id, test_src, src, lines, has_deps: 1};
  };
  
  for(let snp_i = 0; snp_i !== snps_num; snp_i++){
    const {id, test_src, src, lines, has_deps} = get_snp_info(snp_i);
    
    log(id);
    
    if(test_src !== null)
      O.wfs(test_file, test_src);
    
    const lines_num = lines.length;
    const last_line = O.last(lines);
    const last_line_len = last_line.length;
    
    server.send(['open_file', dummy_file_name]);
    
    const intface = new Interface.File();
    const {cmds_queue} = intface;
    
    intface.run_cmds = O.nop;
    
    intface.set_src(src);
    intface.cursors[0] = Cursor.single(lines_num - 1, last_line_len);
    intface.elab();
    
    while(cmds_queue.length !== 0){
      const cmd = cmds_queue[0];
      
      server.send(['elab_cmd', String(cmd.index), cmd.to_str()]);
      await sem.wait();
      intface.add_scopes_to_cmd(cmd_scopes);
    }
    
    for(let y = intface.scopes.length; y !== lines_num; y++)
      intface.add_scopes(y, []);
    
    server.send(['close_file']);
    
    const file_scopes = intface.scopes;
    
    if(has_deps){
      for(let i = 0; i !== 2; i++){
        lines.shift();
        file_scopes.shift();
      }
    }
    
    file_scopes.forEach((line_scopes, i) => {
      const line = lines[i];
      
      for(const scope of line_scopes)
        scope.push(line.slice(scope[1], scope[2]));
    });
    
    scopes.push(file_scopes);
    
    continue;
  }
  
  server.exit();
  save_scopes(scopes);
  
  return scopes;
};

const extract_scopes = async () => {
  const scopes = await extract_scopes_aux();
  
  if(scopes[0][0][0].length === 4){
    for(const snp_scopes of scopes){
      for(const line of snp_scopes){
        for(const scope of line){
          scope[1] = scope[3];
          scope.length = 2;
        }
      }
    }
  }
  
  save_scopes(scopes);
  
  return scopes;
};

const get_dep_cmds = pths => {
  const excluded_cmds = O.arr2obj([
    'import',
    'init_simp',
    'init_comm',
  ]);
  
  const names_list = [];
  const cmds_obj = O.obj();
  
  for(const pth_rel of pths){
    const pth = mk_th_pth(pth_rel);
    const src = O.rfs(pth, 1);
    const cmds = O.sanll(src);
    
    for(const cmd of cmds){
      const lines = O.sanl(cmd);
      const fst_line = lines[0];
      const cmd_name = fst_line.split(' ')[0];
      
      if(O.has(excluded_cmds, cmd_name)) continue;
      if(fst_line.startsWith('@')) lines.shift();
      
      const parts = lines[0].split(/[ \.]/);
      if(parts.length < 2) continue;
      
      const name = parts[1];
      
      if(!O.has(cmds_obj, name)){
        names_list.push(name);
        cmds_obj[name] = [];
      }
      
      cmds_obj[name].push(cmd);
    }
  }
  
  return [names_list, cmds_obj];
};

const mk_th_pth = pth_rel => {
  return path.join(th_dir, `${pth_rel}.${pure_ext}`);
};

const add_import_test = (src, lines) => {
  lines.unshift(`import ${test_file_name}`, '');
  return lines.join('\n');
};

const calc_svg_name = snp_i => {
  const name = String(snp_i + 1).padStart(svg_name_pad, '0');
  return `${name}.${svg_ext}`;
};

const html_escape = str => {
  return str.replace(/./gs, c => {
    return `&#${O.cc(c)};`;
  });
};

main();