'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('../../gui/assert');
const cp = require('child_process');
const O = require('../../gui/js-util');
const config = require('../../gui/config');

const proj = config.project_name;

const cwd = __dirname;
const proj_dir = path.join(cwd, '../..');
const th_dir = path.join(proj_dir, 'theory');

const pure_ext = proj;

const main = () => {
  const queue = [th_dir];

  while(queue.length !== 0){
    const dir = queue.shift();
    const names = fs.readdirSync(dir);

    for(const name of names){
      const pth = path.join(dir, name);
      
      if(fs.statSync(pth).isDirectory()){
        queue.push(pth);
        continue;
      }

      if(!name.endsWith(`.${pure_ext}`))
        continue;

      let str = O.rfs(pth, 1);
      let str0 = str;

      str = str.replace(/\bcsr'/g, 'csr');

      O.wfs(pth, str);
    }
  }

  log('Done');
};

const shift_str = (s, dif) => {
  return [...s].map(c => O.sfcc(O.cc(c) + dif)).join('');
};

main();