@echo off
cls

cd src
ghc -O -outputdir "../build" -o "../pure.exe" -prof -fprof-auto Main.hs
cd ..